<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Home extends User_Controller {
 
	public function __construct() {
		parent::__construct();	
        $this->load->helper(array('utilities','meta'));
        $this->load->model('standard_model');
    }

	public function index() {
        $data['myQuote_list']=$this->getMyQuoteList(); 
        $data['cover_list']=$this->getCoverList();
		$data['catalogs']=$this->getCatalogsList(18);
		$data['search_list']=$this->getPopularSearch();
		$data['featured_product_list']=$this->getFeatured_product(8);
		$data['offer_list']=$this->getOffer_list();
		$data['product_list']=$this->getProduct_list(8);
        $data['top_selling']=$this->getTopSellProduct(3);
        $data['most_view']=$this->getMostViewd(3);
        $data['main_content'] = "home_view";
        $name = namechange();
        $data['title'] = '';
        $data['description'] = '';
        $data['keywords'] = '';
		
        $this->load->view('master',$data);
	}
    private function getMostViewd($limit=''){
        $data['table'] = 'product_list';
        $data['field'] = 'id,title,slug,description,url,views';
        $data['order_by'] = array(
            'views' => 'desc'
        );
        $data['limit'] = '3';

        $this->standard_model->set_query_data($data);
        $results= $this->standard_model->select();
        if(sizeof($results)>0){
            if(is_object($results)) {
                $results = array($results);
            }
               return $results;
        }else{
             return array();
        }
    }
    private function getTopSellProduct($limit=''){
        $data['table'] = 'quote_products';
        $data['field'] = 'product_list.*,quote_products.product_id,COUNT(quote_products.product_id) as total';
        $data['join'] = array(
            "product_list" => 'quote_products.product_id = product_list.id',
        );
        $data['group_by'] = array('quote_products.product_id');
        $data['order_by'] = array(
            'total' => 'desc'
        );
        $data['limit'] = '3';

        $this->standard_model->set_query_data($data);
        $results= $this->standard_model->select();
        if(sizeof($results)>0){
            if(is_object($results)) {
                $results = array($results);
            }
               return $results;
        }else{
             return array();
        }
    }
    
    public function products(){
        $data['myQuote_list']=$this->getMyQuoteList(); 
        $data['product_list']=$this->getProduct_list();
        $data['main_content'] = "product_view";
        $name = namechange();
        $data['title'] = 'Products';
        $data['description'] = '';
        $data['keywords'] = '';
        
        $this->load->view('master',$data);
    }

    public function popular_searches(){
        $data['search_list']=$this->getPopularSearch();
        $data['main_content'] = "popular_searches_view";
        $name = namechange();
        $data['title'] = 'Popular Searches — '.$name;
        $data['description'] = '';
        $data['keywords'] = '';

        $this->load->view('master',$data);
    }

    public function search() {   
        // $data['myQuote_list']=$this->getMyQuoteList(); 
        
        if($_GET['q']!=''){
            $search=$_GET['q'];
            $data['search_data']=$this->getSearchData($search);
            $data['main_content'] = "search_view";
            $name = namechange();
            $data['title'] = 'Search — '.$search;
            $data['description'] = '';
            $data['keywords'] = '';

            $this->load->view('master',$data);
        } else {
            redirect(base_url());
        }
    }

    private function getSearchData($search=''){
        $data['table'] = 'product_list';
        $data['field'] = 'product_list.id,product_list.title,product_list.slug,product_list.description,product_list.url, catalogs.name,product_list.status';

        $data['like'] = array(                 
            "product_list.title" => $search,
        );
        $data['condition']=array(
            "product_list.status" => 'active'
        );
        // $data['or_like'] = array(                 
        //    "brands.name" => $search,
        //    "product_list.part_no" => $search
        // );

        $data['join'] = array(
            "catalogs" => 'catalogs.id = product_list.catalog_id',
        );


       $data['offset'] = '2';

        $data['order_by'] = array(
            'product_list.id' => 'desc'
        );

        $this->standard_model->set_query_data($data);
        $results= $this->standard_model->select();
        if(sizeof($results)>0){
            if(is_object($results)) {
                $results = array($results);
            }
               return $results;
        }else{
             return array();
        }
    }
    private function getMyQuoteList(){
        if(isset($this->user['id'])){
        $data['table']='quote_products';
        $data['field'] = 'quote_products.id,quotes.user_id,quote_products.quantity,product_list.title,product_list.url';
        $data['join']=array(
            "quotes"=>'quote_products.quote_id = quotes.id',
            "product_list"=>'quote_products.product_id = product_list.id'
        );
        if(isset($this->user['id'])){
            $user_id=$this->user['id'];
        }
        $data['condition'] = array(
            "quotes.user_id"=>$user_id,
            "quotes.order_status_id"=>1,
            "quote_products.is_active" => 1 
        );
        $data['order_by'] = array( 'product_list.id' => 'DESC' );
        $this->standard_model->set_query_data($data);
        $result = $this->standard_model->select();
        if(sizeof($result)>0){
            if(is_object($result)) {
            $result = array($result);
            }
            return $result;
        }else{
            return array();
        }
        }
        
        
    }

	private function getCoverList(){
		$data['table']='cover_image';
		$data['field'] = 'id, title	, url';
        $data['condition'] = array(
            "status" => 1 
        );
        $data['order_by'] = array( 'id' => 'DESC' );
        $this->standard_model->set_query_data($data);
        $result = $this->standard_model->select();

        if(is_object($result)) {
            $result = array($result);
        }

        return $result;
	}

	private function getProduct_list($limit='',$offset=''){
		$data['table'] = 'product_list';
        $data['field'] = 'id,title,slug,url';
        $data['condition'] = array(
            'status' => 'active',
            'is_featured' => 0
        );

        $data['order_by'] = array(
            'id' => 'DESC'
        );
        $data['limit'] = $limit;
        $data['offset'] =$offset;
        $this->standard_model->set_query_data($data);
        $results = $this->standard_model->select();
        if(is_object($results)) {
            $results = array($results);
        }
        if (sizeof($results) > 0 ) {
            return $results;            
        } else {
            return array();
        }
	}
	private function getPopularSearch(){
		$data['table']='popular_searches';
		$data['field'] = 'id, title,slug';
        $data['condition'] = array(
            "status" => 1 
        );
        $data['order_by'] = array( 'position' => 'ASC' );
        $this->standard_model->set_query_data($data);
        $result = $this->standard_model->select();

        if(is_object($result)) {
            $result = array($result);
        }

        return $result;
	}
	private function getFeatured_product($limit=''){
		$data['table'] = 'product_list';
        $data['field'] = 'id,title,slug,description,url';
        $data['condition'] = array(
            'status' => 'active',
            'is_featured' => '1'
        );
        $data['order_by'] = array(
            'in_order' => 'ASC'
        );
         $data['limit'] = $limit;
        $this->standard_model->set_query_data($data);
        $results = $this->standard_model->select();
        if(is_object($results)) {
            $results = array($results);
        }
        if (sizeof($results) > 0 ) {
            return $results;            
        } else {
            return array();
        }
	}

	private function getOffer_list(){
		$data['table']='offers';
		$data['field'] = 'id, title,slug,url,link';
        $data['condition'] = array(
            "status" => 1 
        );
        $data['order_by'] = array( 'id' => 'DESC' );
        $this->standard_model->set_query_data($data);
        $result = $this->standard_model->select();

        if(is_object($result)) {
            $result = array($result);
        }

        return $result;
	}

	public function product_overview($slug='') {
        // echo $slug;
        // return;
        if($slug!=''){
            $data['myQuote_list']=$this->getMyQuoteList(); 
            $data['brand_list']=$this->getCatalogsList();
            // $data['brand_details']=$this->get_catalog_id($slug);

            $data_query['field']='id, name';
            $data_query['table']='catalogs';
            $data_query['condition'] = array(
                "slug"=>$slug,
                "status"=>'1'
            );
            $data_query['limit'] = 1;

            $this->standard_model->set_query_data($data_query);
            $catalog = $this->standard_model->select();
            if(isset($catalog->id)) {
                $data['catalog'] = $catalog;
                $data['products'] = $this->get_product_list_by_brand($catalog->id);
                // if(sizeof($data['products'])>0) {
                    $data['main_content'] = "product_overview_view";
            		$name = namechange();		
                    $data['title'] = '';
                    $data['description'] = '';
                    $data['keywords'] = '';
                    $this->load->view('master',$data);
                // }

            } else {
                redirect(base_url());
            }
        } else {
            redirect(base_url());
        }
	}

    private function get_catalog_id($slug=''){
        if($slug!=''){
                $data['field']='id';
                $data['table']='brands';
                $data['condition']=array(
                    "slug"=>$slug,
                    "status"=>'1'
                );

                $this->standard_model->set_query_data($data);
                $results = $this->standard_model->select();

                if ($results!='') {
                    return $results;            
                } else {
                    return array();
                }   
        }
    }
    private function get_product_list_by_brand($id=''){
        if($id!=''){
             $data['table'] = 'product_list';
        $data['field'] = 'product_list.id,product_list.slug,product_list.description, url as image_url';

        // $data['join'] = array(
        //     "product_images" => 'product_images.product_id = product_list.id',
        // );

        $data['condition'] = array(                 
              "product_list.catalog_id" => $id,
              "product_list.status" => 'active',
              // "product_images.is_cover"=>1
        );

        $data['order_by'] = array(
            'product_list.id' => 'desc'
        );

        $this->standard_model->set_query_data($data);
        $results = $this->standard_model->select();

       if (sizeof($results) > 0) {
            if(is_object($results)) {
                $results = array($results);
            }
            return $results;            
        } else {
            return array();
        }

        }
    }

		
	public function product_details ($id='', $slug='') {
        $data['myQuote_list']=$this->getMyQuoteList(); 
        if($id!='' && $slug!=''){
            // $data['product_details']=$this->get_product_detail($id);
            
            // if($data['product_details']->id!=''){
              
                // $id=$data['product_details']->id;
                $data['product']=$this->getProductListById($id);

                $brand_id=$data['product']->catalog_id;
                $data['related_product']=$this->getRelatedProduct($id,$brand_id);

                $data['product_image']=$this->getProductImageById($id);
                $data['product_section']=$this->getProductSectionById($id);

            // echo "<pre>";
            // print_r($data);
            // echo "</pre>";
            // return;
            // }
    		$data['main_content'] = "product_details_view";
    				
            // if(isset($data['product']->title)){
            //     $data['title'] = $data['product']->title;


            //     $meta_description = no_html($data['product']->description);
            //     $meta_description = filter_str($meta_description);
            //     $meta_description = get_words($meta_description);

            //     if(strripos($meta_description, '.')) {
            //         $meta_description = substr($meta_description, 0, strripos($meta_description, '.')+1);
            //     } 
            //     if(strripos($meta_description, '!')) {
            //         $meta_description = substr($meta_description, 0, strripos($meta_description, '!')+1);
            //     }  
            //     if(strripos($meta_description, '?')) {
            //         $meta_description = substr($meta_description, 0, strripos($meta_description, '?')+1);
            //     }        
                
            //     $data['description'] = $meta_description;
            //     $data['keywords'] = 'Medical supply, surgical supply, wholesale medical supply, surgical products and equipment, global medical supply, laboratory supply';
            // }
            $data['title'] = '';
            $data['keywords'] = '';
            $data['description'] = "";

            $this->load->view('master',$data);
        } else {
            redirect('/');
        }
 


	}
    private function get_product_detail($id=0) {
        if($id==0) {
            return;
        }
       
        $data['table'] = 'product_list';
        $data['field'] = 'id,brand_id';


        $data['condition'] = array(   
            "id"=>$id,
            "status" => 'active'
        );

        $this->standard_model->set_query_data($data);
        $results = $this->standard_model->select();

        if ($results!='') {
           return $results;            
        } else {
            return array();
        }
    }

     private function getProductListById($id='') {
       
        $data['table'] = 'product_list';
        $data['field'] = 'id,title,part_no,sku_number,sell_unit,ships_within,description,catalog_id,url as image';


        $data['condition'] = array(   
              "id"=>$id,
              "status" => 'active'
        );

        $data['order_by'] = array(
            'id' => 'asc'
        );

        
        $this->standard_model->set_query_data($data);
        $results = $this->standard_model->select();

        
        if ($results!='') {
          return $results;            
        } else {
            return array();
        }
    }

    private function getRelatedProduct($id='',$brand_id=''){
       
        $data['table'] = 'product_list';
        $data['field'] = 'id,title,slug,description,url';


        $data['condition'] = array(   
              "id !="=>$id,
              "catalog_id"=>$brand_id,
              "status =" => 'active'
        );

        $data['order_by'] = array(
            'id' => 'asc'
        );

        $data['limit'] = 10;
        
        $this->standard_model->set_query_data($data);
        $results = $this->standard_model->select();
        
        if(is_object($results)) {
            $results = array($results);
        }

        return $results;
    }

 private function getProductImageById($id='') {
        $data['table'] = 'product_images';
        $data['field'] = 'product_id,image,container_name,height,width';


        $data['condition'] = array(   
              "product_id"=>$id,
              "status =" => '1'
        );

        $data['order_by'] = array(
            'id' => 'asc'
        );

        $this->standard_model->set_query_data($data);
        $results = $this->standard_model->select();

        if(is_object($results)) {
            $results = array($results);
        }
        
        return $results;
    }

/* start second*/
 private function getProductSectionById($id='') {
       
        $data['table'] = 'product_section';
        $data['field'] = 'product_id,name,description';
        $data['condition'] = array(   
              "product_id"=>$id,
              "status =" => 'active'
    
        );
        $data['order_by'] = array(
            'id' => 'asc'
        );
        $this->standard_model->set_query_data($data);
        $results = $this->standard_model->select();        
        if ($results!='') {
          return $results;            
        } else {
            return array();
        }
    }


	public function quotation () {
     $data['myQuote_list']=$this->getMyQuoteList(); 

        if(isset($this->user['id'])){
            $data['myQuote_list']=$this->getMyQuoteList();
            $data['my_customProduct']=$this->getMyCustomProductList();
            $data['my_attachment_list']=$this->getMyCustomProductAttachmentList(); 

           // print_r($data['my_attachment_list']);exit("check");
        }else{
            $index = 0;
            $cookieList= $this->input->cookie('cpID');

            $productArry=explode(',',$cookieList);
            foreach ($productArry as $value) {
                $cart_data[$index++]=explode(':',$value);

            }
            $data['cart_data']=$cart_data;
          
            $index = 0;
            foreach ($cart_data as $cart) {
               $product_id[$index++] = $cart[0];
            } 
            
            $data['productArray']=[];
            foreach($product_id as $pro){
               $data['cookieData']=$this->getProductDetailByCookieId($pro);
               array_push($data['productArray'],$data['cookieData']); 
            }
            // for attachment
            $mediaList= $this->input->cookie('media');
            $index = 0;
            $mediaArry=explode(',',$mediaList);

            foreach ($mediaArry as $value) {
                $media_data[$index++]=explode(':',$value);
            }
            $index = 0;
           $data['mediaArray']=[];
            foreach ($media_data as $media) {
                $attach[$index++] = $media[0];
                $data['attachmentData']=$this->getAttachmentDetailBymediaId($media[0]);
                array_push($data['mediaArray'],$data['attachmentData']); 
            }

        }
        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";
        // return;
       
		$data['main_content'] = "quotation_view";
        $name = namechange();
		$data['title'] = 'Order';
        $data['description'] = '';
        $data['keywords'] = '';
        
    	$this->load->view('master',$data);
	}
    private function getMyCustomProductList(){
        $data['table']='custom_quote';
        $data['field'] = 'custom_quote.id,custom_quote.name,custom_quote.quantity';

        $data['join']=array(
            "quotes"=>'custom_quote.quote_id = quotes.id'
        );
        if(isset($this->user['id'])){
            $user_id=$this->user['id'];
        }
        $data['condition'] = array(
            "custom_quote.status"=>'1',
            "quotes.user_id"=>$user_id,
            "quotes.order_status_id" => '1' ,
            "quotes.is_active" => '1',
            "quotes.in_process" => '1' 
        );
        $data['order_by'] = array( 'custom_quote.id' => 'DESC' );
        $this->standard_model->set_query_data($data);
        $result = $this->standard_model->select();
        if(sizeof($result)>0){
            if(is_object($result)) {
            $result = array($result);
            }
            return $result;
        }else{
            return array();
        }
    }
     private function getMyCustomProductAttachmentList(){
        $data['table']='quote_attachment';
        $data['field'] = 'quote_attachment.id,quote_attachment.path';

        $data['join']=array(
            "quotes"=>'quote_attachment.quote_id = quotes.id'
        );
        if(isset($this->user['id'])){
            $user_id=$this->user['id'];
        }
        $data['condition'] = array(
            "quote_attachment.status"=>'1',
            "quotes.user_id"=>$user_id,
            "quotes.order_status_id" => '1' ,
            "quotes.is_active" => '1',
            "quotes.in_process" => '1' 
        );
        $data['order_by'] = array( 'quote_attachment.id' => 'DESC' );
        $this->standard_model->set_query_data($data);
        $result = $this->standard_model->select();
        if(sizeof($result)>0){
            if(is_object($result)) {
            $result = array($result);
            }
            return $result;
        }else{
            return array();
        }
    }
    private function getProductDetailByCookieId($product_id) {
        $data['table'] = 'product_list';
        $data['field'] = 'id,title,slug,description,url';
        $data['condition'] = array(
            'id'=> $product_id,
            'status' => 'active'
        );
        $data['order_by'] = array(
            'id' => 'DESC'
        );
      
        $this->standard_model->set_query_data($data);
        $results = $this->standard_model->select();
        if(is_object($results)) {
            $results = array($results);
        }
        if (sizeof($results)>0) {
            return $results;            
        } else {
            return array();
        }
    }
    private function getAttachmentDetailBymediaId($id){
        $data['table'] = 'quote_attachment';
        $data['field'] = 'id,path';
        $data['condition'] = array(
            'id'=> $id
        );
        $data['order_by'] = array(
            'id' => 'DESC'
        );
      
        $this->standard_model->set_query_data($data);
        $results = $this->standard_model->select();
        if(is_object($results)) {
            $results = array($results);
        }
        if (sizeof($results)>0) {
            return $results;            
        } else {
            return array();
        }
    }

	public function contact_us() {
        $data['myQuote_list']=$this->getMyQuoteList(); 
		$data['main_content'] = "contact_us_view";
				
        $data['title'] = 'Contact Us';
        $name = namechange();
        $data['description'] = '';
        $data['keywords'] = '';
        
		
        $this->load->view('master',$data);
	}

		public function dashboard() {
      //  $data['user_info']=$this->user;
		$data['main_content'] = "dashboard_view";
		$name = namechange();		
        $data['title'] = 'Dashboard';
        $data['description'] = '';
        $data['keywords'] = '';
		
        $this->load->view('master',$data);
	}

	public function addresses() {
        $data['myQuote_list']=$this->getMyQuoteList(); 
		$data['main_content'] = "addresses_view";
		$name = namechange();		
        $data['title'] = 'Addresses';
        $data['description'] = '';
        $data['keywords'] = '';
		
        $this->load->view('master',$data);
	}

	public function add_addresses() {
        $data['myQuote_list']=$this->getMyQuoteList(); 
		$data['main_content'] = "add_addresses_view";
		$name = namechange();		
        $data['title'] = 'Add Addresses';
        $data['description'] = '';
        $data['keywords'] = '';
		
        $this->load->view('master',$data);
	}		

	public function checkout() {
        $data['myQuote_list']=$this->getMyQuoteList(); 
		$data['main_content'] = "checkout_view";
		$name = namechange();		
        $data['title'] = 'Checkout — '.$name;
        $data['description'] = $name.' is a global distributor of large volume wholesale medical and surgical products and equipment. Browse our catalog of thousands of SKUs today.';
        $data['keywords'] = 'Medical supply, surgical supply, wholesale medical supply, surgical products and equipment, global medical supply, laboratory supply';
        $this->load->view('master',$data);
	}

		public function about() {
        $data['myQuote_list']=$this->getMyQuoteList(); 
		$data['main_content'] = "about_view";
		$name = namechange();		
        $data['title'] = '';
        $data['description'] = '';
        $data['keywords'] = '';
		
        $this->load->view('master',$data);
	}

		public function faq() {
        $data['myQuote_list']=$this->getMyQuoteList(); 
		$data['main_content'] = "faq_view";
		$name = namechange();		
        $data['title'] = 'FAQ';
        $data['description'] = '';
        $data['keywords'] = '';
		
        $this->load->view('master',$data);
	}

		public function disclaimer() {
        $data['myQuote_list']=$this->getMyQuoteList(); 
		$data['main_content'] = "disclaimer_view";
		$name = namechange();		
        $data['title'] = 'Disclaimer';
        $data['description'] = '';
        $data['keywords'] = '';
		
        $this->load->view('master',$data);
	}

	public function page_not_found() {
        $data['myQuote_list']=$this->getMyQuoteList(); 
		$data['main_content'] = "404_view";
		$name = namechange();		
        $data['title'] = 'Page not Found';
        $data['description'] = '';
        $data['keywords'] = '';
		
        $this->load->view('master',$data);
	}

    public function catalog(){
        $data['myQuote_list']=$this->getMyQuoteList(); 
        $data['catalogs']=$this->getCatalogsList();
        $data['main_content'] = "catalog_view";
        $name = namechange();
        $data['title'] = '';
        $data['description'] = '';
        $data['keywords'] = '';

        $this->load->view('master',$data);
    }

    public function brand() {
        $data['myQuote_list']=$this->getMyQuoteList(); 
        $data['brands']=$this->getBrandList();
        $data['main_content'] = "brand_view";
        $name = namechange();        
        $data['title'] = 'Brands';
        $data['description'] = '';
        $data['keywords'] = '';
        
        $this->load->view('master',$data);
    }

    private function getBrandList($limit=0){
        $data['table'] = 'brands';
        $data['field'] = 'id, name, image, link_url';
        $data['condition'] = array(
            'status' => '1'
        );
        // $data['order_by'] = array(
        //     'order_no' => 'asc'
        // );
        if($limit>0) {
            $data['limit'] = $limit;
        }
        $this->standard_model->set_query_data($data);
        $results = $this->standard_model->select();
        if(is_object($results)) {
            $results = array($results);
        }
        
        return $results;
    }

    private function getCatalogsList($limit=0){
        $data['table'] = 'catalogs';
        $data['field'] = 'id, name, image, slug';

        $data['condition'] = array(
            'status' => '1'
        );
        
        if($limit>0) {
            $data['limit'] = $limit;
        }

        $this->standard_model->set_query_data($data);
        $results = $this->standard_model->select();
        if(is_object($results)) {
            $results = array($results);
        }
        
        return $results;        
    }
}