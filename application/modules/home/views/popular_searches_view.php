
  <!-- breadcrumb start -->
    <div class="breadcrumb-section">
        <div class="container">
            <div class="row text-center-md">
                <div class="col-md-6 col-sm-12">
                    <div class="page-title">
                        <h2>Popular Searches</h2>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <nav aria-label="breadcrumb" class="theme-breadcrumb">
                       <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url();?>">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Popular Searches</li>
                           
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb End -->
    <section class="bg-white popular-section" style="background-image: url(<?php echo base_url();?>assets/images/blueBg.jpg); background-repeat: no-repeat; background-size: cover;">
        <div class="container popular_searches">
                <div class="row">
                <?php if(isset($search_list)){
                    foreach($search_list as $search){
                 ?>
               
                    <div class="col-md-2 search-box"> 
                          <a href="<?php echo base_url();?>search?q=<?php echo $search->title;?>">
                            <h5><?php echo $search->title;?></h5>
                        </a>
                    </div>
               
                <?php } }?>
                  
                </div>
                </div>
            </div>
    </section>
   