  <style>
.current {
  color: green;
}

#pagin li {
  display: inline-block;
}
/*.product-info {
    padding: 10px 15px;
    min-height: 160px!important;
}*/

</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
  <!-- breadcrumb start -->
    <div class="breadcrumb-section">
        <div class="container">
            <div class="row text-center-md">
                <div class="col-md-6 col-sm-12">
                    <div class="page-title">
                        <h2><?php echo $catalog->name;?></h2>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <nav aria-label="breadcrumb" class="theme-breadcrumb">
                       <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url();?>">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Catalog</li>
                            <li class="breadcrumb-item active" aria-current="page"><?php echo $this->uri->segment('2');?> </li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb End -->
    
    <!-- category page -->
    <section class="section-b-space marginTotop">
        <div class="collection-wrapper">
            <div class="container">
                <div class="row">
                    
                    <div class="col-sm-3 collection-filter">
                        <!-- side-bar colleps block stat -->
                        <div class="collection-filter-block">
                            <!-- brand filter start -->
                            <div class="collection-mobile-back">
                                <span class="filter-back">
                                    <i class="fa fa-angle-left" aria-hidden="true"></i> back
                                </span>
                            </div>
                            <div class="collection-collapse-block open">
                                <h3 class="collapse-block-title">brand</h3>
                                <div class="collection-collapse-block-content">
                                    <div class="collection-brand-filter">
                                        <?php 
                                            if($brand_list!=''){
                                                foreach($brand_list as $brand){
                                        ?>
                                        <div class="custom-control custom-checkbox collection-filter-checkbox">
                                            <label>
                                                <input type="checkbox" name="brand_name" class="brand" id="brand_name" <?php echo (isset($catalog->id) && $catalog->id == $brand->id) ? 'checked' :'' ?> value="<?php echo $brand->id;?>"><?php echo $brand->name;?>
                                            </label>
                                        </div>
                                        <?php } } ?>
                                    </div>
                                </div>
                            </div>                           
                        </div>
                       
              
                        <!-- side-bar banner start here -->
                        <div class="collection-sidebar-banner">
                            <a href="#">
                                <img src="assets/images/sidebar_small_banner.jpg" class="img-fluid blur-up lazyload" alt="">
                            </a>
                        </div>
                        <!-- side-bar banner end here -->
                    </div>
                    <div class="collection-content col">
                        <div class="page-main-content">
                            <div class="container-fluid p-0">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="collection-product-wrapper">
                                            <div class="product-wrapper-grid">
                                                <div class="container-fluid">
                                                    <div class="row sapce-category">
                                                    <?php foreach($products as $product){ ?>
                                                        <div class="col-xl-4 col-md-6 col-grid-box">
                                                            <a href="<?php echo base_url();?>product-details/<?php echo $product->id;?>/<?php echo $product->slug;?>">
                                                                <div class="product-box hover">
                                                                <div class="product br-0">
                                                                    <img src="<?php echo base_url() . $product->image_url ;?>" alt="product" class="img-fluid blur-up lazyloaded" style="width: 100%; height: 195px; object-fit: cover;">
                                                                </div>
                                                                <div class="product-info">
                                                                    <p><?php echo $product->description;?></p>
                                                                </div>
                                                                <input type="hidden" name="product_id" value="<?php echo $product->id;?>">

                                                                <?php if(isset($this->user['id'])){
                                                                   $user_id=($this->user['id']);
                                                                 ?>
                                                                <button class="atq btn" onclick="addToQuote(<?php echo $product->id;?>,<?php echo $this->user['id']?>);">Add to cart</button>
                                                                <?php  }else{ $user_id='0';?>
                                                                <button class="atq btn" onclick="SetProductIntoCookie(<?php echo $product->id;?>,1);">Add to cart</button>
                                                                <?php } ?>
                                                                 <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id;?>">

                                                                </div>
                                                            </a>     
                                                        </div>
                                                 
                                                    <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="product-pagination">
                                                <div class="theme-paggination-block">
                                                    <div class="container-fluid p-0">
                                                        <div class="row">
                                                            <div class="col-xl-6 col-md-6 col-sm-12">
                                                                <nav aria-label="Page navigation">
                                                                     <ul class="pagination" id="pagin">
                                                                        <li class="page-item">
                                                                            <a class="page-link" href="#" aria-label="Previous">
                                                                                <span aria-hidden="true"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>
                                                                                <span class="sr-only">Previous</span>
                                                                            </a>
                                                                        </li>
                                                                        <?php 
                                                                        $total=count($products)/8;
                                                                        for($i=1;$i<=$total;$i++){?>
                                                                        <li class="page-item  <?php echo $i=='1'? 'active current':''?>"><a class="page-link" href="#"><?php echo $i;?></a></li>
                                                                       <?php } ?>

                                                                        <li class="page-item">
                                                                            <a class="page-link" href="#" aria-label="Next">
                                                                                <span aria-hidden="true"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
                                                                                <span class="sr-only">Next</span>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </nav>
                                                            </div>
                                                        
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
     <script type="text/javascript">
        pageSize = 8;
        showPage = function(page) {
            $(".line-content").hide();
            $(".line-content").each(function(n) {
                if (n >= pageSize * (page - 1) && n < pageSize * page)
                $(this).show();
            });        
        }
        showPage(1);
        $("#pagin li a").click(function() {
            $("#pagin li a").removeClass("current");
            $(this).addClass("current");
            showPage(parseInt($(this).text())) 
        });
    </script>
    <!-- category page end -->