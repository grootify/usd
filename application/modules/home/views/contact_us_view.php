<div class="breadcrumb-section">
        <div class="container">
            <div class="row text-center-md">
                <div class="col-md-6 col-sm-12">
                    <div class="page-title">
                        <h2>Contact</h2>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <nav aria-label="breadcrumb" class="theme-breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url();?>">Home</a></li>
                            <li class="breadcrumb-item active">Contact</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>

    <section class="contact-page">
        <div class="container">
            <div class="row section-b-space">
                <div class="col-lg-5 ml-auto contactForm">
                    <form class="loginForm" method="post" action="javascript:void(0);" id="contactForm">
                                        <label>
                                            <p class="label-txt">Name *</p>
                                            <input type="text" class="input" id="contact_name" name="contact_name">
                                            <div class="line-box">
                                            <div class="line"></div>
                                            </div>
                                        </label>

                                         <label>
                                            <p class="label-txt">Email *</p>
                                            <input type="email" class="input" id="contact_email" name="contact_email">
                                            <div class="line-box">
                                            <div class="line"></div>
                                            </div>
                                        </label>

                                         <label>
                                            <p class="label-txt">Mobile *</p>
                                            <input type="text" class="input" id="contact_mobile" name="contact_mobile">
                                            <div class="line-box">
                                            <div class="line"></div>
                                            </div>
                                        </label>

                                         <label>
                                            <p class="label-txt">Message *</p>
                                            <textarea type="text" class="input" id="contact_message" name="contact_message"></textarea> 
                                            <div class="line-box">
                                            <div class="line"></div>
                                            </div>
                                        </label>
                             <div id="ajaxResponseDivs" style="bottom: 57px;color: #ff3a3a;"></div>
                             <div class="text-center">
                                <button class="btn btn-theme theme-btn-sm" type="submit" onclick="contactForm();" id="contact-btn">SUBMIT</button>
                            </div>
                        </form>
                </div>
                <div class="col-lg-6 mr-auto">
                    <div class="contact-right">
                        <ul>
                           <!--  <li>
                                <div class="contact-icon">
                                    <img src="<?php echo base_url();?>assets/images/call.png" alt="call" class="blur-up lazyloaded">
                                    <h6>Contact Us</h6>
                                </div>
                                <div class="media-body">
                                    <p>123-456-7898</p>
                                    <p>123-456-7898</p>
                                </div>
                            </li> -->
                       
                            <?php  if (strpos($_SERVER['HTTP_HOST'], 'akglobalmed') !== false) { ?>
                            <li>
                                <div class="contact-icon">
                                    <img src="<?php echo base_url();?>assets/images/mail.png" alt="mail">
                                    <h6>Mail</h6>
                                </div>
                                <div class="media-body">
                                   <a href="mailto:info@akglobalmed.com"><p>info@akglobalmed.com</p></a>
                                </div>
                            </li>
                            <li>
                                <div class="contact-icon">
                                  <i class="fa fa-map-marker" style="color:#040404;"></i>
                                    <h6>Address</h6>
                                </div>
                                <div class="media-body">
                                    <p>500 Westover drive # 3310 Sanford</p>
                                    <p>NC 27330</p>
                                </div>
                            </li>
                            <?php } else { ?>
                                 <li>
                                <div class="contact-icon">
                                    <img src="<?php echo base_url();?>assets/images/mail.png" alt="mail">
                                    <h6>Mail</h6>
                                </div>
                                <div class="media-body">
                                   <a href="mailto:info@akglobalgroup.com"><p>info@akglobalgroup.com</p></a>
                                </div>
                            </li>
                                <li>
                                    <div class="contact-icon">
                                      <i class="fa fa-map-marker" style="color:#040404;"></i>
                                        <h6>Address</h6>
                                    </div>
                                    <div class="media-body">
                                      <p>
                                            AK GLOBAL GROUP<br>
                                            Van Bylandtachterstraat 24 Unit 4<br>
                                            5046MB-TILBURG<br>
                                            Netherlands
                                      </p>
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
          
        </div>
    </section>