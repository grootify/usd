<div class="breadcrumb-section">
        <div class="container">
            <div class="row text-center-md">
                <div class="col-md-6 col-sm-12">
                    <div class="page-title">
                        <h2>Disclaimer</h2>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <nav aria-label="breadcrumb" class="theme-breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url();?>">Home</a></li>
                            <li class="breadcrumb-item active">Disclaimer</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <?php $name = namechange(); ?>
        <section class="faq-section disclaimer">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <span>Introduction</span>
                   <p>
                        Welcome to <?php if (strpos($_SERVER['HTTP_HOST'], 'akglobalmed') !== false) { ?>
                        <a href="http://www.akglobalmed.com">http://www.akglobalmed.com</a>
                        <?php } else { ?>
                            <a href="http://www.akglobalgroup.com">http://www.akglobalgroup.com</a>
                        <?php }  ?> This website is owned and operated by <?php echo $name ?>. By visiting our website and accessing the information, resources, services, products, and tools we provide, you understand and agree to accept and adhere to the following terms and conditions as stated in this policy (hereafter referred to as 'User Agreement'), along with the terms and conditions as stated in our Privacy Policy (please refer to the Privacy Policy section below for more information).
                   </p>
                   <strong>This agreement is in effect as of November 30, 2013.</strong>
                   <p>We reserve the right to change this User Agreement from time to time without notice. You acknowledge and agree that it is your responsibility to review this User Agreement periodically to familiarize yourself with any modifications. Your continued use of this site after such modifications will constitute acknowledgment and agreement of the modified terms and conditions.</p>
                   <p>Responsible Use and Conduct</p>
                   <p>By visiting our website and accessing the information, resources, services, products, and tools we provide for you, either directly or indirectly (hereafter referred to as 'Resources'), you agree to use these Resources only for the purposes intended as permitted by (a) the terms of this User Agreement, and (b) applicable laws, regulations and generally accepted online practices or guidelines.
                   </p>
                   <span>Wherein, you understand that:</span>
                   <ul>
                       <li>
                           a. In order to access our Resources, you may be required to provide certain information about yourself (such as identification, contact details, etc.) as part of the registration process, or as part of your ability to use the Resources. You agree that any information you provide will always be accurate, correct, and up to date.
                       </li>
                       <li>
                           b. You are responsible for maintaining the confidentiality of any login information associated with any account you use to access our Resources. Accordingly, you are responsible for all activities that occur under your account/s.
                       </li>
                       <li>
                           c. Accessing (or attempting to access) any of our Resources by any means other than through the means we provide, is strictly prohibited. You specifically agree not to access (or attempt to access) any of our Resources through any automated, unethical or unconventional means.
                       </li>
                       <li>
                           d. Engaging in any activity that disrupts or interferes with our Resources, including the servers and/or networks to which our Resources are located or connected, is strictly prohibited.
                       </li>
                       <li>
                           e. Attempting to copy, duplicate, reproduce, sell, trade, or resell our Resources is strictly prohibited.
                       </li>
                       <li>
                           f. You are solely responsible any consequences, losses, or damages that we may directly or indirectly incur or suffer due to any unauthorized activities conducted by you, as explained above, and may incur criminal or civil liability.
                       </li>
                       <li>
                           g. We may provide various open communication tools on our website, such as blog comments, blog posts, public chat, forums, message boards, newsgroups, product ratings and reviews, various social media services, etc. You understand that generally we do not pre-screen or monitor the content posted by users of these various communication tools, which means that if you choose to use these tools to submit any type of content to our website, then it is your personal responsibility to use these tools in a responsible and ethical manner. By posting information or otherwise using any open communication tools as mentioned, you agree that you will not upload, post, share, or otherwise distribute any content that:
                           <p>
                            i. Is illegal, threatening, defamatory, abusive, harassing, degrading, intimidating, fraudulent, deceptive, invasive, racist, or contains any type of suggestive, inappropriate, or explicit language;
                            ii. Infringes on any trademark, patent, trade secret, copyright, or other proprietary right of any party;
                            Iii. Contains any type of unauthorized or unsolicited advertising;
                            Iiii. Impersonates any person or entity, including any <?php if (strpos($_SERVER['HTTP_HOST'], 'akglobalmed') !== false) { ?>
                        <a href="http://www.akglobalmed.com">http://www.akglobalmed.com</a>
                        <?php } else { ?>
                            <a href="http://www.akglobalgroup.com">http://www.akglobalgroup.com</a>
                        <?php }  ?> employees or representatives.

                            We have the right at our sole discretion to remove any content that, we feel in our judgment does not comply with this User Agreement, along with any content that we feel is otherwise offensive, harmful, objectionable, inaccurate, or violates any 3rd party copyrights or trademarks. We are not responsible for any delay or failure in removing such content. If you post content that we choose to remove, you hereby consent to such removal, and consent to waive any claim against us.
                           </p>
                       </li>
                       <li>
                           i. You agree to indemnify and hold harmless <?php if (strpos($_SERVER['HTTP_HOST'], 'akglobalmed') !== false) { ?>
                        <a href="http://www.akglobalmed.com">http://www.akglobalmed.com</a>
                        <?php } else { ?>
                            <a href="http://www.akglobalgroup.com">http://www.akglobalgroup.com</a>
                        <?php }  ?> and its parent company and affiliates, and their directors, officers, managers, employees, donors, agents, and licensors, from and against all losses, expenses, damages and costs, including reasonable attorneys' fees, resulting from any violation of this User Agreement or the failure to fulfill any obligations relating to your account incurred by you or any other person using your account. We reserve the right to take over the exclusive defense of any claim for which we are entitled to indemnification under this User Agreement. In such event, you shall provide us with such cooperation as is reasonably requested by us.
                       </li>
                   </ul>
                   <span>Privacy</span>
                   <p>Your privacy is very important to us, which is why we've created a separate Privacy Policy in order to explain in detail how we collect, manage, process, secure, and store your private information. Our privacy policy is included under the scope of this User Agreement. To read our privacy policy in its entirety, click here.</p>
                   <span>Limitation of Liability</span>
                   <p>In conjunction with the Limitation of Warranties as explained above, you expressly understand and agree that any claim against us shall be limited to the amount you paid, if any, for use of products and/or services. <?php echo $name ?> will not be liable for any direct, indirect, incidental, consequential or exemplary loss or damages which may be incurred by you as a result of using our Resources, or as a result of any changes, data loss or corruption, cancellation, loss of access, or downtime to the full extent that applicable limitation of liability laws apply.</p>
                   <span>Copyrights/Trademarks</span>
                   <p>All content and materials available on <?php if (strpos($_SERVER['HTTP_HOST'], 'akglobalmed') !== false) { ?>
                        <a href="http://www.akglobalmed.com">http://www.akglobalmed.com</a>
                        <?php } else { ?>
                            <a href="http://www.akglobalgroup.com">http://www.akglobalgroup.com</a>
                        <?php }  ?> including but not limited to text, graphics, website name, code, images and logos are the intellectual property of <?php if (strpos($_SERVER['HTTP_HOST'], 'akglobalmed') !== false) { ?>
                        <a href="http://www.akglobalmed.com">http://www.akglobalmed.com</a>
                        <?php } else { ?>
                            <a href="http://www.akglobalgroup.com">http://www.akglobalgroup.com</a>
                        <?php }  ?>, and are protected by applicable copyright and trademark law. Any inappropriate use, including but not limited to the reproduction, distribution, display or transmission of any content on this site is strictly prohibited, unless specifically authorized by <?php echo $name ?>.</p>
                   <span>Termination of Use</span>
                   <p>
                       You agree that we may, at our sole discretion, suspend or terminate your access to all or part of our website and Resources with or without notice and for any reason, including, without limitation, breach of this User Agreement. Any suspected illegal, fraudulent or abusive activity may be grounds for terminating your relationship and may be referred to appropriate law enforcement authorities. Upon suspension or termination, your right to use the Resources we provide will immediately cease, and we reserve the right to remove or delete any information that you may have on file with us, including any account or login information.
                   </p>
                   <span>Governing Law</span>
                   <p>This website is controlled by <?php if (strpos($_SERVER['HTTP_HOST'], 'akglobalmed') !== false) { ?>
                        <a href="http://www.akglobalmed.com">http://www.akglobalmed.com</a>
                        <?php } else { ?>
                            <a href="http://www.akglobalgroup.com">http://www.akglobalgroup.com</a>
                        <?php }  ?>  from our offices located 500 Westover drive # 3310 Sanford NC 27330, United States. It can be accessed by most countries around the world. As each country has laws that may differ from those of Illinois, by accessing our website, you agree that the statutes and laws of North Carolina, without regard to the conflict of laws and the United Nations Convention on the International Sales of Goods, will apply to all matters relating to the use of this website and the purchase of any products or services through this site.</p>
                   <p>Furthermore, any action to enforce this User Agreement shall be brought in the federal or state courts located in United States, North Carolina You hereby agree to personal jurisdiction by such courts, and waive any jurisdictional, venue, or inconvenient forum objections to such courts.</p>
                   <span>Payment Terms</span>
                   <p>As a customer of <?php echo $name ?>, you agree to pay for items purchased in accordance with the agreed-upon terms. By default, all new accounts shall be required to submit a valid credit card for payment. At the sole discretion of <?php echo $name ?>, a customer may be granted terms for payment in the form of bank wire transfer, or company check based on creditworthiness. In the event that your company or organization is granted terms for payment, you agree to pay the full amount on or before the invoice due date. If payment is not received by the due date, you agree that <?php echo $name ?> may charge your credit card in the amount due, and if payment is still not fully received, may also procure the services of a third-party firm to collect. Please direct any questions you may have regarding account setup, credit application, or other billing-related matters to <?php if (strpos($_SERVER['HTTP_HOST'], 'akglobalmed') !== false) { ?>
                        <a href="http://www.akglobalmed.com">http://www.akglobalmed.com</a>
                        <?php } else { ?>
                            <a href="http://www.akglobalgroup.com">http://www.akglobalgroup.com</a>
                        <?php }  ?>
                </div>
            </div>
        </div>
        </section>            