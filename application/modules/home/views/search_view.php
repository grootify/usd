<style>
.current {
  color: green;
}
#pagin li {
  display: inline-block;
}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <!-- category page -->
    <section class="section-b-space">
        <div class="collection-wrapper">
            <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="text-center search-result">
                            <h3>Search Result : "<?php echo $_GET['q']?>"</h3>
                            <?php
                                if($search_data!=''){
                                    $totalresult=COUNT($search_data);
                                }else{
                                    $totalresult='0';
                                }
                            ?>
                            <h6 class="mb-0 l-h-1">Showing all '<?php echo $totalresult;?>' result</h6>
                            </div>
                        </div>
                    </div>
                    <hr>
                <div class="row">
                    <div class="collection-content col">
                        <div class="page-main-content">
                            <div class="container-fluid p-0">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="collection-product-wrapper">
                                            <div class="product-wrapper-grid">
                                                <div class="container-fluid">
                                                    <div class="row sapce-category">
                                                   
                                                    <?php if($search_data!='') { foreach($search_data as $data) { ?>
                                                    <div class="col-xl-3 col-md-6 col-grid-box line-content">
                                                        <div class="product-box hover">
                                                            <div class="product br-0">
                                                                <a href="<?php echo base_url();?>product-details/<?php echo $data->id;?>/<?php echo $data->slug;?>">
                                                                    <img src="<?php echo base_url();?><?php echo $data->url;?>" class="img-fluid blur-up lazyloaded" style="width:242px;height:200px;">               
                                                                </a>
                                                            </div>
                                                            <div class="product-info">
                                                                <a href="<?php echo base_url();?>product-details/<?php echo $data->id;?>/<?php echo $data->slug;?>">
                                                                <p><?php echo $data->title;?></p>
                                                            </a>
                                                            </div>
                                                            <input type="hidden" name="product_id" value="<?php echo $data->id;?>">
                                                            <?php if(isset($this->user['id'])){ ?>
                                                            <button class="atq btn" onclick="addToQuote(<?php echo $data->id;?>,<?php echo $this->user['id']?>);">Add to quote</button>
                                                            <?php  }else{?>
                                                            <button class="atq btn" onclick="SetProductIntoCookie(<?php echo $data->id;?>,1);">Add to quote</button>
                                                            <?php } ?>                                                                
                                                        </div>
                                                    </div>
                                                    <?php } }?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="product-pagination">
                                                <div class="theme-paggination-block">
                                                    <div class="container-fluid p-0">
                                                        <div class="row">
                                                            <div class="col-xl-6 col-md-6 col-sm-12">
                                                                <nav aria-label="Page navigation">
                                                                    <ul class="pagination" id="pagin">
                                                                        <li class="page-item">
                                                                            <a class="page-link" href="#" aria-label="Previous">
                                                                                <span aria-hidden="true"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>
                                                                                <span class="sr-only">Previous</span>
                                                                            </a>
                                                                        </li>
                                                                        <?php 
                                                                        $total=count($search_data)/8;
                                                                        for($i=1;$i<=$total;$i++){?>
                                                                        <li class="page-item  <?php echo $i=='1'? 'active current':''?>"><a class="page-link" href="#"><?php echo $i;?></a></li>
                                                                       <?php } ?>

                                                                        <li class="page-item">
                                                                            <a class="page-link" href="#" aria-label="Next">
                                                                                <span aria-hidden="true"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
                                                                                <span class="sr-only">Next</span>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </nav>
                                                            </div>
                                                        
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- category page end -->
    <!--  <script type="text/javascript">
        pageSize = 2;
        showPage = function(page) {
            $(".line-content").hide();
            $(".line-content").each(function(n) {
                if (n >= pageSize * (page - 1) && n < pageSize * page)
                $(this).show();
            });        
        }
        showPage(1);
        $("#pagin li a").click(function() {
            $("#pagin li a").removeClass("current");
            $(this).addClass("current");
            showPage(parseInt($(this).text())) 
        });
    </script> -->