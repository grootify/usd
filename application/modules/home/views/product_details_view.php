<style>
.product-info{
    /*min-height: 100px!important;
    -webkit-line-clamp: 3;*/
}
.product-lbl{
    font-weight: bold;
    float: left;
    margin-bottom: 6px;
    clear: both;
}
.product-desc{
    float: left;
    margin-bottom: 5px;
}
</style>
    <section class="marginTotop productDetails">
        <div class="collection-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-lg-1 col-sm-2 col-xs-12">
                        <div class="row">
                            <div class="col-12 p-0">
                                <div class="slider-right-nav">

                                    <div> <img src="<?php echo base_url() . $product->image;?>" alt="" class="img-fluid blur-up lazyload"></div> 
                                    <!-- start side image -->
                                    <?php foreach($product_image as $pro_img){ ?>
                                    <div> <img src="<?php echo base_url() . $pro_img->container_name . $pro_img->image;?>" alt="" class="img-fluid blur-up lazyload"></div> 
                                    <?php } ?>
                                   <!-- end -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5 col-sm-10 col-xs-12  order-up">
                        <div class="product-right-slick">
                            <div> <img src="<?php echo base_url() . $product->image;?>" alt="" class="img-fluid blur-up lazyload"></div> 
                            <?php foreach($product_image as $pro_img){ ?>
                            <div><img src="<?php echo base_url();?><?php echo $pro_img->container_name;?><?php echo $pro_img->image;?>" alt="" class="img-fluid image_zoom_cls-0 blur-up lazyload down"></div> 
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-lg-6 rtl-text">
                        <div class="product-right">
                           
                        <h2> <?php echo $product->title;?></h2>
                            <div class="border-product">
                                 
                                 <p><?php echo $product->description;?></p>
                               
                                
                            </div>

                        <input type="hidden" id="getViewById" value="<?php echo $product->id;?>">
                        <div class="product-description border-product">
                            <div class="row">
                                 <div class="col-md-12 partnumber">
                                     <div class="col-md-4 product-lbl">MFG Number</div>
                                     <div class="col-md-8 product-desc">
                                         <p><?php echo $product->part_no;?></p>
                                     </div>
                                 </div>
                                 <div class="col-md-12 partnumber">
                                     <div class="col-md-4 product-lbl">HSN Number</div>
                                     <div class="col-md-8 product-desc">
                                         <p><?php echo $product->sku_number;?></p>
                                     </div>
                                 </div>
                                 <div class="col-md-12 partnumber">
                                     <div class="col-md-4 product-lbl">Sell Unit</div>
                                     <div class="col-md-8 product-desc">
                                         <p><?php echo $product->sell_unit;?></p>
                                     </div>
                                 </div>
                                   <div class="col-md-12 partnumber">
                                     <div class="col-md-4 product-lbl">Ships Within</div>
                                     <div class="col-md-8 product-desc">
                                         <p><?php echo $product->ships_within;?></p>
                                     </div>
                                 </div>
                                
                           <!--      <div class="col-md-6">
                                    <h6 class="product-title">quantity</h6>
                                    <div class="qty-box">
                                        <div class="input-group">
                                            <span class="input-group-prepend">
                                                <button type="button" class="btn quantity-left-minus" data-type="minus" data-field="">
                                                    <i class="fa fa-minus" aria-hidden="true"></i>
                                                </button>
                                            </span>
                                            <input type="text"  name="quantity" class="form-control input-number" value="1">
                                            <span class="input-group-prepend">
                                                <button type="button" class="btn quantity-right-plus" data-type="plus" data-field="">
                                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                </div> -->
                                <div class="col-md-6">
                                         <div class="product-buttons text-lg-left ">
                                          <!--   <a href="#"  class="btn btn-theme theme-btn-sm width-100">add to quotes</a> -->

                                            <input type="hidden" name="product_id" value="<?php echo $product->id;?>">
                                             <?php if(isset($this->user['id'])){ ?>
                                                    <button class="atq btn" onclick="addToQuote(<?php echo $product->id;?>,<?php echo $this->user['id']?>);">Add to cart</button>
                                              <?php  }else{?>
                                                 <button class="atq btn" onclick="SetProductIntoCookie(<?php echo $product->id;?>,1);">Add to cart</button>
                                             <?php } ?>
                                        </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         <div class ="col-md-4" id="response" style="display: none;float: right;background: none;border: none;">
            <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            Product added to cart!
            </div>
        </div>
    </section>    
   

<?php if(sizeof($product_section)>0) { ?>
    <div class="tab-product">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-lg-12">
                    <ul class="nav nav-tabs nav-material" id="top-tab" role="tablist">
                        <?php 
                                $i=0;
                                foreach($product_section as $pro){
                        ?>
                        <li class="nav-item">
                            <a class="nav-link <?php echo $i==0? 'active' : ''?>" id="top-<?php echo $pro->name;?>-tab" data-toggle="tab" href="#top-<?php echo $pro->name;?>" role="tab" aria-selected="<?php $i==0?'true':'false'?>"><?php echo $pro->name;?></a>
                            <div class="material-border"></div>
                        </li>
                      <?php $i++;} ?>
                    </ul>
                    <div class="tab-content nav-material" id="top-tabContent">
                         <?php 
                                $i=0;
                                foreach($product_section as $pro){
                        ?>
                        <div class="tab-pane fade <?php echo  $i==0? 'show active' : ''?>" id="top-<?php echo $pro->name;?>" role="tabpanel" aria-labelledby="top-<?php echo $pro->name;?>-tab">
                            <p><?php echo $pro->description;?></p>
                        </div>
                        <?php $i++;} ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php }?>
<!-- <?php if(isset($related_product)){?> -->
     <section class="relatedProduct bg-gray">
        <div class="container">
                <div class="row">
                    <div class="col-md-12 main-title">
                        <h4 class="title">Related products</h4>
                        <hr>
                        <div class="slide-6 theme-arrow product-m">
                            <?php foreach($related_product as $rel_pro){?>
                            <!-- <a href="<?php echo base_url();?>product-details/<?php echo $rel_pro->id;?>/<?php echo $rel_pro->slug;?>"> -->
                              <div class="product-box ">
                                <div class="product br-0">
                                    <img src="<?php echo base_url();?><?php echo $rel_pro->url;?>" alt="product" class="img-fluid blur-up lazyload" style="width: 100%; height: 145px; object-fit: cover;">
                                </div>
                                
                                <div class="product-info">
                                    <p><?php echo $rel_pro->title;?></p>                                    
                                </div>
                                
                                <input type="hidden" name="product_id" value="<?php echo $rel_pro->id;?>">
                                <?php if(isset($this->user['id'])){ ?>
                                <button class="atq btn" onclick="addToQuote(<?php echo $rel_pro->id;?>,<?php echo $this->user['id']?>);">Add to cart</button>
                                <?php  }else{?>
                                <button class="atq btn" onclick="SetProductIntoCookie(<?php echo $rel_pro->id;?>,1);">Add to cart</button>
                                <?php } ?>
                             </div>
                            <!-- </a> -->
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
    </section> 
    <!-- <?php } ?> -->