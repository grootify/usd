
<!-- Header End-->

<!-- Slider -->
 <section class="banner1 marginTotop">
         <div class="container-fluid">
                  <div class="row">
                       <div class="col-lg-7">
                       <div class="bgoverBackground">
                        <div class="outer">
                           <div class="bannerText">
                              <h2>When your time is short and you want to save money on supplies, shop our easy-to-use store today.</h2>
                              <!-- <p>From toothbrushes to digital scanners and office design</p> -->
                              <div class="chooseField">
                                <!--  <h5>Are you a</h5> -->
                                 <div class="row mt-5">
                                    <div class="col-lg-4">
                                       <div class="feildDiv">
                                          <div class="feildCard">
                                             <img src="<?php echo base_url();?>assets/images/icon/buyer_icon.png">
                                             <h3>Practitioners</h3>
                                          </div>

                                          <div class="feildCardText">
                                            Save 25% - 50% on most products

                                   <div class="mt-5"><a href="javascript:void(0);" data-toggle="modal" data-target="#basicPlanModal">Sign in</a></div>
                                          </div>
                                    
                                    </div>
                                    </div>
                                    <div class="col-lg-4">
                                       <div class="feildDiv">
                                         <div class="feildCard">
                                             <img src="<?php echo base_url();?>assets/images/icon/group.png">
                                             <h3>Group Purchasing</h3>
                                          </div>

                                          <div class="feildCardText">
                                            Reduce your cost
by joining our group
purchasing programs

                                   <div class="mt-5"><a href="javascript:void(0);" data-toggle="modal" data-target="#basicPlanModal">Sign in</a></div>
                                          </div>
                                    
                                    </div>
                                    </div>

                                    <div class="col-lg-4">
                                       <div class="feildDiv">
                                          <div class="feildCard">
                                             <img src="<?php echo base_url();?>assets/images/icon//whole_seller.png">
                                             <h3>Practitioners</h3>
                                          </div>

                                          <div class="feildCardText">
                                          Get the best deals
by shopping our wide
range of products. 

                                   <div class="mt-5"><a href="javascript:void(0);" data-toggle="modal" data-target="#basicPlanModal">Sign in</a></div>
                                          </div>
                                    
                                    </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>

                        <ul>
                           <li><img src="<?php echo base_url();?>assets/images/icon/ulliicon.png"> Satisfaction Guaranteed</li>
                           <li><img src="<?php echo base_url();?>assets/images/icon/ulliicon.png">Money-Back Guaranteed</li>
                           <li><img src="<?php echo base_url();?>assets/images/icon/ulliicon.png">Hustel Free</li>
                           <li><img src="<?php echo base_url();?>assets/images/icon/ulliicon.png">Genuine Products</li>
                        </ul>
                     </div>
                   
                  </div>
                      <div class="col-lg-5 px-0">
                    <div class="homeImage">
                        <img src="<?php echo base_url();?>assets/images/home/bigbanner1.jpg">
                    </div>
                </div>
                </div>
            
               </div>
      </section>
   
      <!-- usd about us -->
      <section class="usdAds wow fadeInLeft" data-wow-delay="0.2s" style="visibility: visible; -webkit-animation-delay: 0.8s; -moz-animation-delay: 0.8s; animation-delay: 0.8s;">
          <div class="container-fluid">
              <div class="row">
                <a href="">
                <img src="<?php echo base_url();?>assets/images/home/New-Products-1024x90.jpg">
              </a>
             </div>
         </div>
      </section>
       <section class="FeaturedProduct">
    <div class="container">
        <div class="row">
                 <div class="col-lg-6 padding-5px-right padding-5px-left">
                    <div class="heading text-left ">
                       <h2 class="">Current Promotions</h2>
                    </div>
                 </div>
                  <div class="col-lg-6">
                    <div class="text-right">
                       <a href="<?php echo base_url();?>products" class="productBtnRight">See More</a>
                    </div>
                 </div> 
             </div>

        <div class="row margin-bottom-30px margin-top-30px">
            <?php foreach($product_list as $product) { ?>
                <div class="col-lg-3 padding-5px-right padding-5px-left wow fadeInUp" data-wow-delay="0.1s" style="visibility: visible; -webkit-animation-delay: 0.1s; -moz-animation-delay: 0.1s; animation-delay: 0.1s;">
                    <div class="ProductCard ">
                      <a href="<?php echo base_url();?>product-details/<?php echo $product->id;?>/<?php echo $product->slug;?>">
                        <div class="productImage"> 
                            <img src="<?php echo base_url();?><?php echo $product->url;?>" alt="product">
                        </div>
                        <div class="producttext">
                          <p class="itemName"> <?php echo $product->title; ?></p>
                        </div>
                      </a>
                      <input type="hidden" name="product_id" value="<?php echo $product->id;?>">
                         <div class="addTocart">
                           <?php if(isset($this->user['id'])) { ?>
                            <button onclick="addToQuote(<?php echo $pro->id;?>,<?php echo $this->user['id']?>);">Add to cart</button>
                        <?php  }else{ ?>
                            <button onclick="SetProductIntoCookie(<?php echo $product->id;?>,1);">Add to cart</button>
                        <?php } ?>
                         </div>
                     </div>
                  </div>
            <?php } ?>
        </div>
    </div>
</section>


           <!-- usd about us -->
<section class="FeaturedProduct bg-gray">
    <div class="container">
        <div class="row">
                 <div class="col-lg-6 padding-5px-right padding-5px-left">
                    <div class="heading text-left ">
                       <h2 class="">Popular Products</h2>
                    </div>
                 </div>
                  <div class="col-lg-6">
                    <div class="text-right">
                       <a href="<?php echo base_url();?>products" class="productBtnRight">See More</a>
                    </div>
                 </div> 
             </div>
        <div class="row margin-bottom-28px margin-top-30px">
                <?php foreach($featured_product_list as $pro) { ?>
                <div class="col-lg-3 padding-5px-right padding-5px-left wow fadeInUp" data-wow-delay="0.1s" style="visibility: visible; -webkit-animation-delay: 0.1s; -moz-animation-delay: 0.1s; animation-delay: 0.1s;">
                   <div class="ProductCard ">
                    <a href="<?php echo base_url();?>product-details/<?php echo $pro->id;?>/<?php echo $pro->slug;?>">
                      <div class="productImage"> 
                          <img src="<?php echo base_url();?><?php echo $pro->url;?>" alt="product">
                      </div>
                      <div class="producttext">
                          <p class="itemName"><?php echo $pro->title;?></p>
                      </div>
                    </a>
                    <input type="hidden" name="product_id" value="<?php echo $pro->id;?>">
                       <div class="addTocart">
                        <?php if(isset($this->user['id'])){ ?>
                        <button onclick="addToQuote(<?php echo $pro->id;?>,<?php echo $this->user['id']?>);">Add to Cart</button>
                        <?php } else { ?>
                            <button onclick="SetProductIntoCookie(<?php echo $pro->id;?>,1);">Add to Cart</button>
                        <?php } ?>
                       </div>
                   </div>
                </div>
            <?php } ?>
        </div>
    </div>
</section>



         <section class="sec_categories">
          <div class="container">
              <div class="row">
                 <div class="col-lg-6 padding-5px-right padding-5px-left">
                    <div class="heading text-left ">
                       <h2 class="">Shop By Category</h2>
                    </div>
                 </div>
                <!--  <div class="col-lg-6">
                    <div class="text-right">
                       <a href="" class="productBtnRight">See More</a>
                    </div>
                 </div> -->
             </div>

             <div class="row">
                <div class="col-lg-4 padding-5px-right padding-5px-left wow fadeInUp" data-wow-delay="0.3s" style="visibility: visible; -webkit-animation-delay: 0.3s; -moz-animation-delay: 0.3s; animation-delay: 0.3s;">
                   <div class="categoriesCard">
                      <div class="row">
                        <div class="col-lg-4 padding-5px-right">
                           <div class="categoriesImage">
                             <img src="<?php echo base_url();?>assets/images/products/85265.jpg">
                           </div>
                        </div>
                        <div class="col-lg-8">
                           <div class="categoriesText">
                              <h4>Cosmetic Dentistry</h4>
                              <ul>
                                <li><a href=""> Bonding agents</a></li>
                                <li><a href="">Core materials</a></li>
                                <li><a href="">Flowable composites</a></li>
                                <li><a href="">Glass ionomer filling materials</a></li>
                                <li><a href="">Micro applicators</a></li>
                                <li><a href="">Take-home tooth whitening</a></li>
                              </ul>
                           </div>
                        </div>
                      </div>
                       <div class="text-right mt-4 mr-4">
                        <a href="<?php echo base_url();?>quotation" class="productBtnRight">See More</a>
                       </div>
                   </div>

                </div>
                  <div class="col-lg-4 padding-5px-left padding-5px-right wow fadeInUp" data-wow-delay="0.4s" style="visibility: visible; -webkit-animation-delay: 0.4s; -moz-animation-delay: 0.4s; animation-delay: 0.4s;">
                     <div class="categoriesCard">
                      <div class="row">
                        <div class="col-lg-4 padding-5px-right">
                           <div class="categoriesImage">
                             <img src="<?php echo base_url();?>assets/images/products/127277.jpg">
                           </div>
                        </div>
                        <div class="col-lg-8">
                           <div class="categoriesText">
                              <h4>Cosmetic Dentistry</h4>
                               <ul>
                                <li><a href=""> Bonding agents</a></li>
                                <li><a href="">Core materials</a></li>
                                <li><a href="">Flowable composites</a></li>
                                <li><a href="">Glass ionomer filling materials</a></li>
                                <li><a href="">Micro applicators</a></li>
                                <li><a href="">Take-home tooth whitening</a></li>
                              </ul>
                           </div>
                        </div>
                      </div>
                       <div class="text-right mt-4 mr-4">
                        <a href="" class="productBtnRight">See More</a>
                       </div>
                   </div>
                </div>
                  <div class="col-lg-4 padding-5px-right padding-5px-left  wow fadeInUp" data-wow-delay="0.5s" style="visibility: visible; -webkit-animation-delay: 0.5s; -moz-animation-delay: 0.5s; animation-delay: 0.5s;">
                   <div class="categoriesCard">
                      <div class="row">
                        <div class="col-lg-4 padding-5px-right">
                           <div class="categoriesImage">
                             <img src="<?php echo base_url();?>assets/images/products/370189.jpg">
                           </div>
                        </div>
                        <div class="col-lg-8">
                           <div class="categoriesText">
                              <h4>Cosmetic Dentistry</h4>
                               <ul>
                                <li><a href=""> Bonding agents</a></li>
                                <li><a href="">Core materials</a></li>
                                <li><a href="">Flowable composites</a></li>
                                <li><a href="">Glass ionomer filling materials</a></li>
                                <li><a href="">Micro applicators</a></li>
                                <li><a href="">Take-home tooth whitening</a></li>
                              </ul>
                           </div>
                        </div>
                      </div>
                       <div class="text-right mt-4 mr-4">
                        <a href="" class="productBtnRight">See More</a>
                       </div>
                   </div>
                </div>
                  
                
                 <div class="col-lg-4 padding-5px-left padding-5px-right wow fadeInUp mt-2" data-wow-delay="0.8s" style="visibility: visible; -webkit-animation-delay: 0.8s; -moz-animation-delay: 0.8s; animation-delay: 0.8s;">
                    <div class="categoriesCard">
                      <div class="row">
                        <div class="col-lg-4 padding-5px-right">
                           <div class="categoriesImage">
                             <img src="<?php echo base_url();?>assets/images/products/489454.jpg">
                           </div>
                        </div>
                        <div class="col-lg-8">
                           <div class="categoriesText">
                              <h4>Cosmetic Dentistry</h4>
                               <ul>
                                <li><a href=""> Bonding agents</a></li>
                                <li><a href="">Core materials</a></li>
                                <li><a href="">Flowable composites</a></li>
                                <li><a href="">Glass ionomer filling materials</a></li>
                                <li><a href="">Micro applicators</a></li>
                                <li><a href="">Take-home tooth whitening</a></li>
                              </ul>
                           </div>
                        </div>
                      </div>
                       <div class="text-right mt-4 mr-4">
                        <a href="" class="productBtnRight">See More</a>
                       </div>
                   </div>
                </div>

                <div class="col-lg-4 padding-5px-left padding-5px-right wow fadeInUp mt-2" data-wow-delay="1.1s" style="visibility: visible; -webkit-animation-delay: 1.1s; -moz-animation-delay: 1.1s; animation-delay: 1.1s;">
                    <div class="categoriesCard">
                      <div class="row">
                        <div class="col-lg-4 padding-5px-right">
                           <div class="categoriesImage">
                             <img src="<?php echo base_url();?>assets/images/products/EPS_796544.jpg">
                           </div>
                        </div>
                        <div class="col-lg-8">
                           <div class="categoriesText">
                              <h4>Cosmetic Dentistry</h4>
                               <ul>
                                <li><a href=""> Bonding agents</a></li>
                                <li><a href="">Core materials</a></li>
                                <li><a href="">Flowable composites</a></li>
                                <li><a href="">Glass ionomer filling materials</a></li>
                                <li><a href="">Micro applicators</a></li>
                                <li><a href="">Take-home tooth whitening</a></li>
                              </ul>
                           </div>
                        </div>
                      </div>
                       <div class="text-right mt-4 mr-4">
                        <a href="" class="productBtnRight">See More</a>
                       </div>
                   </div>
                </div>

                <div class="col-lg-4 padding-5px-left padding-5px-right wow fadeInUp mt-2" data-wow-delay="1.3s" style="visibility: visible; -webkit-animation-delay: 1.3s; -moz-animation-delay: 1.3s; animation-delay: 1.3s;">
                    <div class="categoriesCard">
                      <div class="row">
                        <div class="col-lg-4 padding-5px-right">
                           <div class="categoriesImage">
                             <img src="<?php echo base_url();?>assets/images/products/85265.jpg">
                           </div>
                        </div>
                        <div class="col-lg-8">
                           <div class="categoriesText">
                              <h4>Cosmetic Dentistry</h4>
                               <ul>
                                <li><a href=""> Bonding agents</a></li>
                                <li><a href="">Core materials</a></li>
                                <li><a href="">Flowable composites</a></li>
                                <li><a href="">Glass ionomer filling materials</a></li>
                                <li><a href="">Micro applicators</a></li>
                                <li><a href="">Take-home tooth whitening</a></li>
                              </ul>
                           </div>
                        </div>
                      </div>
                       <div class="text-right mt-4 mr-4">
                        <a href="" class="productBtnRight">See More</a>
                       </div>
                   </div>
                </div>
             </div>
          </div>
       </section>



 <section class="sec_brands bg-gray">
          <div class="container">
              <div class="row">
                 <div class="col-lg-6 padding-5px-right padding-5px-left">
                    <div class="heading text-left ">
                       <h2 class="">Shop By Manufacturer</h2>
                    </div>
                 </div>
                  <div class="col-lg-6">
                    <div class="text-right">
                       <a href="<?php echo base_url();?>catalog" class="productBtnRight">See More</a>
                    </div>
                 </div> 
             </div>

             <div class="row">
                <div class="col-lg-2 padding-5px-right padding-5px-left  wow fadeInUp" data-wow-delay="0.1s" style="visibility: visible; -webkit-animation-delay: 0.1s; -moz-animation-delay: 0.1s; animation-delay: 0.1s;">
                  <a href="<?php echo base_url();?>product-overview/3M">
                   <div class="brandCard">
                       <img src="<?php echo base_url();?>assets/images/brand/brands-1.jpg">
                   </div>
                  </a>
                </div>
                 <div class="col-lg-2 padding-5px-right padding-5px-left wow fadeInUp" data-wow-delay="0.2s" style="visibility: visible; -webkit-animation-delay: 0.2s; -moz-animation-delay: 0.2s; animation-delay: 0.2s;">
                   <div class="brandCard ">
                       <img src="<?php echo base_url();?>assets/images/brand/brands-2.jpg">
                   </div>
                </div>
                 <div class="col-lg-2 padding-5px-right padding-5px-left wow fadeInUp" data-wow-delay="0.3s" style="visibility: visible; -webkit-animation-delay: 0.3s; -moz-animation-delay: 0.3s; animation-delay: 0.3s;">
                   <div class="brandCard ">
                       <img src="<?php echo base_url();?>assets/images/brand/brands-3.jpg">
                   </div>
                </div>
                 <div class="col-lg-2 padding-5px-right padding-5px-left wow fadeInUp" data-wow-delay="0.4s" style="visibility: visible; -webkit-animation-delay: 0.4s; -moz-animation-delay: 0.4s; animation-delay: 0.4s;">
                   <div class="brandCard ">
                       <img src="<?php echo base_url();?>assets/images/brand/brands-4.jpg">
                   </div>
                </div>
                 <div class="col-lg-2 padding-5px-right padding-5px-left wow fadeInUp" data-wow-delay="0.5s" style="visibility: visible; -webkit-animation-delay: 0.5s; -moz-animation-delay: 0.5s; animation-delay: 0.5s;">
                   <div class="brandCard">
                       <img src="<?php echo base_url();?>assets/images/brand/brands-5.jpg">
                   </div>
                </div>
                 <div class="col-lg-2 padding-5px-right padding-5px-left wow fadeInUp" data-wow-delay="0.6s" style="visibility: visible; -webkit-animation-delay: 0.6s; -moz-animation-delay: 0.6s; animation-delay: 0.6s;">
                   <div class="brandCard ">
                       <img src="<?php echo base_url();?>assets/images/brand/brands-6.jpg">
                   </div>
                </div>
                 <div class="col-lg-2 padding-5px-right padding-5px-left wow fadeInUp" data-wow-delay="0.7s" style="visibility: visible; -webkit-animation-delay: 0.7s; -moz-animation-delay: 0.7s; animation-delay: 0.7s;">
                   <div class="brandCard">
                       <img src="<?php echo base_url();?>assets/images/brand/brands-7.jpg">
                   </div>
                </div>
                 <div class="col-lg-2 padding-5px-right padding-5px-left wow fadeInUp" data-wow-delay="0.8s" style="visibility: visible; -webkit-animation-delay: 0.8s; -moz-animation-delay: 0.8s; animation-delay: 0.8s;">
                   <div class="brandCard ">
                       <img src="<?php echo base_url();?>assets/images/brand/brands-8.jpg">
                   </div>
                </div>
                 <div class="col-lg-2 padding-5px-right padding-5px-left wow fadeInUp" data-wow-delay="0.9s" style="visibility: visible; -webkit-animation-delay: 0.9s; -moz-animation-delay: 0.9s; animation-delay: 0.9s;">
                   <div class="brandCard ">
                       <img src="<?php echo base_url();?>assets/images/brand/brands-9.jpg">
                   </div>
                </div>
                 <div class="col-lg-2 padding-5px-right padding-5px-left wow fadeInUp" data-wow-delay="1s" style="visibility: visible; -webkit-animation-delay: 1s; -moz-animation-delay: 1s; animation-delay: 1s;">
                   <div class="brandCard ">
                       <img src="<?php echo base_url();?>assets/images/brand/brands-10.jpg">
                   </div>
                </div>
                 <div class="col-lg-2 padding-5px-right padding-5px-left wow fadeInUp" data-wow-delay="1.1s" style="visibility: visible; -webkit-animation-delay: 1.1s; -moz-animation-delay: 1.1s; animation-delay: 1.1s;">
                   <div class="brandCard ">
                       <img src="<?php echo base_url();?>assets/images/brand/brands-11.jpg">
                   </div>
                </div>
                 <div class="col-lg-2 padding-5px-right padding-5px-left wow fadeInUp" data-wow-delay="1.2s" style="visibility: visible; -webkit-animation-delay: 1.2s; -moz-animation-delay:1.2s; animation-delay: 1.2s;">
                   <div class="brandCard ">
                       <img src="<?php echo base_url();?>assets/images/brand/brands-12.jpg">
                   </div>
                </div>
                <div class="col-lg-2 padding-5px-right padding-5px-left wow fadeInUp" data-wow-delay="1.3s" style="visibility: visible; -webkit-animation-delay:1.3s; -moz-animation-delay: 1.3s; animation-delay: 1.3s;">
                   <div class="brandCard ">
                       <img src="<?php echo base_url();?>assets/images/brand/brands-13.jpg">
                   </div>
                </div>
                <div class="col-lg-2 padding-5px-right padding-5px-left wow fadeInUp" data-wow-delay="1.4s" style="visibility: visible; -webkit-animation-delay: 1.4s; -moz-animation-delay: 1.4s; animation-delay: 1.4s;">
                   <div class="brandCard ">
                       <img src="<?php echo base_url();?>assets/images/brand/brands-14.jpg">
                   </div>
                </div>
                <div class="col-lg-2 padding-5px-right padding-5px-left wow fadeInUp" data-wow-delay="1.5s" style="visibility: visible; -webkit-animation-delay:1.5s; -moz-animation-delay: 1.5s; animation-delay: 1.5s;">
                   <div class="brandCard ">
                       <img src="<?php echo base_url();?>assets/images/brand/brands-15.jpg">
                   </div>
                </div>
                <div class="col-lg-2 padding-5px-right padding-5px-left wow fadeInUp" data-wow-delay="1.6s" style="visibility: visible; -webkit-animation-delay:1.6s; -moz-animation-delay: 1.6s; animation-delay: 1.6s;">
                   <div class="brandCard ">
                       <img src="<?php echo base_url();?>assets/images/brand/brands-16.jpg">
                   </div>
                </div>
                <div class="col-lg-2 padding-5px-right padding-5px-left wow fadeInUp" data-wow-delay="1.7s" style="visibility: visible; -webkit-animation-delay: 1.7s; -moz-animation-delay: 1.7s; animation-delay: 1.7s;">
                   <div class="brandCard ">
                       <img src="<?php echo base_url();?>assets/images/brand/brands-17.jpg">
                   </div>
                </div>
                <div class="col-lg-2 padding-5px-right padding-5px-left wow fadeInUp" data-wow-delay="1.8s" style="visibility: visible; -webkit-animation-delay:1.8s; -moz-animation-delay: 0.8s; animation-delay: 1.8s;">
                   <div class="brandCard ">
                       <img src="<?php echo base_url();?>assets/images/brand/brands-18.jpg">
                   </div>
                </div>


             </div>
             
          </div>
       </section>


   
       <!-- footer -->


 <!-- <section class="sec_blog">
          <div class="container">
             <div class="row">
                 <div class="col-lg-10 ml-auto mr-auto">
                    <div class="heading">
                       <h2>Blog</h2>
                    </div>
                 </div>
             </div>
             <div class="row">
              <div class="col-lg-4">
                 <div class="blogCard wow fadeInUp" data-wow-delay="0.2s" style="visibility: visible; -webkit-animation-delay:0.2s; -moz-animation-delay: 0.2s; animation-delay: 0.2s;">
                   <img src="<?php echo base_url();?>assets/images/blog/dental-story-page-20DS6371-equipment-01.jpg">
                   <p>New webinars every month. Learn about important topics relating to Equipment & Technology. No cost to attend and/or register.</p>

                   <a href="">Read More</a>
                 </div>
              </div>

                <div class="col-lg-4">
                 <div class="blogCard wow fadeInUp" data-wow-delay="0.4s" style="visibility: visible; -webkit-animation-delay:0.4s; -moz-animation-delay: 0.4s; animation-delay: 0.4s;">
                   <img src="<?php echo base_url();?>assets/images/blog/dental-story-page-20DS6371-equipment-02a.jpg">
                   <p>New webinars every month. Learn about important topics relating to Equipment & Technology. No cost to attend and/or register.</p>
                    <a href="">Read More</a>
                 </div>
              </div>

                <div class="col-lg-4">
                 <div class="blogCard wow fadeInUp" data-wow-delay="0.6s" style="visibility: visible; -webkit-animation-delay:0.6s; -moz-animation-delay: 0.6s; animation-delay: 0.6s;">
                   <img src="<?php echo base_url();?>assets/images/blog/dental-story-page-20DS6371-equipment-03a.jpg">
                   <p>New webinars every month. Learn about important topics relating to Equipment & Technology. No cost to attend and/or register.</p>
                    <a href="">Read More</a>
                 </div>
              </div>


              
             </div>
          </div>
       </section> -->

       <section class="otherField">
          <div class="container">
            <div class="col-lg-10 ml-auto mr-auto">
             <div class="row">
                <div class="col-lg-4  wow fadeInUp" data-wow-delay="0.2s">
                  <div class="feildCardParentdiv">
                  <div class="feildCard">
                   
                     <img src="<?php echo base_url();?>assets/images/icon/buyer_icon.png">
                       <h3>Practitioners</h3>
                   </div>
                   <div class="feildCardTextB">
                                            Save 25% - 50%
on most products

                                      <div><a href="javascript:void(0);" data-toggle="modal" data-target="#basicPlanModal">Sign in</a></div>
                                          </div>
                </div>
              </div>

              

                 <div class="col-lg-4  wow fadeInUp" data-wow-delay="0.6s">
                   <div class="feildCardParentdiv">
                 <div class="feildCard">
                                            
                                             <img src="<?php echo base_url();?>assets/images/icon/group.png">
                                             <h3>Group Purchasing</h3>
                                          </div>
                                          <div class="feildCardTextB">
                                           Reduce your cost
by joining our group
purchasing program

                                      <div><a href="javascript:void(0);" data-toggle="modal" data-target="#basicPlanModal">Sign in</a></div>
                                          </div>
                </div>
              </div>

                 <div class="col-lg-4  wow fadeInUp" data-wow-delay="0.8s">
                   <div class="feildCardParentdiv">
                     <div class="feildCard">
                                           
                                             <img src="<?php echo base_url();?>assets/images/icon/whole_seller.png">
                                             <h3>Wholesalers</h3>
                                          </div>
                                          <div class="feildCardTextB">
                                          Get the best deals
by shopping our wide
range of products. 

                                      <div><a href="javascript:void(0);" data-toggle="modal" data-target="#basicPlanModal">Sign in</a></div>
                                          </div>
                                        </div>
                </div>
             </div>
          </div>
        </div>
       </section>

       <section class="subscribeUsd">
           <div class="container">
             <div class="row">
               <div class="col-md-12 center-col text-center">
                  <h6 class="heading"> Get the weekly promotions and free offers by subscribing to our newsletter. </h6>
               </div>
            </div>
            
                 <form class="form-row">
                           <div class="form-group col-lg-3 ml-auto">
             
              <input type="name" name="neame" id="name" class="form-control sbscrbForm" placeholder="Name" required="">
            </div>
                           <div class="form-group col-lg-3">
                        <input type="email" name="email" id="email" class="form-control sbscrbForm" placeholder="Email" required="">
            </div>

                                              <div class="text-top-40px col-lg-3 mr-auto">
                            <button type="submit" form="form1" value="Submit" class="btn btn-small btnUsd width-100 text-medium">Submit</button>
                          
                        
              </div></form>
       
      </div></section>