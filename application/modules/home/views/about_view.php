<style>
.no-padding {
    padding: 0 !important;
}
.bg-extra-dark-gray{
    background-color: #1c1c1c;
}

.margin-10px-bottom {
    margin-bottom: 10px;
}
.text-light-gray {
    color: #d6d5d5!important;
}
.num {
    width: 5%;
    display: inline-block;
}
.text-deep-pink {
    color: #3563a9;
}
.text-large {
    font-size: 18px;
    line-height: 26px;
}
.para {
    width: 80%;
    display: inline-block;
    margin-left: 25px;
}
.text-medium {
    font-size: 16px;
    line-height: 23px;
}
.pull-left {
    float: left!important;
}
p {
    padding-top: 0.5rem;
    padding-bottom: 0.5rem;
    line-height: 25px;
}
.col-2-nth .col-sm-6:nth-child(2n+1) {
    clear: left;
}
.margin-six-bottom {
    margin-bottom: 20px;
}
.last-paragraph-no-margin p:last-of-type {
    margin-bottom: 0;
}


</style>
<section class="about-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="banner-section">
                        <img src="<?php echo base_url();?>assets/images/blueBg.jpg" class="img-fluid blur-up blur-up lazyloaded" alt="">
                        <h4>Choose us to handle your medical supplies needs. We’ll do it in a jiffy!</h4>
                    </div>
                </div>
                <div class="col-sm-12 margin-top-20px">
                    <p>AK Global has been simplifying the process of procuring and supplying medical and laboratory supplies since 2007. The fact that we have forayed into global space today, proves our ability to work across borders and time zones, ensuring customer satisfaction at the same time. Our profound knowledge of medical and laboratory supplies, helps us streamline customer orders from a number of suppliers into a single one, thus ensuring hassle free purchase. A wide database, prompt delivery and the willingness to provide sustainable measures has established us as a trustworthy name in the business.</p>
                    
                </div>
            </div>
        </div>
    </section>
    <section class="ak_brands">
        <div class="container">
            <div class="row titleRow">
                <div class="col-sm-6 col-md-6 brandhead">
                  <h4 class="title">OUR VISION</h4>
                </div>
                <hr>
            </div>
             <div class="row">
               <?php $name = namechange(); ?>
                <div class="col-sm-7 margin-top-20px">
                  <p>We at <?php echo $name ?> firmly believe that we must alleviate the purchase and logistics burden, off our customer’s shoulders allowing them to focus on their priorities, ie. treatment of patients and support of scientists and researchers. We have access to over one million products from top brands world- wide. Our vision is to bridge the gap between manufacturer and customer by being their go-to suppliers for laboratory and medical supplies. We operate on the principle of Saving time = Saving money. Our forte lies in chiseling processes to ensure client satisfaction. We have successfully managed to do so and with our customer-centric vision, we continue to progress in the same vein.</p>
                </div>
                <div class="col-sm-5 margin-top-20px">
                 <img src="<?php echo base_url();?>assets/images/vision.jpg" class="img-fluid" style="border-radius:6px;" alt="missionImage">
                    
                </div>
            </div>

        </div>
    </section>
    <section class="ak_brands">
        <div class="container">
            <div class="row titleRow">
                <div class="col-sm-6 col-md-6 brandhead">
                  <h4 class="title">OUR MISSION</h4>
                </div>
                <hr>
            </div>
             <div class="row no-padding wow fadeIn bg-extra-dark-gray" style="margin-top: 30px;">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 cover-background sm-height-500px xs-height-350px wow fadeIn" style="background-image: url(<?php echo base_url();?>assets/images/mission.jpg); visibility: visible; animation-name: fadeIn;"><div class="xs-height-400px"></div></div>

                 <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
                        <div class=" pull-left sm-no-padding-lr md-padding-50px-tb sm-padding-70px-tb padding-20px-all  xs-no-padding">
                           
                            <div class="row xs-text-center">
                                <!-- start feature box item -->
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 margin-six-bottom sm-margin-30px-bottom xs-no-padding last-paragraph-no-margin">
                                    <div class="text-light-gray margin-10px-bottom sm-margin-5px-bottom alt-font"><span class="text-deep-pink  pull-left text-large xs-display-block xs-margin-lr-auto xs-width-100 num">01.</span>
                                            <p class="para text-medium text-light-gray">To provide our clients with  medical and laboratory  supplies in a simplified and efficient manner doing away with the burden of logistics.</p>
                                     </div>
                                
                                </div>
                                <!-- end feature box item -->
                                <!-- start feature box item -->
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 margin-six-bottom sm-margin-30px-bottom xs-no-padding last-paragraph-no-margin">
                                    <div class="text-light-gray margin-10px-bottom sm-margin-5px-bottom alt-font"><span class="text-deep-pink  pull-left text-large xs-display-block xs-margin-lr-auto xs-width-100 num">02.</span>
                                      <p class="para text-medium text-light-gray">To lighten the cumbersome process of product purchases by amalgamating it into one.</p></div>
                                  
                                </div>
                                <!-- end feature box item -->
                                <!-- start feature box item -->
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 md-margin-six-bottom sm-no-margin-bottom xs-margin-30px-bottom xs-no-padding last-paragraph-no-margin">
                                    <div class="text-light-gray margin-10px-bottom sm-margin-5px-bottom alt-font"><span class="text-deep-pink  pull-left text-large xs-display-block xs-margin-lr-auto xs-width-100 num">03.</span>
                                     <p class="para text-medium text-light-gray">To make effective use of technology to curate the best out of a wide variety available, thus making choices easier and efficient. </p></div>
                                  
                                </div>
                                <!-- end feature box item -->
                                <!-- start feature box item -->
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 xs-no-padding last-paragraph-no-margin">
                                   <div class="text-light-gray margin-10px-bottom sm-margin-5px-bottom alt-font"><span class="text-deep-pink  pull-left text-large xs-display-block xs-margin-lr-auto xs-width-100 num">04.</span>
                                    <p class="para text-medium text-light-gray">To endeavor towards strong and effective customer relationship management.</p></div>
                                    
                                </div>
                                 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 xs-no-padding last-paragraph-no-margin">
                                   <div class="text-light-gray margin-10px-bottom sm-margin-5px-bottom alt-font"><span class="text-deep-pink  pull-left text-large xs-display-block xs-margin-lr-auto xs-width-100 num">05.</span>
                                    <p class="para text-medium text-light-gray">To work within the ethical boundaries of the profession and resolving issues with patience and integrity.</p></div>
                                    
                                </div>
                                <!-- end feature box item -->
                            </div>
                        </div>
                    </div> 
            </div>

        </div>
    </section>

    