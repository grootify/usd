<div class="breadcrumb-section">
        <div class="container">
            <div class="row text-center-md">
                <div class="col-md-6 col-sm-12">
                    <div class="page-title">
                        <h2>FAQ</h2>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <nav aria-label="breadcrumb" class="theme-breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url();?>">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">faq</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>

    <section class="faq-section section-b-space">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="accordion theme-accordion" id="accordionExample">
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h5 class="mb-0 pb-0">
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.
                                    </button>
                                </h5>
                            </div>

                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample" style="">
                                <div class="card-body">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingTwo">
                                <h5 class="mb-0 pb-0">
                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Neque porro quisquam est qui dolorem ipsum quia.
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                <div class="card-body">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingThree">
                                <h5 class="mb-0 pb-0">
                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                <div class="card-body">
                                    <p>Curabitur consequat ac tellus vitae consequat. Vestibulum quis dignissim nisl. Aliquam luctus suscipit nibh, quis luctus ipsum lobortis nec. Nullam vestibulum porttitor finibus. Integer a eleifend elit. Nunc in ligula non sem porta maximus et dignissim mi. Curabitur urna leo, maximus vitae arcu at, dapibus pharetra est. Sed pretium augue id tellus posuere interdum. Sed a massa tincidunt, maximus eros quis, congue erat. Mauris faucibus, justo vitae viverra suscipit, arcu nisl varius tellus, ac molestie ligula dui eu odio. Praesent varius ipsum et faucibus pretium. Quisque dignissim ante ut laoreet molestie.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingFour">
                                <h5 class="mb-0 pb-0">
                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                        In hac habitasse platea dictumst. Duis nec placerat nulla.
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                                <div class="card-body">
                                    <p> Sed risus urna, vulputate vitae semper a, maximus ac purus. Morbi faucibus nulla risus, id suscipit ex accumsan id. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In pulvinar libero quis tortor pulvinar, sed iaculis enim efficitur. Integer eu iaculis dui. Cras et lacus vitae augue blandit sagittis. Phasellus eleifend nisl at posuere tempor. Cras convallis sem sit amet leo iaculis tempus. In et libero ante. Nam sagittis tempor orci, quis cursus diam consectetur id. Etiam feugiat orci vel tortor placerat varius.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingFive">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                       Fusce consectetur mi in nibh efficitur euismod.
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
                                <div class="card-body">
                                    <p>Sed fringilla, nisl pulvinar tempor gravida, mauris justo semper magna, sed ornare elit neque at nibh. Nullam arcu quam, tristique vel pellentesque volutpat, tristique sed diam. Suspendisse tincidunt feugiat risus a auctor. Cras elit elit, venenatis vitae risus sit amet, venenatis convallis eros. Vivamus odio odio, lacinia vel blandit at, mattis at sapien. Aenean egestas, orci quis feugiat sollicitudin, odio purus aliquet massa, et dapibus odio velit at mi. Suspendisse eu dui ac orci consequat sollicitudin eget ac neque.</p>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
    </section>