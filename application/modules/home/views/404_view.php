<div class="breadcrumb-section">
        <div class="container">
            <div class="row text-center-md">
                <div class="col-md-6 col-sm-12">
                    <div class="page-title">
                        <h2>404</h2>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <nav aria-label="breadcrumb" class="theme-breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url();?>">Home</a></li>
                            <li class="breadcrumb-item active">404</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container error-section">
        <div class="row">
            <div class="col-md-12 md-order-top">
                <div>
                    <h1>404</h1>
                </div>
            </div>
            <div class="col-md-12">
                <div class="sm-top-0 pt-0">
                    <div>
                    <h2>Sorry, couldn't find what you're looking for</h2>
                    <a href="<?php echo base_url();?>" class="btn btn-theme">back to home</a>
                    </div>
                </div>
            </div>
        </div>
    </div>