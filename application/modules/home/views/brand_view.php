<!-- breadcrumb start -->
<div class="breadcrumb-section">
    <div class="container">
        <div class="row text-center-md">
            <div class="col-md-6 col-sm-12">
                <div class="page-title">
                    <h2>Brands</h2>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <nav aria-label="breadcrumb" class="theme-breadcrumb">
                   <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url();?>">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Brands</li>
                       
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb End -->

<!-- Brand list start -->
 <section class="ak_brands">
    <div class="container">
        <div class="row margin-bottom-40px margin-top-40px">
            <?php foreach ($brands as $brand) { ?>
            <div class="col-md-2 col-sm-12 brandbox" style="margin-bottom: 30px;">
                <a href="<?php echo $brand->link_url; ?>" target="_blank" style="text-align: center;">
                    <div class="product-box hover">
                        <img src="<?php echo base_url().$brand->image;?>" style="width:120px;height:110px;margin-left: 19px;" alt="<?php echo $brand->name;?>">
                    </div>
                </a>
            </div>
            <?php } ?>
        </div>
    </div>
</section>
<!-- Brand list end -->
