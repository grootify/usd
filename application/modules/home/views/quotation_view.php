<!-- breadcrumb start -->
    <div class="breadcrumb-section marginTotop">
        <div class="container">
            <div class="row text-center-md">
                <div class="col-md-6 col-sm-12">
                    <div class="page-title">
                        <h2>My Cart</h2>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <nav aria-label="breadcrumb" class="theme-breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url();?>">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">My Cart</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb End -->
    
    <!-- Cart start-->
    <section class="cart-section ">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <table class="table cart-table table-responsive-xs">
                        <thead>
                        <tr class="table-head">
                            <th scope="col">Image</th>
                            <th scope="col">Product name</th>
                            <th scope="col">Quantity</th>
                            <th scope="col">Action</th>
                        </tr>
                        </thead>
                        <tbody class="quote-data">
                            <?php 
                            error_reporting(0);
                                if($myQuote_list!=''){
                                    foreach($myQuote_list as $myQuote){
                            ?>
                        <tr>
                            <td>
                                <a href="#">
                                    <img src="<?php echo base_url();?>assets/images/aje7y08w9hlsefb4jraf.jpg" alt="" class="">
                                </a>
                            </td>
                            <td class="product-name"><a href="#"><?php echo $myQuote->title;?></a>
                                
                            </td>
                            <td class="forMobileQtyBox qtyBox" style="border-bottom: none;">
                                    <div class="qty-box">
                                        <div class="input-group">
                                            <span class="input-group-prepend">
                                                <button type="button" class="btn quantity-left-minus" data-type="minus" data-field="">
                                                    <i class="fa fa-minus" aria-hidden="true"></i>
                                                </button>
                                            </span>
                                            <input type="text"  name="quantity" class="form-control input-number inputmobile" value="<?php echo $myQuote->quantity;?>">
                                            <input type="hidden" class="inputP" name="cart_id" value="<?php echo $myQuote->id;?>">
                                            <span class="input-group-prepend">
                                                <button type="button" class="btn quantity-right-plus" data-type="plus" data-field="">
                                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                            </td>
                            <td class="quoteBtn" style="border-bottom: none;">
                                <button type="button" class="btn icon deletebtn" onclick="setStatus(<?php echo $myQuote->id;?>);">
                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </button>
                            
                            </td>
                        </tr>
                    <?php } } else{
                        if($productArray!=''){
                             foreach($productArray as $pro){
                                if($pro[0]->title!=''){
                                    foreach($cart_data as $cart){
                                        if($cart[0]==$pro[0]->id){
                                        
                     ?>
                              <tr style="border-bottom:1px solid #eeeeee;">
                            <td style="border-bottom: none;">
                                <a href="#">
                                    <img src="<?php echo base_url();?><?php echo $pro[0]->url;?>" alt="" class="">
                                </a>
                            </td>
                            <td style="border-bottom: none;" class="product-name"><a href="#"><?php echo $pro[0]->title;?></a>
                                
                            </td>
                            <td class="forMobileQtyBox qtyBox" style="border-bottom: none;">
                                    <div class="qty-box">
                                        <div class="input-group">
                                            <span class="input-group-prepend">
                                                <button type="button" class="btn quantity-left-minus" data-type="minus" data-field="">
                                                    <i class="fa fa-minus" aria-hidden="true"></i>
                                                </button>
                                            </span>
                                            
                                            <input type="text"  name="quantity"  class="form-control input-number inputmobile" value="<?php echo $cart[1];?>">
                                            <input type="hidden" class="inputPro" name="product_id" value="<?php echo $pro[0]->id;?>">
                                            <span class="input-group-prepend">
                                                <button type="button" class="btn quantity-right-plus" data-type="plus" data-field="">
                                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                            </td>
                            <td class="quoteBtn" style="border-bottom: none;">
                                <button type="button" class="btn icon deletebtn" onclick="deleteWishlist(<?php echo $pro[0]->id;?>,<?php echo $cart[1];?>)">
                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </button>
                            
                            </td>
                        </tr>
                    <?php } } }  }  }  }?>
                        </tbody>
                       

                    </table>
                </div>
            </div>
            <div class="row cart-buttons">
                <!-- <div class="col-12 col-sm-6">
                    <a href="<?php echo base_url();?>" class="btn btn-theme">continue exploring</a>
                </div> -->
                <div class="col-12 col-sm-6">
                       <?php if(isset($this->user['id'])){?>
                                    <a href="<?php echo base_url();?>checkout" class="btn btn-theme">Submit request</a>    
                             <?php }else{ ?>
                                     <a href="#" class="btn btn-theme" id="loginFormbtn" data-toggle="modal" data-target="#basicPlanModal">Submit request</a>
                            <?php } ?>

                </div>
            </div>
        </div>
    </section>
    <!-- Cart End-->