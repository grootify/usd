<!-- breadcrumb start -->
<div class="breadcrumb-section">
    <div class="container">
        <div class="row text-center-md">
            <div class="col-md-6 col-sm-12">
                <div class="page-title">
                    <h2>Catalog</h2>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <nav aria-label="breadcrumb" class="theme-breadcrumb">
                   <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url();?>">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Catalog</li>
                       
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb End -->

<!-- Brand list start -->
<!--  <section class="ak_brands">
    <div class="container">
        <div class="row margin-bottom-40px margin-top-40px">
            <?php foreach($catalogs as $catalog){ ?>
            <div class="col-md-2 col-sm-12 brandbox" style="margin-bottom: 30px;">
                <a href="<?php echo base_url();?>product-overview/<?php echo $catalog->slug;?>" style="text-align: center;">
                 <div class="product-box hover">
                    <img src="<?php echo base_url();?><?php echo $catalog->image;?>" style="width:120px;height:110px;margin-left: 19px;" alt="">
                </div> 
                </a>
             
            </div>
          <?php } ?>
        </div>
    </div>
</section> -->

<section class="sec_brands bg-gray">
          <div class="container">
              <div class="row margin-top-40px">
                 <div class="col-lg-6 padding-5px-right padding-5px-left">
                    <div class="heading text-left ">
                       <h2 class="">Shop By Manufacturer</h2>
                    </div>
                 </div>
                 <!--  <div class="col-lg-6">
                    <div class="text-right">
                       <a href="<?php echo base_url();?>catalog" class="productBtnRight">See More</a>
                    </div>
                 </div>  -->
             </div>

             <div class="row">
                <div class="col-lg-2 padding-5px-right padding-5px-left  wow fadeInUp" data-wow-delay="0.1s" style="visibility: visible; -webkit-animation-delay: 0.1s; -moz-animation-delay: 0.1s; animation-delay: 0.1s;">
                   <div class="brandCard">
                       <img src="<?php echo base_url();?>assets/images/brand/brands-1.jpg">
                   </div>
                </div>
                 <div class="col-lg-2 padding-5px-right padding-5px-left wow fadeInUp" data-wow-delay="0.2s" style="visibility: visible; -webkit-animation-delay: 0.2s; -moz-animation-delay: 0.2s; animation-delay: 0.2s;">
                   <div class="brandCard ">
                       <img src="<?php echo base_url();?>assets/images/brand/brands-2.jpg">
                   </div>
                </div>
                 <div class="col-lg-2 padding-5px-right padding-5px-left wow fadeInUp" data-wow-delay="0.3s" style="visibility: visible; -webkit-animation-delay: 0.3s; -moz-animation-delay: 0.3s; animation-delay: 0.3s;">
                   <div class="brandCard ">
                       <img src="<?php echo base_url();?>assets/images/brand/brands-3.jpg">
                   </div>
                </div>
                 <div class="col-lg-2 padding-5px-right padding-5px-left wow fadeInUp" data-wow-delay="0.4s" style="visibility: visible; -webkit-animation-delay: 0.4s; -moz-animation-delay: 0.4s; animation-delay: 0.4s;">
                   <div class="brandCard ">
                       <img src="<?php echo base_url();?>assets/images/brand/brands-4.jpg">
                   </div>
                </div>
                 <div class="col-lg-2 padding-5px-right padding-5px-left wow fadeInUp" data-wow-delay="0.5s" style="visibility: visible; -webkit-animation-delay: 0.5s; -moz-animation-delay: 0.5s; animation-delay: 0.5s;">
                   <div class="brandCard">
                       <img src="<?php echo base_url();?>assets/images/brand/brands-5.jpg">
                   </div>
                </div>
                 <div class="col-lg-2 padding-5px-right padding-5px-left wow fadeInUp" data-wow-delay="0.6s" style="visibility: visible; -webkit-animation-delay: 0.6s; -moz-animation-delay: 0.6s; animation-delay: 0.6s;">
                   <div class="brandCard ">
                       <img src="<?php echo base_url();?>assets/images/brand/brands-6.jpg">
                   </div>
                </div>
                 <div class="col-lg-2 padding-5px-right padding-5px-left wow fadeInUp" data-wow-delay="0.7s" style="visibility: visible; -webkit-animation-delay: 0.7s; -moz-animation-delay: 0.7s; animation-delay: 0.7s;">
                   <div class="brandCard">
                       <img src="<?php echo base_url();?>assets/images/brand/brands-7.jpg">
                   </div>
                </div>
                 <div class="col-lg-2 padding-5px-right padding-5px-left wow fadeInUp" data-wow-delay="0.8s" style="visibility: visible; -webkit-animation-delay: 0.8s; -moz-animation-delay: 0.8s; animation-delay: 0.8s;">
                   <div class="brandCard ">
                       <img src="<?php echo base_url();?>assets/images/brand/brands-8.jpg">
                   </div>
                </div>
                 <div class="col-lg-2 padding-5px-right padding-5px-left wow fadeInUp" data-wow-delay="0.9s" style="visibility: visible; -webkit-animation-delay: 0.9s; -moz-animation-delay: 0.9s; animation-delay: 0.9s;">
                   <div class="brandCard ">
                       <img src="<?php echo base_url();?>assets/images/brand/brands-9.jpg">
                   </div>
                </div>
                 <div class="col-lg-2 padding-5px-right padding-5px-left wow fadeInUp" data-wow-delay="1s" style="visibility: visible; -webkit-animation-delay: 1s; -moz-animation-delay: 1s; animation-delay: 1s;">
                   <div class="brandCard ">
                       <img src="<?php echo base_url();?>assets/images/brand/brands-10.jpg">
                   </div>
                </div>
                 <div class="col-lg-2 padding-5px-right padding-5px-left wow fadeInUp" data-wow-delay="1.1s" style="visibility: visible; -webkit-animation-delay: 1.1s; -moz-animation-delay: 1.1s; animation-delay: 1.1s;">
                   <div class="brandCard ">
                       <img src="<?php echo base_url();?>assets/images/brand/brands-11.jpg">
                   </div>
                </div>
                 <div class="col-lg-2 padding-5px-right padding-5px-left wow fadeInUp" data-wow-delay="1.2s" style="visibility: visible; -webkit-animation-delay: 1.2s; -moz-animation-delay:1.2s; animation-delay: 1.2s;">
                   <div class="brandCard ">
                       <img src="<?php echo base_url();?>assets/images/brand/brands-12.jpg">
                   </div>
                </div>
                <div class="col-lg-2 padding-5px-right padding-5px-left wow fadeInUp" data-wow-delay="1.3s" style="visibility: visible; -webkit-animation-delay:1.3s; -moz-animation-delay: 1.3s; animation-delay: 1.3s;">
                   <div class="brandCard ">
                       <img src="<?php echo base_url();?>assets/images/brand/brands-13.jpg">
                   </div>
                </div>
                <div class="col-lg-2 padding-5px-right padding-5px-left wow fadeInUp" data-wow-delay="1.4s" style="visibility: visible; -webkit-animation-delay: 1.4s; -moz-animation-delay: 1.4s; animation-delay: 1.4s;">
                   <div class="brandCard ">
                       <img src="<?php echo base_url();?>assets/images/brand/brands-14.jpg">
                   </div>
                </div>
                <div class="col-lg-2 padding-5px-right padding-5px-left wow fadeInUp" data-wow-delay="1.5s" style="visibility: visible; -webkit-animation-delay:1.5s; -moz-animation-delay: 1.5s; animation-delay: 1.5s;">
                   <div class="brandCard ">
                       <img src="<?php echo base_url();?>assets/images/brand/brands-15.jpg">
                   </div>
                </div>
                <div class="col-lg-2 padding-5px-right padding-5px-left wow fadeInUp" data-wow-delay="1.6s" style="visibility: visible; -webkit-animation-delay:1.6s; -moz-animation-delay: 1.6s; animation-delay: 1.6s;">
                   <div class="brandCard ">
                       <img src="<?php echo base_url();?>assets/images/brand/brands-16.jpg">
                   </div>
                </div>
                <div class="col-lg-2 padding-5px-right padding-5px-left wow fadeInUp" data-wow-delay="1.7s" style="visibility: visible; -webkit-animation-delay: 1.7s; -moz-animation-delay: 1.7s; animation-delay: 1.7s;">
                   <div class="brandCard ">
                       <img src="<?php echo base_url();?>assets/images/brand/brands-17.jpg">
                   </div>
                </div>
                <div class="col-lg-2 padding-5px-right padding-5px-left wow fadeInUp" data-wow-delay="1.8s" style="visibility: visible; -webkit-animation-delay:1.8s; -moz-animation-delay: 0.8s; animation-delay: 1.8s;">
                   <div class="brandCard ">
                       <img src="<?php echo base_url();?>assets/images/brand/brands-18.jpg">
                   </div>
                </div>


             </div>
             
          </div>
       </section>
<!-- Brand list end -->