<style>
.current {
  color: green;
}

#pagin li {
  display: inline-block;
}
.product-info {
    padding: 10px 15px;
    min-height: 80px!important;
}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
  <!-- breadcrumb start -->
    <div class="breadcrumb-section marginTotop">
        <div class="container">
            <div class="row text-center-md">
                <div class="col-md-6 col-sm-12">
                    <div class="page-title">
                        <h2>Products</h2>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <nav aria-label="breadcrumb" class="theme-breadcrumb">
                       <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url();?>">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Products</li>
                           
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb End -->
  <!--   <img src="<?php echo base_url();?>assets/images/products/Midmark 9A371003.jpg" width="300" height="200" alt="check pic"> -->
    <!-- category page -->
    <section class="section-b-space">
        <div class="collection-wrapper">
            <div class="container">
                <div class="row">
                    
                  
                    <div class="collection-content col">
                        <div class="page-main-content">
                            <div class="container-fluid p-0">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="collection-product-wrapper">
                                            <div class="product-wrapper-grid">
                                                <div class="container-fluid">
                                                    <div class="row sapce-category">
                                                        <?php 
                                                            if($product_list!=''){
                                                             foreach($product_list as $pro){
                                                         ?>
                                                    <div class="col-xl-3 col-md-6 col-grid-box line-content">
                                                        <a href="<?php echo base_url();?>product-details/<?php echo $pro->id;?>/<?php echo $pro->slug;?>">
                                                            <div class="product-box hover">
                                                            <div class="product br-0">
                                                                <img src="<?php echo base_url();?><?php echo $pro->url;?>" alt="product" class="img-fluid blur-up lazyloaded" style="width: 100%; height: 180px; min-height:180px; object-fit: cover;">
                                                            </div>
                                                            <div class="product-info">
                                                                <p><?php echo $pro->title;?></p>
                                                            </div>
                                                             <?php if(isset($this->user['id'])){ ?>
                                                            <button class="atq btn" onclick="addToQuote(<?php echo $pro->id;?>,<?php echo $this->user['id'];?>);">Add to cart</button>
                                                            <?php  }else{?>
                                                            <button class="atq btn" onclick="SetProductIntoCookie(<?php echo $pro->id;?>,1);">Add to cart</button>
                                                            <?php } ?>
                                                            </div>
                                                        </a>                                                      
                                                    </div>
                                                    <?php } }?>
                                                    </div>
                                                </div>
                                            <div class ="col-md-4" id="response" style="display: none;float: right;background: none;border: none;">
                                            <div class="alert alert-success" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            Product added to quote!
                                            </div>
                                            </div>
                                            </div>
                                            <div class="product-pagination">
                                                <div class="theme-paggination-block">
                                                    <div class="container-fluid p-0">
                                                        <div class="row">
                                                            <div class="col-xl-6 col-md-6 col-sm-12">
                                                                <nav aria-label="Page navigation">
                                                                    <ul class="pagination" id="pagin">
                                                                        <li class="page-item">
                                                                            <a class="page-link" href="#" aria-label="Previous">
                                                                                <span aria-hidden="true"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>
                                                                                <span class="sr-only">Previous</span>
                                                                            </a>
                                                                        </li>
                                                                        <?php 
                                                                        $total=count($product_list)/8;
                                                                        for($i=1;$i<=$total;$i++){?>
                                                                        <li class="page-item  <?php echo $i=='1'? 'active current':''?>"><a class="page-link" href="#"><?php echo $i;?></a></li>
                                                                       <?php } ?>

                                                                        <li class="page-item">
                                                                            <a class="page-link" href="#" aria-label="Next">
                                                                                <span aria-hidden="true"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
                                                                                <span class="sr-only">Next</span>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </nav>
                                                            </div>
                                                        
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
   <!--  <section class="featuredProducts">
          <div class="container">
            <div class="row">
                 <div class="col-lg-6 padding-5px-right padding-5px-left">
                    <div class="heading text-left ">
                       <h2 class="">Current Promotions</h2>
                    </div>
                 </div>
                 <div class="col-lg-6">
                    <div class="text-right">
                       <a href="" class="productBtnRight">See More</a>
                    </div>
                 </div>
            </div>

             <div class="row">
                <div class="col-lg-2 padding-5px-right padding-5px-left wow fadeInUp" data-wow-delay="0.3s" style="visibility: visible; -webkit-animation-delay: 0.3s; -moz-animation-delay: 0.3s; animation-delay: 0.3s;">
                   <div class="ProductCard ">
                   <div class="productImage"> 
                       <img src="<?php echo base_url();?>assets/images/products/85265.jpg">
                     </div>
                     <div class="producttext">
                       <div class="itemNo">Item #:<span>12332</span></div>
                       <p class="itemName">Saliva Ejectors, 100/Pkg</p>
                       <span class="ItemManufacture">Kuraray Dental</span>
                     </div>
                       <div class="addTocart">
                         <h6>Add to Cart</h6>
                       </div>
                   </div>
                </div>
                  <div class="col-lg-2 padding-5px-left padding-5px-right wow fadeInUp" data-wow-delay="0.4s" style="visibility: visible; -webkit-animation-delay: 0.4s; -moz-animation-delay: 0.4s; animation-delay: 0.4s;">
                    <div class="ProductCard ">
                   <div class="productImage"> 
                       <img src="<?php echo base_url();?>assets/images/products/127277.jpg">
                     </div>
                      <div class="producttext">
                       <div class="itemNo">Item #:<span>12332</span></div>
                       <p class="itemName">Saliva Ejectors, 100/Pkg</p>
                       <span class="ItemManufacture">Kuraray Dental</span>
                     </div>
                       <div class="addTocart">
                         <h6>Add to Cart</h6>
                       </div>
                   </div>
                </div>
                  <div class="col-lg-2 padding-5px-right padding-5px-left  wow fadeInUp" data-wow-delay="0.5s" style="visibility: visible; -webkit-animation-delay: 0.5s; -moz-animation-delay: 0.5s; animation-delay: 0.5s;">
                   <div class="ProductCard ">
                   <div class="productImage"> 
                       <img src="<?php echo base_url();?>assets/images/products/370189.jpg">
                     </div>
                      <div class="producttext">
                       <div class="itemNo">Item #:<span>12332</span></div>
                       <p class="itemName">Saliva Ejectors, 100/Pkg</p>
                       <span class="ItemManufacture">Kuraray Dental</span>
                     </div>
                       <div class="addTocart">
                         <h6>Add to Cart</h6>
                       </div>
                   </div>
                </div>
                  <div class="col-lg-2 padding-5px-left padding-5px-right wow fadeInUp" data-wow-delay="0.6s" style="visibility: visible; -webkit-animation-delay: 0.6s; -moz-animation-delay: 0.6s; animation-delay: 0.6s;">
                   <div class="ProductCard ">
                   <div class="productImage"> 
                       <img src="<?php echo base_url();?>assets/images/products/489454.jpg">
                     </div>
                      <div class="producttext">
                       <div class="itemNo">Item #:<span>12332</span></div>
                       <p class="itemName">Saliva Ejectors, 100/Pkg</p>
                       <span class="ItemManufacture">Kuraray Dental</span>
                     </div>
                       <div class="addTocart">
                         <h6>Add to Cart</h6>
                       </div>
                   </div>
                </div>
                  <div class="col-lg-2 padding-5px-right padding-5px-left wow fadeInUp" data-wow-delay="0.7s" style="visibility: visible; -webkit-animation-delay: 0.7s; -moz-animation-delay: 0.7s; animation-delay: 0.7s;">
                  <div class="ProductCard ">
                   <div class="productImage"> 
                       <img src="<?php echo base_url();?>assets/images/products/EPS_796544.jpg">
                     </div>
                      <div class="producttext">
                       <div class="itemNo">Item #:<span>12332</span></div>
                       <p class="itemName">Saliva Ejectors, 100/Pkg</p>
                       <span class="ItemManufacture">Kuraray Dental</span>
                     </div>
                       <div class="addTocart">
                         <h6>Add to Cart</h6>
                       </div>
                   </div>
                </div>
                 <div class="col-lg-2 padding-5px-left padding-5px-right wow fadeInUp" data-wow-delay="0.8s" style="visibility: visible; -webkit-animation-delay: 0.8s; -moz-animation-delay: 0.8s; animation-delay: 0.8s;">
                    <div class="ProductCard ">
                   <div class="productImage"> 
                       <img src="<?php echo base_url();?>assets/images/products/85265.jpg">
                     </div>
                      <div class="producttext">
                       <div class="itemNo">Item #:<span>12332</span></div>
                       <p class="itemName">Saliva Ejectors, 100/Pkg</p>
                       <span class="ItemManufacture">Kuraray Dental</span>
                     </div>
                       <div class="addTocart">
                         <h6>Add to Cart</h6>
                       </div>
                   </div>
                </div>
             </div>
          </div>
       </section> -->

    <!-- category page end -->
   
    <script type="text/javascript">
        pageSize = 8;
        showPage = function(page) {
            $(".line-content").hide();
            $(".line-content").each(function(n) {
                if (n >= pageSize * (page - 1) && n < pageSize * page)
                $(this).show();
            });        
        }
        showPage(1);
        $("#pagin li a").click(function() {
            $("#pagin li a").removeClass("current");
            $(this).addClass("current");
            showPage(parseInt($(this).text())) 
        });
    </script>