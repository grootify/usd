<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Checkout extends User_Controller {
 
	public function __construct() {
		 parent::__construct();	
         $this->load->model('standard_model');
          if (!isset($this->user['id'])) {
            redirect(base_url());
          }   		 
	}

	public function index() {
        $data['myQuote_list']=$this->getMyQuoteList(); 
        $data['address_list']=$this->getUserAddress();
        $data['inProcess_Quote']=$this->getInProcessMyQuote();
        $data['my_product_list']=$this->getMyProductList();
        $data['my_customProduct']=$this->getMyCustomProductList();
        $data['my_attachment_list']=$this->getMyCustomProductAttachmentList();

		$data['main_content'] = "checkout_view";
		$data['title'] = '';
        $data['description'] = '';
        $data['keywords'] = '';
		
        $this->load->view('master',$data);
	}
    private function getInProcessMyQuote(){
        if(isset($this->user['id'])){
        $data['table']='quotes';
        $data['field'] = 'quotes.id as quote_id';
       
        if(isset($this->user['id'])){
            $user_id=$this->user['id'];
        }
        $data['condition'] = array(
            "quotes.user_id"=>$user_id,
            "quotes.order_status_id"=>1,
            "quotes.is_active" => 1,
            "quotes.in_process" => 1

        );
        $data['order_by'] = array( 'quotes.id' => 'DESC' );
        $this->standard_model->set_query_data($data);
        $result = $this->standard_model->select();
        if($result){
            return $result;
        }else{
            return array();
        }
        }
    }
    private function getMyQuoteList(){
        if(isset($this->user['id'])){
        $data['table']='quote_products';
        $data['field'] = 'quote_products.id,quotes.user_id,quote_products.quantity,product_list.title,product_list.url';
        $data['join']=array(
            "quotes"=>'quote_products.quote_id = quotes.id',
            "product_list"=>'quote_products.product_id = product_list.id'
        );
        if(isset($this->user['id'])){
            $user_id=$this->user['id'];
        }
        $data['condition'] = array(
            "quotes.user_id"=>$user_id,
            "quotes.order_status_id"=>1,
            "quote_products.is_active" => 1 
        );
        $data['order_by'] = array( 'product_list.id' => 'DESC' );
        $this->standard_model->set_query_data($data);
        $result = $this->standard_model->select();
        if(sizeof($result)>0){
            if(is_object($result)) {
            $result = array($result);
            }
            return $result;
        }else{
            return array();
        }
        }
        
        
    }
    private function getMyProductList(){
        $data['table']='quote_products';
        $data['field'] = 'quote_products.id,quote_products.quantity,product_list.title,product_list.description,product_list.url';

        $data['join']=array(
            "quotes"=>'quote_products.quote_id = quotes.id',
            "product_list"=>'quote_products.product_id = product_list.id'
        );
        if(isset($this->user['id'])){
            $user_id=$this->user['id'];
        }
        $data['condition'] = array(
            "quote_products.is_active"=>'1',
            "quotes.user_id"=>$user_id,
            "quotes.order_status_id" => '1' ,
            "quotes.is_active" => '1' ,
            "quotes.in_process" => '1' ,
        );
        $data['order_by'] = array( 'quote_products.id' => 'DESC' );
        $this->standard_model->set_query_data($data);
        $result = $this->standard_model->select();
        if(sizeof($result)>0){
            if(is_object($result)) {
            $result = array($result);
            }
            return $result;
        }else{
            return array();
        }
    }
    private function getMyCustomProductList(){
        $data['table']='custom_quote';
        $data['field'] = 'custom_quote.name,custom_quote.quantity';

        $data['join']=array(
            "quotes"=>'custom_quote.quote_id = quotes.id'
        );
        if(isset($this->user['id'])){
            $user_id=$this->user['id'];
        }
        $data['condition'] = array(
            "custom_quote.status"=>'1',
            "quotes.user_id"=>$user_id,
            "quotes.order_status_id" => '1' ,
            "quotes.is_active" => '1',
            "quotes.in_process" => '1' 
        );
        $data['order_by'] = array( 'custom_quote.id' => 'DESC' );
        $this->standard_model->set_query_data($data);
        $result = $this->standard_model->select();
        if(sizeof($result)>0){
            if(is_object($result)) {
            $result = array($result);
            }
            return $result;
        }else{
            return array();
        }
    }
    private function getMyCustomProductAttachmentList(){
        $data['table']='quote_attachment';
        $data['field'] = 'quote_attachment.path';

        $data['join']=array(
            "quotes"=>'quote_attachment.quote_id = quotes.id'
        );
        if(isset($this->user['id'])){
            $user_id=$this->user['id'];
        }
        $data['condition'] = array(
            "quote_attachment.status"=>'1',
            "quotes.user_id"=>$user_id,
            "quotes.order_status_id" => '1' ,
            "quotes.is_active" => '1',
            "quotes.in_process" => '1' 
        );
        $data['order_by'] = array( 'quote_attachment.id' => 'DESC' );
        $this->standard_model->set_query_data($data);
        $result = $this->standard_model->select();
        if(sizeof($result)>0){
            if(is_object($result)) {
            $result = array($result);
            }
            return $result;
        }else{
            return array();
        }
    }
    private function getUserAddress(){
        if(isset($this->user['id'])){
            $user_id=$this->user['id'];
        if($user_id){
            $data['table']='address';
            $data['field']='id,user_id,company,contact_f_name,contact_l_name,telephone,address_1,address_2,city,state,postal_code,country,is_default';
            $data['condition']=array(
                "user_id"=>$user_id,
                "is_active"=>'1'
            );
            $data['order_by'] = array(
                'id' => 'desc'
            );

            $this->standard_model->set_query_data($data);
            $results= $this->standard_model->select();
            if(sizeof($results)>0){
                if(is_object($results)) {
                    $results = array($results);
                }
                    return $results;
                }else{
                    return array();
                }
        }
        }
    }

	
		public function page_not_found() {

		$data['main_content'] = "404_view";
				
        $data['title'] = '';
        $data['description'] = '';
        $data['keywords'] = '';
		
        $this->load->view('master',$data);
	}
}