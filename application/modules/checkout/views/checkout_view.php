<!-- category page -->
<style>
    .addAddress{
        padding: 72px 29px;
    }
    .mg-5{
        margin-bottom: 5px;
    }
</style>
<?php
$current_url="c";
?>
<section class="">
    <div class="collection-wrapper">
        <div class="container">
            <div class="row checkout-title">
                <div class="col-lg-12">
                    <div class="text-center userDash">
                        <h4>Checkout</h4>
                    </div>
                </div>
               

            </div>
            <hr>
            <div class="row margin-top-40px margin-bottom-40px checkout">
                <div class="col-md-6">
                    <form class="loginForm">
                        <h4>Shipping Address</h4>
                         <div class="row margin-top-40px margin-bottom-40px">

                <?php
                  $deafult_address_id='0';
                    if(isset($address_list)){
                      
                    foreach ($address_list as $address) {
                     if($address->is_default=='1'){
                        $deafult_address_id=$address->id;
                ?>
                <div class="col-md-6 mg-5">
                    <div class="addressesCard active">

                        <h5><?php echo $address->contact_f_name ."&nbsp;". $address->contact_l_name;?></h5>
                        <p><?php echo $address->address_1;?><br> 
                        <?php echo $address->city;?><br><?php echo $address->country;?>, <?php echo $address->postal_code;?></p>
                        <p>Phone number: <?php echo $address->telephone;?></p>
                        <div class="row addresses_eds">
                            <div class="col-md-4">
                                 <p><a href="<?php echo base_url();?>edit_addresses/<?php echo $address->id;?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> &nbsp;Edit</a></p> 
                            </div>
                            <div class="col-md-4">
                                <p><a href="javascript:void(0);" type="button" onclick="deleteAddress(<?php echo $address->id;?>);" style="-webkit-appearance: none;"><i class="fa fa-trash-o" aria-hidden="true"></i> &nbsp;Delete</a></p>
                               
                            </div>
                        </div>
                    </div>    
                </div>
                <?php }else{?>
                    <div class="col-md-6 mg-5">
                    <div class="addressesCard">
                         <h5><?php echo $address->contact_f_name ."&nbsp;". $address->contact_l_name;?></h5>
                        <p><?php echo $address->address_1;?><br> 
                        <?php echo $address->city;?><br><?php echo $address->country;?>, <?php echo $address->postal_code;?></p>
                        <p>Phone number: <?php echo $address->telephone;?></p>
                        <div class="row addresses_eds">
                            <div class="col-md-3">
                                 <p><a href="<?php echo base_url();?>edit_addresses/<?php echo $address->id;?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> &nbsp;Edit</a></p> 
                           </div>
                            <div class="col-md-4">
                                 <p><a href="javascript:void(0);" type="button" onclick="deleteAddress(<?php echo $address->id;?>);" style="-webkit-appearance: none;"><i class="fa fa-trash-o" aria-hidden="true"></i> &nbsp;Delete</a></p>

                            </div>
                            <div class="col-md-5" style="padding: 0px;">
                             <p><a href="javascript:void(0);" type="button" onclick="setDefaultAddress(<?php echo $address->id;?>,<?php echo $address->user_id;?>);" style="-webkit-appearance: none;"><i class="fa fa-map-marker" aria-hidden="true"></i> &nbsp;Set as Default</a></p>
                            </div>
                        </div>
                    </div>    
                </div>
            <?php } } }?>
            <div class="col-md-6 mg-5">
                    <div class="addressesCard">
                        <p class="addAddress">
                           <a href="<?php echo base_url();?>add_addresses/<?php echo $current_url?>" style="color: #021b4e;"><i class="fa fa-plus-square-o" aria-hidden="true"></i>&nbsp; Add Addresses</a>
                        </p>
                    </div>    
                </div>
            </div>
                        
                      
                    </form>
                </div>
                <div class="col-md-6">
                    <?php if(isset($my_product_list) || isset($my_customProduct) || isset($my_attachment_list)){
                        if(!empty($my_product_list) || !empty($my_customProduct) || !empty($my_attachment_list)){
                    ?>
                    <div class="CheckoutOrder">

                        <?php if(isset($my_product_list)){
                                foreach ($my_product_list as $my_product) {
                        ?>
                        <div class="row  myQuotation_body">
                            <div class="col-md-2 text-center">
                                <img width="100%" src="<?php echo base_url();?><?php echo $my_product->url;?>" alt="" class="blur-up lazyloaded">
                            </div>
                            <div class="col-md-6">
                                <h4><?php echo $my_product->description;?></h4>
                            </div>
                            <div class="col-md-4">
                                <h5>Quantity: <?php echo $my_product->quantity;?></h5>
                            </div>
                        </div>
                    <?php } }?>

                   
                        <?php 
                            if(isset($my_customProduct)){
                                if($my_customProduct!=''){
                            foreach ($my_customProduct as $custom_product) {
                        ?>
                      
                        <div class="row  myQuotation_body">
                            <div class="col-md-6">
                                <h4><?php echo $custom_product->name;?></h4>
                            </div>
                            <div class="col-md-4">
                                <h5>Quantity: <?php echo $custom_product->quantity;?></h5>
                            </div>
                        </div>
                    <?php } } }?>
                    <?php if(isset($my_attachment_list)){
                            if(!empty($my_attachment_list)){
                              
                     ?>
                     <div class="row  myQuotation_body">
                        <?php foreach ($my_attachment_list as $attach) {?>
                            <div class="col-md-2 text-center">
                                <a href="<?php echo base_url();?><?php echo $attach->path;?>" target="_blank">
                                    <img width="100%" src="<?php echo base_url();?>assets/images/pdf.png" alt="" class="blur-up lazyloaded" style="width: 40px;height: 40px;">
                                </a>
                                
                            </div>
                        <?php }  ?>
                      
                           
                        </div>
                     <?php } }?>
                       <div class="text-center margin-top-30px">
                            <?php
                             if(isset($inProcess_Quote)){
                                if(!empty($inProcess_Quote)){
                                    $quote_id=$inProcess_Quote->quote_id;
                                }else{
                                    $quote_id='';
                                }
                                if($quote_id!=''){
                            ?>
                            <a href="javascript:void(0);" type="button" class="btn btn-theme" onclick="submitQuotation('<?php echo $this->user['email'];?>',<?php echo $this->user['id'];?>,<?php echo $quote_id;?>,<?php echo $deafult_address_id?>);">Submit quotation request</a>
                        <?php } } ?>
                        </div>
                    </div>
                <?php } } ?>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- category page end -->