<!-- category page -->
<section class="">
    <div class="collection-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-center userDash">
                        <h4>Personal Information</h4>
                        <h5><a href="<?php echo base_url();?>dashboard">Your Account</a> >  <span class="theme-color">Change Password</span> </h5>
                    </div>
                </div>

            </div>
            <hr>
            <div class="row margin-top-40px margin-bottom-40px">
                <div class="col-md-6 ml-auto mr-auto">
                     <p><em id="reset_response"></em></p>
                        <form id="resetForm" class="loginForm" method="post" action="javascript:void(0);" method="post" novalidate="novalidate">
                           
                            <div class="row">
                                   <input type="hidden" name="reset_email" id="reset_email" value="<?php echo $this->user['email'];?>">
                                    <div class="col-md-6">
                                        <label>
                                            <p class="label-txt">New password *</p>
                                            <input type="password" class="input" id="reset_password" name="reset_password">
                                            <div class="line-box">
                                            <div class="line"></div>
                                            </div>
                                        </label>
                                    </div>
                                     <div class="col-md-6">
                                        <label>
                                            <p class="label-txt">Confirm password *</p>
                                            <input type="password" class="input" id="reset_confirm_password" name="reset_confirm_password">
                                            <div class="line-box">
                                            <div class="line"></div>
                                            </div>
                                        </label>
                                    </div>
                            </div>
                        
                            <div class="text-center">
                                <button class="btn btn-theme  margin-bottom-20px btnSubmit" onclick="resetPassword();">Submit</button>
                            </div>
                        </form>

              
                </div>
            </div>
       
      
        </div>
    </div>
</section>
<!-- category page end -->