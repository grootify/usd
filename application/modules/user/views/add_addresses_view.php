<?php 
        $id='';
        $user_id='';
        $company='';
        $contact_f_name='';
        $contact_l_name='';
        $email='';
        $telephone='';
        $fax='';
        $attn='';
        $address_1='';
        $address_2='';
        $city='';
        $state='';
        $postal_code='';
        $country='';
    if(isset($addressData)){
        $id = $addressData->id;
        $user_id=$addressData->user_id;
        $company=$addressData->company;
        $contact_f_name=$addressData->contact_f_name;
        $contact_l_name=$addressData->contact_l_name;
        $email=$addressData->email;
        $telephone=$addressData->telephone;
        $fax=$addressData->fax;
        $attn=$addressData->attn;
        $address_1=$addressData->address_1;
        $address_2=$addressData->address_2;
        $city=$addressData->city;
        $state=$addressData->state;
        $postal_code=$addressData->postal_code;
        $country=$addressData->country;
    }

?>
<!-- category page -->
<section class="">
    <div class="collection-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-center userDash">
                        <h4><?php if(isset($addressData)){ echo "Edit address"; }else{ echo "Add a new address";}?></h4>
                        <h5><a href="<?php echo base_url();?>dashboard">Your Account</a> > <a href="<?php echo base_url();?>addresses">Your Addresses</a> > <span class="theme-color"><?php if(isset($addressData)){ echo "Edit address"; }else{ echo "Add a new address";}?></span> </h5>
                    </div>
                </div>

            </div>
            <hr>
            <div class="row margin-top-40px margin-bottom-40px">
                <div class="col-md-6 ml-auto mr-auto">
                        <p class="center"><em id="addAddress_response"></em></p>
                        <form id="user_addressForm" class="loginForm" action="javascript:void(0);" method="post" novalidate="novalidate">
                            
                        <div class="alert alert-success" id="address_added_successful" style="display: none;"> Your address is created.</div>
                        <div class="row">
                            <div class="col-md-12">
                                <input type="hidden" name="url" id="url" value="<?php if(isset($url)){echo $url;}?>">
                                <input type="hidden" name="address_id" id="address_id" value="<?php if(isset($addressData)){ echo $addressData->id; }?>">
                                <input type="hidden" name="user_id" id="user_id" value="<?php echo $this->user['id'];?>">
                                <label>
                                    <p class="label-txt">Company *</p>
                                    <input type="text" class="input" name="company" id="company" <?php if(isset($addressData->company)){ echo "value = ". $addressData->company."";}?> >
                                            <div class="line-box">
                                            <div class="line"></div>
                                            </div>
                                    </label>
                                </div>
                            </div>
                         <div class="row ">
                            <div class="col-md-6 ">
                                <label>
                                    <p class="label-txt ">Contact first name *</p>
                                    <input type="text " class="input" name="contact_f_name" id="contact_f_name" <?php if(isset($addressData->contact_f_name)){ echo "value = ". $addressData->contact_f_name."";}?> >
                                    <div class="line-box">
                                        <div class="line"></div>
                                    </div>
                                </label>
                            </div>
                            <div class="col-md-6">
                                <label>
                                    <p class="label-txt"> Contact last name *</p>
                                    <input type="text" class="input" name="contact_l_name" id="contact_l_name" <?php if(isset($addressData->contact_l_name)){ echo "value = ". $addressData->contact_l_name."";}?>>
                                    <div class="line-box">
                                        <div class="line"></div>
                                    </div>
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label>
                                    <p class="label-txt">Email *</p>
                                    <input type="text" class="input" name="email" id="email" <?php if(isset($addressData->email)){ echo "value = ". $addressData->email."";}?>>
                                            <div class="line-box ">
                                            <div class="line "></div>
                                            </div>
                                        </label>
                                    </div>
                                    <div class="col-md-6 ">
                                        <label>
                                            <p class="label-txt "> Telephone *</p>
                                            <input type="text " class="input" name="telephone" id="telephone" <?php if(isset($addressData->telephone)){ echo "value = ". $addressData->telephone."";}?>>
                                            <div class="line-box ">
                                            <div class="line "></div>
                                            </div>
                                        </label>
                                    </div>
                            </div>
                                    <div class="row ">
                                    <div class="col-md-6 ">
                                        <label>
                                            <p class="label-txt ">Fax </p>
                                            <input type="text " class="input" name="fax" id="fax" <?php if(isset($addressData->fax)){ echo "value = ". $addressData->fax."";}?>>
                                    <div class="line-box">
                                        <div class="line"></div>
                                    </div>
                                </label>
                            </div>
                            <div class="col-md-6">
                                <label>
                                    <p class="label-txt"> Deliver to the ATTN of </p>
                                    <input type="text" class="input" name="attn" id="attn" <?php if(isset($addressData->attn)){ echo "value = ". $addressData->attn."";}?>>
                                    <div class="line-box">
                                        <div class="line"></div>
                                    </div>
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label>
                                    <p class="label-txt"> Address Line 1 * </p>
                                    <input type="text" class="input" name="address_1" id="address_1" <?php if(isset($addressData->address_1)){ echo "value = ". $addressData->address_1."";}?>>
                                    <div class="line-box">
                                        <div class="line"></div>
                                    </div>
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label>
                                    <p class="label-txt"> Address Line 2 </p>
                                    <input type="text" class="input" name="address_2" id="address_2" <?php if(isset($addressData->address_2)){ echo "value = ". $addressData->address_2."";}?>>
                                    <div class="line-box">
                                        <div class="line"></div>
                                    </div>
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label>
                                    <p class="label-txt">City *</p>
                                    <input type="text" class="input" name="city" id="city" <?php if(isset($addressData->city)){ echo "value = ". $addressData->city."";}?>>
                                            <div class="line-box ">
                                            <div class="line "></div>
                                            </div>
                                        </label>
                                    </div>
                                    <div class="col-md-6 ">
                                        <label>
                                            <p class="label-txt "> Region / State *</p>
                                            <input type="text " class="input" name="state" id="state" <?php if(isset($addressData->state)){ echo "value = ". $addressData->state."";}?>>
                                            <div class="line-box ">
                                            <div class="line "></div>
                                            </div>
                                        </label>
                                    </div>
                            </div>
                               <div class="row ">
                                    <div class="col-md-6 ">
                                        <label>
                                            <p class="label-txt ">Postal Code  *</p>
                                            <input type="text " class="input" name="postal_code" id="postal_code" <?php if(isset($addressData->postal_code)){ echo "value = ". $addressData->postal_code."";}?>>
                                    <div class="line-box">
                                        <div class="line"></div>
                                    </div>
                                </label>
                            </div>
                            <div class="col-md-6">
                                <label>
                                    <p class="label-txt"> Country *</p>
                                    <input type="text" class="input" name="country" id="country" <?php if(isset($addressData->country)){ echo "value = ". $addressData->country."";}?>>
                                    <div class="line-box">
                                        <div class="line"></div>
                                    </div>
                                </label>
                            </div>
                        </div>
                        <div class="text-center">
                                  <button class="btn btn-theme  margin-bottom-20px btnSubmit" onclick="saveUserAddress();">Submit</button>

                        </div>
                    </form>
                </div>
            </div>
       
              </div>
    </div>
</section>
<!-- category page end -->

