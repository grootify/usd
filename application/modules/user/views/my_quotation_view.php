<!--category page -->
<section class="">
    <div class="collection-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-center userDash">
                        <h4>Your Quotation</h4>
                        <h5><a href="<?php echo base_url();?>dashboard">Your Account</a> >  <span class="theme-color">Your Quotation</span> </h5>
                    </div>
                </div>

            </div>
            <hr>
            <?php if(empty($my_quotation_list)){?>
            <div class="row margin-top-40px margin-bottom-40px">
              <div class="col-md-9 ml-auto mr-auto">
                  <h5 style="text-align: center;">There are no quotation.</h5>
              </div>
            </div>
            <?php } ?>
            <div class="row margin-top-40px margin-bottom-40px">
                <div class="col-md-9 ml-auto mr-auto">
                  <?php if(isset($my_quotation_list)){ 
                        foreach ($my_quotation_list as $quotation) {
                    ?>
                    <div class="myQuotation">
                        <div class="row myQuotation_header">
                            <div class="col-md-6">
                                <h4>Quotation id: <?php echo $quotation->quote_no;?></h4>
                            </div>
                            <div class="col-md-6">
                                <div class="float-right">
                                    <a href="<?php echo base_url();?>my_quotation__details/<?php echo $quotation->id;?>" class="btn btn-theme"> View Details</a>
                                </div>
                            </div>
                        </div>
                        <hr>
                          <div class="row">
                             <div class="col-md-5 ml-auto">
                                <?php foreach($my_address as $myAddress){?>
                                <div class="addressesCard">
                                    <h4>Shipping Address</h4>
                                    <h5><?php echo $myAddress->contact_f_name;?> &nbsp;<?php echo $myAddress->contact_l_name;?></h5>
                                    <p><?php echo $myAddress->address_1;?><br> 
                                     <?php echo $myAddress->city;?>,<?php echo $myAddress->state;?><br> <?php echo $myAddress->country;?> <?php echo $myAddress->postal_code;?></p>
                                    
                                </div>
                              <?php }?>
                             </div>
                             <div class="col-md-5 mr-auto">
                                   <div class="addressesCard">
                                    <h4>Other information</h4>
                                      <p>Order on <?php echo $quotation->quote_created;?></p>
                                  <!--   <p>Order on Mon, Feb 11th '19</p> -->
                                    <p>Email: <?php echo $this->user['email'];?></p>
                                    <p>Phone number: <?php echo $this->user['mobile'];?></p>
                                    
                                </div>
                             </div>
                            
                            
                         </div>

                    </div>
                  <?php } } ?>
                  
                </div>
            </div>
       
      
        </div>
    </div>
</section>
