<!-- category page -->

<section class="">
    <div class="collection-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-center userDash">
                        <h4>Your Quotation</h4>
                        <h5><a href="<?php echo base_url();?>dashboard">Your Account</a> > <a href="<?php echo base_url();?>my_quotation">Your Quotation</a> >  <span class="theme-color">Quotation Details</span> </h5>
                    </div>
                </div>

            </div>
            <hr>
            <div class="row margin-top-40px margin-bottom-40px">
                <div class="col-md-9 ml-auto mr-auto">
                    <div class="myQuotation">
                        <?php if(isset($my_quotation_list)){
                                foreach ($my_quotation_list as $Quotation) {
                        ?>
                        <div class="row myQuotation_header">
                            <div class="col-md-6">
                                <h4>Quotation  id: <?php echo $Quotation->quote_no;?></h4>
                            </div>
                            <!-- <div class="col-md-6">
                                <div class="float-right">
                                    <a href="" class="btn btn-theme"><i class="fa fa-download" aria-hidden="true"></i> Download Quotation </a>
                                </div>
                            </div> -->
                        </div>
                        <hr>
                         <div class="row">
                              <?php foreach($my_address as $myAddress){  ?>
                             <div class="col-md-5 ml-auto">
                               <div class="addressesCard">
                                    <h4>Shipping Address</h4>
                                    <h5><?php echo $myAddress->contact_f_name;?> <?php echo $myAddress->contact_l_name;?></h5>
                                    <p><?php echo $myAddress->address_1;?><br> 
                                     <?php echo $myAddress->city;?>,<?php echo $myAddress->state;?>,<br> <?php echo $myAddress->postal_code;?>  <?php echo $myAddress->country;?></p>
                                    
                                </div>
                             </div>
                              <?php } ?>
                             <div class="col-md-5 mr-auto">
                                   <div class="addressesCard">
                                    <h4>Other information</h4>
                                    <p>Order on <?php echo $Quotation->quote_created?></p>
                                    <p>Email: <?php echo $this->user['email']?></p>
                                    <p>Phone number: <?php echo $this->user['mobile']?></p>
                                    
                                </div>
                             </div>
                         </div>
                     <?php } }?>
                         <hr>
                         <?php if(isset($my_quotationDetail_list)){
                            foreach ($my_quotationDetail_list as $my_quotation) {
                             
                         ?>
                        <div class="row margin-top-30px myQuotation_body quotationmobile">
                            <div class="col-md-2 text-center">
                               <img width="100%" src="<?php echo base_url();?><?php echo $my_quotation->url;?>" alt="" class="blur-up lazyloaded"> 
                            </div>
                            <div class="col-md-6">
                                <h4><?php echo $my_quotation->description;?></h4>
                            </div>
                            <div class="col-md-4">
                                <h5>Quantity: <?php echo $my_quotation->quantity;?></h5>
                            </div>
                        </div>
                    <?php } }?>
                 </div>

                   
                </div>
            </div>
       
      
        </div>
    </div>
</section>
<!-- category page end -->