<!-- category page -->
<section class="">
    <div class="collection-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-center userDash">
                        <h4>Personal Information</h4>
                        <h5><a href="<?php echo base_url();?>dashboard">Your Account</a> >  <span class="theme-color">Personal Information</span> </h5>
                    </div>
                </div>

            </div>
            <hr>
            <div class="row margin-top-40px margin-bottom-40px">
                <div class="col-md-6 ml-auto mr-auto">
                    <p><em id="Personal_info_response"></em></p>
                        <form id="Personal_info_form" class="loginForm" action="javascript:void(0);" method="post" novalidate="novalidate">
                            <div class="row">
                                    <div class="col-md-6">
                                        <input type="hidden" name="user_email" id="setting_user_email" value="<?php echo $this->user['email']?>">
                                        <label>
                                            <p class="label-txt">Full name *</p>
                                            <input type="text" class="input" id="user_name" name="user_name" value="<?php echo $this->user['name'];?>">
                                            <div class="line-box">
                                            <div class="line"></div>
                                            </div>
                                        </label>
                                    </div>
                                    <div class="col-md-6">
                                        <label>
                                            <p class="label-txt"> Mobile *</p>
                                            <input type="text" class="input" id="user_mobile" id="user_mobile" value="<?php echo $this->user['mobile'];?>">
                                            <div class="line-box">
                                            <div class="line"></div>
                                            </div>
                                        </label>
                                    </div>
                            </div>
                        
                            <div class="text-center">
                                <button class="btn btn-theme  margin-bottom-20px btn-submit" onclick="changeUserInfo();">Submit</button>
                            </div>
                        </form>

                        <hr>
                             <div class="text-center margin-top-20px">
            <a href="<?php echo base_url();?>change_password" class="btn btn-theme  margin-bottom-20px" id="hideshow"><i class="fa fa-key" aria-hidden="true"></i> Change Password</a>
        </div>
                </div>
            </div>
       
      
        </div>
    </div>
</section>
<!-- category page end -->