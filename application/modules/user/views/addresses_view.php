<!-- category page -->
<section class="">
    <div class="collection-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-center userDash">
                        <h4>Your Addresses</h4>
                        <h5><a href="<?php echo base_url();?>dashboard">Your Account</a> > <span class="theme-color">Your Addresses</span> </h5>
                    </div>
                </div>

            </div>
            <hr>
              <div id="address_status"></div>
            <div class="row margin-top-40px margin-bottom-40px">

                <?php
                    foreach ($address_list as $address) {
                     if($address->is_default=='1'){
                ?>
                <div class="col-md-4">
                    <div class="addressesCard active">
                        <h5><?php echo $address->contact_f_name ."&nbsp;". $address->contact_l_name;?></h5>
                        <p><?php echo $address->address_1;?><br> 
                        <?php echo $address->city;?><br><?php echo $address->country;?>, <?php echo $address->postal_code;?></p>
                        <p>Phone number: <?php echo $address->telephone;?></p>
                        <div class="row addresses_eds">
                            <div class="col-md-4">
                                 <p><a href="<?php echo base_url();?>edit_addresses/<?php echo $address->id;?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> &nbsp;Edit</a></p> 
                            </div>
                            <div class="col-md-4">
                                <p><button style="background: none;border: none;" onclick="deleteAddress(<?php echo $address->id;?>);"><i class="fa fa-trash-o" aria-hidden="true"></i> &nbsp;Delete</button></p>   
                            </div>
                        </div>
                    </div>    
                </div>
                <?php }else{?>
                    <div class="col-md-4">
                    <div class="addressesCard">
                         <h5><?php echo $address->contact_f_name ."&nbsp;". $address->contact_l_name;?></h5>
                        <p><?php echo $address->address_1;?><br> 
                        <?php echo $address->city;?><br><?php echo $address->country;?>, <?php echo $address->postal_code;?></p>
                        <p>Phone number: <?php echo $address->telephone;?></p>
                        <div class="row addresses_eds">
                            <div class="col-md-3">
                                 <p><a href="<?php echo base_url();?>edit_addresses/<?php echo $address->id;?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> &nbsp;Edit</a></p> 
                           </div>
                            <div class="col-md-4">
                               <p><button style="background: none;border: none;" onclick="deleteAddress(<?php echo $address->id;?>);"><i class="fa fa-trash-o" aria-hidden="true"></i> &nbsp;Delete</button></p>   
                            </div>
                            <div class="col-md-5" style="padding: 0px;">
                             <p><button style="background: none;border: none;" onclick="setDefaultAddress(<?php echo $address->id;?>,<?php echo $address->user_id;?>);"><i class="fa fa-map-marker" aria-hidden="true"></i> &nbsp;Set as Default</button></p>

                            </div>
                        </div>
                    </div>    
                </div>
            <?php } } ?>
            </div>
            <hr>
            <div class="text-center margin-top-20px">
            <a href="<?php echo base_url();?>add_addresses" class="btn btn-theme  margin-bottom-20px" ><i class="fa fa-plus" aria-hidden="true"></i> Add new addresse</a>
        </div>
        </div>
    </div>
</section>
<!-- category page end -->