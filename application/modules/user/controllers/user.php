<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User extends User_Controller {
 
	public function __construct() {
		 parent::__construct();	
         $this->load->model('standard_model');	
        if (!isset($this->user['id'])) {
            redirect(base_url());
        }	 
	}


    public function dashboard() {
        $data['myQuote_list']=$this->getMyQuoteList(); 
        $data['main_content'] = "dashboard_view";
				
        $data['title'] = '';
        $data['description'] = '';
        $data['keywords'] = '';
		
        $this->load->view('master',$data);
	}
    private function getMyQuoteList(){
        if(isset($this->user['id'])){
        $data['table']='quote_products';
        $data['field'] = 'quote_products.id,quotes.user_id,quote_products.quantity,product_list.title,product_list.url';
        $data['join']=array(
            "quotes"=>'quote_products.quote_id = quotes.id',
            "product_list"=>'quote_products.product_id = product_list.id'
        );
        if(isset($this->user['id'])){
            $user_id=$this->user['id'];
        }
        $data['condition'] = array(
            "quotes.user_id"=>$user_id,
            "quotes.order_status_id"=>1,
            "quote_products.is_active" => 1 
        );
        $data['order_by'] = array( 'product_list.id' => 'DESC' );
        $this->standard_model->set_query_data($data);
        $result = $this->standard_model->select();
        if(sizeof($result)>0){
            if(is_object($result)) {
            $result = array($result);
            }
            return $result;
        }else{
            return array();
        }
        }
        
        
    }

	public function addresses() {
                $data['myQuote_list']=$this->getMyQuoteList(); 
        $data['address_list']=$this->getUserAddress();
        $data['main_content'] = "addresses_view";
				
        $data['title'] = '';
        $data['description'] = '';
        $data['keywords'] = '';
		
        $this->load->view('master',$data);
	}
    public function my_quotation() {
        $data['myQuote_list']=$this->getMyQuoteList(); 
        $data['my_quotation_list']=$this->getQuotationOfUser($this->user['id']);
        $data['my_address']=$this->getMyAddress($this->user['id']);

        $data['main_content'] = "my_quotation_view";
                
        $data['title'] = '';
        $data['description'] = '';
        $data['keywords'] = '';
        
        $this->load->view('master',$data);
    }   
    private function getMyAddress($user_id){
        $data['table']='address';
        $data['field'] = 'address.contact_f_name,address.contact_l_name,address.address_1,address.city,address.state,address.postal_code,address.country';
       
        if(isset($this->user['id'])){
            $user_id=$this->user['id'];
        }
        $data['condition'] = array(
            "address.user_id"=>$user_id,
            "address.is_default"=>'1',
            "address.is_active" => '1' 
        );
        $data['order_by'] = array( 'address.id' => 'DESC' );
        $this->standard_model->set_query_data($data);
        $result = $this->standard_model->select();
        if(sizeof($result)>0){
            if(is_object($result)) {
            $result = array($result);
            }
            return $result;
        }else{
            return array();
        }
    }
    private function getQuotationOfUser($user_id){
        $data['table']='quotes';
        $data['field'] = 'quotes.id,quotes.quote_no,quotes.quote_created';
       
        if(isset($this->user['id'])){
            $user_id=$this->user['id'];
        }
        $data['condition'] = array(
            "quotes.user_id"=>$user_id,
            "quotes.order_status_id"=>'2',
            "quotes.is_active" => '1',
            "quotes.in_process" => '0',
        );
        $data['order_by'] = array( 'quotes.id' => 'DESC' );
        $this->standard_model->set_query_data($data);
        $result = $this->standard_model->select();
        if(sizeof($result)>0){
            if(is_object($result)) {
            $result = array($result);
            }
            return $result;
        }else{
            return array();
        }
    }

    public function my_quotation__details() {
        $data['myQuote_list']=$this->getMyQuoteList(); 
        $segment = $this->uri->segment_array(); 
        if(isset($segment['2'])){
            $quote_id=$segment['2'];
        }
        $data['my_quotation_list']=$this->getMyQuotationById($quote_id);
        $data['my_quotationDetail_list']=$this->getMyQuotationDetailById($quote_id);
        $data['my_address']=$this->getMyAddress($this->user['id']);
        $data['main_content'] = "my_quotation__details_view";
                
        $data['title'] = '';
        $data['description'] = '';
        $data['keywords'] = '';
        
        $this->load->view('master',$data);
    }
     private function getMyQuotationById($quote_id){
        $data['table']='quotes';
        $data['field'] = 'quotes.quote_no,quotes.address_id,quotes.quote_created';

        
        if(isset($this->user['id'])){
            $user_id=$this->user['id'];
        }
        $data['condition'] = array(
            "quotes.id"=>$quote_id,
            "quotes.user_id"=>$user_id,
            "quotes.is_active" => '1' 
        );
        $data['order_by'] = array( 'quotes.id' => 'DESC' );
        $this->standard_model->set_query_data($data);
        $result = $this->standard_model->select();
        if(sizeof($result)>0){
            if(is_object($result)) {
            $result = array($result);
            }
            return $result;
        }else{
            return array();
        }
    }

    private function getMyQuotationDetailById($quote_id){
        $data['table']='quote_products';
        $data['field'] = 'quote_products.id,quote_products.quantity,quotes.quote_no,product_list.title,product_list.description,product_list.url';

        $data['join']=array(
            "quotes"=>'quote_products.quote_id = quotes.id',
            "product_list"=>'quote_products.product_id = product_list.id'
        );
        if(isset($this->user['id'])){
            $user_id=$this->user['id'];
        }
        $data['condition'] = array(
            "quotes.id"=>$quote_id,
            "quotes.user_id"=>$user_id,
            "quotes.is_active" => '1' 
        );
        $data['order_by'] = array( 'quote_products.id' => 'DESC' );
        $this->standard_model->set_query_data($data);
        $result = $this->standard_model->select();
        if(sizeof($result)>0){
            if(is_object($result)) {
            $result = array($result);
            }
            return $result;
        }else{
            return array();
        }
    }

	public function add_addresses() {
       $data['myQuote_list']=$this->getMyQuoteList(); 
       $segment = $this->uri->segment_array(); 
       if(isset($segment['2'])){
         $data['url']=$segment['2'];
       }
      
        /*if($data['url']=='c'){
           exit("lkdjhgf");
        }*/
        
        $data['main_content'] = "add_addresses_view";
				
        $data['title'] = '';
        $data['description'] = '';
        $data['keywords'] = '';
		
        $this->load->view('master',$data);
	}

   public function change_password()
   {
        $data['myQuote_list']=$this->getMyQuoteList(); 
        $data['main_content'] = "change_password_view";
                
        $data['title'] = '';
        $data['description'] = '';
        $data['keywords'] = '';
        
        $this->load->view('master',$data);
    }
   
    public function edit_addresses($id){
       $data['myQuote_list']=$this->getMyQuoteList();
        if($id){
           $data['addressData']=$this->getAddressById($id);
        }
        $data['main_content'] = "add_addresses_view";
                
        $data['title'] = '';
        $data['description'] = '';
        $data['keywords'] = '';
        
        $this->load->view('master',$data);
    }

    private function getAddressById($id='')
    {
            $data['table']='address';
            $data['field']='id,user_id,company,contact_f_name,contact_l_name,email,telephone,fax,attn,address_1,address_2,city,state,postal_code,country';
            $data['condition']=array(
                "id"=>$id       
             );
           
            $this->standard_model->set_query_data($data);
            $results= $this->standard_model->select();
           
          if($results){
                return $results;
          }
    }
    private function getUserAddress(){
        $user_id=$this->user['id'];
        if($user_id){
            $data['table']='address';
            $data['field']='id,user_id,company,contact_f_name,contact_l_name,telephone,address_1,address_2,city,state,postal_code,country,is_default';
            $data['condition']=array(
                "user_id"=>$user_id,
                "is_active"=>'1'
            );
            $data['order_by'] = array(
                'id' => 'desc'
            );

            $this->standard_model->set_query_data($data);
            $results= $this->standard_model->select();
            if(sizeof($results)>0){
                if(is_object($results)) {
                    $results = array($results);
                }
                    return $results;
                }else{
                    return array();
                }
        }
        

    }
  

	public function settings() {
        $data['myQuote_list']=$this->getMyQuoteList();
		$data['main_content'] = "settings_view";
				
        $data['title'] = '';
        $data['description'] = '';
        $data['keywords'] = '';
		
        $this->load->view('master',$data);
	}


	

	
		public function page_not_found() {

		$data['main_content'] = "404_view";
				
        $data['title'] = '';
        $data['description'] = '';
        $data['keywords'] = '';
		
        $this->load->view('master',$data);
	}
}