<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller
{
	public function __construct(){        
        parent::__construct();
        // $this->load->library('memcached_library');
        $this->load->model('standard_model');
        $this->load->helper(array('utilities','query'));
    }

    public function index(){

        $data['main_content'] = "dashboard_view";
        $data['title'] = "dashboard";
        $data['section'] = "dashboard";
        $data['type'] = '';
                
        $this->load->view('master',$data);
    }

    public function brands($type="list", $brand_id=0){
        if($type=='list'){
            $data['brands'] = $this->get_brands_lists();
            $data['main_content'] = "brands_view";            

        } else if( $type=='create' || $type == 'edit') {
            if($brand_id!=0) {
                $data['brand_id'] = $brand_id;
            }
            $data['main_content'] = "brand_edit_view";            
        } else {
            redirect(base_url('admin'));
        }
        $data['title'] = "Brands";
        $data['section'] = "Brands";
        $data['type'] = $type;
                
        $this->load->view('master',$data);
    }

    public function Catalogs($type="list", $catalog_id=0){
        if($type=='list'){
            $data['catalogs'] = $this->get_catalogs_lists();
            $data['main_content'] = "catalogs_view";            

        } else if( $type=='create' || $type == 'edit') {
            if($catalog_id!=0) {
                $data['catalog_id'] = $catalog_id;
            }
            
            $data['main_content'] = "catalog_edit_view";            
        } else {
            redirect(base_url('admin'));
        }
        $data['title'] = "Catalogs";
        $data['section'] = "Catalogs";
        $data['type'] = $type;
                
        $this->load->view('master',$data);
    }

    public function covers($type="list", $cover_id=0)
    {
        if($type=='list'){
            $data['covers'] = $this->get_covers_lists();
            $data['main_content'] = "covers_view";         
        } else if( $type=='create' || $type == 'edit') {
            if($cover_id!=0) {
                $data['cover_id'] = $cover_id;                
            }
            $data['main_content'] = "cover_edit_view";            
        } else {
            redirect(base_url('admin'));
        }
        $data['title'] = "covers";
        $data['section'] = "covers";
        $data['type'] = 'list';
                
        $this->load->view('master',$data);
    }
    public function offers($type="list", $offer_id=0)
    {
        if($type=='list'){
            $data['offers'] = $this->get_offers_lists();
            $data['main_content'] = "offers_view";         
        } else if( $type=='create' || $type == 'edit') {
            if($offer_id!=0) {
                $data['offer_id'] = $offer_id;                
            }
            $data['main_content'] = "offer_edit_view";            
        } else {
            redirect(base_url('admin'));
        }
        $data['title'] = "offers";
        $data['section'] = "offers";
        $data['type'] = 'list';
                
        $this->load->view('master',$data);
    }
    public function users($type="list", $user_id=0)
    {
        if($type=='list'){
            $data['users'] = $this->get_users_lists();
            $data['inactive_users'] = $this->get_inactive_users_lists();
            $data['total_users'] = $this->get_total_users_lists();
            $data['active_user'] = count($data['users']);
            $data['inactive_user'] = count($data['inactive_users']);
            $data['total_user'] = count($data['total_users']);
            $data['main_content'] = "users_view";     
        } else if( $type=='create' || $type == 'edit') {
          
            if($user_id!=0) {
                 $data['user_id'] = $user_id;                
            }
                $data['main_content'] = "user_edit_view";            
        } else {
                 redirect(base_url('admin'));
            }
      
            $data['title'] = "users";
            $data['section'] = "users";
            $data['type'] = 'list';
            $this->load->view('master',$data);
    }

    public function quotations($type="list", $quote_id=0)
    {
       if($type=='list'){
            $data['quotations'] = $this->get_quotation_lists();
            $data['quotations_incompleted'] = $this->get_quotation_incompleted_lists();
            $data['total_quotations'] = $this->get_total_quotations_lists();
            $data['quotations_completed'] = count($data['quotations']);
            $data['quotation_incompleted'] = count($data['quotations_incompleted']);
            $data['total_quotation'] = count($data['total_quotations']);
            $data['main_content'] = "quotations_view";     
         }// else if( $type=='create' || $type == 'edit') {
          
        //     if($quote_id!=0) {
        //          $data['quote_id'] = $quote_id;                
        //     }
        //         $data['main_content'] = "quotation_edit_view";            
        // }
         else {
                 redirect(base_url('admin'));
            }
      
            $data['title'] = "Orders";
            $data['section'] = "quotations";
            $data['type'] = 'list';
            $this->load->view('master',$data);
    }
    public function quotation_detail()
    {
        $quote_id=$this->uri->segment('3');
        $data['quotation_detail'] = $this->getQuotationDetailByQuoteID($quote_id);
        $data['address_detail'] = $this->getAddress($data['quotation_detail']->address_id);
        $data['quote_products'] = $this->getQuoteProducts($data['quotation_detail']->id);
        $data['custom_products'] = $this->getCustomProducts($data['quotation_detail']->id);
        $data['attachment_list'] = $this->getAttachment($data['quotation_detail']->id);
        $data['attachment_data'] = $this->getAttachmentProduct($data['quotation_detail']->id);
        $data['main_content'] = "quotation_edit_view";
        $data['title'] = "quotations";
        $data['section'] = "quotations";
        $data['type'] = 'list';
        $this->load->view('master',$data);

    }
    private function getAttachment($quote_id)
    {
        $data['table'] = 'quote_attachment';
        $data['field'] = 'id,quote_id,path';

        $data['condition'] = array(    
              "quote_id" => $quote_id,
              "status" =>1
        );

        $data['order_by'] = array(
            'id' => 'asc'
        );

        $this->standard_model->set_query_data($data);
        $results = $this->standard_model->select();

        if (sizeof($results) > 0) {
            if(is_object($results)) {
                $results = array($results);
            }
            return $results;            
        } else {
            return array();
        }
    }
    private function getCustomProducts($quote_id)
    {
        $data['table'] = 'custom_quote';
        $data['field'] = 'id,quote_id,name,quantity,total';

        $data['condition'] = array(    
              "quote_id" => $quote_id,
              "status" => 1
        );

        $data['order_by'] = array(
            'id' => 'asc'
        );

        $this->standard_model->set_query_data($data);
        $results = $this->standard_model->select();

        if (sizeof($results) > 0) {
            if(is_object($results)) {
                $results = array($results);
            }
            return $results;            
        } else {
            return array();
        }
    }
    private function getAttachmentProduct($quote_id)
    {
        $data['table'] = 'attachment_data';
        $data['field'] = 'id,quote_id,item_name,quantity,total';

        $data['condition'] = array(    
              "quote_id" => $quote_id
        );

        $data['order_by'] = array(
            'id' => 'asc'
        );

        $this->standard_model->set_query_data($data);
        $results = $this->standard_model->select();

        if (sizeof($results) > 0) {
            if(is_object($results)) {
                $results = array($results);
            }
            return $results;            
        } else {
            return array();
        }
    }
    private function getQuoteProducts($quote_id)
    {
        $data['table'] = 'quote_products';
        $data['field'] = 'quote_products.id,quote_products.product_id,quote_products.quantity,quote_products.total,product_list.title,product_list.sku_number,product_list.part_no';

        $data['join'] = array(
           "product_list" =>'product_list.id=quote_products.product_id'
        );
        $data['condition'] = array(    
              "quote_products.quote_id" => $quote_id
        );

        $data['order_by'] = array(
            'id' => 'asc'
        );

        $this->standard_model->set_query_data($data);
        $results = $this->standard_model->select();

        if (sizeof($results) > 0) {
            if(is_object($results)) {
                $results = array($results);
            }
            return $results;            
        } else {
            return array();
        }
    }
    private function getAddress($address_id)
    {
        $data['table'] = 'address';
        $data['field'] = 'id,user_id,company,contact_f_name,contact_l_name,email,telephone,address_1,city,state,postal_code';

        $data['condition'] = array(    
              "id" => $address_id
        );

        $data['order_by'] = array(
            'id' => 'desc'
        );

        $this->standard_model->set_query_data($data);
        $results = $this->standard_model->select();

        if ($results) {
          return $results;            
        } else {
            return array();
        }
    }
    private function getQuotationDetailByQuoteID($quote_id)
    {
        $data['table'] = 'quotes';
        $data['field'] = 'id,quote_no,user_id,invoice_no,address_id,total,quote_modified';

        $data['condition'] = array(    
              "id" => $quote_id
        );

        $data['order_by'] = array(
            'id' => 'asc'
        );

        $this->standard_model->set_query_data($data);
        $results = $this->standard_model->select();

        if ($results) {
          return $results;            
        } else {
            return array();
        }
    }
    private function get_quotation_lists()
    {
        $data['table'] = 'quotes';
        $data['field'] = 'quotes.id,quotes.quote_no,quotes.user_id,quotes.invoice_no,quotes.address_id,quotes.total,quotes.quote_modified,users.email,users.full_name,quotes.site';
        $data['join'] = array(
            "users" => 'users.id=quotes.user_id'
        );
        $data['condition'] = array(    
              "in_process" => '0',
              'is_active' => 1

        );

        $data['order_by'] = array(
            'id' => 'desc'
        );

        $this->standard_model->set_query_data($data);
        $results = $this->standard_model->select();

        if (sizeof($results) > 0) {
            if(is_object($results)) {
                $results = array($results);
            }
            return $results;            
        } else {
            return array();
        }
    }
    private function get_quotation_incompleted_lists()
    {
        $data['table'] = 'quotes';
        $data['field'] = 'id';

        $data['condition'] = array(    
              "in_process" => '1'
        );

        $data['order_by'] = array(
            'id' => 'desc'
        );

        $this->standard_model->set_query_data($data);
        $results = $this->standard_model->select();

        if (sizeof($results) > 0) {
            if(is_object($results)) {
                $results = array($results);
            }
            return $results;            
        } else {
            return array();
        }
    }
    private function get_total_quotations_lists()
    {
        $data['table'] = 'quotes';
        $data['field'] = 'id';
        $data['condition'] = array(
            'is_active' => 1
        );
        $data['order_by'] = array(
            'id' => 'desc'
        );

        $this->standard_model->set_query_data($data);
        $results = $this->standard_model->select();

        if (sizeof($results) > 0) {
            if(is_object($results)) {
                $results = array($results);
            }
            return $results;            
        } else {
            return array();
        }
    }
    private function get_inactive_users_lists()
    {
       $data['table'] = 'users';
        $data['field'] = 'id,';

        $data['condition'] = array(    
             "user_status" => 'inactive'
        );

        $data['order_by'] = array(
            'id' => 'desc'
        );

        $this->standard_model->set_query_data($data);
        $results = $this->standard_model->select();

        if (sizeof($results) > 0) {
            if(is_object($results)) {
                $results = array($results);
            }
            return $results;            
        } else {
            return array();
        }
    }
     private function get_total_users_lists()
    {
        $data['table'] = 'users';
        $data['field'] = 'id,';

       
        $data['order_by'] = array(
            'id' => 'desc'
        );

        $this->standard_model->set_query_data($data);
        $results = $this->standard_model->select();

        if (sizeof($results) > 0) {
            if(is_object($results)) {
                $results = array($results);
            }
            return $results;            
        } else {
            return array();
        }
    }
    public function users_detail()
    {
        $user_id=$this->uri->segment('3');
        $data['user_info'] = $this->getUserInfo($user_id);
        $data['last_login'] = $this->getUserLastLogin($user_id);
        $data['user_address'] = $this->getUserAddress($user_id);
        $data['user_quotes'] = $this->getUserQuotes($user_id);
        $data['main_content'] = "user_detail_view"; 
        $data['title'] = "users";
        $data['section'] = "users";
        $data['type'] = 'list';
        $this->load->view('master',$data);
    }
    private function getUserQuotes($user_id='')
    {
        $data['table'] = 'quotes';
        $data['field'] = 'id,total,quote_modified';

        $data['condition'] = array(  
              "user_id" => $user_id,
              "in_process" => '0'
        );

        $data['order_by'] = array(
            'id' => 'desc'
        );

        $this->standard_model->set_query_data($data);
        $results = $this->standard_model->select();

        if (sizeof($results) > 0) {
            if(is_object($results)) {
                $results = array($results);
            }
            return $results;            
        } else {
            return array();
        }
    }
    private function getUserAddress($user_id='')
    {
        $data['table'] = 'address';
        $data['field'] = 'id,contact_f_name,contact_l_name,telephone,address_1,city,state,postal_code';

        $data['condition'] = array(  
              "user_id" => $user_id,
              "is_active" => '1'
        );

        $data['order_by'] = array(
            'id' => 'desc'
        );

        $this->standard_model->set_query_data($data);
        $results = $this->standard_model->select();

        if (sizeof($results) > 0) {
            if(is_object($results)) {
                $results = array($results);
            }
            return $results;            
        } else {
            return array();
        }
    }
    private function getUserLastLogin($user_id='')
    {
        $data['table'] = 'login_history';
        $data['field'] = 'id,date_login';

        $data['condition'] = array(  
              "user_id" => $user_id
        );

        $data['order_by'] = array(
            'id' => 'desc'
        );

        $this->standard_model->set_query_data($data);
        $results = $this->standard_model->select();

        if (sizeof($results) > 0) {
            if(is_object($results)) {
                $results = array($results);
            }
            return $results;            
        } else {
            return array();
        }
    }
    private function getUserInfo($user_id='')
    {
       $data['table'] = 'users';
        $data['field'] = 'id,email,full_name,mobile,joining_date';

        $data['condition'] = array(  
              "id" => $user_id,
              "is_email_verified" => '1',          
              "user_status" => 'active'
        );

        $data['order_by'] = array(
            'id' => 'desc'
        );

        $this->standard_model->set_query_data($data);
        $results = $this->standard_model->select();

        if ($results) {
            return $results;            
        } else {
            return array();
        }
    }
    private function get_users_lists()
    {
        $data['table'] = 'users';
        $data['field'] = 'id,email,full_name,mobile,user_status,joining_date';

        $data['condition'] = array(    
              "is_email_verified" => '1',          
              "user_status" => 'active'
        );

        $data['order_by'] = array(
            'id' => 'desc'
        );

        $this->standard_model->set_query_data($data);
        $results = $this->standard_model->select();

        if (sizeof($results) > 0) {
            if(is_object($results)) {
                $results = array($results);
            }
            return $results;            
        } else {
            return array();
        }
    }
    private function get_offers_lists()
    {
       $data['table'] = 'offers';
        $data['field'] = 'id,title,url';

        $data['condition'] = array(                 
              "status" => '1'
        );

        $data['order_by'] = array(
            'id' => 'desc'
        );

        $this->standard_model->set_query_data($data);
        $results = $this->standard_model->select();

        if (sizeof($results) > 0) {
            if(is_object($results)) {
                $results = array($results);
            }
            return $results;            
        } else {
            return array();
        }
    }
    private function get_covers_lists()
    {
        $data['table'] = 'cover_image';
        $data['field'] = 'id,title,url';

        $data['condition'] = array(                 
              "status" => '1'
        );

        $data['order_by'] = array(
            'id' => 'desc'
        );

        $this->standard_model->set_query_data($data);
        $results = $this->standard_model->select();

        if (sizeof($results) > 0) {
            if(is_object($results)) {
                $results = array($results);
            }
            return $results;            
        } else {
            return array();
        }
    }
    
    private function get_brands_lists(){
        $data['table'] = 'brands';
        $data['field'] = '*';

        $data['condition'] = array(
            "status" => '1'
        );

        $data['order_by'] = array(
            'id' => 'desc'
        );

        $this->standard_model->set_query_data($data);
        $results = $this->standard_model->select();

        if (sizeof($results) > 0) {
            if(is_object($results)) {
                $results = array($results);
            }
            return $results;            
        } else {
            return array();
        }       
    }

    private function get_catalogs_lists(){
        $data['table'] = 'catalogs';
        $data['field'] = '*';

        $data['condition'] = array(
            "status" => '1'
        );

        $data['order_by'] = array(
            'id' => 'desc'
        );

        $this->standard_model->set_query_data($data);
        $results = $this->standard_model->select();

        if (sizeof($results) > 0) {
            if(is_object($results)) {
                $results = array($results);
            }
            return $results;            
        } else {
            return array();
        }       
    }

    public function products($type="list", $product_id=0){
        if($type=='list'){
            $data = $this->get_product_lists();            
            $data['main_content'] = "products_view";
        } else if( $type=='create' || $type == 'edit') {
            $data['catalogs'] = $this->get_catalogs_lists();
           if($product_id!=0) {
                $data['product_id'] = $product_id;                
            }
            $data['main_content'] = "product_edit_view";            
        } else {
            redirect(base_url('admin'));
        }
        $data['title'] = "products";
        $data['section'] = "products";
        $data['type'] = 'list';
                
        $this->load->view('master',$data);
    }
    private function get_product_lists(){
        $page = !$this->input->get('page') ? 1 : $this->input->get('page');
        
        $data_query['table'] = 'product_list';
        $data_query['field'] = 'product_list.id,product_list.title,product_list.url, product_list.status';
        $data_query['condition'] = array(                 
              "product_list.status !=" => 'deleted'
        );

        $data_query['order_by'] = array(
            'id' => 'desc'
        );
        $data_query['limit'] = 12;
        $data_query['offset'] = $data_query['limit'] * ($page - 1);


        $this->standard_model->set_query_data($data_query);
        $results = $this->standard_model->select();

        if (sizeof($results) > 0) {
            if(is_object($results)) {
                $results = array($results);
            }            
        } else {
            $results = array();
        }

        $data['products'] = $results;
        $data['total_pages'] = get_total_pages($data_query, $data_query['limit']);       
        $data['page_no'] = $page;
        return $data;
    }

    public function blog($type="list", $blog_id=0){
        
        $data['section'] = "blog";
        $data['toggle_element'] = "blog";
        $data['type'] = $type;

        if( $type=='list' ){
            $data['articles'] = $this->get_blog_lists();
            $data['main_content'] = "blog_list_view";            
        } else if( $type=='create' || $type == 'edit') {
            if($blog_id!=0) {
                $data['blog_id'] = $blog_id;                
            }
            $data['main_content'] = "blog_edit_view";            
        } else {
            redirect(base_url('admin'));
        }
        
        $this->load->view('master',$data);
    }

    private function get_blog_lists() {
        $data['table'] = 'articles';
        $data['field'] = 'articles.id, articles.title, categories.name as cat_name, articles.cover_image, articles.slug, articles.status, articles.date_of_action';

        $data['join'] = array(
            "categories" => 'categories.id = articles.cat_id',
        );

        $data['condition'] = array(                 
              "articles.status !=" => 'deleted'
        );

        $data['order_by'] = array(
            'id' => 'desc'
        );

        $data['limit'] = 50;
        $this->standard_model->set_query_data($data);
        $results = $this->standard_model->select();

        if (sizeof($results) > 0) {
            if(is_object($results)) {
                $results = array($results);
            }
            return $results;            
        } else {
            return array();
        }
    }   

    public function announcements(){
        
        $data['section'] = "more";
        $data['toggle_element'] = "more";
        $data['type'] = 'announcements';

        $data['main_content'] = "announcements_view";

        $this->load->view('master',$data);
    }

    public function gallery(){
        
        $data['section'] = "more";
        $data['toggle_element'] = "more";
        $data['type'] = 'gallery';

        $data['main_content'] = "gallery_view";

        $this->load->view('master',$data);
    }
	
}