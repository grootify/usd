<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends Admin_Rest_Controller
{
	public function __construct(){        
        parent::__construct();
        $this->load->helper('utilities');
        $this->load->model('standard_model');
    }

    public function index_get(){
        $this->response(array('status'=>false));
    }
    
    public function update_cover_post() {
         if($this->input->post('id')) {
            $updatedata['table'] = 'cover_image';
            $updatedata['condition'] = array(
                  "id" => $this->input->post('id')
            );

            $this->standard_model->set_query_data($updatedata);
             if($this->input->post('action') == 'delete' ){
                $stat = $this->standard_model->update(array('status'=>'0'));
            } else {
                $stat = false;
            }

            if($stat) {
                $this->response(array('status'=>true),200);
            } else {                
                $this->response(array('status'=>false));
            }
        } else{
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('status'=>false, 'error'=>'data invalid')));
        }       
    }

    public function update_user_post() {
        if($this->input->post('id')) {
            $updatedata['table'] = 'users';
            $updatedata['condition'] = array(
                  "id" => $this->input->post('id')
            );

            $this->standard_model->set_query_data($updatedata);
             if($this->input->post('action') == 'delete' ){
                $stat = $this->standard_model->update(array('user_status'=>'inactive'));
            } else {
                $stat = false;
            }

            if($stat) {
                $this->response(array('status'=>true),200);
            } else {                
                $this->response(array('status'=>false));
            }
        } else{
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('status'=>false, 'error'=>'data invalid')));
        }  
    }

    public function update_offer_post() {
       if($this->input->post('id')) {
            $updatedata['table'] = 'offers';
            $updatedata['condition'] = array(
                  "id" => $this->input->post('id')
            );

            $this->standard_model->set_query_data($updatedata);
             if($this->input->post('action') == 'delete' ){
                $stat = $this->standard_model->update(array('status'=>'0'));
            } else {
                $stat = false;
            }

            if($stat) {
                $this->response(array('status'=>true),200);
            } else {                
                $this->response(array('status'=>false));
            }
        } else{
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('status'=>false, 'error'=>'data invalid')));
        }  
    }

    public function update_product_post() {
        if($this->input->post('id')) {
            $updatedata['table'] = 'product_list';
            $updatedata['condition'] = array(
                  "id" => $this->input->post('id')
            );

            $this->standard_model->set_query_data($updatedata);
             if($this->input->post('action') == 'delete' ){
                $stat = $this->standard_model->update(array('status'=>'deleted'));
            } else if($this->input->post('action') == 'publish' ){
                $stat = $this->standard_model->update(array('status'=>'active'));
            } else if($this->input->post('action') == 'unpublish' ){
                $stat = $this->standard_model->update(array('status'=>'inactive'));
            } else {
                $stat = false;                
            }

            if($stat) {
                $this->response(array('status'=>true),200);
            } else {                
                $this->response(array('status'=>false));
            }
        } else{
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('status'=>false, 'error'=>'data invalid')));
        }  
    }

    public function update_quoteProduct_post() {
        if($this->input->post('id')) {
            $updatedata['table'] = 'quote_products';
            $updatedata['condition'] = array(
                  "id" => $this->input->post('id'),
                  "product_id" => $this->input->post('product_id')
            );
            $total=$this->input->post('total');

            $this->standard_model->set_query_data($updatedata);
             if($this->input->post('id')){
                $stat = $this->standard_model->update(array('total'=>$total));
            } else {
                $stat = false;
            }

            if($stat) {
                $this->response(array('status'=>true),200);
            } else {                
                $this->response(array('status'=>false));
            }
        } else{
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('status'=>false, 'error'=>'data invalid')));
        }  
    }

    public function update_customProduct_post() {
       if($this->input->post('custom_id')) {
            $updatedata['table'] = 'custom_quote';
            $updatedata['condition'] = array(
                  "id" => $this->input->post('custom_id'),
                  "quote_id" => $this->input->post('quote_id')
            );
            $total=$this->input->post('total');

            $this->standard_model->set_query_data($updatedata);
             if($this->input->post('custom_id')){
                $stat = $this->standard_model->update(array('total'=>$total));
            } else {
                $stat = false;
            }

            if($stat) {
                $this->response(array('status'=>true),200);
            } else {                
                $this->response(array('status'=>false));
            }
        } else{
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('status'=>false, 'error'=>'data invalid')));
        }  
    }

    public function sendQuotation_post() {
        $quote_id=$this->input->post('quote_id');

        $view_data['quotation_detail']=$this->getQuotationDetailByQuoteID($quote_id);
        $email_to=$view_data['quotation_detail']->email;
        $view_data['address_detail'] = $this->getAddress($view_data['quotation_detail']->address_id);
        $view_data['quote_product_item']=$this->getCustomerQuoteProduct($quote_id);
        $view_data['custom_quote_item']=$this->getCustomerCustomProduct($quote_id);
        $view_data['attachment_quote_item']=$this->getAttachmentQuote($quote_id);

        $msg = $this->load->view('admin_quote_mail_view', $view_data, true);
        
        try{                
            if ($this->send_email('Your order has been acknowledged', $msg, $email_to)) {
              $this->response(array('status'=>"true", 'message'=>'Mail sent successfully!'));
            } else {
              $this->response(array('status'=>"false", 'message'=>'Error while sending mail!'));
            }
        } catch(Exception $e){
            return array('status' => false , 'message'=>  "Try after some time!");
        }
    }

    private function getAddress($address_id) {
        $data['table'] = 'address';
        $data['field'] = 'id,user_id,company,contact_f_name,contact_l_name,email,telephone,address_1,city,state,postal_code';

        $data['condition'] = array(    
              "id" => $address_id
        );

        $data['order_by'] = array(
            'id' => 'desc'
        );

        $this->standard_model->set_query_data($data);
        $results = $this->standard_model->select();

        if ($results) {
          return $results;            
        } else {
            return array();
        }
    }

    private function getCustomerQuoteProduct($quote_id) {
      $data['table'] = 'quote_products';
        $data['field'] = 'quote_products.id,quote_products.product_id,quote_products.quantity,quote_products.total,product_list.title,product_list.sku_number,product_list.part_no';

        $data['join'] = array(
           "product_list" =>'product_list.id=quote_products.product_id'
        );
        $data['condition'] = array(    
              "quote_products.quote_id" => $quote_id
        );

        $data['order_by'] = array(
            'id' => 'asc'
        );

        $this->standard_model->set_query_data($data);
        $results = $this->standard_model->select();

        if (sizeof($results) > 0) {
            if(is_object($results)) {
                $results = array($results);
            }
            return $results;            
        } else {
            return array();
        }
    }

    private function getCustomerCustomProduct($quote_id) {
        $data['table'] = 'custom_quote';
        $data['field'] = 'id,quote_id,name,quantity,total';

        $data['condition'] = array(    
              "quote_id" => $quote_id
        );

        $data['order_by'] = array(
            'id' => 'asc'
        );

        $this->standard_model->set_query_data($data);
        $results = $this->standard_model->select();

        if (sizeof($results) > 0) {
            if(is_object($results)) {
                $results = array($results);
            }
            return $results;            
        } else {
            return array();
        }
    }

    private function getAttachmentQuote($quote_id) {
     $data['table'] = 'attachment_data';
        $data['field'] = 'id,quote_id,item_name,quantity,total';

        $data['condition'] = array(    
              "quote_id" => $quote_id
        );

        $data['order_by'] = array(
            'id' => 'asc'
        );

        $this->standard_model->set_query_data($data);
        $results = $this->standard_model->select();

        if (sizeof($results) > 0) {
            if(is_object($results)) {
                $results = array($results);
            }
            return $results;            
        } else {
            return array();
        }
    }

    private function getQuotationDetailByQuoteID($quote_id) {
        $data['table'] = 'quotes';
        $data['field'] = 'quotes.id,quotes.quote_no,quotes.user_id,quotes.invoice_no,quotes.address_id,quotes.total,quotes.quote_modified,users.full_name,users.email';
        $data['join'] = array(
            "users" => 'users.id = quotes.user_id'
        );
        $data['condition'] = array(    
              "quotes.id" => $quote_id
        );

        $data['order_by'] = array(
            'id' => 'asc'
        );

        $this->standard_model->set_query_data($data);
        $results = $this->standard_model->select();

        if ($results) {
          return $results;            
        } else {
            return array();
        }
    }

    private function send_email($subject='', $message='', $mailTo='') {
        $this->load->library('email');
             
        $config['protocol']  = "smtp";       
        $config['smtp_host'] = 'ssl://smtp.zoho.com';
        $config['smtp_port'] = '465';
        $config['smtp_user'] = 'mktg@grootify.com';
        $config['smtp_pass'] = 'Hexzen@1601';
        $config['mailtype']  = 'html';
        $config['charset']   = 'utf-8';
        $config['newline']   = "\r\n";
        $config['wordwrap']  = TRUE;

        $this->email->initialize($config);
        $this->email->from('mktg@grootify.com','AK Global');
        $this->email->to($mailTo);   
        $this->email->cc('deepak@grootify.com');

        $this->email->subject($subject);
        $this->email->message($message);
     
        if($message != '') {
            $res = $this->email->send();
            if($res) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }               
    }

    public function update_quoteAmount_post() {
         if($this->input->post('quote_id')) {
            $total1=$this->getTotalFromQuoteProduct($this->input->post('quote_id'));
            $total2=$this->getTotalFromCustomProduct($this->input->post('quote_id'));
            $total3=$this->getTotalFromAttachmentProduct($this->input->post('quote_id'));
            $updatedata['table'] = 'quotes';
            $updatedata['condition'] = array(
                  "id" => $this->input->post('quote_id')
            );
            if($total1==''){
                $total1='0';
            }else{
                $total1=$total1->total_amount;
            }
            if($total2==''){
                $total2='0';
            }else{
                $total2=$total2->total_amount;
            }
            if($total3==''){
                $total3='0';
            }else{
                $total3=$total3->total_amount;
            }
            $total=$total1+$total2+$total3;
            $this->standard_model->set_query_data($updatedata);
            $stat = $this->standard_model->update(array('total'=>$total));
            if($stat) {
             $this->response(array('status'=>true),200);
            } else {                
                $this->response(array('status'=>false));
            }

        }
    }
    
    private function getTotalFromQuoteProduct($quote_id) {
      if($quote_id == 0) {
            $this->response(array('status'=>false));
        }

        $data['table'] = 'quote_products';
        $data['field'] = 'SUM(total) as total_amount';

        $data['condition'] = array(                 
              "quote_id" => $quote_id
        );        
        
        $this->standard_model->set_query_data($data);
        $result = $this->standard_model->select();
        return $result;
    }

    private function getTotalFromCustomProduct($quote_id) {
       if($quote_id == 0) {
            $this->response(array('status'=>false));
        }

        $data['table'] = 'custom_quote';
        $data['field'] = 'SUM(total) as total_amount';

        $data['condition'] = array(                 
              "quote_id" => $quote_id
        );        
        
        $this->standard_model->set_query_data($data);
        $result = $this->standard_model->select();
        return $result;
    }

    private function getTotalFromAttachmentProduct($quote_id) {
       if($quote_id == 0) {
            $this->response(array('status'=>false));
        }

        $data['table'] = 'attachment_data';
        $data['field'] = 'SUM(total) as total_amount';

        $data['condition'] = array(                 
              "quote_id" => $quote_id
        );        
        
        $this->standard_model->set_query_data($data);
        $result = $this->standard_model->select();
        return $result;
    }

    public function delete_brand_post() {
        if($this->input->post('id')) {
            $updatedata['table'] = 'brands';
            $updatedata['condition'] = array(
                "id" => $this->input->post('id')
            );

            $this->standard_model->set_query_data($updatedata);
             
            $stat = $this->standard_model->update(array('status'=>'0'));
            
            if($stat) {
                $this->response(array('status'=>true),200);
            } else {                
                $this->response(array('status'=>false));
            }
        } else {
            $this->response(array('status'=>false, 'error'=>'Invalid data!'));            
        }       
    }

    public function delete_catalog_post() {
        if($this->input->post('id')) {
            $updatedata['table'] = 'catalogs';
            $updatedata['condition'] = array(
                "id" => $this->input->post('id')
            );

            $this->standard_model->set_query_data($updatedata);
             
            $stat = $this->standard_model->update(array('status'=>'0'));
            
            if($stat) {
                $this->response(array('status'=>true),200);
            } else {                
                $this->response(array('status'=>false));
            }
        } else {
            $this->response(array('status'=>false, 'error'=>'Invalid data!'));            
        }       
    }

    public function update_article_post() {
        if($this->input->post('id')) {
            $updatedata['table'] = 'articles';
            $updatedata['condition'] = array(
                  "id" => $this->input->post('id')
            );

            $this->standard_model->set_query_data($updatedata);

            if($this->input->post('action') == 'unpublish' ) {
                $stat = $this->standard_model->update(array('status'=>'inactive'));
            } else if($this->input->post('action') == 'delete' ){
                $stat = $this->standard_model->update(array('status'=>'deleted'));
            } else if($this->input->post('action') == 'publish' ){
                $stat = $this->standard_model->update(array('status'=>'active'));
            } else {
                $stat = false;
            }

            if($stat) {
                $this->response(array('status'=>true),200);
            } else {                
                $this->response(array('status'=>false));
            }
        } else{
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('status'=>false, 'error'=>'data invalid')));
        }       
    } 

    public function save_cover_post() {
       if($this->input->post('title')) {
            $data['title'] = $this->input->post('title');
        }
        if($this->input->post('url')) {
            $data['url'] = $this->input->post('url');
        }else{
             $data['url'] = '';
        }
        $data_query['table'] = 'cover_image';        

        $cover_id = $this->input->post('cover_id');
       
        if($cover_id && is_numeric($cover_id) && $cover_id!='') {
           $data_query['condition'] = array(
                "id" => $this->input->post('cover_id')
            );
            $this->standard_model->set_query_data($data_query);
            $this->standard_model->update($data);
            $this->response(array('status'=>true));
        } else {
            $data['status'] = '1';
            $this->standard_model->set_query_data($data_query);
            $cover_id = $this->standard_model->insert_and_id($data);
            $this->response(array('status'=>true, 'cover_id'=>$cover_id));
        }
    }

    public function save_offer_post() {
       if($this->input->post('title')) {
            $data['title'] = $this->input->post('title');
            $data['slug'] = slugify($this->input->post('title'));
        }
        if($this->input->post('url')) {
            $data['url'] = $this->input->post('url');
        }else{
             $data['url'] = '';
        }
        $data_query['table'] = 'offers';        

        $offer_id = $this->input->post('offerID');
      
        if($offer_id && is_numeric($offer_id) && $offer_id!='') {
           $data_query['condition'] = array(
                "id" => $this->input->post('offer_id')
            );
            $this->standard_model->set_query_data($data_query);
            $this->standard_model->update($data);
            $this->response(array('status'=>true));
        } else {
            $data['date_of_action'] = date('Y-m-d H:i:s');
            $data['status'] = '1';
            $this->standard_model->set_query_data($data_query);
            $offer_id = $this->standard_model->insert_and_id($data);
            $this->response(array('status'=>true, 'offer_id'=>$offer_id));
        }
    }

    public function save_brand_post() {
        if($this->input->post('name')) {
            $data['name'] = $this->input->post('name');            
        }
        if($this->input->post('link')) {
            $data['link_url'] = $this->input->post('link');
        }else{
             // $data['url'] = '';
        }

        if($this->input->post('image')) {
            $data['image'] = $this->input->post('image');
        }else{
             // $data['image'] = '';
        }
        $data_query['table'] = 'brands';        

        $brand_id = $this->input->post('brand_id');
        // $data['modified_date'] = date('Y-m-d H:i:s');
        if($brand_id && is_numeric($brand_id) && $brand_id!='') {
           $data_query['condition'] = array(
                "id" => $this->input->post('brand_id')
            );
            $this->standard_model->set_query_data($data_query);
            $this->standard_model->update($data);
            $this->response(array('status'=>true));
        } else {
            $data['is_featured']='1';
            $data['order_no']='0';
            $data['status'] = '1';
            $data['date_created'] = date('Y-m-d H:i:s');
            $this->standard_model->set_query_data($data_query);
            $brand_id = $this->standard_model->insert_and_id($data);
            $this->response(array('status'=>true, 'brand_id'=>$brand_id));
        }
    }

    public function save_catalog_post() {
        if($this->input->post('name')) {
            $data['name'] = $this->input->post('name');
            $data['slug'] =slugify($this->input->post('name'));
        }
        if($this->input->post('image')) {
            $data['image'] = $this->input->post('image');
        }else{
             // $data['image'] = '';
        }
        $data_query['table'] = 'catalogs';        

        $catalog_id = $this->input->post('catalog_id');
        // $data['modified_date'] = date('Y-m-d H:i:s');
        if($catalog_id && is_numeric($catalog_id) && $catalog_id!='') {
           $data_query['condition'] = array(
                "id" => $this->input->post('catalog_id')
            );
            $this->standard_model->set_query_data($data_query);
            $this->standard_model->update($data);
            $this->response(array('status'=>true));
        } else {
            $data['is_featured']='1';
            $data['order_no']='0';
            $data['status'] = '1';
            $data['date_created'] = date('Y-m-d H:i:s');
            $this->standard_model->set_query_data($data_query);
            $catalog_id = $this->standard_model->insert_and_id($data);
            $this->response(array('status'=>true, 'catalog_id'=>$catalog_id));
        }
    }

    public function save_user_post() {
        if($this->input->post('email')!=''){
             $data['email']=$this->input->post('email');
        }
        if($this->input->post('full_name')!=''){
             $data['full_name']=$this->input->post('full_name');
        }
        if($this->input->post('mobile')!=''){
             $data['mobile']=$this->input->post('mobile');
        }
        $this->load->helper('agents');
        $data['registration_ip'] = get_ip_address();

        if($data['registration_ip'] == "::1") {
            $data['registration_ip'] = '127.0.0.1';
        }
       
        $data_query['table'] = 'users';        
        $user_id = $this->input->post('user_id');
       
        if($user_id && is_numeric($user_id) && $user_id!='') {
           $data_query['condition'] = array(
                "id" => $this->input->post('user_id')
            );
            $this->standard_model->set_query_data($data_query);
            $this->standard_model->update($data);
            $this->response(array('status'=>true));
        } else {
            $data['password']='123';
            $data['user_role']='user';
            $data['user_status']='active';
            $data['is_email_verified']='1';
            $data['joining_date']=date('Y-m-d H:i:s');
            $this->standard_model->set_query_data($data_query);
            $user_id = $this->standard_model->insert_and_id($data);
           $this->response(array('status'=>true, 'user_id'=>$user_id));
        }
    }

    public function save_product_post() {
        if($this->input->post('title')!=''){
             $data['title']=$this->input->post('title');
             $data['slug']=slugify($this->input->post('title'));
        }
        if($this->input->post('catalog_id')!=''){
             $data['catalog_id']=$this->input->post('catalog_id');
        }
        if($this->input->post('part_no')!=''){
             $data['part_no']=$this->input->post('part_no');
        }
        if($this->input->post('sku_number')!=''){
             $data['sku_number']=$this->input->post('sku_number');
        }
        if($this->input->post('sell_unit')!=''){
             $data['sell_unit']=$this->input->post('sell_unit');
        }
        if($this->input->post('ships_within')!=''){
            $data['ships_within']=$this->input->post('ships_within');
        }
        if($this->input->post('list_price')!=''){
             $data['list_price']=$this->input->post('list_price');
        }
        if($this->input->post('product_description')!=''){
             $data['description']=$this->input->post('product_description');
        }
        if($this->input->post('url')!=''){
             $data['url']=$this->input->post('url');
             
        }
        // if($this->input->post('productInfoArry')!=''){
        //     $productInfoArry=$this->input->post('productInfoArry');
        // }
        $mediaIDs ='';
        if($this->input->post('mediaIDs')!=''){
            $mediaIDs=$this->input->post('mediaIDs');
        }
        $data_query['table'] = 'product_list';        
        $product_id = $this->input->post('product_id');
        if($product_id && is_numeric($product_id) && $product_id!='') {
           $data['date_modified'] = date('Y-m-d H:i:s');
           $data_query['condition'] = array(
                "id" => $this->input->post('product_id')
            );
            $this->standard_model->set_query_data($data_query);
            $this->standard_model->update($data);

            if($mediaIDs!='') {
                $media = $this->updateProduct_images($product_id,$mediaIDs);
            }
            
            // $this->insert_medias($data['url'], $product_id, 1);
            $this->response(array('status'=>true));
        } else {
            $data['status']='active';
            $data['views']='0';
            $data['date_created']=date('Y-m-d H:i:s');
            $this->standard_model->set_query_data($data_query);
            $product_id = $this->standard_model->insert_and_id($data);
            // $section = $this->addProductSection($product_id,$productInfoArry);
            if($mediaIDs!='') {
                $media = $this->updateProduct_images($product_id,$mediaIDs);
            }
            // $this->insert_medias($data['url'], $product_id, 1);
            $this->response(array('status'=>true, 'product_id'=>$product_id));
        }
    }

    /*private function insert_medias($insert_url, $id, $is_cover=0) {

        $data_query['table'] = 'product_images';
        $data['product_id'] = $id;
        $data['image'] = $insert_url;
        $data['container_name'] = 'assets/images/products/';
        $data['is_cover'] = $is_cover;
        // $data['height'] = '445';
        // $data['width'] = '340';
        $data['status'] = '1';
        $data['date_created'] =date('Y-m-d H:i:s');
        $this->standard_model->set_query_data($data_query);
        $media_id = $this->standard_model->insert_and_id($data);
        if($media_id) 
            return $media_id;
    }*/

    private function addProductSection($product_id='',$productInfoArry='') {
        for($i=0;$i<count($productInfoArry);$i++){
            $data_query['table'] = 'product_section';
            $data['product_id']=$product_id;
            $data['name']=$productInfoArry[$i]['section_name'];
            $data['description']=$productInfoArry[$i]['section_description'];
            $data['status']='1';
            $data['date_created']=date('Y-m-d H:i:s');
            $this->standard_model->set_query_data($data_query);
            $section_id = $this->standard_model->insert_and_id($data);
         }
    
    }

    public function updateAttachement_post() {
        if($this->input->post('quote_id')!=''){
             $data['quote_id']=$this->input->post('quote_id');
        }
        if($this->input->post('attachInfoArry')!=''){
            $attachInfoArry=$this->input->post('attachInfoArry');
            $total='0';
            for($i=0;$i<count($attachInfoArry);$i++){
                $data_query['table'] = 'attachment_data';
                $data['item_name']=$attachInfoArry[$i]['item_name'];
                $data['quantity']=$attachInfoArry[$i]['quantity'];
                $data['total']=$attachInfoArry[$i]['attach_amount'];
                $data['status']='1';
                $data['date_created']=date('Y-m-d H:i:s');
                $this->standard_model->set_query_data($data_query);
                $attachment_id = $this->standard_model->insert_and_id($data);
                $total=$total+$attachInfoArry[$i]['attach_amount'];
             }
            $this->response(array('status'=>true, 'attachment_id'=>$attachment_id));
        }    
    }

    private function updateProduct_images($product_id='',$mediaIDs='') {
        $data_reset['status'] = 0;
        $data_reset_query['table'] = 'product_images';
        $data_reset_query['condition']=array(
           "product_id" => $product_id
        );        
        $this->standard_model->set_query_data($data_reset_query);
        $res = $this->standard_model->update($data_reset);

        for($i=0;$i<count($mediaIDs);$i++){
            $data_query['table'] = 'product_images';
            $data['product_id']=$product_id;
            $data_query['condition']=array(
               "id" => $mediaIDs[$i]
            );            
            $data['status'] = 1;
            $this->standard_model->set_query_data($data_query);
            $media = $this->standard_model->update($data);
        }
    }

    public function save_article_post() {

        if($this->input->post('title')) {
            $data['title'] = $this->input->post('title');
        }
        if($this->input->post('description')) {
            $data['description'] = $this->input->post('description');
        }
        if($this->input->post('cover_image')) {
            $data['cover_image'] = $this->input->post('cover_image');
        }
        if($this->input->post('tags')) {
            $data['tags'] = $this->input->post('tags');
        }
        if($this->input->post('slug')) {
            $data['slug'] = $this->input->post('slug');
            $data['slug'] = slugify($data['slug']);
        }


        $data_query['table'] = 'articles';        

        $blog_id = $this->input->post('blog_id');
        $data['date_of_action'] = date('Y-m-d H:i:s');
        if($blog_id && is_numeric($blog_id) && $blog_id!='') {
            $data_query['condition'] = array(
                "id" => $this->input->post('blog_id')
            );
            $this->standard_model->set_query_data($data_query);
            $this->standard_model->update($data);
            $this->response(array('status'=>true));
        } else {
            $data['status'] = 'inactive';
            $data['cat_id'] = '1';
            $data['date_of_creation'] = date('Y-m-d H:i:s');
            $this->standard_model->set_query_data($data_query);
            $blog_id = $this->standard_model->insert_and_id($data);
            $this->response(array('status'=>true, 'blog_id'=>$blog_id));
        }
    }

    public function get_user_get($user_id=0) {
       if($user_id == 0) {
            $this->response(array('status'=>false));
        }

        $data['table'] = 'users';
        $data['field'] = 'id, full_name, mobile,email,joining_date';

        $data['condition'] = array(                 
              "id" => $user_id
        );        
        
        $this->standard_model->set_query_data($data);
        $result = $this->standard_model->select();
        
        $this->response(array('status'=>true, 'data'=>$result));
    }

    public function get_offer_get($offer_id=0) {
       if($offer_id == 0) {
            $this->response(array('status'=>false));
        }

        $data['table'] = 'offers';
        $data['field'] = 'id, title, url';

        $data['condition'] = array(                 
              "id" => $offer_id
        );        
        
        $this->standard_model->set_query_data($data);
        $result = $this->standard_model->select();
        
        $this->response(array('status'=>true, 'data'=>$result));
    }

    public function get_brand_get($brand_id=0) {
         if($brand_id == 0) {
            $this->response(array('status'=>false));
        }

        $data['table'] = 'brands';
        $data['field'] = 'id, name, image, link_url as link';

        $data['condition'] = array(                 
              "id" => $brand_id
        );        
        
        $this->standard_model->set_query_data($data);
        $result = $this->standard_model->select();
        
        $this->response(array('status'=>true, 'data'=>$result));
    }

    public function get_catalog_get($brand_id=0) {
         if($brand_id == 0) {
            $this->response(array('status'=>false));
        }

        $data['table'] = 'catalogs';
        $data['field'] = '*';

        $data['condition'] = array(                 
              "id" => $brand_id
        );        
        
        $this->standard_model->set_query_data($data);
        $result = $this->standard_model->select();
        
        $this->response(array('status'=>true, 'data'=>$result));
    }

    public function get_cover_get($cover_id=0) {
         if($cover_id == 0) {
            $this->response(array('status'=>false));
        }

        $data['table'] = 'cover_image';
        $data['field'] = 'id, title, url';

        $data['condition'] = array(                 
              "id" => $cover_id
        );        
        
        $this->standard_model->set_query_data($data);
        $result = $this->standard_model->select();
        
        $this->response(array('status'=>true, 'data'=>$result));
    }

    public function get_product_get($product_id=0) {
        if($product_id == 0) {
            $this->response(array('status'=>false));
        }

        $data['table'] = 'product_list';
        $data['field'] = 'product_list.id,product_list.title,product_list.part_no,product_list.sku_number,product_list.sell_unit,product_list.ships_within,product_list.list_price,product_list.description,product_list.url,product_list.catalog_id';
         $data['condition'] = array(                 
              "id" => $product_id
        );        
        
        $this->standard_model->set_query_data($data);
        $data['product_info'] = $this->standard_model->select();
        $data['product_section'] =$this->getProductSectionById($product_id);
        $data['product_images'] =$this->getProductImagesById($product_id);
        // echo '<pre>';
        // print_r($data['product_info']);
        // echo '</pre>';

        // echo "</br>";

        // echo '<pre>';
        // print_r($data['product_section']);
        // echo '</pre>';
        // echo "</br>";
        //  echo '<pre>';
        // print_r($data['product_images']);
        // echo '</pre>';

        $this->response(array('status'=>true, 'data'=>$data));
    }

    private function getProductSectionById($product_id='') {
       $data['table'] = 'product_section';
        $data['field'] = 'name,description';
         $data['condition'] = array(                 
              "product_id" => $product_id
        );        
        
        $this->standard_model->set_query_data($data);
        $result = $this->standard_model->select();
        return $result;
    }

    private function getProductImagesById($product_id='') {
        $data['table'] = 'product_images';
        $data['field'] = 'image,container_name, id';
         $data['condition'] = array(                 
              "product_id" => $product_id,
              "status"=>'1'
        );        
        
        $this->standard_model->set_query_data($data);
        $result = $this->standard_model->select();

        if(is_object($result)) {
            $result = array($result);
        }
        return $result;
    }

    public function get_article_get($blog_id=0) {
        if($blog_id == 0) {
            $this->response(array('status'=>false));
        }

        $data['table'] = 'articles';
        $data['field'] = 'id, title, cat_id, cover_image, slug, status, description, tags';

        $data['condition'] = array(                 
              "id" => $blog_id
        );        
        
        $this->standard_model->set_query_data($data);
        $result = $this->standard_model->select();
        
        $this->response(array('status'=>true, 'data'=>$result));
    }

    public function get_articles_get() {

        $data['table'] = 'articles';
        $data['field'] = 'articles.id, articles.title, categories.name as cat_name, articles.cover_image, articles.slug, articles.status';

         $data['join'] = array(
            "categories" => 'categories.id = articles.cat_id',
        );

        $data['condition'] = array(                 
              "articles.id" => $blog_id
        );        
        
        $this->standard_model->set_query_data($data);
        $result = $this->standard_model->select();
        
        $this->response(array('status'=>true, 'data'=>$result));
    }

    public function delete_media_post() {
       $updatedata['table'] = 'product_images';
       $updatedata['condition'] = array(
             "id" => $this->input->post('media_id')
       );

       $this->standard_model->set_query_data($updatedata);

       $stat = $this->standard_model->update(array('status'=>0));
       if($stat) {
           $this->response(array('status'=>true));
       }
    } 
	
}