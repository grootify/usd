<style>
    .product-info:hover {
        box-shadow: 0 2rem 4rem -1rem rgba(0, 0, 0, .5);
        -webkit-transition: all .55s ease-in-out;
        -o-transition: all .55s ease-in-out;
        transition: all .55s ease-in-out;
    }
    
    .current {
        color: green;
    }
    
    #pagin li {
        display: inline-block;
    }
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<div class="container">
    <div class="az-content-breadcrumb mt-3">
        <span>Home</span>
        <span><?php echo $title;?></span>
    </div>
</div>

<div class="az-content pd-y-20 pd-lg-y-30 pd-xl-y-40">
    <div class="container">

        <div class="az-content-body pd-lg-l-40 d-flex flex-column">
            <div class="row mb-3 mt-3">
                <div class="col-md-6">
                    <h3>Brands</h3>
                    <!--                             <input type="search" class="form-control" placeholder="Search..."> -->
                </div>
                <div class="col-md-6">
                    <a href="<?php echo base_url();?>admin/brands/create" class="text-white"><i class="fa fa-plus" aria-hidden="true">
                    <button class="btn btn-primary float-right"></i> ADD NEW BRANDS</button>
                </a>
                </div>
            </div>

            <div class="row row-sm">
                <?php foreach($brands as $brand) { ?>
                    <div class="col-md-6 col-lg-3 mt-4 line-content" id="brand_<?php echo $brand->id;?>">
                        <div class="card bd text-center product-info">
                            <img class="img-fluid" src="<?php echo base_url();?><?php echo $brand->image;?>" alt="Image" style="width: 150px;height: 110px;margin-left: 52px;">
                            <div class="card-body prdDetails">
                                <p class="card-text mb-1"><a href=""><?php echo $brand->name?></a></p>
                            </div>
                            <div class="row prdED">
                                <div class="col-md-6">
                                    <p><a href="<?php echo base_url();?>admin/brands/edit/<?php echo $brand->id;?>"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a></p>
                                </div>
                                <div class="col-md-6">
                                    <p><a href="javascript:void(0)" type="button" onclick="deleteBrand(<?php echo $brand->id; ?>)" style="-webkit-appearance: none;"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a></p>
                                </div>
                            </div>
                        </div>
                        <!-- card -->
                    </div>
                <?php } ?>
                <!-- col-4 -->
            </div>

            <div class="mg-lg-b-30"></div>