<style>
.current {
  color: green;
}
#pagin li {
  display: inline-block;
}
</style> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
 <div class="az-content pd-y-20 pd-lg-y-30 pd-xl-y-40">
        <div class="container">
            <div class="az-content-body pd-lg-l-40 d-flex flex-column">
                <div class="az-content-breadcrumb">
                    <span><a href="<?php echo base_url();?>">Home</a></span>
                    <span>Users</span>
                </div>
                 <div class="card card-dashboard-seven">
                    <div class="card-body">
                        <div class="row row-sm">
                            <div class="col-6 col-lg-4">
                                <label class="az-content-label">Total Users</label>
                                <h2><?php echo $total_user;?></h2>
                            </div>
                            <!-- col -->
                            <div class="col-6 col-lg-4">
                                <label class="az-content-label">Active Users</label>
                                <h2><?php echo $active_user;?></h2>
                            </div>
                            <!-- col -->
                            <div class="col-6 col-lg-4 mg-t-20 mg-lg-t-0">
                                <label class="az-content-label">Inactive Users</label>
                                <h2><?php echo $inactive_user;?></h2>
                            </div>
                            <!-- col -->
                        </div>
                        <!-- row -->
                    </div>
                    <!-- card-body -->
                </div>
                <div>
                 <div class="row mb-3 mt-3">
                        <div class="col-md-6">
<!--                             <input type="search" class="form-control" placeholder="Search..."> -->
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary float-right"><a href="<?php echo base_url();?>admin/users/create" class="text-white"><i class="fa fa-plus" aria-hidden="true"></i> ADD NEW USERS</a></button>
                        </div>
                    </div>
                
                <div>
                   <div class="card card-table-one">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Mobile</th>
                                        <th>Date of Registration</th>
                                        <th>Action</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        if(isset($users)){
                                            $this->load->helper('utilities');
                                            $i=1;
                                            foreach ($users as $user) {
                                    ?>
                                    <tr id="user_<?php echo $user->id;?>" class="line-content">
                                        <td><?php echo $i;?></td>
                                        <td><strong><a href="<?php echo base_url('admin/users_detail');?>/<?php echo $user->id;?>" title="View Details"><?php echo $user->full_name;?></a></strong></td>
                                        <td><?php echo $user->email;?></td>
                                        <td><?php echo $user->mobile;?></td>
                                        <td><?php echo datify($user->joining_date);?></td>
                                        <td><a href="<?php echo base_url();?>admin/users/edit/<?php echo $user->id;?>" class="btn btn-success block-btn btn-sm">Edit</button></td>
                                        <td>
                                            <button class="btn btn-danger block-btn btn-sm" onclick="setUserStatus(<?php echo $user->id?>,'delete');"><i class="fa fa-ban" aria-hidden="true"></i> Delete</button>
                                        </td>
                                    </tr>
                                <?php $i++; } } ?>
                                </tbody>
                            </table>

                        </div>
                        <!-- table-responsive -->

                    </div>

                <div>
                   <ul class="pagination pagination-circled pull-right mt-3" id="pagin">
                        <li class="page-item"><a class="page-link" href="#"><i class="icon ion-ios-arrow-back"></i></a></li>
                        <?php 
                            $total=count($users)/8;
                            for($i=1;$i<=$total;$i++){
                        ?>
                             <li class="page-item  <?php echo $i=='1'? 'active current':''?>"><a class="page-link" href="#"><?php echo $i;?></a></li>
                        <?php } ?>
                     
                        <li class="page-item"><a class="page-link" href="#"><i class="icon ion-ios-arrow-forward"></i></a></li>
                    </ul>
                </div>
                </div>

                <div class="ht-40"></div>

     <script type="text/javascript">
        pageSize = 8;
        showPage = function(page) {
            $(".line-content").hide();
            $(".line-content").each(function(n) {
                if (n >= pageSize * (page - 1) && n < pageSize * page)
                $(this).show();
            });        
        }
        showPage(1);
        $("#pagin li a").click(function() {
            $("#pagin li a").removeClass("current");
            $(this).addClass("current");
            showPage(parseInt($(this).text())) 
        });
    </script>   