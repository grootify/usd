<style type="text/css">
.frontsubmit-card-delete {
    position: absolute;
    right: 10px;
    padding: 0px;
    margin: 0px;
    line-height: 16px;
    background: #777777;
    color: #fff;
    border: none;
    border-bottom-left-radius: 4px;
}
</style>  
  <div class="container">
        <div class="az-content-breadcrumb mt-3">
            <span>Home</span>
            <span><?php echo $title;?></span>
        </div>
    </div>

    <div class="az-content pd-y-20 pd-lg-y-30 pd-xl-y-40">
        <div class="container">
            <input id="user-id" type="hidden" name="user-id" value="<?php if(isset($user_id)){ echo $user_id; } ?>">
                <div class="az-content-body pd-lg-l-40 d-flex flex-column" id="userEdit" data-toggle="validator">
                    <h6 class="addprd_title">About User</h6>
                    <div class="row row-sm mb-4">
                        <div class="col-lg-4">
                            <p class="mg-b-10">User Name</p>
                            <input class="form-control" name="full_name" id="full_name" placeholder="Enter Name " type="text" required>
                        </div>
                        <div class="col-lg-4">
                            <p class="mg-b-10">Email</p>
                            <input class="form-control" name="email" id="email" placeholder="Enter Email " type="email">
                        </div>
                        <div class="col-lg-4">
                            <p class="mg-b-10">Mobile</p>
                            <input class="form-control" name="mobile" id="mobile" placeholder="Enter Mobile Number" type="text">
                        </div>
                         <div id="ajaxResponseDiv" style="bottom: 57px;color: #ff3a3a;"></div>
                    </div>
                     <div class="row row-sm mb-4">
                         <div class="col-lg-2">
                            <button type="submit" onclick="saveUser()" class="btn btn-theme"><i class="material-icons"></i> Save</button>

                        </div>
                     </div>




                    <div class="mg-lg-b-30"></div>
