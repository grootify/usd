<style type="text/css">
    [data-role="dynamic-fields"] > .form-inline + .form-inline {
        margin-top: 0.5em;
    }
    
    [data-role="dynamic-fields"] > .form-inline [data-role="add"] {
        display: none;
    }
    
    [data-role="dynamic-fields"] > .form-inline:last-child [data-role="add"] {
        display: inline-block;
    }
    
    [data-role="dynamic-fields"] > .form-inline:last-child [data-role="remove"] {
        display: none;
    }
    
    .frontsubmit-card-delete {
        position: absolute;
        right: 10px;
        padding: 0px;
        margin: 0px;
        line-height: 16px;
        background: #777777;
        color: #fff;
        border: none;
        border-bottom-left-radius: 4px;
    }
</style>
<div class="container">
    <div class="az-content-breadcrumb mt-3">
        <span><a href="<?php echo base_url();?>admin">Home</a></span>
        <span><a href="<?php echo base_url();?>admin/products"><?php echo $title;?></a></span>
    </div>
</div>

<div class="az-content pd-y-20 pd-lg-y-30 pd-xl-y-20">
    <div class="container">
        <div class="az-content-body d-flex flex-column" id="productEdit">
            <h6 class="addprd_title">About Product</h6>
            <input id="product-id" type="hidden" name="product-id" value="<?php if(isset($product_id)){ echo $product_id; } ?>">
            <div class="row row-sm mb-4">
                <div class="col-lg-3">
                    <p class="mg-b-2">Product Title</p>
                    <input class="form-control" name="title" id="title" placeholder="Enter Product Title " type="text">
                </div>
                <div class="col-lg-3">
                    <p class="mg-b-2">Select Catalog</p>
                    <select class="form-control select2-no-search" name="catalog_id" id="catalog_id">
                        <option label="Choose Catalog"></option>
                        <?php foreach($catalogs as $catalog){?>
                            <option value="<?php echo $catalog->id;?>">
                                <?php echo $catalog->name;?>
                            </option>
                            <?php } ?>
                    </select>
                </div>
                <div class="col-lg-3">
                    <p class="mg-b-2">MFG Number</p>
                    <input class="form-control" name="part_no" id="part_no" placeholder="Enter Product Part Number " type="text">
                </div>
                <div class="col-lg-3">
                    <p class="mg-b-2">HSN Number</p>
                    <input class="form-control" name="sku_number" id="sku_number" placeholder="Enter Product SKU Number " type="text">
                </div>

            </div>
            <div class="row row-sm mb-2">
                <div class="col-lg-3">
                    <div class="frontsubmit-upload tx-center">
                        <p class="mg-b-2 tx-left">Cover Image</p>
                        <input type="hidden" name="product_logo" id="product_logo">
                        <div class="frontsubmit-media-upload-form" id="drop-area">
                            <div id="product-unuploaded">
                                <p class="frontsubmit-drag-drop-info">Drop Files Here</p>
                                <p>or</p>
                                <p class="frontsubmit-drag-drop-buttons">
                                    <input type="file" name="product_image" id="product_image" class="inputfile" />
                                    <label for="product_image">Choose a file</label>
                                </p>
                                <p class="frontsubmit-max-upload-size"> Maximum upload file size: 2 MB.</p>
                            </div>
                        </div>
                        
                        <div id="product-uploaded" style="position: relative; display: none;">
                            <div class="frontsubmit-object-actions">
                                <a href="javascript:void(0);" id="deleteProduct" class="frontsubmit-card-action frontsubmit-card-action-delete" title="Remove" style="display: none;"><i class="fa fa-times"></i></a>
                            </div>                                
                        </div>                        
                    </div>                        
                </div>
                <div class="col-lg-6">
                    <p class="mg-b-2">Product Description</p>
                    <textarea style="height: 186px;" name="product_description" id="product_description" class="form-control" placeholder="Enter Product Description"></textarea>
                </div>
                <div class="col-lg-3">
                    <div class="mg-b-10">
                        <p class="mg-b-2">Ships Within</p>
                        <input class="form-control" name="ships_within" id="ships_within" placeholder="Enter Ships within " type="text">
                    </div>
                    <div class="mg-b-10">
                        <p class="mg-b-2">Sell Unit</p>
                        <input class="form-control" name="sell_unit" id="sell_unit" placeholder="Enter Product Sell Unit " type="text">
                    </div>
                    <div class="mg-b-10">
                        <p class="mg-b-2">List Price</p>
                        <input class="form-control" name="list_price" id="list_price" placeholder="Enter Product List Price" type="text">
                    </div>
                </div>
            </div>

            <p class="mg-b-2 tx-left">Product Media</p>
            <div id="gallery-uploaded" class="row row-sm mb-2">                    
                <div class="col col-lg-3 frontsubmit-edit-post-row-description clearfix">
                    <div class="frontsubmit-upload">
                        <input type="hidden" name="galleryImage[]" class='galleryImage'>
                        <div class="frontsubmit-media-upload-form drop-area" id="gallery-drop-area">
                            <div id="gallery-unuploaded" style="text-align: center;">
                                <p class="frontsubmit-drag-drop-info">Drop a File Here</p>
                                <p>or</p>
                                <p class="frontsubmit-drag-drop-buttons">
                                    <input type="file" name="gallery_image" id="gallery_image" class="inputfile" />
                                    <label for="gallery_image">Choose a file</label>
                                </p>
                                <p class="frontsubmit-max-upload-size"> Maximum upload file size: 2 MB.</p>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="frontsubmit-object-actions clearfix">
                        <a href="javascript:void(0);" id="deleteMedia" class="frontsubmit-card-action frontsubmit-card-action-delete" title="Remove" style="display: none;">Remove</a>
                    </div> -->
                </div>                
            </div>

            <p class="mg-b-2" style="display: none;">Sections</p>
            <div class="row row-sm mb-2 product_section frontsubmit-form-main" id="product_section" style="display: none;">                    
                <div class="col-lg-3 mt-0 dynamic form-inline" data-role="dynamic-fields">
                    <div class="card pd-10 wd-100p">
                        <p class="mg-b-2 tx-left wd-100p">Section Name</p>
                        <input class="form-control section_name" name="section_name[]" id="section_name" placeholder="Enter Name " type="text" style="width:100%;">
                        
                        <p class="mg-b-2 mg-t-10 tx-left wd-100p">Section Description</p>
                        <textarea rows="3" name="section_description[]" id="section_description" class="form-control section_description" placeholder="Enter Description" style="width:100%;"></textarea>
                        
                        <div class="pd-t-10 wd-100p">
                            <button class="btn btn-primary actionBtn" data-role="add">
                                <i class="fa fa-plus"></i> ADD
                            </button>                                
                            <!-- <button class="btn btn-danger actionBtn" data-role="remove">
                                <i class="fa fa-times"></i> Remove
                            </button> -->
                        </div>
                    </div>
                </div>
            </div>
            <div id="ajaxResponseDiv mg-b-10"></div>

            <div id="errResponse" class="pos-fixed z-index-100 t-10 r-10">
            </div>
                
            <div class="row row-sm mb-4">
                <div class="col-lg-12 tx-center">
                    <button type="submit" onclick="saveProduct()" class="btn btn-theme wd-200 tx-xl-semibold">Save Product</button>
                </div>
            </div>            
        </div>
    </div>
</div>