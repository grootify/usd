
    <div class="container">
        <div class="az-content-breadcrumb mt-3">
            <span>Home</span>
            <span><?php echo $title?></span>
        </div>
    </div>

    <div class="az-content pd-y-20 pd-lg-y-30 pd-xl-y-40">
        <div class="container">
                <div class="az-content-body pd-lg-l-40 d-flex flex-column" id="brandEdit">
                    <h6 class="addprd_title">About Brand</h6>
                  <input id="brand-id" type="hidden" name="brand-id" value="<?php if(isset($brand_id)){ echo $brand_id; } ?>">
                    <div class="row row-sm mb-4">
                        <div class="col-lg-4">
                            <p class="mg-b-2">Brand Name</p>
                            <input class="form-control" id="brand_name" placeholder="Enter Brand Name " type="text">
                        </div>
                     </div>

                     <div class="row row-sm mb-4">
                        <div class="col-lg-4">
                            <p class="mg-b-2">Brand Website Link</p>
                            <input class="form-control" id="brand_link" placeholder="Enter website url" type="text">
                        </div>
                     </div>

                     <div class="row row-sm mb-4 frontsubmit-upload" >
                        <div class="col-lg-4" style="text-align: center;">
                                <input type="hidden" name="brands_logo" id="brands_logo">
                            <div class="frontsubmit-media-upload-form" id="drop-area">
                                <div id="brand-unuploaded">
                                    <p class="frontsubmit-drag-drop-info">Drop Files Here</p>
                                    <p>or</p>
                                    <p class="frontsubmit-drag-drop-buttons">
                                        <input type="file" name="brand_image" id="brand_image" class="inputfile" />
                                        <label for="brand_image">Choose a file</label>                                    
                                    </p>
                                    <p class="frontsubmit-max-upload-size"> Maximum upload file size: 2 MB.</p>
                                </div>
                                <div id="brand-uploaded"></div>                                
                            </div>
                            <div class="frontsubmit-object-actions">
                                <a href="javascript:void(0);" id="deleteBrand" class="frontsubmit-card-action frontsubmit-card-action-delete" title="Remove" style="display: none; right: 10px;"><i class="fa fa-times"></i></a>
                            </div>
                        </div>
                        </div>
                      <div class="row row-sm mb-4">
                         <div class="col-lg-12">
                              <div id="ajaxResponseDiv" style="bottom: 57px;color: #ff3a3a;"></div>
                        </div>
                         <div class="col-lg-4 mg-t-10">
                            <button type="submit" onclick="saveBrand()" class="btn btn-theme"><i class="material-icons"></i> Save</button>
                        </div>
                     </div>
                 </div>
             </div>
         </div>
