<style>
.product-info:hover{
    box-shadow: 0 2rem 4rem -1rem rgba(0,0,0,.5);
    -webkit-transition: all .55s ease-in-out;
    -o-transition: all .55s ease-in-out;
    transition: all .55s ease-in-out;
}
</style>
 <div class="container">
         <div class="az-content-breadcrumb mt-3">
                <span>Home</span>
                <span><?php echo $title;?></span>
            </div>
    </div>

    <div class="az-content pd-y-20 pd-lg-y-30 pd-xl-y-40">
        <div class="container">

            

            <div class="az-content-body pd-lg-l-40 d-flex flex-column">
                  <div class="row mb-3 mt-3">
                        <div class="col-md-6">
<!--                             <input type="search" class="form-control" placeholder="Search..."> -->
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary float-right"><a href="<?php echo base_url();?>admin/offers/create" class="text-white"><i class="fa fa-plus" aria-hidden="true"></i> ADD NEW OFFERS</a></button>
                        </div>
                    </div>

                <div class="row row-sm">
                   <?php
                        if(isset($offers)){
                            foreach($offers as $offer){
                   ?>
                    <div class="col-md-6 col-lg-3 mt-4" id="offer_<?php echo $offer->id;?>">
                        <div class="card bd text-center product-info">
                            <img class="img-fluid" src="<?php echo base_url();?><?php echo $offer->url;?>" alt="Image" style="width:100%;height: auto;">
                            <div class="card-body prdDetails">
                                <p class="card-text mb-1"><a href=""><?php echo $offer->title?></a></p>
                            </div>
                            <div class="row prdED">
                                <div class="col-md-6">
                                  <p><a href="<?php echo base_url();?>admin/offers/edit/<?php echo $offer->id;?>"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a></p>  
                                </div>
                                 <div class="col-md-6">
                                  <p><a href="javascript:void(0)" type="button" onclick="setOfferStatus(<?php echo $offer->id?>,'delete')" style="-webkit-appearance: none;"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a></p>  
                                </div>
                            </div>
                        </div>
                        <!-- card -->
                    </div>
                    <?php } } ?>
                    <!-- col-4 -->
        
                  
                </div>
            
             

                <div class="mg-lg-b-30"></div>

              