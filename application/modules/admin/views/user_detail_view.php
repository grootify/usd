<div class="az-content az-content-profile">
        <div class="container">
            <div class="az-content-breadcrumb mb-5">
                <span><a href="index.html">Home</a></span>
                <span><a href="<?php echo base_url('admin/users');?>"><?php echo $title;?></a></span>
                <span><?php echo $user_info->full_name;?></span>
            </div>
        </div>

        <div class="container mn-ht-100p">
            <div class="az-content-left az-content-left-profile">

                <div class="az-profile-overview">
                    <div class="az-img-user">
                        <img src="img/avatar.png" alt="">
                    </div>
                    <!-- az-img-user -->
                    <div class="d-flex justify-content-between mg-b-20">
                        <div>
                            <h5 class="az-profile-name"><?php echo $user_info->full_name;?></h5>
                        </div>
                    </div>
                    <hr class="mg-y-30">

                    <div class="az-content-label tx-13 mg-b-25">Contact Information</div>
                    <div class="az-profile-contact-list">
                        <div class="media">
                            <div class="media-icon"><i class="icon ion-md-phone-portrait"></i></div>
                            <div class="media-body">
                                <span>Mobile</span>
                                <div><?php echo $user_info->mobile;?></div>
                            </div>
                            <!-- media-body -->
                        </div>
                        <!-- media -->
                        <div class="media">
                            <div class="media-icon"><i class="icon ion-ios-mail"></i></div>
                            <div class="media-body">
                                <span>Email</span>
                                <div><?php echo $user_info->email;?></div>
                            </div>
                            <!-- media-body -->
                        </div>
                        <!-- media -->

                    </div>
                    <!-- az-profile-contact-list -->

                </div>
                <!-- az-profile-overview -->

            </div>
            <!-- az-content-left -->
            <div class="az-content-body az-content-body-profile">
                <nav class="nav nav-tabs az-nav-line">
                    <a class="nav-link active" data-toggle="tab" href="#tabCont1">Quotes</a>
                    <a class="nav-link" data-toggle="tab" href="#tabCont2">Addresses</a>
                    <a class="nav-link" data-toggle="tab" href="#tabCont3">Last login</a>
                </nav>

                <div class="card-body   tab-content">
                    <div id="tabCont1" class="tab-pane active show">
                        <div class="card card-table-one">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th class="wd-45p">Quote Id</th>
                                            <th>Date</th>
                                            <th>Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if(isset($user_quotes)){
                                            foreach($user_quotes as $quote){
                                            $s = $quote->quote_modified;
                                            $dt = new DateTime($s);
                                        ?>
                                        <tr>
                                            <td><a href="<?php echo base_url();?>admin/quotation_detail/<?php echo $quote->id;?>"><strong><?php echo $quote->id;?></strong></a></td>
                                           <td><?php echo $dt->format('d/m/Y');?></td>
                                            <td><i class="fa fa-dollar" aria-hidden="true"></i> <?php echo $quote->total;?></td>
                                        </tr>
                                        <?php } } ?>
                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- tab-pane -->
                    <div id="tabCont2" class="tab-pane">
                        <div class="row">
                            <?php if(isset($user_address)){
                                    foreach($user_address as $address){
                            ?>
                            <div class="col-md-4">
                                <div class="addressesCard">
                                    <h5><?php echo $address->contact_f_name;?>&nbsp;<?php echo $address->contact_l_name;?></h5>
                                    <p><?php echo $address->address_1;?>
                                        <br> <?php echo $address->city;?>(<?php echo $address->state;?>), <?php echo $address->postal_code;?></p>
                                    <p>Phone number: <?php echo $address->telephone;?></p>
                                </div>
                            </div>
                            <?php } } ?>
                        </div>
                    </div>
                    <div id="tabCont3" class="tab-pane">
                        <div class="card card-table-one">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th class="wd-45p">Date</th>
                                            <th>Time</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if(isset($last_login)){
                                                foreach($last_login as $login){
                                                    $s = $login->date_login;
                                                    $dt = new DateTime($s);
                                        ?>
                                        <tr>
                                            <td><strong><?php echo $dt->format('d/m/Y');?></strong></td>
                                            <td><?php echo $dt->format('H:i:s:a');?></td>
                                        </tr>
                                        <?php } } ?>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                    <!-- card-body -->

                </div>

            </div>
            <!-- container -->
        </div>
       