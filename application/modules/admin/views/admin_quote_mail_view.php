<style>
.table {
    color: #3b4863;
}
.table-bordered {
    border: 1px solid #cdd4e0;
}
.table {
    width: 100%;table {
    border-collapse: collapse;
}
table {
    border-collapse: collapse;
}
thead {
    display: table-header-group;
    vertical-align: middle;
    border-color: inherit;
}
.table .thead-dark th {
    color: #fff!important;
    background-color: #051c51!important;
    border-color: #ccc!important;
}
.table-bordered thead th, .table-bordered thead td {
    border-top-width: 1px!important;
    padding-top: 7px!important;
    padding-bottom: 7px!important;
    background-color: rgba(255, 255, 255, 0.5)!important;
}
.table thead th, .table thead td {
    color: #7987a1!important;
    font-weight: 700;
    font-size: 11px;
    letter-spacing: .5px;
    text-transform: uppercase;
    border-bottom-width: 1px;
    border-top-width: 0;
    padding: 0 15px 5px!important;
}
.table-bordered thead th, .table-bordered thead td {
    border-bottom-width: 2px;
}
.table thead th {
    vertical-align: bottom!important;
    border-bottom: 2px solid #cdd4e0!important;
}
.table th, .table td {
    padding: 9px 15px!important;
    line-height: 1.462!important;
}
.table-bordered th, .table-bordered td {
    border: 1px solid #cdd4e0!important;
}
tbody {
    display: table-row-group!important;
    vertical-align: middle!important;
    border-color: inherit!important;
}
.table tbody tr {
    background-color: rgba(255, 255, 255, 0.5);
}
.table th, .table td {
    padding: 9px 15px!important;
    line-height: 1.462!important;
}
.table-bordered th, .table-bordered td {
    border: 1px solid #cdd4e0!important;
}
.table th, .table td {
    padding: 0.75rem;
    vertical-align: top!important;
    border-top: 1px solid #cdd4e0;
}
</style>
<table  width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-left:20px; margin-right:20px; border-left: 1px solid #dbd9d9; border-right: 1px solid #dbd9d9; border-top:1px solid #dbd9d9;">
        <tbody>
            <tr>
                <td align="center" style="line-height: 0px;">
                    <img style="display:block; line-height:0px; font-size:0px; border:0px;" src="<?php echo base_url();?>assets/images/logo.png" width="250" height="80" alt="logo">
                </td>
            </tr>

            <tr>
                <td height="20"></td>
            </tr>

            <tr>
                <td align="center" style="font-size:22px; font-weight: bold; color:#2a3a4b;">Quotation Acknowledged</td>
            </tr>                  
           
			 <tr>
                <td style="text-align: left; font-size:14px; color:#000; line-height:20px; font-weight: 300;padding: 12px 48px; font-weight: 500;">Hi <?php if(isset($quotation_detail)){ echo $quotation_detail->full_name; } ?> , your quotation has been acknowledged.</td>
            </tr>
	    </tbody>
</table>
           
<table  width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:'Roboto',sans-serif">
	<tbody>
	<tr>
			<td align="left">
					<table align="center" class="col-180" width="180" border="0" cellspacing="0" cellpadding="0" style="">
						<tbody>
							   <tr>
								   <td>
									   <div style="margin-bottom: 20px;display: block;position: relative;    border-radius: 100%;width:100%;">
										   <img style="display:block; line-height:0px; font-size:0px; border:0px;" src="<?php echo base_url();?>assets/images/order.png" width="120" height="100" alt="quote">
									    </div>
								   </td>
							   </tr>
								<tr>
									<td>
										<div style="margin-bottom: 15px;justify-content: space-between !important;display: flex !important;width:100%;">
					                       <div>
					                            <h5 style="font-size: 14px;color: #1c273c;font-weight: 700;    margin-top: 0;" class="az-profile-name az-order-id ">Quote No: <?php echo $quotation_detail->quote_no;?></h5>
					                        </div>	
					                  	</div>
		                    			<hr class="mg-y-30">
									</td>
								</tr>
								<tr>
									<td>
										<div style="width:100%;font-size: 13px;margin-bottom: 15px;color: #1c273c;font-weight: 800;text-transform: uppercase;line-height: 1;letter-spacing: .2px;margin-top: 9px;">Other Information</div>
									</td>
								</tr>
								<tr>
									<td>
										<div style="align-items: center;display: flex;">
	                    					<div>
	                    						Invoice No: <strong><?php echo $quotation_detail->invoice_no;?></strong>
	                    					</div>
	                    				</div>
									</td>
								</tr>
								<tr>
									<td>
										<div style="margin-top: 17px;align-items: center;display: flex;">
										<?php 
											$this->load->helper('utilities');
											$s = $quotation_detail->quote_modified;
											$dt = new DateTime($s);
										?>
	                    					<div>
	                    						Quotation Date: <strong><?php echo datify($quotation_detail->quote_modified);?>| <?php echo $dt->format('H:i');?></strong>
	                    					</div>
	                    				</div>
									</td>
								</tr>
						</tbody>
					</table>
			</td>
			<td align="right">
				<table align="center" class="col-690" width="690" border="0" cellspacing="0" cellpadding="0" style="flex: 1;padding-left: 5px!important;margin-top:0px!important;border-left: 1px solid #dbd9d9;">
					<tbody>
						<tr>
							<h4 style="font-size: 1.3125rem;margin-bottom: 0.5rem;font-weight: 400;line-height: 1.2;">Address</h4>
						</tr>
						<tr>
							<table class="table table-bordered table-hover" style="width:100%;border:1px solid #ccc;">
								<thead class="thead-dark" style="background: #051c51;text-align: center;color:#fff;">
									<tr>
	                                    <th valign="middle">Name</th>
	                                    <th valign="middle">Email</th>
	                                    <th>Mobile</th>
	                                    <th>Address</th>
	                                    <th>Location</th>
	                                    <th>Pincode</th>
                               		 </tr>
								</thead>
								<tbody>
									<tr>
										<td style="padding: 7px 13px!important;"><?php echo $address_detail->contact_f_name;?>&nbsp;<?php echo $address_detail->contact_l_name;?></td>
										<td style="padding: 7px 13px!important;"><?php echo $address_detail->email;?></td>
										<td style="padding: 7px 13px!important;"><?php echo $address_detail->telephone;?></td>
										<td style="padding: 7px 13px!important;"><?php echo $address_detail->address_1;?></td>
										<td style="padding: 7px 13px!important;"><?php echo $address_detail->city;?>, <?php echo $address_detail->state;?></td>
										<td style="padding: 7px 13px!important;"><?php echo $address_detail->postal_code;?></td>
									</tr>
								</tbody>
							</table>
						</tr>
						<!-- quote product start-->
						<tr>
							<h4 style="font-size: 1.3125rem;margin-bottom: 0.5rem;font-weight: 400;line-height: 1.2;">Products</h4>
						</tr>
						<tr>
							<table class="table table-bordered table-hover" style="width:100%;border:1px solid #ccc;">
								<thead class="thead-dark" style="background: #051c51;text-align: center;color:#fff;">
									<tr>
										<th>Product Id</th>
										<th>Name</th>
										<th>SKU</th>
										<th>Part No.</th>
										<th>Quantity</th>
										<th>Amount</th>
                               		 </tr>
								</thead>
								<tbody>
									<?php 
	                                    if(isset($quote_product_item)){
	                                    foreach($quote_product_item as $product){
                               		 ?>
	                                <tr>
	                                    <td style="padding: 7px 13px!important;"><?php echo $product->product_id;?></td>
	                                    <td style="padding: 7px 13px!important;"><?php echo $product->title;?></td>
	                                    <td style="padding: 7px 13px!important;"><?php echo $product->sku_number;?></td>
	                                    <td style="padding: 7px 13px!important;"><?php echo $product->part_no;?></td>
	                                    <td style="padding: 7px 13px!important;"><?php echo $product->quantity;?></td>
	                                    <td style="padding: 7px 13px!important;"><?php echo $product->total;?></td>
	                                 </tr>
                                	<?php } } ?>
								</tbody>
							</table>
						</tr>
						<!-- quote product end -->
						<!-- custom quote start -->
						<tr>
							<h4 style="font-size: 1.3125rem;margin-bottom: 0.5rem;font-weight: 400;line-height: 1.2;">Custom Products</h4>
						</tr>
						<tr>
							<table class="table table-bordered table-hover" style="width:100%;border:1px solid #ccc;">
								<thead class="thead-dark" style="background: #051c51;text-align: center;color:#fff;">
									<tr>
										<th>Custom Id</th>
										<th>Item Name</th>
										<th>Quantity</th>
										<th>Amount</th>
                               		 </tr>
								</thead>
								<tbody>
									<?php 
										if(isset($custom_quote_item)){
										foreach($custom_quote_item as $custom){
									?>
	                                <tr>
	                                    <td style="padding: 7px 13px!important;"><?php echo $custom->id;?></td>
	                                    <td style="padding: 7px 13px!important;"><?php echo $custom->name;?></td>
	                                    <td style="padding: 7px 13px!important;"><?php echo $custom->quantity;?></td>
	                                    <td style="padding: 7px 13px!important;"><?php echo $custom->total;?></td>
	                                </tr>
                               		<?php } } ?>
								</tbody>
							</table>
						</tr>
						<!-- custom quote end -->
						<!-- attachment quote start -->
						<tr>
							<h4 style="font-size: 1.3125rem;margin-bottom: 0.5rem;font-weight: 400;line-height: 1.2;">Attachment Products</h4>
						</tr>
						<tr>
							<table class="table table-bordered table-hover" style="width:100%;border:1px solid #ccc;">
								<thead class="thead-dark" style="background: #051c51;text-align: center;color:#fff;">
									<tr>
										<th>Id</th>
										<th>Item Name</th>
										<th>Quantity</th>
										<th>Amount</th>
                               		 </tr>
								</thead>
								<tbody>
									<?php 
	                                    if(isset($attachment_quote_item)){
	                                    foreach($attachment_quote_item as $attach){
                                	?>  
                                <tr>
                                    <td style="padding: 7px 13px!important;"><?php echo $attach->id;?></td>
                                    <td style="padding: 7px 13px!important;"><?php echo $attach->item_name;?></td>
                                    <td style="padding: 7px 13px!important;"><?php echo $attach->quantity;?></td>
                                    <td style="padding: 7px 13px!important;"><?php echo $attach->total;?></td>
                                  
                                </tr>
                                <?php } } ?>
								</tbody>
							</table>
						</tr>
						<!-- attachment quote end -->
						<!-- total -->

						<tr>
							<table  class="table table-bordered table-hover" style="width:100%;border:1px solid #ccc;">
								<tbody>
									<tr>
										<td style="text-align: center;">TOTAL</td>
										<td>$<?php echo $quotation_detail->total;?></td>
									</tr>
								</tbody>
							</table>
							
						</tr>
						
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>