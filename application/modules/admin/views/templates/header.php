<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Meta -->
    <meta name="description" content="">
    <meta name="author" content="">
    <title>USD</title>
    <!-- vendor css -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/admin/ionicons/css/ionicons.min.css" rel="stylesheet">
    <!-- <link href="<?php echo base_url();?>assets/admin/css/typicons.css" rel="stylesheet"> -->
    <link href="<?php echo base_url();?>assets/admin/css/flag-icon.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons" />
     <link href="<?php echo base_url();?>assets/admin/css/dataTables.material.min.css" rel="stylesheet" />
    <!-- azia CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/css/style.css?v=1.0">
</head>
<style>
 .inStock{
    display: block;
    position: fixed;
    bottom: 34px;
    width: 300px;
    text-align: center;
    right: 30px;
    z-index: 999;
    
}
</style>
<body>
    <div class="alert alert-danger inStock" id="response" style="display: none"></div>
    <div class="az-header">
        <div class="container">
            <div class="az-header-left">
                <a href="<?php echo base_url();?>admin" class="navbar-brand"><span></span> USD</a>
                <a href="#" id="azMenuShow" class="az-header-menu-icon d-lg-none"><span></span></a>
            </div>
            <!-- az-header-left -->
            <div class="az-header-menu">
                <div class="az-header-menu-header">
                    <a href="<?php echo base_url();?>admin" class="az-logo"><span></span> <img src="<?php echo base_url();?>assets/images/logo.png" alt="AK Global" /></a>
                    <a href="#" class="close">&times;</a>
                </div>
                <!-- az-header-menu-header -->
                <ul class="nav">
                    <li class="nav-item <?php echo $title=='users'?'active':''?>">
                        <a href="<?php echo base_url('admin/users')?>" class="nav-link">Users</a>
                    </li>
                     <li class="nav-item <?php echo $title=='quotations'?'active':''?>">
                        <a href="<?php echo base_url();?>admin/quotations" class="nav-link">Orders</a>
                   <!--  </li>
                      <li class="nav-item <?php echo $title=='covers'?'active':''?>">
                        <a href="<?php echo base_url('admin/covers')?>" class="nav-link">Covers</a>
                    </li> -->
                     <!-- <li class="nav-item <?php echo $title=='offers'?'active':''?>">
                        <a href="<?php echo base_url('admin/offers')?>" class="nav-link">Offers</a>
                    </li> -->
                    <!-- <li class="nav-item <?php echo $title=='catalogs'?'active':''?>">
                        <a href="<?php echo base_url('admin/catalogs')?>" class="nav-link">Catalogs</a>
                    </li> -->
                    <li class="nav-item <?php echo $title=='brands'?'active':''?>">
                        <a href="<?php echo base_url('admin/brands')?>" class="nav-link">Brands</a>
                    </li>
                    <li class="nav-item <?php echo $title=='products'?'active':''?>">
                        <a href="<?php echo base_url('admin/products');?>" class="nav-link">Products</a>
                    </li>
                   
                </ul>
            </div>
            <!-- az-header-menu -->
            <div class="az-header-right">
                <div class="dropdown az-profile-menu">
                    <a href="#" class="az-img-user"><img src="<?php echo base_url();?>assets/images/avatar.png" alt=""></a>
                    <div class="dropdown-menu">
                        <div class="az-dropdown-header d-sm-none">
                            <a href="#" class="az-header-arrow"><i class="icon ion-md-arrow-back"></i></a>
                        </div>
                        <div class="az-header-profile">
                            <div class="az-img-user">
                                <img src="<?php echo base_url();?>assets/images/avatar.png" alt="">
                            </div>
                            <!-- az-img-user -->
                            <h6>Admin</h6>
                            <!-- <span>Premium Member</span> -->
                        </div>
                        <!-- az-header-profile -->

                      <!--   <a href="#" class="dropdown-item"><i class="typcn typcn-user-outline"></i> My Profile</a>
                        <a href="#" class="dropdown-item"><i class="typcn typcn-edit"></i> Edit Profile</a>
                        <a href="#" class="dropdown-item"><i class="typcn typcn-time"></i> Activity Logs</a>
                        <a href="#" class="dropdown-item"><i class="typcn typcn-cog-outline"></i> Account Settings</a> -->
                        <a href="<?php echo base_url('admin/logout');?>" class="dropdown-item"><i class="typcn typcn-power-outline"></i> Sign Out</a>
                    </div>
                    <!-- dropdown-menu -->
                </div>
            </div>
            <!-- az-header-right -->
        </div>
        <!-- container -->
    </div>
    <!-- az-header -->