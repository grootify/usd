     <div class="az-footer ht-40">
            <div class="container ht-100p pd-t-0-f">
                <span>&copy; 2019 AK Global</span>
            </div>
            <!-- container -->
        </div>
        <!-- az-footer -->
    <script type="text/javascript">
        var BASE_URL = "<?php echo base_url(); ?>";
    </script>
    <script src="<?php echo base_url();?>assets/admin/vendors/jquery-3.1.1.min.js" type="text/javascript"></script>
    
    <script src="<?php echo base_url();?>assets/admin/vendors/jquery-ui.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/admin/vendors/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/admin/vendors/material.min.js" type="text/javascript"></script>


    <!-- Forms Validations Plugin -->
    <script src="<?php echo base_url();?>assets/admin/vendors/jquery.validate.min.js"></script>

    <!--  DataTables.net Plugin    -->
    <script src="<?php echo base_url();?>assets/admin/vendors/jquery.datatables.min.js"></script>  
    <script src="<?php echo base_url();?>assets/admin/vendors/dataTables.material.min.js"></script>  
    <!-- <script src="<?php echo base_url();?>assets/admin/js/tinymce/tinymce.min.js"></script> -->

    <script src="<?php echo base_url();?>assets/admin/js/admin.js?v=1.0"></script>
    <script src="<?php echo base_url();?>assets/admin/vendors/material.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/admin/vendors/dataTables.material.min.js"></script>  
    <script src="<?php echo base_url();?>assets/admin/js/dataTables.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>assets/admin/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url();?>assets/admin/js/responsive.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>assets/admin/js/select2.min.js"></script>
    
    <script src="<?php echo base_url();?>assets/admin/js/main.js?v=1.0"></script>
</body>

</html>