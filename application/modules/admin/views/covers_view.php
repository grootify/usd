 <style>
.product-info:hover{
    box-shadow: 0 2rem 4rem -1rem rgba(0,0,0,.5);
    -webkit-transition: all .55s ease-in-out;
    -o-transition: all .55s ease-in-out;
    transition: all .55s ease-in-out;
}
</style>
 <div class="container">
         <div class="az-content-breadcrumb mt-3">
                <span>Home</span>
                <span><?php echo $title;?></span>
            </div>
    </div>

    <div class="az-content pd-y-20 pd-lg-y-30 pd-xl-y-40">
        <div class="container">

            

            <div class="az-content-body pd-lg-l-40 d-flex flex-column">
                  <div class="row mb-3 mt-3">
                        <div class="col-md-6">
<!--                             <input type="search" class="form-control" placeholder="Search..."> -->
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary float-right"><a href="<?php echo base_url();?>admin/covers/create" class="text-white"><i class="fa fa-plus" aria-hidden="true"></i> ADD NEW COVERS</a></button>
                        </div>
                    </div>

                <div class="row row-sm">
                   <?php
                        if(isset($covers)){
                            foreach($covers as $cover){
                   ?>
                    <div class="col-md-6 col-lg-3 mt-4" id="cover_<?php echo $cover->id;?>">
                        <div class="card bd text-center product-info">
                            <img class="img-fluid" src="<?php echo base_url();?><?php echo $cover->url;?>" alt="Image" style="width:100%;height: auto;">
                            <div class="card-body prdDetails">
                                <p class="card-text mb-1"><a href=""><?php echo $cover->title?></a></p>
                            </div>
                            <div class="row prdED">
                                <div class="col-md-6">
                                  <p><a href="<?php echo base_url();?>admin/covers/edit/<?php echo $cover->id;?>"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a></p>  
                                </div>
                                 <div class="col-md-6">
                                  <p><a href="javascript:void(0)" type="button" onclick="setCoverStatus(<?php echo $cover->id?>,'delete')" style="-webkit-appearance: none;"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a></p>  
                                </div>
                            </div>
                        </div>
                        <!-- card -->
                    </div>
                    <?php } } ?>
                    <!-- col-4 -->
        
                  
                </div>
            
             

                <div class="mg-lg-b-30"></div>

              