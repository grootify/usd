<style>
.product-info:hover{
    box-shadow: 0 2rem 4rem -1rem rgba(0,0,0,.5);
    -webkit-transition: all .55s ease-in-out;
    -o-transition: all .55s ease-in-out;
    transition: all .55s ease-in-out;
}
.current {
  color: green;
}

#pagin li {
  display: inline-block;
}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<div class="container">
    <div class="az-content-breadcrumb mt-3">
        <span><a href="<?php echo base_url();?>admin">Home</a></span>
        <span><?php echo $title;?></span>
    </div>
</div>

    <div class="">
        <div class="container">

           

            <div class=" d-flex flex-column">
                  <div class="row mb-3 mt-3">
                        <div class="col-md-6">
                            <!-- <input type="search" class="form-control" placeholder="Search..."> -->
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary float-right"><a href="<?php echo base_url();?>admin/products/create" class="text-white"><i class="fa fa-plus" aria-hidden="true"></i> ADD NEW PRODUCTS</a></button>
                        </div>
                    </div>

                <div class="row row-sm">
                   <?php
                        if(isset($products)){
                            foreach($products as $product){
                   ?>
                    <div class="col-md-6 col-lg-3 mt-4 " id="product_<?php echo $product->id;?>">
                        <div class="card bd text-center product-info" style="position: relative;">
                            <img class="img-fluid" src="<?php echo base_url();?><?php echo $product->url;?>" alt="Image Unavailable" style="width: 208;height:150px;">
                            <div class="card-body prdDetails">
                                <p class="card-text mb-1"><a href=""><?php echo $product->title?></a></p>
                            </div>
                            <div class="card-footer productAction">
                                <a class="btn btn-info" href="<?php echo base_url();?>admin/products/edit/<?php echo $product->id;?>"><i class="fa fa-pencil" aria-hidden="true"></i> </a>
                                
                                <?php if($product->status == 'inactive') { ?>
                                    <button title="Publish" class="btn btn-success" type="button" onclick="setProductStatus(<?php echo $product->id?>, this)" data-action="publish"><i class="fa fa-play-circle" aria-hidden="true"></i> </button>
                                <?php } else if($product->status == 'active') { ?>
                                    <button title="Unpublish" class="btn btn-warning" type="button" onclick="setProductStatus(<?php echo $product->id?>, this)"  data-action="unpublish"><i class="fa fa-pause-circle" aria-hidden="true"></i> </button>
                                <?php } ?>
                                    <button title="Delete" class="btn btn-danger" type="button" onclick="setProductStatus(<?php echo $product->id?>, this)"  data-action="delete"><i class="fa fa-trash" aria-hidden="true"></i> </button>
                                
                            </div>                            
                            
                            
                        </div>
                        <!-- card -->
                    </div>
                    <?php } } ?>
                    <!-- col-4 -->
        
                  
                </div>
            
             <div>
                <!-- pagination view start -->
                    <?php if(isset($products[0]->id)){
                        setPager($total_pages, $page_no);
                    } ?>
                    <!-- pagination view end -->
             </div>

                <div class="mg-lg-b-30"></div>

     <script type="text/javascript">
        pageSize = 8;
        showPage = function(page) {
            $(".line-content").hide();
            $(".line-content").each(function(n) {
                if (n >= pageSize * (page - 1) && n < pageSize * page)
                $(this).show();
            });        
        }
        showPage(1);
        $("#pagin li a").click(function() {
            $("#pagin li a").removeClass("current");
            $(this).addClass("current");
            showPage(parseInt($(this).text())) 
        });
    </script>  