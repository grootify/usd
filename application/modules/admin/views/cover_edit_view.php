
    <div class="container">
        <div class="az-content-breadcrumb mt-3">
            <span>Home</span>
            <span><?php echo $title?></span>
        </div>
    </div>

    <div class="az-content pd-y-20 pd-lg-y-30 pd-xl-y-40">
        <div class="container">
                <div class="az-content-body pd-lg-l-40 d-flex flex-column" id="coverEdit">
                    <h6 class="addprd_title">About Covers</h6>
                  <input id="cover-id" type="hidden" name="cover-id" value="<?php if(isset($cover_id)){ echo $cover_id; } ?>">
                    <div class="row row-sm mb-4">
                        <div class="col-lg-4">
                            <p class="mg-b-10">Cover Title</p>
                            <input class="form-control" id="title" placeholder="Enter Cover Title " type="text">
                        </div>
                     </div>

                     <div class="row row-sm mb-4 frontsubmit-upload" >
                        <div class="col-lg-4" style="text-align: center;">
                                <input type="hidden" name="coverImage" id="coverImage">
                            <div class="frontsubmit-media-upload-form" id="drop-area">
                                <div id="cover-unuploaded">
                                    <p class="frontsubmit-drag-drop-info">Drop Files Here</p>
                                    <p>or</p>
                                    <p class="frontsubmit-drag-drop-buttons">
                                        <input type="file" name="cover_image" id="cover_image" class="inputfile" />
                                        <label for="cover_image">Choose a file</label>                                    
                                    </p>
                                    <p class="frontsubmit-max-upload-size"> Maximum upload file size: 2 MB.</p>
                                </div>
                                <div id="cover-uploaded"></div>                                
                            </div>
                            <div class="frontsubmit-object-actions">
                                <a href="javascript:void(0);" id="deleteCover" class="frontsubmit-card-action frontsubmit-card-action-delete" title="Remove" style="display: none;">Remove Cover</a>
                            </div>
                        </div>

                        </div>
                      
                      <div class="row row-sm mb-4">
                         <div class="col-lg-12">
                              <div id="ajaxResponseDiv" style="bottom: 57px;color: #ff3a3a;"></div>
                        </div>
                         <div class="col-lg-4">
                            <button type="submit" onclick="saveCover()" class="btn btn-theme"><i class="material-icons"></i> Save</button>
                        </div>
                     </div>
                 </div>
             </div>
         </div>
