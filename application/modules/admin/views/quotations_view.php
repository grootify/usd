<style>
.current {
  color: green;
}
#pagin li {
  display: inline-block;
}
</style>
<div class="az-content pd-y-20 pd-lg-y-30 pd-xl-y-40">
        <div class="container">
            <div class="az-content-body pd-lg-l-40 d-flex flex-column">
                <div class="az-content-breadcrumb">
                    <span><a href="<?php echo base_url();?>">Home</a></span>
                    <span><?php echo $title;?></span>
                </div>

                <div class="row row-sm">
                    <div class="col-sm-6 col-lg-4 col-xl-4">
                        <div class="card card-body card-dashboard-fifteen">
                            <h1 class="mb-1"><?php echo '0'?></h1>
                            <label class="tx-primary">Total Orders</label>
                        </div>
                        <!-- card -->
                    </div>
                    <!-- col -->
                    <div class="col-sm-6 col-lg-4 col-xl-4 mg-t-20 mg-sm-t-20 mg-lg-t-0">
                        <div class="card card-body card-dashboard-fifteen">
                            <h1 class="mb-1"><?php echo $quotations_completed;?></h1>
                            <label class="tx-primary">Orders Completed</label>
                        </div>
                        <!-- card -->
                    </div>
                    <!-- col -->
                    <div class="col-sm-6 col-lg-4 col-xl-4 mg-t-20 mg-sm-t-0">
                        <div class="card card-body card-dashboard-fifteen">
                            <h1 class="mb-1"><?php echo $quotation_incompleted;?></h1>
                            <label class="tx-primary">Orders Pending</label>
                        </div>
                        <!-- card -->
                    </div>
                    <!-- col -->
                </div>
                <div>
                    <div class="row mb-3 mt-3">
                        <div class="col-md-9">

                        </div>
                       <!--  <div class="col-md-3">
                            <input type="search" class="form-control" placeholder="Search...">
                        </div> -->
                    </div>
                    <div class="card card-table-one">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th class="wd-30">Id</th>
                                        <th>Name</th>
                                        <th>Site</th>
                                        <th>Email</th>
                                        <th>Order No</th>
                                        <th>Invoice No</th>
                                        <th>Total</th>
                                        <th>Order Date</th>
                                        <th>Actions</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if(isset($quotations)){
                                        $i=1;
                                        foreach($quotations as $quotation){
                                    ?>
                                    <tr class=""line-content>
                                        <td><?php echo $i;?></td>
                                        <td><?php echo $quotation->full_name;?></td>
                                        <?php if ($quotation->site == 'akglobalmed') { ?>
                                            <td><?php echo 'akglobalmed'; ?></td>
                                        <?php } else { ?>
                                            <td><?php echo 'Netherlands' ?></td>
                                        <?php } ?>
                                        <td><?php echo $quotation->email;?></td>
                                        <td><a href="<?php echo base_url();?>admin/quotation_detail/<?php echo $quotation ->id;?>" title="View Details"><strong><?php echo $quotation ->quote_no;?></strong></a></td>
                                        <td><?php echo $quotation ->invoice_no;?></td>
                                        <td><?php echo $quotation ->total;?></td>
                                        <td><?php echo $quotation ->quote_modified;?></td>
                                        <td>
                                            <button class="btn btn-primary  block-btn"><a href="<?php echo base_url();?>admin/quotation_detail/<?php echo $quotation ->id;?>" class="text-white">Show Details </a></button>
                                        </td>
                                    </tr>
                                    <?php $i++;} } ?>
                                </tbody>
                            </table>

                        </div>
                        <!-- table-responsive -->

                    </div>

                    <div>
                   <ul class="pagination pagination-circled pull-right mt-3" id="pagin">
                        <li class="page-item"><a class="page-link" href="#"><i class="icon ion-ios-arrow-back"></i></a></li>
                        <?php 
                            $total=count($quotations)/8;
                            for($i=1;$i<=$total;$i++){
                        ?>
                             <li class="page-item  <?php echo $i=='1'? 'active current':''?>"><a class="page-link" href="#"><?php echo $i;?></a></li>
                        <?php } ?>
                     
                        <li class="page-item"><a class="page-link" href="#"><i class="icon ion-ios-arrow-forward"></i></a></li>
                    </ul>
                </div>

                <div class="ht-40"></div>
<script type="text/javascript">
        pageSize = 8;
        showPage = function(page) {
            $(".line-content").hide();
            $(".line-content").each(function(n) {
                if (n >= pageSize * (page - 1) && n < pageSize * page)
                $(this).show();
            });        
        }
        showPage(1);
        $("#pagin li a").click(function() {
            $("#pagin li a").removeClass("current");
            $(this).addClass("current");
            showPage(parseInt($(this).text())) 
        });
    </script>   