 <style type="text/css">
[data-role="dynamic-fields"] > .form-inline + .form-inline {
    margin-top: 0.5em;
}
[data-role="dynamic-fields"] > .form-inline [data-role="add"] {
    display: none;
}

[data-role="dynamic-fields"] > .form-inline:last-child [data-role="add"] {
    display: inline-block;
}
[data-role="dynamic-fields"] > .form-inline:last-child [data-role="remove"] {
    display: none;
}
</style>  
 <div class="az-content az-content-profile">
        <div class="container">
            <div class="az-content-breadcrumb mb-5">
                <span><a href="<?php echo base_url();?>">Home</a></span>
                <span><a href="<?php echo base_url('admin/Quotations');?>"><?php echo $title;?></a></span>
                <span>Quotations Details</span>
            </div>
        </div>

        <div class="container mn-ht-100p">
            <div class="az-content-left az-content-left-profile">

                <div class="az-profile-overview">
                    <div class="az-img-user">
                        <img src="<?php echo base_url();?>assets/images/order.png" alt="">
                    </div>
                    <!-- az-img-user -->
                    <div class="d-flex justify-content-between mg-b-20">
                        <div>
                            <h5 class="az-profile-name az-order-id ">Quote No: <?php echo $quotation_detail->quote_no;?></h5>
                        </div>
                    </div>
                    <hr class="mg-y-30">

                    <div class="az-content-label tx-14 mg-b-25">Other Information</div>
                    <div class="az-profile-contact-list">
                      <!--   <div class="media">
                            <div>Payment Id: <strong>pay_Ce6F0SPCD23EqO</strong></div>
                        </div> -->
                        <!-- media -->
                        <div class="media">
                            <div>Invoice No: <strong><?php echo $quotation_detail->invoice_no;?></strong></div>
                        </div>
                        <!-- media -->
                        <div class="media">
                            <?php 
                                 $this->load->helper('utilities');
                                 $s = $quotation_detail->quote_modified;
                                 $dt = new DateTime($s);
                            ?>
                            <div>Quotation Date: <strong><?php echo datify($quotation_detail->quote_modified);?>| <?php echo $dt->format('H:i');?></strong></div>
                        </div>
                        <!-- media -->
                    </div>
                    <!-- az-profile-contact-list -->

                </div>
                <!-- az-profile-overview -->

            </div>
            <!-- az-content-left -->
            <div class="az-content-body az-content-body-profile pl-3">

                <div class="row">
                    <div class="col col-lg-12">
                        <h4>Address</h4>
                        <table class="table table-bordered table-hover" style="text-align: left;">
                            <thead class="thead-dark">
                                <tr>
                                    <th valign="middle">Name</th>
                                    <th valign="middle">Email</th>
                                    <th>Mobile</th>
                                    <th>Address</th>
                                    <th>Location</th>
                                    <th>Pincode</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?php echo $address_detail->contact_f_name;?>&nbsp;<?php echo $address_detail->contact_l_name;?></td>
                                    <td><?php echo $address_detail->email;?></td>
                                    <td><?php echo $address_detail->telephone;?></td>
                                    <td><?php echo $address_detail->address_1;?></td>
                                    <td><?php echo $address_detail->city;?>, <?php echo $address_detail->state;?></td>
                                    <td><?php echo $address_detail->postal_code;?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row product_section">
                    <div class="col col-lg-12">
                        <h4>Products</h4>
                        <table class="table table-bordered table-hover centered quote_product">
                            <thead class="thead-dark">
                                <tr>
                                    <th>Product Id</th>
                                    <th>Name</th>
                                    <th>SKU</th>
                                    <th>Part No.</th>
                                    <th>Quantity</th>
                                    <th>Amount</th>
                                    <th>Update</th>
                                </tr>
                            </thead>
                            <tbody class="quote_data">
                                <?php 
                                    if(isset($quote_products)){
                                    foreach($quote_products as $product){
                                ?>
                                <tr>
                                    <td><?php echo $product->product_id;?></td>
                                    <td><?php echo $product->title;?></td>
                                    <td><?php echo $product->sku_number;?></td>
                                    <td><?php echo $product->part_no;?></td>
                                    <td><?php echo $product->quantity;?></td>
                                    <td><input type="text" id="amount_<?php echo $product->product_id?>" value="<?php echo $product->total;?>"></td>
                                    <td><button class="btn btn-primary" onclick="updateAmount(<?php echo $this->uri->segment('3')?>,<?php echo $product->id?>,<?php echo $product->product_id?>);">UPDATE</button></td>
                                </tr>
                                <?php } } ?>
                            </tbody>
                            <!-- <tfoot class="quote_foot">
                                <tr>
                                    <td colspan="5" style="text-align: center;">SUB TOTAL</td>
                                    <td>$<?php echo $quotation_detail->total;?></td>
                                </tr>
                            </tfoot> -->
                        </table>
                    </div>
                </div>
                 <div class="row custom_section">
                    <div class="col col-lg-12">
                        <h4>Custom Products</h4>
                        <table class="table table-bordered table-hover centered custom_product">
                            <thead class="thead-dark">
                                <tr>
                                    <th>Custom Id</th>
                                    <th>Item Name</th>
                                    <th>Quantity</th>
                                    <th>Amount</th>
                                    <th>Update</th>
                                </tr>
                            </thead>
                            <tbody class="custom_data">
                                <?php 
                                    if(isset($custom_products)){
                                    foreach($custom_products as $custom){
                                ?>
                                <tr>
                                    <td><?php echo $custom->id;?></td>
                                    <td><?php echo $custom->name;?></td>
                                    <td><?php echo $custom->quantity;?></td>
                                    <td><input type="text" id="custom_<?php echo $custom->id?>" value="<?php echo $custom->total;?>"></td>
                                    <td><button class="btn btn-primary" onclick="updateCustomProductAmount(<?php echo $this->uri->segment('3')?>,<?php echo $custom->id?>);">UPDATE</button></td>
                                </tr>
                                <?php } } ?>
                            </tbody>
                            <!-- <tfoot class="quote_foot">
                                <tr>
                                    <td colspan="3" style="text-align: center;">SUB TOTAL</td>
                                    <td>$<?php echo $quotation_detail->total;?></td>
                                </tr>
                            </tfoot> -->
                        </table>
                    </div>
                </div>
                  <div class="row attachment_section">
                    <div class="col col-lg-12">
                        <h4>Attachment</h4>
                        <table class="table table-bordered table-hover centered attachment_table">
                            <thead class="thead-dark">
                                <tr>
                                    <th>Attachment Id</th>
                                    <th>File</th>
                                </tr>
                            </thead>
                            <tbody class="attach_data">
                                <?php 
                                    if(isset($attachment_list)){
                                    foreach($attachment_list as $attach){
                                ?>
                                <tr>
                                    <td><?php echo $attach->id;?></td>
                                    <td><a href="<?php echo base_url();?><?php echo $attach->path;?>"download><img src="<?php echo base_url();?>assets/images/pdf.png" style="width:50px;height:40px;"></a></td>
                                </tr>
                                <?php } } ?>
                            </tbody>
                        </table>
                    </div>
                </div>  
                   <div class="row custom_section">
                    <div class="col col-lg-12">
                        <h4>Attachment Products</h4>
                        <table class="table table-bordered table-hover centered custom_product">
                            <thead class="thead-dark">
                                <tr>
                                    <th>Id</th>
                                    <th>Item Name</th>
                                    <th>Quantity</th>
                                    <th>Amount</th>
                                </tr>
                            </thead>
                            <tbody class="custom_data">
                                <?php 
                                    if(isset($attachment_data)){
                                    foreach($attachment_data as $attach){
                                ?>  
                                <tr>
                                    <td><?php echo $attach->id;?></td>
                                    <td><?php echo $attach->item_name;?></td>
                                    <td><?php echo $attach->quantity;?></td>
                                    <td><?php echo $attach->total;?></td>
                                  
                                </tr>
                                <?php } } ?>
                            </tbody>
                            <!-- <tfoot class="quote_foot">
                                <tr>
                                    <td colspan="3" style="text-align: center;">SUB TOTAL</td>
                                    <td>$<?php echo $quotation_detail->total;?></td>
                                </tr>
                            </tfoot> -->
                        </table>
                    </div>
                </div>
                  <div class="row custom_section">
                    <div class="col col-lg-12">
                        <table class="table table-bordered table-hover centered custom_product">
                            <tfoot class="quote_foot">
                                <tr>
                                    <td colspan="3" style="text-align: center;">TOTAL</td>
                                    <td>$<?php echo $quotation_detail->total;?></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

                <!-- end custom product -->
                 <div class="row row-sm mb-4 attachemnt_section frontsubmit-form-main" id="attachemnt_section">
                        <div class="col-lg-12 dynamic" data-role="dynamic-fields">
                            <div class="form-inline mt-4">
                            <div class="col-lg-3">
                                 <p class="mg-b-10">Item Name</p>
                                 <input class="form-control dimension item_name" name="item_name[]" id="item_name" placeholder="Enter Item Name " type="text" style="width:100%;">
                            </div>
                            
                            <div class="col-lg-3">
                                 <p class="mg-b-10">Quantity</p>
                                  <input class="form-control dimension quantity" name="quantity[]" id="quantity" placeholder="Enter Quantity" type="text" style="width:100%;">
                            </div>
                            <div class="col-lg-3">
                                 <p class="mg-b-10">Amount</p>
                                  <input class="form-control dimension attach_amount" name="attach_amount[]" id="attach_amount" placeholder="Enter Amount" type="text" style="width:100%;">
                            </div>
                            <div class="col-lg-1" style="margin-top: 23px;">
                                <button class="btn btn-primary" data-role="add">
                                     <i class="fa fa-plus"></i>
                                </button>
                            </div>
                            <div class="col-lg-1" style="margin-top: 23px;">
                                <button class="btn btn-danger" data-role="remove">
                                     <i class="fa fa-minus"></i>
                                </button>
                            </div>
                            
                            
                            </div>  
                        </div>  
                     </div>
                
                 <div class="row">
                    <div class="col-lg-6 text-right">
                        <button class="btn btn-primary" onclick="updateAttachmentQuotation(<?php echo $this->uri->segment('3')?>)"><i class="fa fa-file-text-o" aria-hidden="true"></i> UPDATE QUOTATION</button>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-lg-6 text-right">
                        <button class="btn btn-primary"><a href="<?php echo base_url();?>admin/quotations" class="text-white"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK TO ALL QUOTATIONS</a></button>
                    </div>
                     <div class="col-lg-6 text-left">
                        <button class="btn btn-primary" onclick="sendQuotation(<?php echo $this->uri->segment('3')?>)"><i class="fa fa-file-text-o" aria-hidden="true"></i> SUBMIT QUOTATION</button>
                    </div>
                </div>

            </div>
        </div>
    </div>



  