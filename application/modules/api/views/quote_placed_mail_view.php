<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:'Roboto',sans-serif">
    <tbody>
        <tr>
            <td align="center">
                <table class="col-600" width="600" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-left:20px; margin-right:20px; border-left: 1px solid #dbd9d9; border-right: 1px solid #dbd9d9; border-top:1px solid #dbd9d9;">
                    <tbody>
                        <tr>
                            <td align="center" style="line-height: 0px;">
                                <img style="display:block; line-height:0px; font-size:0px; border:0px;" src="<?php echo base_url();?>assets/images/logo.png" width="250" height="80" alt="logo">
                            </td>
                        </tr>

                        <tr>
                            <td height="20"></td>
                        </tr>

                        <tr>
                            <td align="center" style="font-size:22px; font-weight: bold; color:#2a3a4b;">Quotation Request</td>
                        </tr>                  
                       
						 <tr>
                            <td style="text-align: left; font-size:14px; color:#000; line-height:20px; font-weight: 300;padding: 12px 48px; font-weight: 500;">Hi <?php if(isset($userinfo)){ echo $userinfo->full_name; } ?>, we have received your request. We will share the quotation soon.</td>
                        </tr>
				    </tbody>
                </table>
            </td>
        </tr>

        <tr>
			<td align="center">
            	<table align="center" class="col-600" width="600" border="0" cellspacing="0" cellpadding="0" style="border-left: 1px solid #dbd9d9; border-right: 1px solid #dbd9d9;">
					<tbody>
						
					<tr>
						<td dir="ltr" style="padding:10px 48px;color:#fff;font-weight:400;text-align:left;font-size:16px; background-color: #00174e;" valign="top"> Quote Id: <?php echo $product_info[0]->quote_no;?>
						</td>
						<td dir="ltr" style="padding:10px 48px;color:#fff;font-weight:400;text-align:right;font-size:16px;background-color: #00174e;" valign="top"> Quote Date :
							<?php echo $product_info[0]->quote_modified;?>
						</td>
					</tr>	
					
										
					</tbody>
				</table>
			</td>
		</tr>

		<tr>
			<td align="center">
            	<table align="center" class="col-600" width="600" border="0" cellspacing="0" cellpadding="0" style="border-left: 1px solid #dbd9d9; border-right: 1px solid #dbd9d9;">
					<tbody>
					<tr>
						<td dir="ltr" width="60%" valign="top" style="padding:10px 48px;"> 
							<h3 style=" color:#000;text-align:left;font-size:14px;line-height:1.5rem;margin-bottom: 5px; font-weight: 600; text-transform: uppercase;">Customer Information</h3>
							<p style="font-size:14px;margin:0;color:#000;padding-bottom:5px"> <?php echo $addresses->contact_f_name ."&nbsp;".$addresses->contact_l_name; ?> </p>
							
							<a style="font-size:14px;margin:0;color:#ooo;padding-bottom:5px;display:block;"> <?php echo $addresses->telephone; ?> </a>
							
						</td>
						<td dir="ltr" style="padding:10px;color:#000;text-align:left;font-weight:300;font-size:12px;" valign="top"> 
							<h3 style=" color:#000;text-align:left;font-size:14px;line-height:1.5rem;margin-bottom: 5px; font-weight: 600; text-transform: uppercase;">Billing Address</h3>
							<p style="font-size:13px;margin:0;color:#000;padding-bottom:5px;font-weight:400; line-height: 20px;"> <?php echo $addresses->address_1 . ', '. $addresses->address_2 . ', '. $addresses->city . ', '. $addresses->state .', '.$addresses->postal_code; ?></p>
						</td>
					</tr>								
					</tbody>
				</table>
			</td>
		</tr>

        <tr>
            <td align="center">

            	<table align="center" class="col-600" width="600" border="0" cellspacing="0" cellpadding="0" style="border-left: 1px solid #dbd9d9; border-right: 1px solid #dbd9d9;">
					<tbody>
						<tr>
							<td dir="ltr" valign="top" style="font-size:15px;color:#000;border-top:solid thin #ddd;border-bottom:solid thin #ddd;padding:10px 10px">  </td>
							<td dir="ltr" valign="top" style="font-size:15px;color:#000;border-top:solid thin #ddd;border-bottom:solid thin #ddd;padding:10px 10px; width: 40%;"> Products Name </td>
							<td dir="ltr" valign="top" style="font-size:15px;color:#000;border-top:solid thin #ddd;border-bottom:solid thin #ddd;padding:10px 10px;text-align:center"> Qty </td>
							
						</tr>	
						
						<?php
							foreach ($product_info as $product) { 
						?>
						<tr>
							<td dir="ltr" valign="top" style="font-size:15px;color:#000;border-bottom:solid thin #ddd;padding:10px 10px">	<span style="display:inline-block;width:50px;text-align:center">
									<img src="<?php echo base_url() . $product->url;?>" style="width:auto;max-width:100%" tabindex="0">									
								</span> </td>
							<td dir="ltr" valign="top" style="font-size:15px;color:#000;border-bottom:solid thin #ddd;padding:15px 10px"> 
							
								<span style="display:inline-block;vertical-align:top;font-size:14px;padding:5px 5px"><?php echo $product->title;?></span>
							</td>
							<td dir="ltr" valign="top" style="font-size:15px;color:#000;border-bottom:solid thin #ddd;padding:15px 10px;text-align:center"> <?php echo $product->quantity;?></td>
							
						</tr>
						<?php } ?>
						</tbody>
				</table>

                          
            </td>
        </tr>

        <tr>
            <td align="center">
                <table class="col-1000" width="600" border="0" align="center" cellpadding="0" cellspacing="0" style="border-left: 1px solid #dbd9d9; border-right: 1px solid #dbd9d9;border-bottom:1px solid #dbd9d9;">

                    <tbody>
                        <tr>
                            <td height="20"></td>
                        </tr>
                             <tr align="center" valign="top">
                            <td>
                                <table class="button" style="border: 2px solid #e9955c;" bgcolor="#00174e" width="25%" border="0" cellpadding="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td width="10"></td>
                                            <td height="30" align="center" style="font-family: 'Open Sans', Arial, sans-serif; font-size:13px; color:#ffffff;">
                                                <a href="<?php echo base_url();?>my_quotation" style="color:#ffffff;text-decoration: none;font-weight: 500;">VIEW YOUR REQUEST</a>
                                            </td>
                                            <td width="10"></td>
                                        </tr>
                                     
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                           <tr>
                               
                                <h3 style="margin-bottom: 4px; font-size: 17px;">Thank you for visiting AK Global Med</h3>

                                <p style="margin: 8px 0; font-size: 13px;">We hope to see you again soon!</p>
                                
                                </td>

                            </tr>
                    </tbody>
                </table>
            </td>
        </tr>
</table>



                   
