<section class="wow fadeIn padding-ten-tb margin-70px-top cover-background background-position-top">    
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 display-table page-title-large">
                <div class="display-table-cell vertical-align-middle text-center padding-30px-tb">
					<p style="font-size: 16px;">Activation link has been sent to: <b><?php echo $this->input->cookie('__registered');?></b><br/>
					Please check your email and click on the link to activate your account.</p>
                </div>
            </div>
        </div>
    </div>
</section>