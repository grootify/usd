  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<section class="wow fadeIn padding-eight-top no-margin cover-background background-position-top">    
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 display-table page-title-large">
                <div class="display-table-cell vertical-align-middle text-center padding-30px-tb">
                    <h4>Confirm password reset</h4>
					<p style="font-size: 16px; margin: 0px;">Enter your new password below to reset your password:</p>
                    <div class="col-md-4 col-sm-6 col-xs-12 col-md-offset-4" style="text-align: left;">
                        <p class="center margin-10px-tb" style="font-size: 16px;"><em id="reset_response"></em></p>
                        <form id="resetForm" class="form-signin" action="javascript:void(0)" novalidate="novalidate">
                            <input type="hidden" id="reset_code" name="reset_code" value="<?php echo $user->password_reset_code; ?>" />
                            <input type="hidden" id="reset_email" name="reset_email" value="<?php echo $user->email; ?>" />

                            <div class="form-group loginGroup">
                                <label>New password: </label>
                                <input type="password" class="form-control loginForm-control" id="reset_password" placeholder="New Password" name="reset_password" required />
                            </div>
                            <div class="form-group loginGroup">
                                <label>Re-enter password: </label>
                                <input type="password" class="form-control loginForm-control" id="reset_confirm_password" placeholder="Re-enter password" name="reset_confirm_password" required />
                            </div>
                            <div class="form-group loginGroup recaptchaGroup margin-15px-top">
                                <div id="recaptcha-reset" class="recaptcha"></div>
                                <em id="recaptcha-reset-error" class="help-block recaptcha-error"></em>
                            </div>
                            <button class="btn btn-lg btn-block black-bg btnSubmit margin-5px-tb" type="submit" onclick="resetPassword();" >
                                Reset Password</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>