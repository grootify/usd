<section class="wow fadeIn padding-ten-tb margin-70px-top cover-background background-position-top">    
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 display-table page-title-large">
                <div class="display-table-cell vertical-align-middle text-center padding-30px-tb">
                    <h4>Error Verifying Email</h4>
					<p style="font-size: 16px;">You might have already verified your email or the verification link has expired.</p>
                </div>
            </div>
        </div>
    </div>
</section>