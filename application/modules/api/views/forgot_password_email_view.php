<div bgcolor="#fafbfc" style="margin:0;padding:0">
   <table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tbody>
         <tr>
            <td>
               <center style="width:100%;">
                  <div style="max-width:620px;font-size:0;margin:0 auto">
                     <table border="0" cellpadding="0" cellspacing="0" style="width:100%">
                        <tbody>
                           <tr>
                              <td>
                                 <table border="0" cellpadding="0" cellspacing="0" style="width:100%">
                                    <tbody>
                                       <tr>
                                          <td align="center" style="padding-bottom:20px">
                                             <table border="0" cellpadding="0" cellspacing="0">
                                                <tbody>
                                                   <tr>
                                                      <td style="padding:20px 0 10px 0">
                                                         <a href="<?php echo base_url(); ?>" style="text-decoration:none" target="_blank">
                                                         <img alt="AK Global" border="0" height="50" width="200" src="<?php echo base_url();?>assets/images/logo.png">
                                                         </a>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </td>
                                       </tr>
                                    </tbody>
                                 </table>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <table border="0" cellpadding="0" cellspacing="0" style="width:100%; border:1px solid #eee; box-shadow: 0px 0px 8px #ccc; background-color: #fff;" bgcolor="#fff">
                                    <tbody>
                                       <tr>
                                          <td>
                                             <table border="0" cellpadding="0" cellspacing="0" style="width:100%">
                                                <tbody>
                                                   <tr>
                                                      <td style="text-align:center;padding:40px 40px 40px 40px;">
                                                         <div style="display:inline-block;width:100%;max-width:520px">
                                                            <table border="0" cellpadding="0" cellspacing="0" style="font-family:'Open+Sans','Open Sans',Helvetica,Arial,sans-serif;font-size:14px;line-height:24px;color:#525c65;text-align:left;width:100%">
                                                               <tbody>
                                                                  <tr>
                                                                     <td>
                                                                        <p style="margin:0;font-size:18px;line-height:23px;color:#102231;font-weight:bold">
                                                                           <strong>Hi <?php echo $name; ?>,</strong>
                                                                           <br><br>
                                                                        </p>
                                                                     </td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td>
                                                                        You are receiving this email because you have requested to reset your password. Please click the following to reset your password:
                                                                        <br><br>
                                                                     </td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td align="center" style="padding:15px 0 40px 0;">
                                                                        <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate!important;border-radius:15px;width:210px">
                                                                           <tbody>
                                                                              <tr>
                                                                                 <td align="center" valign="top">
                                                                                    <a href="<?php echo $verification_link; ?>" style="background-color: #0066d2; border-radius: 4px; color: #ffffff; display: inline-block; font-size: 14px; font-weight: bold; text-align: center; text-decoration: none; letter-spacing: 1px; padding: 10px 20px;" target="_blank">RESET PASSWORD</a>
                                                                                 </td>
                                                                              </tr>
                                                                           </tbody>
                                                                        </table>
                                                                     </td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td>
                                                                        <p style="font:14px/16px Arial,Helvetica,sans-serif;color:#363636;padding:0 0 14px;">
                                                                           If you do not wish to reset your password, please delete this email. <br>
                                                                        </p>
                                                                     </td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td style="font:14px/16px Arial,Helvetica,sans-serif;color:#363636;padding:0 0 14px">
                                                                        Cheers,
                                                                     </td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td style="font:bold 14px/16px Arial,Helvetica,sans-serif;color:#363636;padding:0 0 7px">
                                                                        The AK Global Team
                                                                     </td>
                                                                  </tr>
                                                               </tbody>
                                                            </table>
                                                         </div>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </td>
                                       </tr>
                                    </tbody>
                                 </table>
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
               </center>
            </td>
         </tr>
      </tbody>
   </table>
</div>