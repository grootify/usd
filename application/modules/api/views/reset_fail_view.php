<section class="wow fadeIn padding-ten-tb margin-70px-top cover-background background-position-top">    
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 display-table page-title-large">
                <div class="display-table-cell vertical-align-middle text-center padding-30px-tb">
                    <h4>Error In Reset Password</h4>
					<p style="font-size: 16px;">Unable to reset password due to an unknown error. <br/> You might have already reset your password or the verification link has expired.</p>
                </div>
            </div>
        </div>
    </div>
</section>