<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH.'libraries/REST_Controller.php');

Class Auth extends REST_Controller 
{
    public function __construct() {
        parent::__construct();
        
        $this->user = array(); 

        $this->load->model('standard_model');
        
    }

    // Check for user login process
    public function login_post() {
        // $recaptchaResponse = trim($this->input->post('g-recaptcha-response'));
        // $recaptcha_verified = $this->validate_recaptcha($recaptchaResponse);

        // if( !$recaptcha_verified['status'] ) {
        //     $this->response($recaptcha_verified);
        // }

        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $remember_me = $this->input->post('remember-me');

        $update_arr['__xdt'] = bin2hex(openssl_random_pseudo_bytes(12));

        if (empty($email)) {
            $this->response(array('status' => false, 'message' => 'Invalid Email!'));
        } else if (empty($password)) {
            $this->response(array('status' => false, 'message' => 'Invalid Password!'));
        } else {
            $user = $this->login_validate($email, $password);
            if (isset($user->id)) {                
                $update_arr['id'] = $user->id;
                $update_arr['name'] = $user->name;
                // $update_arr['full_name'] = $user->f_name;
                 $update_arr['email'] = $user->email;
                 $update_arr['mobile'] = $user->mobile;
                 $update_arr['remember_me'] = $remember_me;

                // if($user->is_email_verified == 0) {
                //     // $this->send_verification_email($user->email);
                //     $this->response(array('status' => true, 'name' => $user->name, 'otp_pending' => 1));
                // }

                if($user->is_email_verified != "1") {
                    $this->response(array('status' => false, 'message' => 'You have not verified your email.', 'user_status' => 'pending'));
                }
                
                if($this->update_token($update_arr)) {
                    // cloud session token
                    $cst = $this->generate_JWT_token($update_arr);
                    if($cst){
                        $quote_id='';
                        $check_quote=$this->checkQuote($user->id);
                        if($check_quote['status']!=''){
                            $quote_id=$check_quote['quote_id'];
                        }else{
                            $quote_id=$this->addQuoteFromCookie($user->id);
                        }
                        if($this->input->cookie('cpID')){
                            $cookieList= $this->input->cookie('cpID');
                            $index = 0;
                            $productArry=explode(',',$cookieList);
                            foreach ($productArry as $value) {
                                 $cart_data[$index++]=explode(':',$value);
                            }
                            $index = 0;
                            $productArray=[];
                            foreach ($cart_data as $cart) {
                                $product_id[$index++] = $cart[0];
                                $cart_qty[$index++] = $cart[1];
                                array_push($productArray,$cart); 
                               
                            } 
                            $result=$this->addQuoteProductFromCookie($quote_id,$productArray);
                        }

                          if ($this->input->cookie('customQuotes')) {
                            $cookieList= $this->input->cookie('customQuotes');
                            $index = 0;
                            $customArry=explode(',',$cookieList);
                            foreach ($customArry as $value) {
                                 $custom_data[$index++]=explode(':',$value);
                            }
                            $index = 0;
                            $customArray=[];
                            foreach ($custom_data as $custom) {
                                $item[$index++] = $custom[0];
                                $qty[$index++] = $custom[1];
                                array_push($customArray,$custom); 
                            } 
                         
                            $result=$this->addCustomQuoteFromCookie($quote_id,$customArray);
                           
                            if($this->input->cookie('media')){
                                    $mediaList= $this->input->cookie('media');
                                    $index = 0;
                                    $mediaArry=explode(',',$mediaList);

                                    foreach ($mediaArry as $value) {
                                        $media_data[$index++]=explode(':',$value);
                                    }
                                    $index = 0;
                                    $mediaArray=[];
                                  
                                    foreach ($media_data as $media) {
                                        $attach[$index++] = $media[0];
                                        array_push($mediaArray,$media); 
                                     } 


                                    $result=$this->addQuoteAttachmentFromCookie($quote_id,$mediaArray);
                                 
                            }
                        }
                        
                         
                    }

                    $this->response(array('status' => true, 'cst' => $cst));
                } else {
                    $this->response(array('status' => false, 'message' => 'Try Again!'));
                }
                
            } else {
                $this->response(array('status' => false, 'message' => 'Username or Password not valid!'));
            }
        }        
    }
    private function checkQuote($user_id){
        $data['table']='quotes';
        $data['field'] = 'quotes.id';
       
         $data['condition'] = array(
            "quotes.user_id"=>$user_id,
            "quotes.order_status_id"=>1,
            "quotes.in_process"=>1
            
        );
        $data['order_by'] = array( 'quotes.id' => 'DESC' );
        $this->standard_model->set_query_data($data);
        $result = $this->standard_model->select();
        if($result){
            return array('status' => true,'quote_id' => $result->id);
        }else{
            return array('status' => false);
        }
    }
    private function getUserName($user_id=''){

        $data['table']='users';
        $data['field'] = 'full_name';
        $data['condition'] = array(
            'id' => $user_id,
            'user_status'=>'active'
            );
        
        $data['order_by'] = array( 'id' => 'DESC' );
        $this->standard_model->set_query_data($data);
        $result = $this->standard_model->select();
        if($result){
           return $result;
        }else{
           return $result;
        }
    }
    private function getCustomerAddress($user_id){
        $data['table']='address';
        $data['field'] = 'address.contact_f_name,address.contact_l_name,address.telephone,address.address_1,address.address_2,address.city,address.state,address.postal_code';
        $data['condition'] = array(
            'user_id' => $user_id,
            'is_default'=>'1',
            'is_active'=>'1',
            );
        
        $data['order_by'] = array( 'id' => 'DESC' );
        $this->standard_model->set_query_data($data);
        $result = $this->standard_model->select();
        if($result){
           return $result;
        }else{
           return $result;
        }
    }
    private function getCustomerOrderedProduct($user_id,$quote_id){
        $data['table']='quotes';
        $data['field'] = 'quotes.quote_no,quotes.quote_modified,quote_products.product_id,quote_products.quantity,product_list.title,product_list.url';
        $data['join']=array(
            "quote_products"=>'quotes.id=quote_products.quote_id',
            "product_list"=>'quote_products.product_id=product_list.id'
        );
         $data['condition'] = array(
            "quotes.user_id"=>$user_id,
            "quotes.id"=>$quote_id,
            "quote_products.quote_id"=>$quote_id,
            "quotes.in_process"=>0
            
        );
        $data['order_by'] = array( 'quotes.id' => 'DESC' );
        $this->standard_model->set_query_data($data);
        $result = $this->standard_model->select();
        if(sizeof($result)>0){
            if(is_object($result)) {
                $result = array($result);
            }
               return $result;
        }else{
             return array();
        }
    }
    public function sendQuotationInMail_post(){
        $email_to=$this->input->post('email');
        $user_id=$this->input->post('user_id');
        $quote_id=$this->input->post('quote_id');

        $view_data['userinfo']=$this->getUserName($user_id);
        $view_data['addresses']=$this->getCustomerAddress($user_id);
        $view_data['product_info']=$this->getCustomerOrderedProduct($user_id,$quote_id);
        $msg = $this->load->view('quote_placed_mail_view', $view_data, true);
        
        try{                
            if ($this->send_email('Your order has been confirmed', $msg, $email_to)) {
                return array('status' => true, 'message' => 'Mail sent successfully!');
            } else {
                return array('status' => false, 'message' => 'Error while sending mail!');
            }
        } catch(Exception $e){
            return array('status' => false , 'message'=>  "Try after some time!");
        }
    }
    private function addQuoteFromCookie($id){
        $quote_no=rand(1,1000);
        $invoice_no=rand();
        $data['quote_no']=$quote_no;
        $data['user_id']=$id;
        $data['invoice_no']=$invoice_no;
        $data['address_id']='0';
        $data['total']='0';
        $data['shping_fee']='0';
        $data['order_status_id']='1';
        $data['is_active']='1';
        $data['in_process']='1';
        $data['quote_created']=date('Y-m-d H:i:s');
       // $data['quote_modified']= '0000-00-00 00:00:00';
        $data_query['table'] = 'quotes';
            
        $this->standard_model->set_query_data($data_query);
        $quote_id = $this->standard_model->insert_and_id($data);
        if($id){
             return $quote_id;
        }
    }
    private function addQuoteProductFromCookie($quote_id,$productArray){
      
          for($i=0;$i<count($productArray);$i++){
                    $data['quote_id']=$quote_id;
                    $data['product_id']=$productArray[$i][0];
                    $data['quantity']=$productArray[$i][1];
                    $data['total']='0';
                    $data['shiping_fee']='0';
                    $data['is_active']='1';
                    $data['created_date']=date('Y-m-d H:i:s');
                    $data_query['table'] = 'quote_products';

                    $this->standard_model->set_query_data($data_query);
                    $id = $this->standard_model->insert_and_id($data);
                   
          }
          $result="true";
          return $result;
     
    }
    private function addCustomQuoteFromCookie($quote_id,$customArray){
        for($i=0;$i<count($customArray);$i++){
                    $data['quote_id']=$quote_id;
                    $data['name']=$customArray[$i][0];
                    $data['quantity']=$customArray[$i][1];
                    $data['status']='1';
                    $data['date_created']=date('Y-m-d H:i:s');
                    $data_query['table'] = 'custom_quote';

                    $this->standard_model->set_query_data($data_query);
                    $id = $this->standard_model->insert_and_id($data);
                   
          }
          $result="true";
          return $result;
    }
    private function addQuoteAttachmentFromCookie($quote_id,$mediaArray){
            for($i=0;$i<count($mediaArray);$i++){
                    $data['quote_id']=$quote_id;
                    $data['date_modified']=date('Y-m-d H:i:s');
                    $data_query['table'] = 'quote_attachment';
                    $data_query['condition']=array(
                        "id"=>$mediaArray[$i][0]
                    );
                    $this->standard_model->set_query_data($data_query);
                    $this->standard_model->update($data);
                   
            }
          $result="true";
          return $result;
    }

    private function login_validate($email, $password)
    {
        $this->load->library('Bcrypt');
        
        $data['table']  = 'users';
        $data['field']  = 'id, full_name as name, email,mobile, password, user_status as status, is_email_verified';
        $data['condition'] = array(
            'email' => $email
        );
        $data['limit'] = 1;
        $this->standard_model->set_query_data($data);
        $result = $this->standard_model->select();  

        if (isset($result->password)) {
            if ($this->bcrypt->check_password($password, $result->password)) {
                return $result;                
            } else {
                return false;
            }
        } else {
            return false;
        }
    }


    public function resend_activation_post(){
        $email_to = $this->input->post('email');
        if($email_to) {
            $res = $this->send_verification_email($email_to);
            $this->response($res);
        } else {
            $this->response(array('status' => false, 'message' => 'Email is missing!'));
        }
    }
    private function send_verification_email($email_to ="") {
        if($email_to =="") {
            return array('status' => false, 'message' => 'Email is missing!');
        }
        
        $data['table']  = 'users';
        $data['field']  = 'id, full_name as name';
        $data['condition'] = array(
            'email' => $email_to
        );
        $data['limit'] = 1;
        $this->standard_model->set_query_data($data);
        $user = $this->standard_model->select();
        
        if (!isset($user->id)) {
            return array('status' => false, 'message' => 'Error! User not available.');
        }
        $this->load->library('encrypt');        
        $encrypted_string = $this->encrypt->encode($email_to);
        $encrypted_string = strtr($encrypted_string,array('+' => '.', '=' => '', '/' => '~'));

        $view_data['name'] = $user->name;
        $view_data['verification_link'] = base_url() . 'api/auth/activate/' .urlencode($encrypted_string);
        $msg = $this->load->view('verification_email_view', $view_data, true);
        
        try{                
            if ($this->send_email('Verify Your Email on AK Global', $msg, $email_to)) {
                return array('status' => true, 'message' => 'Mail sent successfully!');
            } else {
                return array('status' => false, 'message' => 'Error while sending mail!');
            }
        } catch(Exception $e){
            return array('status' => false , 'message'=>  "Try after some time!");
        }
    }

    public function activate_get($encrypted_string='') {
        if($encrypted_string == '') {
            redirect(base_url());
            return;
        }
        $this->load->library('encrypt');
        $encrypted_string = urldecode($encrypted_string);
        $encrypted_string = strtr($encrypted_string,array('.' => '+', '-' => '', '~' => '/'));        
        $email =  $this->encrypt->decode($encrypted_string);
        if (!filter_var($email, FILTER_VALIDATE_EMAIL) || $email == '') {
            $data['main_content'] = "verification_fail_view";
            $data['title'] = 'Account Verification Fail';
            $data['description'] = '';
            $data['keywords'] = '';
            $this->load->view('master',$data);      
            return;                   
        }
        
        $data_query['table'] = 'users';
        $data_query['field'] = 'id, full_name as name, email, f_name,mobile';
        
        $data_query['condition'] = array(
            'email' => $email,
            'is_email_verified'=> 0,
        );
        $data_query['limit'] = 1;
        
        $this->standard_model->set_query_data($data_query);
        $user_data = $this->standard_model->select();

        if(isset($user_data->id)) {
            $update_query['table'] = 'users';
            $update_query['condition'] = array(
                'id' => $user_data->id,
            );
            $d = array(
                'is_email_verified'=> 1,
                'user_status' => 'active'
            );
            $this->standard_model->set_query_data($update_query);
            $id = $this->standard_model->update($d);
            if($id) {
                $update_arr['__xdt'] = bin2hex(openssl_random_pseudo_bytes(12));
                $update_arr['id'] = $user_data->id;
                $update_arr['name'] = $user_data->name;
                $update_arr['email'] = $user_data->email;
                $update_arr['mobile'] = $user_data->mobile;

                if($this->update_token($update_arr)) {
                    // cloud session token
                    $cst = $this->generate_JWT_token($update_arr);
                    
                    $this->setCookie("__logged", 'true', 30);
                    $this->setCookie("__cst", $cst, 30);
                }
                redirect(base_url('dashboard'));
            } else {
                $data['main_content'] = "verification_fail_view";
                $data['title'] = 'Account Verification Fail';
                $data['description'] = '';
                $data['keywords'] = '';
                $this->load->view('master',$data);                
            }
        } else {
            $data['main_content'] = "verification_fail_view";
            $data['title'] = 'Account Verification Fail';
            $data['description'] = '';
            $data['keywords'] = '';
            $this->load->view('master',$data);
        }
        
    }

    public function forget_password_post(){
       /* $recaptchaResponse = trim($this->input->post('g-recaptcha-response'));
        $recaptcha_verified = $this->validate_recaptcha($recaptchaResponse);

        if( !$recaptcha_verified['status'] ) {
            $this->response($recaptcha_verified);
        }*/

        $email_to = $this->input->post('email');
        if($email_to) {
            $data['table']  = 'users';
            $data['field']  = 'id, full_name as name';
            $data['condition'] = array(
                'email' => $email_to,                    
            );
            $data['limit'] = 1;
            $this->standard_model->set_query_data($data);
            $user = $this->standard_model->select();
            
            if (!isset($user->id)) {
                $this->response(array('status' => false, 'message' => 'Error! User not available.'));
            }

            $update_query['table'] = 'users';
            $update_query['condition'] = array(
                'id' => $user->id,
            );
            $d = array(
                'is_password_reset_request'=> 1,
                'password_reset_code' => bin2hex(openssl_random_pseudo_bytes(20))
            );
            $this->standard_model->set_query_data($update_query);
            $this->standard_model->update($d);
            

            $this->load->library('encrypt');        
            $encrypted_string = $this->encrypt->encode($email_to);
            $encrypted_string = strtr($encrypted_string,array('+' => '.', '=' => '', '/' => '~'));

            $view_data['name'] = $user->name;
            $view_data['verification_link'] = base_url() . 'api/auth/reset_password/' .urlencode($encrypted_string);
            
            // $msg = $this->load->view('verification_email_view', $view_data, true);
            $msg = $this->load->view('forgot_password_email_view', $view_data, true);
            
            try{                
                if ($this->send_email('Verify Your Email on AK Global', $msg, $email_to)) {
                    $this->response(array('status' => true, 'message' => 'Mail sent successfully!'));
                } else {
                    $this->response(array('status' => false, 'message' => 'Error while sending mail!'));
                }
            } catch(Exception $e){
                $this->response(array('status' => false , 'message'=>  "Try after some time!"));
            }
        } else {
            $this->response(array('status' => false, 'message' => 'Email is missing!'));
        }        
    }

    private function validate_recaptcha($recaptchaResponse=''){
        if($recaptchaResponse =='') {
            return array('status' => false, 'message' => 'Invalid recaptcha response!');
        }

        $this->load->helper('agents');
        $userIP = get_ip_address();
        if($userIP=="::1") {
            $userIP = '127.0.0.1';
        }
      
        $url="https://www.google.com/recaptcha/api/siteverify?secret=".RECAPTCHA_SECRET."&response=".$recaptchaResponse."&remoteip=".$userIP;
        $this->load->library('curl');
        $response = $this->curl->simple_get($url);
  
        $status= json_decode($response, true);

        if($status['success']) {
            return array('status' => true);
        } else {
            return array('status' => false, 'message' => 'Recaptcha: '.$status['error-codes'][0]);            
        }
    }

    public function reset_password_get($encrypted_string=''){
        if($encrypted_string == '') {
            redirect(base_url());
            return;
        }

        $this->load->library('encrypt');
        $encrypted_string = urldecode($encrypted_string);
        $encrypted_string = strtr($encrypted_string,array('.' => '+', '-' => '', '~' => '/'));        
        $email =  $this->encrypt->decode($encrypted_string);
        if (!filter_var($email, FILTER_VALIDATE_EMAIL) || $email == '') {
            $data['main_content'] = "reset_fail_view";
            $data['title'] = 'Password Reset Fail';
            $data['description'] = '';
            $data['keywords'] = '';
            $this->load->view('master',$data);
            return;
        }
        
        $data_query['table'] = 'users';
        $data_query['field'] = 'id, full_name as name, password_reset_code';
        
        $data_query['condition'] = array(
            'email' => $email,            
            'is_password_reset_request'=>'1'
        );
        $data_query['limit'] = 1;
        
        $this->standard_model->set_query_data($data_query);
        $user_data = $this->standard_model->select();

        if(isset($user_data->id)) {
            $user_data->email = $email;
            $data['user'] = $user_data;
            $data['main_content'] = "reset_password_view";
            $data['title'] = 'Password Reset';
            $data['description'] = '';
            $data['keywords'] = '';
            $this->load->view('master',$data);
        } else {
            $data['main_content'] = "reset_fail_view";
            $data['title'] = 'Password Reset Fail';
            $data['description'] = '';
            $data['keywords'] = '';
            $this->load->view('master',$data);
        }
    }
    public function reset_userInfo_post(){
        $email = $this->input->post('email');
        $full_name = $this->input->post('full_name');
        $mobile = $this->input->post('mobile');
        if(!$email || $email=='') {
            $this->response(array('status' => false, 'message' => 'Email not provided!'));
        }

        $data_query['table']  = 'users';
        $data_query['field']  = 'id';
        $data_query['condition'] = array(
            'email' => $email
        );
        $data_query['limit'] = 1;
        $this->standard_model->set_query_data($data_query);
        $user = $this->standard_model->select();
        
        if(isset($user->id)) {
            $update_data['full_name'] = $full_name;
            $update_data['mobile'] = $mobile;
            $id = $this->standard_model->update($update_data);
            if($id) {
                $this->response(array('status' => true, 'message' => "Awesome, you've successfully updated your personal info."));
            } else {
                $this->response(array('status' => false, 'message' => 'Try Again!'));
            }
        } else {
            $this->response(array('status' => false, 'message' => 'Invalid Request!'));
        }        
    }

    public function reset_confirm_post() {

        $email = $this->input->post('email');
        $old_password = $this->input->post('old_password');
        $new_password = $this->input->post('new_password');
        if(!$email || $email=='') {
            $this->response(array('status' => false, 'message' => 'Email not provided!'));
        }

        $data_query['table']  = 'users';
        $data_query['field']  = 'id';
        $data_query['condition'] = array(
            'email' => $email
        );
        $data_query['limit'] = 1;
        $this->standard_model->set_query_data($data_query);
        $user = $this->standard_model->select();
        
        if(isset($user->id)) {
            $this->load->library('Bcrypt');
            $update_data['password'] = $this->bcrypt->hash_password($new_password);
            $update_data['is_password_reset_request'] = 0;
            $id = $this->standard_model->update($update_data);
            if($id) {
                $this->response(array('status' => true, 'message' => "Password reset successful! Awesome, you've successfully updated your password."));
            } else {
                $this->response(array('status' => false, 'message' => 'Try Again!'));
            }
        } else {
            $this->response(array('status' => false, 'message' => 'Invalid Reset Request!'));
        }        
    }

    private function send_email($subject='', $message='', $mailTo='') {
        $this->load->library('email');
             
        $config['protocol']  = "smtp";       
        $config['smtp_host'] = 'ssl://smtp.zoho.com';
        $config['smtp_port'] = '465';
        $config['smtp_user'] = 'mktg@grootify.com';
        $config['smtp_pass'] = 'Hexzen@1601';
        $config['mailtype']  = 'html';
        $config['charset']   = 'utf-8';
        $config['newline']   = "\r\n";
        $config['wordwrap']  = TRUE;

        $this->email->initialize($config);
        $this->email->from('mktg@grootify.com','AK Global');
        $this->email->to($mailTo);   
        $this->email->cc('deepak@grootify.com');

        $this->email->subject($subject);
        $this->email->message($message);
     
        if($message != '') {
            $res = $this->email->send();
            if($res) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }               
    }

    private function generate_JWT_token($tokenData = null){
        if($tokenData==null) {
            return 0;
        } else {
            $this->load->helpers(array('jwt', "authorization"));
            $output['token'] = AUTHORIZATION::generateToken($tokenData);
            return $output['token'];
        }
    }

    public function gen_password_get($password="empire")
    {
        $this->load->library('Bcrypt');
        $hash = $this->bcrypt->hash_password($password);
        echo $hash;
    } 



    public function register_get($slug=''){
        if($slug=='') {  
            redirect(base_url());
        }
        
        if($slug == 'pending') {
            if ($this->input->cookie('__registered')) {
                $data['main_content'] = "verification_pending_view";                
                $data['title'] = 'Account activation pending';
                $data['description'] = '';
                $data['keywords'] = '';
                $this->load->view('master',$data);                
            } else {
                redirect(base_url());       
            }
            return;
        }
    }

    public function register_post(){

        // $recaptchaResponse = trim($this->input->post('g-recaptcha-response'));
       
        // $recaptcha_verified = $this->validate_recaptcha($recaptchaResponse);

        // if( !$recaptcha_verified['status'] ) {
        //     $this->response($recaptcha_verified);
        // }

        $data['full_name'] = $this->input->post('full_name');
        if(!$data['full_name'] || $data['full_name'] == "") {
            $invalid = array('status' => false, 'message' => "Invalid Name!");
            $this->response($invalid);
        }
        
        $data['email'] = $this->input->post('email');            
        if(!$data['email'] || $data['email'] == "") {
            $invalid = array('status' => false, 'message' => "Invalid Email!");
            $this->response($invalid);
        }

         $data['mobile'] = $this->input->post('mobile');            
        if(!$data['mobile'] || $data['mobile'] == "") {
            $invalid = array('status' => false, 'message' => "Invalid Number!");
            $this->response($invalid);
        }
        
        $is_account_available = $this->check_available_email($data['email']);

        if($is_account_available) {
            $invalid = array( 'status' => false, 'message' => "This email is already registered!");
            $this->response($invalid);
        } else {                
            $this->load->library('Bcrypt');
            $unhashed = $this->input->post('password');
            $data['password'] = $this->bcrypt->hash_password($unhashed);            
            $data['user_role'] = "user";//investor
            $data['user_status'] = "pending";
            // $data['otp'] = mt_rand(100000, 999999);  
            $data['is_email_verified']='0';         
            $data['joining_date'] = date('Y-m-d H:i:s');

            $this->load->helper('agents');
            $data['registration_ip'] = get_ip_address();

            if($data['registration_ip'] == "::1") {
                $data['registration_ip'] = '127.0.0.1';
            }

            $data_query['table'] = 'users';
            
            $this->standard_model->set_query_data($data_query);
            $id = $this->standard_model->insert_and_id($data);
            // echo $id;
            // return;
            if($id) {
                $this->send_verification_email($data['email']);
                $valid = array( 'status' => true, 'message' => "Account Registered");
                $this->response($valid);
            }
        }        
    }

    private function check_available_email($email="") {
        if($email == '') {
            return 0;
        }

        $data['table'] = 'users';
        $data['field'] = 'id';
        $data['condition'] = array(
            'email' => $email
        );
        $data['limit'] = 1;
        
        $this->standard_model->set_query_data($data);
        $res = $this->standard_model->select();

        if(isset($res->id)) {
            return 1;
        } else {
            return 0;
        }
    }

    public function validate_otp_post(){
        $email = $this->input->post('email');
        $otp = $this->input->post('otp');

        if( $email != '' && $otp != '' ){
            $data_query['table'] = 'users';
            $data_query['field'] = 'id, otp, full_name';
            $data_query['condition'] = array(
                'email' => $email
            );
            $data_query['limit'] = 1;
            
            $this->standard_model->set_query_data($data_query);
            $res = $this->standard_model->select();

            if(isset($res->id) && $res->otp == $otp) {
                $update_query['table'] = 'users';
                $update_query['condition'] = array(
                    'id' => $res->id,
                );
                $d = array(
                    'is_email_verified'=> 1
                );
                $this->standard_model->set_query_data($update_query);
                $id = $this->standard_model->update($d);
                if($id) {
                    $this->response(array('status' => true, 'user_name' => $res->full_name));
                } else {
                    $this->response(array('status' => false, 'message' => 'Try Again!'));
                }
            } else {
                $this->response(array('status' => false, 'message' => 'Invalid OTP!'));
            }
        } else {
            $this->response(array('status' => true, 'message' => 'Empty Email or Password!'));
        }
    }

    private function update_token($userdata, $action="login") {
        if(isset($userdata['__xdt'])) {
            $return = 0;
            if($action == "logout") {
                $data['table'] = 'login_history';
                $data['condition'] = array(
                    'device_token' => $userdata['__xdt'], 
                );
                $d = array(                    
                    'date_logout'=> date('Y-m-d H:i:s')
                );
                $this->standard_model->set_query_data($data);
                $id = $this->standard_model->update($d);
                if($id) {
                    $return = 1;
                }
            } else {                
                $data['table'] = 'login_history';
                $data['field'] = 'id, ip';
                $data['condition'] = array(
                    'device_token' => $userdata['__xdt'],
                    'date_logout' => 'IS NULL',                    
                    'expire_time >=' => date('Y-m-d H:i:s')
                );
                $data['limit'] = 1;
                
                $this->standard_model->set_query_data($data);
                $res = $this->standard_model->select();

                $this->load->helper('agents');
                $userIP = get_ip_address();
                if($userIP=="::1") {
                    $userIP = '127.0.0.1';
                }

                if(isset($res->id) && ($res->ip == $userIP)) {
                    $return = 1;
                } else {
                    if(isset($userdata['remember_me'])) {
                        $expire_time = date('Y-m-d H:i:s', strtotime('+30 days'));
                    } else {
                        $expire_time = date('Y-m-d H:i:s', strtotime('+1 day'));
                    }
                    $provider = 'email';
                    if(isset($userdata['provider'])) {
                        $provider = $userdata['provider'];
                    }

                    $d = array(
                        'user_id' => $userdata['id'],
                        'login_via' => $provider,
                        'ip' => $userIP,                        
                        'platform'=> $_SERVER['HTTP_USER_AGENT'],                
                        'device_token' => $userdata['__xdt'],
                        'expire_time'=>  $expire_time,
                        'date_login'=> date('Y-m-d H:i:s')
                    );                    
                    $this->standard_model->set_query_data($data);
                    $id = $this->standard_model->insert_and_id($d);
                    if($id) {
                        $return = 1;
                    }
                }
            }
            return $return;                
        } else {
            return 0;
        }
    }

    // Logout from admin page
    public function logout_get() {
        $this->load->helper('cookie');
        if ($this->input->cookie('__cst')) {
            $decodedToken = false;
            try {
                $this->load->helpers(array('jwt', "authorization"));
                $decodedToken = AUTHORIZATION::validateToken($this->input->cookie('__cst'));
            } catch (Exception $e) {}
        }
        if(isset($decodedToken->__xdt)) {
            $arrRtn['__xdt'] = $decodedToken->__xdt;
            $this->update_token($arrRtn, 'logout');
        }
        
        delete_cookie("__cst");

        if (isset($_COOKIE['__cst'])) {
            unset($_COOKIE['__cst']);
        }

        if(isset($_SESSION['FBRLH_state'])) {
            unset($_SESSION['FBRLH_state']);
        }    

        if (!$this->input->is_ajax_request()) {
            redirect(base_url());
        } else {
            $this->response(array('status' => true));
        }
    }

    public function me_get(){         
        $this->load->helper('cookie');
        $auth_header = $this->input->get_request_header('Authorization');        
        if ($auth_header) {

            if (preg_match('/Bearer\s(\S+)/', $auth_header, $matches)) {
                $auth_header = $matches[1];
            }
            
            $decodedToken = false;
            try {
                $this->load->helpers(array('jwt', "authorization"));
                $decodedToken = AUTHORIZATION::validateToken($auth_header);
            } catch (Exception $e) {                
                $invalid = array(
                    'status' => false,
                    'redirect'  => true,
                    'message' => $e->getMessage()
                );
                $this->response($invalid, 401);
            }

            // $this->response($decodedToken);

            if (isset($decodedToken->id) && isset($decodedToken->__xdt)) {
                    $this->load->model('standard_model');
                    $data['table'] = 'login_history';
                    $data['field'] = 'login_history.id, ip, user_id, full_name as name';

                    $data['condition'] = array(
                        'user_id' => $decodedToken->id,
                        'device_token' => $decodedToken->__xdt,
                        'date_logout' => '0000-00-00 00:00:00',
                        'expire_time >=' => date('Y-m-d H:i:s')
                    );
                    
                    $data['join']  = array(
                        "users" => "users.id = login_history.user_id",
                    );
                    $data['limit'] = 1;
                    
                    $this->standard_model->set_query_data($data);
                    $res = $this->standard_model->select();

                    $this->load->helper('agents');
                    $userIP = get_ip_address();
                    if($userIP=="::1") {
                        $userIP = '127.0.0.1';
                    }

                    if(isset($res->id) && ($res->ip == $userIP)) {
                        $res_data['status'] = true;                
                        $res_data['data'] = array(
                            /*'id' => $res->id,*/
                            'name' => $res->name 
                        );
                        $this->response($res_data);
                        
                    } else {
                        delete_cookie("__cst");
                        if (isset($_COOKIE['__cst'])) {
                            unset($_COOKIE['__cst']);
                        }
                        delete_cookie("__logged");
                        if (isset($_COOKIE['__logged'])) {
                            unset($_COOKIE['__logged']);
                        }

                        $this->response(array(
                            'status'  => false,                            
                            'redirect'  => true,
                            'message' => 'Unauthorized Access!',
                        ), 401);
                    }
            } else {
                $this->response(array(
                    'status'  => false,
                    'redirect'  => true,
                    'message' => 'Unauthorized Access!',
                ), 401);
            }
        } else {
            $this->response(array(
                'status'  => false,                
                'message' => 'Unauthorized Access!',
            ), 401);
        }        
    }

    public function fb_callback_get() {
        $this->load->library('facebook');

        $userData = array();
        $access_token = $this->facebook->is_authenticated();
        // Check if user is logged in                
        if($access_token){
            // Get user facebook profile details
            $fbUserProfile = $this->facebook->request('get', '/me?fields=id,first_name,last_name,email');
          

            if(isset($fbUserProfile['id'])) {                
                $userData['provider'] = 'facebook';
                $userData['id'] = $fbUserProfile['id'];
                if(isset($fbUserProfile['first_name'])) {
                    $userData['first_name'] = $fbUserProfile['first_name'];                    
                }
                if(isset($fbUserProfile['last_name'])) {
                    $userData['last_name'] = $fbUserProfile['last_name'];                   
                }
                $userData['full_name'] = $userData['first_name'] . ' ' . $userData['last_name'];

                if(isset($fbUserProfile['email'])){
                    $userData['email'] = $fbUserProfile['email'];                    
                }
                $userData['profile_image'] = 'https://graph.facebook.com/'.$fbUserProfile['id'].'/picture?type=large';
                // $userData["cover_image_url"] = "https://graph.facebook.com/".$fbUserProfile['id']."?fields=cover&access_token=".$accessToken;
            }
            
            $this->handle_user_account_data($userData);

            // echo "<pre>";           
            // print_r($user_data);
            // echo "</pre>";
            
            // Get logout URL
            // $data['logoutURL'] = $this->facebook->logout_url();
        }else{
            // Get login URL
            // redirect(base_url());
            $data['authURL'] =  $this->facebook->login_url();
            echo "<pre>";
            print_r($data['authURL']);
            echo "</pre>";
        }        
    }

    public function google_login_get(){
        $this->load->library('google');
        $loginURL = $this->google->loginURL();
        echo $loginURL;
    }

    public function google_callback_get() {
        if(isset($_GET['code'])){
            $this->load->library('google');
            //authenticate user
            try {

                if($this->google->getAuthenticate()){

                    //get user info from google
                    $gaInfo = $this->google->getUserInfo();

                    if(isset($gaInfo['id'])) {
                            $userData['provider'] = 'google';
                            $userData['id'] = $gaInfo['id'];
                            $userData['first_name'] = '';
                            $userData['last_name'] = '';
                            if(isset($gaInfo['given_name'])) {
                                $userData['first_name'] = $gaInfo['given_name'];                    
                            }
                            if(isset($gaInfo['family_name'])) {
                                $userData['last_name'] = $gaInfo['family_name'];                   
                            }
                            $userData['full_name'] = $userData['first_name'] . ' ' . $userData['last_name'];

                            if(isset($gaInfo['email'])){
                                $userData['email'] = $gaInfo['email'];                    
                            }
                            $userData['profile_image'] = !empty($gaInfo['picture'])?$gaInfo['picture']:'';
                        }
                        
                        $this->handle_user_account_data($userData);
                }
            } catch(Exception $e) {
                $loginURL = $this->google->loginURL();
                // echo 'Message: ' .$e->getMessage();
                echo $loginURL;
            }
        }        
    }

    private function handle_user_account_data($authenticate_data) {

        if(isset($authenticate_data['id'])) {
            // $login_via = 'social_login_id';
            // $social_id=$authenticate_data['id'];
            // $login_history['login_via'] = $authenticate_data['provider'];
            // $social_data['id'] = $authenticate_data['id'];
            if(isset($authenticate_data['provider'])){
                if($authenticate_data['provider'] == 'facebook') {
                    $data['fb_uid'] = $authenticate_data['id'];
                    $update_arr['provider'] = 'facebook';
                }  else if($authenticate_data['provider'] == 'google') {
                    $data['ga_uid'] = $authenticate_data['id'];
                    $update_arr['provider'] = 'google';                
                } 
            }

            if(isset($authenticate_data['first_name']) && $authenticate_data['first_name'] != "") {
                $data['f_name'] = $authenticate_data['first_name'];                
            }

            if(isset($authenticate_data['last_name']) && $authenticate_data['last_name'] != "") {
                $data['l_name'] = $authenticate_data['last_name'];
            }

            $data['full_name'] = $authenticate_data['full_name'];
            $data['mcrypt_module_get_algo_block_size(algorithm)le'] = $authenticate_data['mobile'];
            
            if(isset($authenticate_data['profile_image']) && $authenticate_data['profile_image'] != "") {
                $data['profile_image'] = $authenticate_data['profile_image'];
            }            
            
            $data['email'] = isset($authenticate_data['email']) ? $authenticate_data['email'] : '';            
            $user_data = $this->get_user_data($authenticate_data['id'], $authenticate_data['provider'], $data['email']);
            $update_arr['__xdt'] = bin2hex(openssl_random_pseudo_bytes(12));

            if(!$user_data) {
                $user_id = $this->insert_user_data($data);
                $update_arr['id'] = $user_id;
                $update_arr['mobile'] = $data['mobile'];
                $update_arr['name'] = $data['full_name'];
                $update_arr['email'] = $data['email'];
            } else {
                if(isset($user_data->id)) {
                    $user_id = $user_data->id;                    
                } else {
                    $user_id = 0;
                }
                $update_arr['id'] = $user_id;
                $update_arr['mobile'] = $user_data->mobile;
                $update_arr['name'] = $user_data->full_name;
                $update_arr['email'] = $user_data->email;
            }

            if(isset($user_data->profile_image) && $user_data->profile_image !=""){
                $update_arr['profile_image'] = $user_data->profile_image;
            } else {
                if(isset($authenticate_data['profile_image'])){                
                    $update_arr['profile_image'] = $this->save_profile_image($user_id, $authenticate_data['provider'], $authenticate_data['profile_image']);
                }
            }

            if ($user_id) {
                if($this->update_token($update_arr)) {
                    // cloud session token
                    $cst = $this->generate_JWT_token($update_arr);
                    
                    $this->setCookie("__logged", 'true', 30);
                    $this->setCookie("__cst", $cst, 30);
                }
                redirect(base_url());
            }           
        } else {        
            return "Enter a valid data";
        }
    }

    private function setCookie($name, $value, $days) {
        $this->load->helper('cookie');
       $cookie= array(
           'name'   => $name,
           'value'  => $value,                            
           'expire' => 86400 * $days           
       );

       $this->input->set_cookie($cookie);
   }


    private function get_user_data($social_id, $social_provider, $email)
    {   
        $data_query['table'] = 'users' ;         
        $data_query['field'] = 'id, email, f_name, full_name, mobile,profile_image';
        if($social_id != "" && $social_provider!="" ){
            if($email != NULL) {
                $data_query['condition'] = array(
                    'email' => $email
                );
            } else {
                if($social_provider == 'facebook') {
                    $data_query['condition'] = array(
                        'fb_uid' => $social_id
                    );
                } else if( $social_provider == 'gmail') {
                    $data_query['condition'] = array(
                        'gplus_uid' => $social_id
                    );
                }
            }
        } else {
            if($email != NULL) {
                $data_query['condition'] = array(
                    'email' => $email
                );
            }
        }
        $data_query['order_by'] = array('id' => 'asc');
        $data_query['limit'] = '1';
        
        $this->standard_model->set_query_data($data_query);
        $data = $this->standard_model->select();

        if(isset($data->id)) {
            if($data->id!='') {
                return $data;
            }
        }        
        return false;
    }

    private function insert_user_data($userdata){
        if($userdata == NULL)
            return "Enter some data, Data can not be empty";

        $userdata['joining_date'] = date('Y-m-d H:i:s');
        $userdata['user_role'] = 'investor';

        $this->load->helper('agents');
        $userdata['registration_ip'] = get_ip_address();
        if($userdata['registration_ip']=="::1") {
            $userdata['registration_ip'] = '127.0.0.1';
        }

        $data_query['table'] = 'users';        
        $this->standard_model->set_query_data($data_query);

        $user_id = $this->standard_model->insert_and_id($userdata);        
        return $user_id;
    }

    private function save_profile_image($user_id, $provider, $profile_image)
    {
        if(isset($profile_image)) {
            $this->load->helper('string');
            $p_file_name = random_string('alnum', 20).'.jpg';
            
            $data_query['table'] = "users" ; 
            // $data_query['database'] = 'default';
            $data_query['condition'] = array(
                'id' => $user_id,
            );
            $this->standard_model->set_query_data($data_query);
            $flag = $this->standard_model->update(array('profile_image' => $p_file_name));
            
            $this->load->library('image_resize');
            $size = array('medium'=>400);            
            
            foreach ($size as $key=>$value) 
            {
                $w = $value;
                $h = $value;
                $img =$p_file_name;                               
                $img_path = FCPATH.'assets/uploads/profiles/'.$p_file_name;
                $extension = strtolower(strrchr($img_path, '.'));

                if($provider == 'facebook') {
                    $profile_image = str_replace("normal","400x400",$profile_image);
                }

                $img_str = file_get_contents(''.$profile_image);
                $imgg = imagecreatefromstring($img_str);
                
                $this->image_resize->setImage($imgg);
                $this->image_resize->resizeImage($w, $h, 'landscape');
                $resizedImage = $this->image_resize->getImageResource($extension, $w);
                
                file_put_contents($img_path, $resizedImage);
                // $input = $this->s3->inputFile($img_path);
                
                // $this->s3->putObject($input, S3BUCKET , 'user_images/'.$user_id."/".$p_file_name, 'public-read-write');
                
                /*Delete img from 'img_filepath'*/
                // unlink($img_path);
                if($img_path) {
                    return $p_file_name;
                }                

            }

        }
    }

    public function test_token_get(){
        $decodedToken = new stdClass();
        if ($this->input->cookie('__cst')) {

            try {
                $this->load->helpers(array('jwt', "authorization"));
                $decodedToken = AUTHORIZATION::validateToken($this->input->cookie('__cst'));  

            } catch (Exception $e) {
                echo "<pre>";
                print_r('Exception: ' . $e);
                echo "</pre>";                
                return;
            }
        }

       
    }
}
