<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH.'libraries/REST_Controller.php');

class Upload extends MY_Controller
{
    public function __construct(){        
        parent::__construct();
        $this->load->helper('utilities');
        $this->load->model('standard_model');
    }

    public function index_get(){
        $this->response(array('status'=>false));
    }

    public function media_post(){

    /*******************************************************
        * Only these origins will be allowed to upload images *
        ******************************************************/
        $accepted_origins = array("http://localhost", "http://show.grootify.com","http://www.akglobalgroup.com");

        /*********************************************
        * Change this line to set the upload folder *
        *********************************************/
      
        $imageFolder = FCPATH . "assets/uploads/";
        // $imagePath = S3_IMAGE_PATH . "assets/images/uploads/";

        reset ($_FILES);
        $temp = current($_FILES);
        if (is_uploaded_file($temp['tmp_name'])){
            if (isset($_SERVER['HTTP_ORIGIN'])) {
                // same-origin requests won't set an origin. If the origin is set, it must be valid.
                if (in_array($_SERVER['HTTP_ORIGIN'], $accepted_origins)) {
                    header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
                } else {
                    header("HTTP/1.1 403 Origin Denied");
                    return;
                }
            }

            /*
              If your script needs to receive cookies, set images_upload_credentials : true in
              the configuration and enable the following two headers.
            */
            // header('Access-Control-Allow-Credentials: true');
            // header('P3P: CP="There is no P3P policy."');

            // Sanitize input
            if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", $temp['name'])) {
                header("HTTP/1.1 400 Invalid file name.");
                return;
            }

            // Verify extension
            if (!in_array(strtolower(pathinfo($temp['name'], PATHINFO_EXTENSION)), array("gif", "jpg", "png", "jpeg"))) {
                header("HTTP/1.1 400 Invalid extension.");
                return;
            }

            // Accept upload if there was no origin, or if it is an accepted origin
            $file_name = $this->random_string(20) . '.' . pathinfo($temp['name'], PATHINFO_EXTENSION);
            $filetowrite = $imageFolder . $file_name;
            move_uploaded_file($temp['tmp_name'], $filetowrite);
            
            if(file_exists($filetowrite)) {
                chmod($filetowrite, 0777);

                $filetowrite = base_url(). 'assets/uploads/'. $file_name;
            }
            // Respond to the successful upload with JSON.
            // Use a location key to specify the path to the saved image resource.

            $this->response(array('location' => $filetowrite));
        } else {
            // Notify editor that the upload failed
            header("HTTP/1.1 500 Server Error");
        }
    }

    public function toppers_post(){

    /*******************************************************
        * Only these origins will be allowed to upload images *
        ******************************************************/
        $accepted_origins = array("http://localhost", "http://show.grootify.com","http://www.akglobalgroup.com");

        /*********************************************
        * Change this line to set the upload folder *
        *********************************************/
      
        $imageFolder = FCPATH . "assets/uploads/toppers/";
        // $imagePath = S3_IMAGE_PATH . "assets/images/uploads/";

        reset ($_FILES);
        $temp = current($_FILES);
        if (is_uploaded_file($temp['tmp_name'])){
            if (isset($_SERVER['HTTP_ORIGIN'])) {
                // same-origin requests won't set an origin. If the origin is set, it must be valid.
                if (in_array($_SERVER['HTTP_ORIGIN'], $accepted_origins)) {
                    header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
                } else {
                    header("HTTP/1.1 403 Origin Denied");
                    return;
                }
            }

            /*
              If your script needs to receive cookies, set images_upload_credentials : true in
              the configuration and enable the following two headers.
            */
            // header('Access-Control-Allow-Credentials: true');
            // header('P3P: CP="There is no P3P policy."');

            // Sanitize input
            if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", $temp['name'])) {
                header("HTTP/1.1 400 Invalid file name.");
                return;
            }

            // Verify extension
            if (!in_array(strtolower(pathinfo($temp['name'], PATHINFO_EXTENSION)), array("gif", "jpg", "png", "jpeg"))) {
                header("HTTP/1.1 400 Invalid extension.");
                return;
            }

            // Accept upload if there was no origin, or if it is an accepted origin
            $file_name = $this->random_string(20) . '.' . pathinfo($temp['name'], PATHINFO_EXTENSION);
            $filetowrite = $imageFolder . $file_name;
            move_uploaded_file($temp['tmp_name'], $filetowrite);
            
            if(file_exists($filetowrite)) {
                chmod($filetowrite, 0777);

                $filetowrite = base_url(). 'assets/uploads/toppers/'. $file_name;
            }
            // Respond to the successful upload with JSON.
            // Use a location key to specify the path to the saved image resource.

            if($this->input->post('result_id')) {
                $result_id = $this->input->post('result_id');
            } else {
                $result_id = 0;
            }
            $insert_url = 'assets/uploads/toppers/'. $file_name;
            $media_id = $this->insert_toppers($insert_url, $result_id);
            
            if($media_id) {
                $this->response(array('location' => $insert_url, 'media_id' => $media_id));                
            } else {
                $this->response(array('status' => false));
            }

        } else {
            // Notify editor that the upload failed
            header("HTTP/1.1 500 Server Error");
        }
    }

    private function insert_toppers($insert_url, $resultId) {
        $data_query['table'] = 'result_toppers';

        $data['status'] = '1';
        $data['image'] = $insert_url;
        $data['result_id'] = $resultId;
        $this->standard_model->set_query_data($data_query);
        $media_id = $this->standard_model->insert_and_id($data);
        if($media_id) 
            return $media_id;
    }

    public function faculties_post(){

    /*******************************************************
        * Only these origins will be allowed to upload images *
        ******************************************************/
        $accepted_origins = array("http://localhost", "http://show.grootify.com","http://www.akglobalgroup.com");

        /*********************************************
        * Change this line to set the upload folder *
        *********************************************/
      
        $imageFolder = FCPATH . "assets/uploads/faculties/";
        // $imagePath = S3_IMAGE_PATH . "assets/images/uploads/";

        reset ($_FILES);
        $temp = current($_FILES);
        if (is_uploaded_file($temp['tmp_name'])){
            if (isset($_SERVER['HTTP_ORIGIN'])) {
                // same-origin requests won't set an origin. If the origin is set, it must be valid.
                if (in_array($_SERVER['HTTP_ORIGIN'], $accepted_origins)) {
                    header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
                } else {
                    header("HTTP/1.1 403 Origin Denied");
                    return;
                }
            }

            /*
              If your script needs to receive cookies, set images_upload_credentials : true in
              the configuration and enable the following two headers.
            */
            // header('Access-Control-Allow-Credentials: true');
            // header('P3P: CP="There is no P3P policy."');

            // Sanitize input
            if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", $temp['name'])) {
                header("HTTP/1.1 400 Invalid file name.");
                return;
            }

            // Verify extension
            if (!in_array(strtolower(pathinfo($temp['name'], PATHINFO_EXTENSION)), array("gif", "jpg", "png", "jpeg"))) {
                header("HTTP/1.1 400 Invalid extension.");
                return;
            }

            // Accept upload if there was no origin, or if it is an accepted origin
            $file_name = $this->random_string(20) . '.' . pathinfo($temp['name'], PATHINFO_EXTENSION);
            $filetowrite = $imageFolder . $file_name;
            move_uploaded_file($temp['tmp_name'], $filetowrite);
            
            if(file_exists($filetowrite)) {
                chmod($filetowrite, 0777);

                $filetowrite = base_url(). 'assets/uploads/faculties/'. $file_name;
            }
            // Respond to the successful upload with JSON.
            // Use a location key to specify the path to the saved image resource.

            if($this->input->post('course_id')) {
                $course_id = $this->input->post('course_id');
            } else {
                $course_id = 0;
            }
            $insert_url = 'assets/uploads/faculties/'. $file_name;
            $media_id = $this->insert_faculties($insert_url, $course_id);
            
            if($media_id) {
                $this->response(array('location' => $insert_url, 'media_id' => $media_id));                
            } else {
                $this->response(array('status' => false));
            }

        } else {
            // Notify editor that the upload failed
            header("HTTP/1.1 500 Server Error");
        }
    }

 

 public function attachment_post(){
  
      
         /*******************************************************
        * Only these origins will be allowed to upload images *
        ******************************************************/
        $accepted_origins = array("http://localhost", "http://show.grootify.com","http://www.akglobalgroup.com");

        /*********************************************
        * Change this line to set the upload folder *
        *********************************************/
      
        $imageFolder = FCPATH . "assets/attachment/";
        // $imagePath = S3_IMAGE_PATH . "assets/images/uploads/";

        reset ($_FILES);
        $temp = current($_FILES);
        if (is_uploaded_file($temp['tmp_name'])){
            if (isset($_SERVER['HTTP_ORIGIN'])) {
                // same-origin requests won't set an origin. If the origin is set, it must be valid.
                if (in_array($_SERVER['HTTP_ORIGIN'], $accepted_origins)) {
                    header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
                } else {
                    header("HTTP/1.1 403 Origin Denied");
                    return;
                }
            }

            /*
              If your script needs to receive cookies, set images_upload_credentials : true in
              the configuration and enable the following two headers.
            */
            // header('Access-Control-Allow-Credentials: true');
            // header('P3P: CP="There is no P3P policy."');

            // Sanitize input
            if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", $temp['name'])) {
                header("HTTP/1.1 400 Invalid file name.");
                return;
            }

            // Verify extension
            if (!in_array(strtolower(pathinfo($temp['name'], PATHINFO_EXTENSION)), array("jpg", "png", "jpeg", "pdf"))) {
                header("HTTP/1.1 400 Invalid extension.");
                return;
            }

            // Accept upload if there was no origin, or if it is an accepted origin
            $file_name = $this->random_string(20) . '.' . pathinfo($temp['name'], PATHINFO_EXTENSION);
            $filetowrite = $imageFolder . $file_name;
            move_uploaded_file($temp['tmp_name'], $filetowrite);
            
            if(file_exists($filetowrite)) {
                chmod($filetowrite, 0777);

                $filetowrite = base_url(). 'assets/attachment/'. $file_name;
            }
            // Respond to the successful upload with JSON.
            // Use a location key to specify the path to the saved image resource.

            $insert_url = 'assets/attachment/'. $file_name;
            $attachment_id = $this->insert_media($insert_url);
            
            if($attachment_id) {
                $this->response(array('location' => $filetowrite, 'attachment_id' => $attachment_id));                
            } else {
                $this->response(array('status' => false));
            }

        } else {
            // Notify editor that the upload failed
            header("HTTP/1.1 500 Server Error");
        }
    }

     private function insert_media($insert_url) {

        $data_query['table'] = 'quote_attachment';
        $data['path'] = $insert_url;
        $data['status'] = '1';
        $data['date_created'] = date('Y-m-d H:i:s');
        $this->standard_model->set_query_data($data_query);
        $attachment_id = $this->standard_model->insert_and_id($data);
        if($attachment_id) 
            return $attachment_id;
    }
    

   private function random_string($length) {
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }
    
    
}