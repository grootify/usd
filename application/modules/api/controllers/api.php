<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH.'libraries/REST_Controller.php');
class Api extends Api_Controller {

	public function __construct()
	{
		 parent::__construct();		 
	     $this->load->model('standard_model');
	}


	public function index_get() {			
		redirect(base_url());
	}
	public function getUserAddress_get(){
		$user_id=$this->input->get('user_id');
		$data['table']='address';
        $data['field'] = 'id';
        $data['condition'] = array(
        	'user_id' => $user_id,
        	'is_active'=>'1'
    		);
       	
        $data['order_by'] = array( 'id' => 'DESC' );
        $this->standard_model->set_query_data($data);
        $result = $this->standard_model->select();

        if($result){
        	$this->response(array('status'=>true));
        }else{
        	$this->response(array('status'=>false));
        }
	}
	public function showProductByBrand_post(){
		$brands=$this->input->post('brands');
		echo "<pre>";
		print_r($brands);
		echo "</pre>";
		return;
		$brand_data=[];
		for ($i=0; $i < count($brands) ; $i++) { 
			if($brands[$i]){
				$brand_id=$brands[$i];
				$data['table']='catalogs';
				$data['field'] = 'product_list.id,product_list.title,product_list.slug,product_list.description,product_list.url as image_url';
				$data['join'] = array(
					'product_list' =>'catalogs.id=product_list.catalog_id'
				);
				$data['condition'] = array(
					'product_list.catalog_id' => $brand_id,
					'product_list.status'=>'active'
				);

				$data['order_by'] = array( 'id' => 'DESC' );
				$this->standard_model->set_query_data($data);
				$result = $this->standard_model->select();
				array_push($brand_data,$result);
			}
		}
		if($result){
				$this->response(array('status'=>true,'data'=>$brand_data));
			}else{
				$this->response(array('status'=>false));
			}
	}
	public function updateProductViews_post(){
		$product_id=$this->input->post('product_id');
		if($product_id!=''){
			$getView=$this->getProductViews($product_id);
			$views=$getView+1;
			$data['views']=$views;
			$data['date_modified']=date('Y-m-d H:i:s');
			$data_query['table']='product_list';
			$data_query['condition']=array(
				"id"=>$product_id
			);
			$this->standard_model->set_query_data($data_query);
			$this->standard_model->update($data);
			$this->response(array('status'=>true));
		}
		
	}
	private function getProductViews($product_id){
		$data['table']='product_list';
        $data['field'] = 'views';
        $data['condition'] = array(
        	'id' => $product_id
    		);
       	
        $data['order_by'] = array( 'id' => 'DESC' );
        $this->standard_model->set_query_data($data);
        $result = $this->standard_model->select();
        if($result){
        	return $result->views;
        }else{
            return array('status' => false);
        }
	}

	public function add_custom_item_post() {
		$user_id=$this->user['id'];
		$name = $this->input->post('name');
		if(!$name || $name=='') {
			$this->response(array('status'=>false, 'message' => 'Invalid Item Name!'));
		}
		$quantity = $this->input->post('quantity');
		if(!$quantity || $quantity=='' || $quantity=='0') {
			$this->response(array('status'=>false, 'message' => 'Invalid Item Quantity!'));
		}
		$check_quote=$this->checkQuote($user_id);
		$quote_id=$check_quote['quote_id'];

		if($quote_id) {
			$data['quote_id']=$quote_id;
			$data['name']=$name;
			$data['quantity']=$quantity;
			$data['status']='1';
			$data['date_created']=date('Y-m-d H:i:s');
			$data['date_modified']=date('Y-m-d H:i:s');
			$data_query['table'] = 'custom_quote';
			$this->standard_model->set_query_data($data_query);
			$result = $this->standard_model->insert_and_id($data);
			$this->response(array('status'=>true));
		} else {
			$this->response(array('status'=>false, 'message' => 'Try after sometime!'));
		}
	}

	public function addQuote_post(){
		$product_id=$this->input->post('product_id');
		$user_id=$this->input->post('user_id');
		$check_data=$this->checkProduct($product_id,$user_id);
		if($check_data['status']==1){
			$this->response(array('status'=>true, 'message' => 'Product already added to cart!'));
		}else{
			$check_quote=$this->checkQuote($user_id);
			$quote_id=$check_quote['quote_id'];
			if($quote_id!=''){
				$this->addProductInQuote($check_quote['quote_id'],$product_id);
		        $this->response(array('status'=>true, 'quote_id'=>$check_quote['quote_id']));
			}else{
				$quote_no=rand(1,1000);
		        $invoice_no=rand();
		        $data['quote_no']=$quote_no;
		        $data['user_id']=$this->input->post('user_id');
		        $data['invoice_no']=$invoice_no;
		        $data['address_id']='0';
		        $data['total']='0';
		        $data['shping_fee']='0';
		        $data['order_status_id']='1';
		        $data['is_active']='1';
		        $data['in_process']='1';
		        $data['quote_created']=date('Y-m-d H:i:s');
		        $data_query['table'] = 'quotes';
				if (strpos($_SERVER['HTTP_HOST'], 'akglobalmed') !== false) {
				    $data['site'] = 'akglobalmed';
				} else {
				    $data['site'] = 'netherlands';
				}
		        $this->standard_model->set_query_data($data_query);
		        $quote_id = $this->standard_model->insert_and_id($data);
		        if($quote_id){
		        	$this->addProductInQuote($quote_id,$product_id);
		        	$this->response(array('status'=>true, 'quote_id'=>$quote_id));
		        }else{
					$this->response(array('status'=>false));
				}
			}
			
		}
		
	}
	private function checkQuote($user_id){
		$data['table']='quotes';
        $data['field'] = 'quotes.id';
       
       	 $data['condition'] = array(
            "quotes.user_id"=>$user_id,
            "quotes.order_status_id"=>1,
            "quotes.in_process"=>1
            
        );
        $data['order_by'] = array( 'quotes.id' => 'DESC' );
        $this->standard_model->set_query_data($data);
        $result = $this->standard_model->select();
        if($result){
        	return array('status' => true,'quote_id' => $result->id);
        }else{
            return array('status' => false);
        }
	}
	private function checkProduct($product_id,$user_id){
		$data['table']='quote_products';
        $data['field'] = 'quote_products.id';
        $data['join']=array(
            "quotes"=>'quote_products.quote_id = quotes.id'
           
        );
       	 $data['condition'] = array(
            "quotes.user_id"=>$user_id,
            "quote_products.product_id"=>$product_id,
            "quotes.order_status_id"=>1,
            "quote_products.is_active" => 1,
            "quotes.in_process"=>1,
            
        );
        $data['order_by'] = array( 'quote_products.id' => 'DESC' );
        $this->standard_model->set_query_data($data);
        $result = $this->standard_model->select();
        if($result){
        	return array('status' => true);
        }else{
            return array('status' => false);
        }
	}
	private function addProductInQuote($quote_id,$product_id){
		$data['quote_id']=$quote_id;
		$data['product_id']=$product_id;
		$data['quantity']='1';
		$data['total']='0';
		$data['shiping_fee']='0';
		$data['is_active']='1';
		$data['created_date']=date('Y-m-d H:i:s');
		$data_query['table'] = 'quote_products';

		$this->standard_model->set_query_data($data_query);
		$id = $this->standard_model->insert_and_id($data);
		if($id){
			$this->response(array('status'=>true, 'id'=>$id));
		}else{
			$this->response(array('status'=>false));
		}
	}
	public function updateQuotationProcess_post(){
			$quote_id=$this->input->post('quote_id');
			$user_id=$this->input->post('user_id');
			$address_id=$this->input->post('address_id');
			$data['address_id']=$address_id;
			$data['order_status_id']='2';
			$data['in_process']='0';
			$data['quote_modified']=date('Y-m-d H:i:s');
			$data_query['table']='quotes';
			$data_query['condition']=array(
				"id"=>$quote_id,
				"user_id"=>$user_id
			);
			$this->standard_model->set_query_data($data_query);
			$this->standard_model->update($data);
			$this->setquoteProductStatus($quote_id);

			$this->response(array('status'=>true));
	}
	private function setquoteProductStatus($quote_id){
		    $data['is_active']='0';
			$data['modified_date']=date('Y-m-d H:i:s');
			$data_query['table']='quote_products';
			$data_query['condition']=array(
				"quote_id"=>$quote_id
				);
			$this->standard_model->set_query_data($data_query);
			$this->standard_model->update($data);
			
		$this->response(array('status'=>true));
	}
	public function addCustomQuote_post(){
		$quoteArry=$this->input->post('quoteArry');
		$attachmentArry=$this->input->post('attachment_id');
		$check_quote=$this->checkQuote($this->input->post('user_id'));
			if($check_quote['quote_id']){
				$quote_id=$check_quote['quote_id'];
				for($i=0;$i<count($quoteArry);$i++){
					$data['quote_id']=$quote_id;
					$data['name']=$quoteArry[$i]['Itemname'];
					$data['quantity']=$quoteArry[$i]['quantity'];
					$data['status']='1';
					$data['date_created']=date('Y-m-d H:i:s');
					$data_query['table'] = 'custom_quote';

					$this->standard_model->set_query_data($data_query);
					$result = $this->standard_model->insert_and_id($data);
					
				}
				$result=$this->updateQuoteAttachment($quote_id,$attachmentArry);
			}else{
				$quote_id=$this->addCustomQuote($this->input->post('user_id'));
	
				if($quote_id){
					for($i=0;$i<count($quoteArry);$i++){
						$data['quote_id']=$quote_id;
						$data['name']=$quoteArry[$i]['Itemname'];
						$data['quantity']=$quoteArry[$i]['quantity'];
						$data['status']='1';
						$data['date_created']=date('Y-m-d H:i:s');
						$data_query['table'] = 'custom_quote';

						$this->standard_model->set_query_data($data_query);
						$result = $this->standard_model->insert_and_id($data);
						
					}
					$result=$this->updateQuoteAttachment($quote_id,$attachmentArry);
					
				}
			}
	   
		if($result){
			$this->response(array('status'=>true));
		}else{
			$this->response(array('status'=>false));
		}
		


	}
	private function updateQuoteAttachment($quote_id,$attachmentArry)
	{
		for($i=0;$i<count($attachmentArry);$i++){
			$data['quote_id']=$quote_id;
			$data['date_modified']=date('Y-m-d H:i:s');
			$data_query['table']='quote_attachment';
			$data_query['condition']=array(
				"id"=>$attachmentArry[$i]
			);
			$this->standard_model->set_query_data($data_query);
			$this->standard_model->update($data);
		}
		$this->response(array('status'=>true));
	}
	private function addCustomQuote($user_id){
		$quote_no=rand(1,1000);
        $invoice_no=rand();
        $data['quote_no']=$quote_no;
        $data['user_id']=$user_id;
        $data['invoice_no']=$invoice_no;
        $data['address_id']='0';
        $data['total']='0';
        $data['shping_fee']='0';
        $data['order_status_id']='1';
        $data['is_active']='1';
        $data['in_process']='1';
        $data['quote_created']=date('Y-m-d H:i:s');
        $data_query['table'] = 'quotes';
            
        $this->standard_model->set_query_data($data_query);
        $quote_id = $this->standard_model->insert_and_id($data);
        if($quote_id){
             return $quote_id;
        }
	}
		public function addAddress_post(){

		if($this->input->post('user_id')){
			$data['user_id']=$this->input->post('user_id');
		}
		if($this->input->post('company')){
			$data['company']=$this->input->post('company');
		}
		if($this->input->post('contact_f_name')){
			$data['contact_f_name']=$this->input->post('contact_f_name');
		}
		if($this->input->post('contact_l_name')){
			$data['contact_l_name']=$this->input->post('contact_l_name');
		}
		if($this->input->post('email')){
			$data['email']=$this->input->post('email');
		}
		if($this->input->post('telephone')){
			$data['telephone']=$this->input->post('telephone');
		}
		if($this->input->post('fax')){
			$data['fax']=$this->input->post('fax');
		}
		if($this->input->post('attn')){
			$data['attn']=$this->input->post('attn');
		}
		if($this->input->post('address_1')){
			$data['address_1']=$this->input->post('address_1');
		}
		if($this->input->post('address_2')){
			$data['address_2']=$this->input->post('address_2');
		}
		if($this->input->post('city')){
			$data['city']=$this->input->post('city');
		}
		if($this->input->post('state')){
			$data['state']=$this->input->post('state');
		}
		if($this->input->post('postal_code')){
			$data['postal_code']=$this->input->post('postal_code');
		}
		if($this->input->post('country')){
			$data['country']=$this->input->post('country');
		}
			$data['is_default']='0';
			$data['is_active']='1';
			
		if($this->input->post('id')==0){
			
			$data['date_of_creation']=date('Y-m-d H:i:s');
			$data_query['table']='address';
			$this->standard_model->set_query_data($data_query);
			$id = $this->standard_model->insert_and_id($data);
			if($id){
				$this->response(array('status'=>true, 'id'=>$id));
			}else{
				$this->response(array('status'=>false));
			}
		}else{
			$id=$this->input->post('id');
			$data['modified_date']=date('Y-m-d H:i:s');
			$data_query['table']='address';
			$data_query['condition']=array(
				"id"=>$id
			);
			$this->standard_model->set_query_data($data_query);
			$this->standard_model->update($data);
			$this->response(array('status'=>true));
		}
		
		
		
	}
	public function setDefaultAdress_post()
    {
        $id=$this->input->post('id');
        $user_id=$this->input->post('user_id');
        $data_query['table']='address';
        $data_query['condition']=array(
                "user_id"=>$user_id,
                "id"=>$id
            );
        $data['is_default']='1';
        $data['modified_date']=date('Y-m-d H:i:s');

		$this->standard_model->set_query_data($data_query);
		$update=$this->standard_model->update($data);
		if($update){
			$this->updateDefaultStatus($id,$user_id);
		}
		$this->response(array('status'=>true));
    }
    private function updateDefaultStatus($id,$user_id){
    	$data_query['table']='address';
        $data_query['condition']=array(
                "user_id" => $user_id,
                "id !=" => $id
            );
        $data['is_default']='0';
        $data['modified_date']=date('Y-m-d H:i:s');

		$this->standard_model->set_query_data($data_query);
		$this->standard_model->update($data);
		$this->response(array('status'=>true));
    }


  
  

    public function deleteAdress_post(){
    	$id=$this->input->post('id');
     	$data_query['table']='address';
        $data_query['condition']=array(
                "id"=>$id
            );
        $data['is_active']='0';
        $data['modified_date']=date('Y-m-d H:i:s');

		$this->standard_model->set_query_data($data_query);
		$this->standard_model->update($data);
		
		$this->response(array('status'=>true));
    }
    public function delete_attachment_post(){
    	$id=$this->input->post('id');
     	$data_query['table']='quote_attachment';
        $data_query['condition']=array(
                "id"=>$id
            );
        $data['status']='0';
        $data['date_modified']=date('Y-m-d H:i:s');

		$this->standard_model->set_query_data($data_query);
		$this->standard_model->update($data);
		
		$this->response(array('status'=>true));
    }
        public function delete_customQuote_post(){
    	$id=$this->input->post('id');
     	$data_query['table']='custom_quote';
        $data_query['condition']=array(
                "id"=>$id
            );
        $data['status']='0';
        $data['date_modified']=date('Y-m-d H:i:s');

		$this->standard_model->set_query_data($data_query);
		$this->standard_model->update($data);
		
		$this->response(array('status'=>true));
    }
	public function updateModifiedData_post(){

	    $data_query['table']='quote_products';
		$data_query['condition']=array(
			'id' => $this->input->post('id')
		);
		$data['quantity']=$this->input->post('quantity');
		$data['modified_date']=date('Y-m-d H:i:s');
		
	 
		$this->standard_model->set_query_data($data_query);
		$this->standard_model->update($data);
		$this->response(array('status'=>true));
	}

	public function updateStatus_post(){
		$data_query['table']='quote_products';
		$data_query['condition']=array(
			'id' => $this->input->post('id')
		);
		$data['is_active']='0';
		$data['modified_date']=date('Y-m-d H:i:s');
		$this->standard_model->set_query_data($data_query);
		$this->standard_model->update($data);
		$this->response(array('status'=>true));

	}
	
	public function send_message_post() {
		
		if($this->input->post('contact_name') && $this->input->post('contact_name')!='') {
			$data['name'] = $this->input->post('contact_name');
		} else {
			$this->response(array('status'=>false, 'message' => 'Invalid Name!'));
		}

		if($this->input->post('contact_email') && $this->input->post('contact_email')!='') {
			$data['email'] = $this->input->post('contact_email');
		} else {
			$this->response(array('status'=>false, 'message' => 'Invalid Email!'));
		}

		if($this->input->post('contact_mobile') && $this->input->post('contact_mobile')!='') {
			$data['mobile'] = $this->input->post('contact_mobile');
		} else {
			$this->response(array('status'=>false, 'message' => 'Invalid mobile!'));
		}
		if($this->input->post('contact_message') && $this->input->post('contact_message')!='') {
			$data['message'] = $this->input->post('contact_message');
		} else {
			$this->response(array('status'=>false, 'message' => 'Invalid Message!'));
		}
		$subject = 'Contact - AK Global ' ;
        $message = '<span>Name: '.$data['name'].'</span><br><span>Email: '.$data['email'].'</span><br><span>Mobile: '.$data['mobile'].'</span><br><span>Message: '.$data['message'].'</span>';    
		$sent = $this->send_mail($subject, $message, 'ayushi@grootify.com');
		if($sent) {
	        $this->response(array('status'=>true, 'message' => 'Thank You. We will contact you soon.'));
		} else {
			$this->response(array('status'=>false, 'message' => 'Error! Try after some time.'));
		}
	}

	private function send_mail($subject='', $message='', $mailTo='') {

		$this->load->library('email');
             
        $config['protocol']  = "smtp";       
        $config['smtp_host'] = 'ssl://smtp.gmail.com';
        $config['smtp_port'] = '465';
        $config['smtp_user'] = 'grootifymrkt@gmail.com';
        $config['smtp_pass'] = 'GM12345indore';
        $config['mailtype']  = 'html';
        $config['charset']   = 'utf-8';
        $config['newline']   = "\r\n";
        $config['wordwrap']  = TRUE;

        $this->email->initialize($config);
$this->email->from('grootifymrkt@gmail.com','AK Global - Contact');
        $this->email->to($mailTo);   
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->cc('deepak@grootify.com');
     
        if($message != '') {
            $res = $this->email->send();
	        if($res) {
	        	return true;
	        } else {
	        	return false;
	        }
        } else {
        	return false;
        }
               
	}

}
