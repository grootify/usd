<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "home";
$route['dashboard'] = "user/dashboard";
$route['addresses'] = "user/addresses";
$route['add_addresses'] = "user/add_addresses";
$route['add_addresses/(:any)'] = "user/add_addresses";
$route['edit_addresses/(:any)'] = "user/edit_addresses/$1";
$route['settings']="user/settings";
$route['change_password']="user/change_password";

$route['product-overview'] = "home/product_overview";
$route['product-overview/(:any)'] = "home/product_overview/$1";

$route['product-details'] = "home/product_details";
$route['product-details/(:any)'] = "home/product_details/$1";
$route['product-details/(:any)/(:any)'] = "home/product_details/$1/$2";

$route['my_quotation'] = "user/my_quotation";
$route['my_quotation__details/(:any)'] = "user/my_quotation__details";

$route['checkout']="checkout";
$route['search?q='] ="search";
$route['api/(:any)'] = "api/$1";
$route['api/(:any)/(:any)'] = "api/$1/$2";



$route['admin'] = "admin";
$route['admin/(:any)'] = "admin/$1";
$route['admin/(:any)/(:any)'] = "admin/$1/$2";
$route['admin/(:any)/(:any)/(:any)'] = "admin/$1/$2/$3";

$route['admin/login'] = "admin/auth/login";
$route['admin/logout'] = "admin/auth/logout";

$route['contact-us'] = "home/contact_us";

$route['(:any)'] = "home/$1";
$route['(:any)/(:any)'] = "home/$1/$2";