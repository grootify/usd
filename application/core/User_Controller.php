<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class User_Controller extends CI_Controller {

    public $user = array();

    public function __construct() {
        parent::__construct();
        
        $this->check_user();
    }

    private function check_user() {

        // $auth_header = $this->input->get_request_header('Authorization');
        $auth_header = $this->input->cookie('__cst');
        if ($auth_header) {

            // if (preg_match('/Bearer\s(\S+)/', $auth_header, $matches)) {
            //     $auth_header = $matches[1];
            // }
            
            $decodedToken = false;
            try {
                $this->load->helpers(array('jwt', "authorization"));
                $decodedToken = AUTHORIZATION::validateToken($auth_header);

        
            } catch (Exception $e) {
                // $invalid = array(
                //     'status' => false,                    
                //     'message' => $e->getMessage()
                // );
                // $this->response($invalid, 401);
                $this->user = array();
            }

            // echo "<pre>";
            // print_r($decodedToken);
            // echo "</pre>";
            // return;
            if (isset($decodedToken->id) && isset($decodedToken->__xdt)) {
                $this->load->model('standard_model');
                $data['table'] = 'login_history';
                $data['field'] = 'id, ip';

                $data['condition'] = array(
                    'user_id' => $decodedToken->id,
                    'device_token' => $decodedToken->__xdt,
                  //  'date_logout' => '0000-00-00 00:00:00',
                    'expire_time >=' => date('Y-m-d H:i:s')
                );
                $data['limit'] = 1;
                
                $this->standard_model->set_query_data($data);
                $res = $this->standard_model->select();
                $this->load->helper('agents');
                $userIP = get_ip_address();
                if($userIP=="::1") {
                    $userIP = '127.0.0.1';
                }
                
                if(!isset($res->id) || ($res->ip != $userIP)) {
                    $this->user = array();
                } else {
                    $this->user = array(
                        'id' => $decodedToken->id,                    
                        'name' => $decodedToken->name,
                       // 'f_name' => isset($decodedToken->f_name)? $decodedToken->f_name : '',
                        'email' => isset($decodedToken->email)? $decodedToken->email : '',
                        'mobile' => $decodedToken->mobile,
                        //'profile_image' => isset($decodedToken->profile_image) ? $decodedToken->profile_image : 'default_profile.png'
                    );


                }
            }
        }
    }

}
?>
