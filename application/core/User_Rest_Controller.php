<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH.'libraries/REST_Controller.php');

class User_Rest_Controller extends REST_Controller {

    protected $user = array();

    public function __construct() {
        parent::__construct();
        
        $this->check_user();
        print_r($this->user);
exit("cehck user rest controller");
    }

    private function check_user() {
        // $auth_header = $this->input->get_request_header('Authorization');
        $auth_header = $this->input->cookie('__cst');
        
        if ($auth_header) {

            if (preg_match('/Bearer\s(\S+)/', $auth_header, $matches)) {
                $auth_header = $matches[1];
            }
            
            $decodedToken = false;
            try {
                $this->load->helpers(array('jwt', "authorization"));
                $decodedToken = AUTHORIZATION::validateToken($auth_header);
            } catch (Exception $e) {
                $invalid = array(
                    'status' => false,
                    'redirect'  => true,
                    'message' => $e->getMessage()
                );
                $this->response($invalid, 401);
            }

            if (isset($decodedToken->id) && isset($decodedToken->__xdt)) {
                $this->load->model('standard_model');
                $data['table'] = 'login_history';
                $data['field'] = 'id, ip';
                $data['condition'] = array(
                    'user_id' => $decodedToken->id,
                    // 'user_type' => 'investor',
                    'device_token' => $decodedToken->__xdt,
                    'date_logout' => '0000-00-00 00:00:00',
                    'expire_time >=' => date('Y-m-d H:i:s')
                );
                $data['limit'] = 1;
                
                $this->standard_model->set_query_data($data);
                $res = $this->standard_model->select();

                $this->load->helper('agents');
                $userIP = get_ip_address();
                if($userIP=="::1") {
                    $userIP = '127.0.0.1';
                }

                
                if(!isset($res->id) || ($res->ip != $userIP)) {
                    // Invalid Message
                } else {
                    $this->user = array(
                        'id' => $decodedToken->id,                    
                        'name' => $decodedToken->name,
                       // 'f_name' => $decodedToken->f_name,
                        'email' => $decodedToken->email,
                        //'profile_image' => isset($decodedToken->profile_image) ? $decodedToken->profile_image : ''
                    );
                }
            }
        }
    }
}
?>
