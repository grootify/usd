<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="<?php echo base_url();?>images/favicon/favicon13.png" type="image/x-icon" />
    <!--    <link rel="shortcut icon" href="images/favicon/favicon13.png" type="image/x-icon"/> -->
    <title><?php echo $title; ?></title>
    <meta name="description" content="<?php echo $description;?>">
    <meta name="keywords" content="<?php echo $keywords;?>">

    <!--Google font-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Fontawesome -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/font-awesome.css">

    <!-- Slick css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/slick-theme.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/slick.css">

    <!-- Flaticon icon -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/flaticon.css">

    <!-- Themify icon -->
    <!--    <link rel="stylesheet" type="text/css" href="assets/css/themify.css"> -->

    <!-- Animate icon -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/animate.css">

    <!-- Bootstrap css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.css">

    <!-- Color css -->

 

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css?v=2.0"> -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style_group.css?v=2.0">

    <script>
      (function () {
        var s = document.createElement('script');
        s.type = 'text/javascript';
        s.async = true;
        s.src = 'https://app.termly.io/embed.min.js';
        s.id = 'f6805a74-5488-43f8-84c3-93939e57770c';
        s.setAttribute("data-name", "termly-embed-banner");
        var x = document.getElementsByTagName('script')[0];
        x.parentNode.insertBefore(s, x);
      })();
    </script>
</head>
<style>
 .inStock{
    display: block;
    position: fixed;
    bottom: 34px;
    width: 300px;
    text-align: center;
    right: 30px;
    z-index: 999;
    
}
</style>

<body>
<div class="alert alert-danger inStock" id="response" style="display: none"></div>