<div class="modal fade show" id="basicPlanModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<!-- <div class="modal fade show" id="basicPlanModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"> -->
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <div class="">
  <div class="navModal">
   <!--  <ul class="nav nav-pills nav-fill mb-1" id="pills-tab" role="tablist">
      <li class="nav-item">
       <a class="nav-link active mr-1" id="pills-signin-tab" data-toggle="pill" href="#pills-signin" role="tab" aria-controls="pills-signin" aria-selected="true">Sign In</a> 
    </li>
      <li class="nav-item">
       <a class="nav-link " id="pills-signup-tab" data-toggle="pill" href="#pills-signup" role="tab" aria-controls="pills-signup" aria-selected="false">Sign Up</a> 
    </li>
    </ul> -->
    <div class="tab-content contentTab" id="pills-tabContent">
      <div class="tab-pane fade show active" id="pills-signin" role="tabpanel" aria-labelledby="pills-signin-tab">
        <div class="col-sm-12 border rounded pt-2 mt-2">
         <!--  <div class="text-center contentTabImage">
            <img src="assets/images/logo/logo.png" alt="logo">
         </div> -->
          <form method="post" id="singninFrom" class="modalForm">
            <div class="form-group">
              <label class="">Email</label>
              <input type="email" name="email" id="email" class="form-control" placeholder="Enter valid email" required>
            </div>
            <div class="form-group">
              <label class="">Password </label>
              <input type="password" name="password" id="password" class="form-control" placeholder="***********" required>
            </div>
            <div class="form-group">
              <div class="row">
                <!-- <div class="col">
                  <label><input type="checkbox" name="condition" id="condition"> Remember me.</label>
                </div> -->
                <div class="col text-right"> <a href="javascript:;" data-toggle="modal" data-target="#forgotPass">Forgot Password?</a> </div>
              </div>
            </div>
            <div class="form-group">
              <input type="submit" name="submit" value="Log In" class="btn btn-blue btn-block">
            </div>
            <div class="donthaveAc">
              <h4>Don't have an account?</h4>
               <input type="submit" name="submit" value="Sign Up" class="btn btn-blue btn-block">
            </div>
          </form>
        </div>
      </div>
      <div class="tab-pane fade" id="pills-signup" role="tabpanel" aria-labelledby="pills-signup-tab">
        <div class="col-sm-12 border rounded pt-2 mt-2">
          <form method="post" id="singnupFrom" class="modalForm">
            <div class="form-group">
              <label class="">Email </label>
              <input type="email" name="signupemail" id="signupemail" class="form-control" placeholder="Enter valid email" required>
            </div>
            <div class="form-group">
              <label class="">User Name </label>
              <input type="text" name="signupusername" id="signupusername" class="form-control" placeholder="Choose your user name" required>
            </div>
            <div class="form-group">
              <label class="">Phone #</label>
              <input type="text" name="signupphone" id="signupphone" class="form-control" placeholder="(000)-(0000000)">
            </div>
            <div class="form-group">
              <label class="">Password </label>
              <input type="password" name="signuppassword" id="signuppassword" class="form-control" placeholder="***********" pattern="^\S{6,}$" onchange="this.setCustomValidity(this.validity.patternMismatch ? 'Must have at least 6 characters' : ''); if(this.checkValidity()) form.password_two.pattern = this.value;"
                required>
            </div>
            <div class="form-group">
              <label class="">Confirm Password </label>
              <input type="password" name="signupcpassword" id="signupcpassword" class="form-control" pattern="^\S{6,}$" onchange="this.setCustomValidity(this.validity.patternMismatch ? 'Please enter the same Password as above' : '');" placeholder="***********" required>
            </div>
            <div class="form-group">
              <label><input type="checkbox" name="signupcondition" id="signupcondition" required> I agree with the <a href="javascript:;">Terms &amp; Conditions</a> for Registration.</label>
            </div>
            <div class="form-group">
              <input type="submit" name="signupsubmit" value="Sign Up" class="btn btn-block btn-blue">
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="forgotPass" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <form method="post" id="forgotpassForm">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Forgot Password</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label>Email </label>
              <input type="email" name="forgotemail" id="forgotemail" class="form-control" placeholder="Enter your valid email..." required>
            </div>
            <div class="form-group">
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Sign In</button>
            <button type="submit" name="forgotPass" class="btn btn-primary"><i class="fa fa-envelope"></i> Send Request</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
      </div>
    </div>
  </div>
</div>