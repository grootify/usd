 <header>
         <!--  Remove dark nav  -->
         <div class="upperSlip">
            <div class="container">
               <div class="row">
                  <div class="col-lg-6">
                     <div class="infoText">
                        <h6>Free Shipping for Orders Over $200</h6>
                     </div>
                  </div>
                   <div class="col-lg-6">
                     <div class="infoText text-right">
                         <h6> Need Information & Help? Call us today<span class="phneicn"><img src="<?php echo base_url();?>assets/images/icon/phone.png"></span> 302.966.9743</h6>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <nav class="navbar navbar-expand-lg navbar-light navbarAgribzr">
            <div class="container">
               <a class="navbar-brand" href="<?php echo base_url();?>"><img src="<?php echo base_url();?>assets/images/logo/logo-usd.jpg" alt="logo"> </a>
               <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
               <span class="navbar-toggler-icon"></span>
               </button>
             
               <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <div class="row">
                    <div class="col-lg-12">
                       <ul class="topNav navbar-nav float-right">
                           <li class="nav-item"><a class="nav-link" href=""> ORDER HISTORY </a></li>
                           <li class="nav-item"><a class="nav-link" href=""> SHOPPING LIST </a></li>
                           <li class="nav-item"><a class="nav-link" href=""> REGULAR INVENTORY LIST  </a></li>
                           <li class="nav-item"><a class="nav-link" href=""> QUICK ORDER FORM  </a></li>
                           <li class="nav-item"><a class="nav-link" href=""> UPLOAD QUOTE/ORDER  </a></li>
                           <li class="nav-item"><a class="nav-link" href=""> NEGOTIATE A PRICE </a></li>
                       </ul>
                     </div>
                     <div class="col-lg-12">
                        <ul class="navbar-nav bottomNav float-right">
                         
                           <!--  <li class="nav-item dropdown">
                              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  Resources
                              </a>
                              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                  <a class="dropdown-item" href="#">Carfax</a>
                                  <a class="dropdown-item" href="#">Carproof</a>
                                  <div class="dropdown-divider"></div>
                                  <a class="dropdown-item" href="#">Omnivic</a>
                              </div>
                              </li> -->
                           <li class="nav-item">
                              <a class="nav-link arrow" href="#">Products</a>
                           </li>
                
                           <li class="nav-item">
                              <a class="nav-link" href="#"> Promotions/Free Offers </a>
                           </li>
                             <li class="nav-item">
                              <a class="nav-link arrow" href="#">  My account </a>
                           </li>
                             <li class="nav-item">
                              <a class="nav-link " href="#"> Company Info </a>
                           </li>
                             <li class="nav-item">
                              <div class="input-group">
                                 <input type="text" class="form-control" placeholder="Search Products, Item or Manufacturer">
                                 <div class="input-group-append">
                                    <button class="btn btnSearch" type="button">
                                    <img src="<?php echo base_url();?>assets/images/icon/searchicon.png">
                                    </button>
                                 </div>
                              </div>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link btn loginBtn" type="button" data-toggle="modal" data-target="#basicPlanModal">Sign In</a>                  
                           </li>
                            <li class="nav-item">
                              <div id="countQuote" class="cartTotal" style="display: inline;">
                                  <?php 
                                      if($this->input->cookie('totalQuote')){
                                  $totalQuote=$this->input->cookie('totalQuote');?>
                                    <div class="quoteTotal" id="quoteTotal"><?php echo $totalQuote;?></div>
                                  <?php  }else if (isset($myQuote_list) && count($myQuote_list)!='0') {?>
                                    <div class="quoteTotal" id="quoteTotal"><?php echo count($myQuote_list);?></div>
                                  <?php }  ?>

                              </div>
                              <a href="<?php echo base_url();?>quotation" class="nav-link cart arrow2"><img src="<?php echo base_url();?>assets/images/icon/my-cart.png"></a>
                            </li>
                        </ul>
                 </div>
               </div>
               </div>
            </div>
         </nav>
      </header>