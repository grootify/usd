 <section class="footer">
          <div class="container">
              <div class="row">
                 
                  <div class="col-lg-8">
                      <div class="row">
                         <div class="col-lg-3">
                           <div class="footerLinks">
                            <h3>ABOUT US</h3>
                            <ul>
                               <li><a href=""> Patterson Companies</a></li>
                               <li><a href="">Patterson Foundation</a></li>
                               <li><a href="">Investors</a></li>
                               <li><a href="">Careers</a></li>
                            </ul>

                            
                             
                         </div>
                         </div>
                         <div class="col-lg-3">
                           <div class="footerLinks">
                            <h3>CONTACT US</h3>
                            <ul>
                               <li><a href="">Get Help</a></li>
                               <li><a href="">Locate a Branch</a></li>
                               <li><a href="">Request Service</a></li>
                               <li><a href="">Reset Password</a></li>
                               <li><a href="">Newsletters</a></li>
                            </ul>

                            
                             
                         </div>
                         </div>
                         <div class="col-lg-3">
                           <div class="footerLinks">
                            <h3>PRODUCTS</h3>
                            <ul>
                               <li><a href="">Saliva Ejectors </a></li>
                               <li><a href="">Autoclave Pouches</a></li>
                               <li><a href="">Dental Supply Gloves</a></li>
                               <li><a href="">Saliva Ejectors & HVE</a></li>
                               <li><a href="">Matrix Bands Metal</a></li>
                               <li><a href="">Topical Anesthetics</a></li>
                               <li><a href="">Endodontic Hand Files</a></li>
                            </ul>

                            
                             
                         </div>
                         </div>
                         <div class="col-lg-3">
                           <div class="footerLinks">
                            
                            

                            <h3>Quick Links</h3>
                             <ul>
                               <li><a href="">Create an Account</a></li>
                               <li><a href="">Search MSDS/SDS</a></li>
                               <li><a href="">Vendor Resources</a></li>
                               <li><a href="">FAQs</a></li>
                            </ul>
                         </div>
                         </div>
                      </div>
                  </div>

                   <div class="col-lg-4">
                     <div class="footerDtl">
                        <h4>Headquarters</h4>
                        <p>1031 Mendota Heights Rd Saint Paul, MN 55120.</p>

                        <h4>Main Office</h4>
                        <p>1031 Mendota Heights Rd Saint Paul, MN 55120.</p>

                     </div>
                  </div>
              </div>

              <div class="row">
                 
                 <div class="col-lg-4">
                    <div class="socialLinks">
                        <ul class="list-inline mx-auto justify-content-center">
                           <li><a href=""> <img src="assets/images/icon/fb.png"> </a></li>
                           <li><a href=""> <img src="assets/images/icon/tw.png"> </a></li>
                           <li><a href=""> <img src="assets/images/icon/yt.png"> </a></li>
                           <li><a href=""> <img src="assets/images/icon/in.png"> </a></li>
                        </ul>
                    </div>
                 </div>

                   <div class="col-lg-4 ml-auto">
                     <ul class="other3link">
                                <li><a href="#">Disclaimer </a></li>
                                <li><a href="#">Terms of Use </a></li>
                                <li><a href="#">Sitemap </a></li>
                    </ul>
                 </div>
              </div>

              
                  <div class="otherLinks">
                      <div class="row">
                         <div class="col-lg-12 text-center">
                             <p>© 2020 United Source Dental. All rights reserved</p>
                         </div>

                        
                      </div>
                  </div>
          </div>
      </section>



  <script
         src="https://code.jquery.com/jquery-3.4.1.min.js"
         integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
         crossorigin="anonymous"></script>
          <script type="text/javascript" src="<?php echo base_url();?>assets/js/wow.min.js"></script>
      <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
      <script src="<?php echo base_url();?>assets/js/swiper.min.js"></script>
      <script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url();?>assets/js/script.js"></script>

    <script src="<?php echo base_url();?>assets/js/jquery-3.3.1.min.js"></script>
    
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <!-- popper js-->
    <script type="text/javascript">
         var BASE_URL = '<?php echo base_url(); ?>';
    </script>
    <script src="<?php echo base_url();?>assets/js/script.js?v=1.0"></script>
    <script src="<?php echo base_url();?>assets/js/popper.min.js"></script>
    <!-- Bootstrap js-->
    <script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
    <!-- slick js-->
    <script src="<?php echo base_url();?>assets/js/slick.js"></script>

    
    <!-- Menu js -->
    <script src="<?php echo base_url();?>assets/js/menu.js"></script>
    <!-- Lazy Load js -->
    <script src="<?php echo base_url();?>assets/js/lazysizes.js"></script>
    <!-- bootstrap-notify js -->
    <script src="<?php echo base_url();?>assets/js/bootstrap-notify.min.js"></script>
    <!-- Custome scripts js-->
    <script src="<?php echo base_url();?>assets/js/custom-scripts.js?v=1.0"></script>
   <!--  <script src="js/timer-two.js"></script> -->

   <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
  <script src='https://www.google.com/recaptcha/api.js'></script> 
    <script type="text/javascript">
        // $(window).on('load',function() {
        
        //     try{             
        //         if( $('#recaptcha-login').length ) { 
        //              window.loginRecaptcha = grecaptcha.render('recaptcha-login', {'sitekey' : '6Ld8_yITAAAAABXHvPJ5gdMdnwwrUBe3pZrLJXEt', "callback":reCaptchaCallback});
        //         }
        //         if( $('#recaptcha-register').length ) { 
        //             window.registerRecaptcha = grecaptcha.render('recaptcha-register', {'sitekey' : '6Ld8_yITAAAAABXHvPJ5gdMdnwwrUBe3pZrLJXEt', "callback":reCaptchaCallback});
        //         }
        //         if( $('#recaptcha-subscribe').length ) { 
        //               window.subscribeRecaptcha = grecaptcha.render('recaptcha-subscribe', {'sitekey' : '6Ld8_yITAAAAABXHvPJ5gdMdnwwrUBe3pZrLJXEt', "callback":reCaptchaCallback});
        //         }
        //         if( $('#recaptcha-forgot').length ) { 
        //              window.forgotRecaptcha = grecaptcha.render('recaptcha-forgot', {'sitekey' : '6Ld8_yITAAAAABXHvPJ5gdMdnwwrUBe3pZrLJXEt', "callback":reCaptchaCallback});
        //         }
        //         if( $('#recaptcha-reset').length ) { 
        //             window.resetRecaptcha = grecaptcha.render('recaptcha-reset', {'sitekey' : '6Ld8_yITAAAAABXHvPJ5gdMdnwwrUBe3pZrLJXEt', "callback":reCaptchaCallback});
        //         }

        //     } catch(e) {}
        // }); 
        // window.onscroll = function() {
        //     scrollFunction()
        // };

        function scrollFunction() {
            if($('#navbar-sticky').length) {
                if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                    document.getElementById("navbar-sticky").style.top = "0";
                } else {
                    document.getElementById("navbar-sticky").style.top = "-110px";
                }
            }
        }

        $(document).ready(function() {
            $('.slider').carousel({
                dist: 0,
                shift: 0,
                padding: 20,
            });
        });
    </script>

<script type="text/javascript">

  
jQuery(document).ready(function(){
    jQuery('#hideshow').on('click', function(event) {        
       jQuery('#content').toggle('show');
    });
});

$(function() {
    // Remove button click
    $(document).on(
        'click',
        '[data-role="dynamic-fields"] > .form-inline [data-role="remove"]',
        function(e) {
            e.preventDefault();
            $(this).closest('.form-inline').remove();
           
        }
    );
   
    // Add button click
    $(document).on(
        'click',
        '[data-role="dynamic-fields"] > .form-inline [data-role="add"]',
        function(e) {
            e.preventDefault();
            var container = $(this).closest('[data-role="dynamic-fields"]');
            var total = $('.form-inline').length;
            if(total==2){
               // $("#removeSec").hide();
                 new_field_group = container.children().filter('.form-inline:first-child').find('#removeSec').hide();
            }else{
              // $("#removeSec").show();
                new_field_group = container.children().filter('.form-inline:first-child').find('#removeSec').show();
            }


           // new_field_group = container.children().filter('.form-inline:first-child').find('#removeSec').hide();
            new_field_group = container.children().filter('.form-inline:first-child').clone();
            new_field_group.find('label').html('Upload Document'); new_field_group.find('input').each(function(){
                $(this).val('');
            });
            container.append(new_field_group);

        }
    );
});
$(document).ready(function(){
    
    var total = $('.form-inline').length;
    if(total==2){
        $("#removeSec").hide();
    }/*else{
        $("#removeSec").show();
    }
    console.log(total);
    console.log("total");*/
});



/*for upload attachment*/
$(function() {
    // Remove button click
    $(document).on(
        'click',
        '[data-role="dynamic-fields"] > .form-inlines [data-role="remove"]',
        function(e) {
           // e.preventDefault();
            /* start delete cookie */
                Itemname = $(this).find('.Itemname').val();
                quantity = $(this).find('.quantityItemname').val();
                var quoteArry=[];
                var customQuotes=$.cookie('customQuotes');
                if(customQuotes) {
                    quoteArry = customQuotes.split(',');
                }
                console.log(customQuotes);
                console.log("check");
                console.log(quoteArry);
                return;
               /* for(var i=0;i<quoteArry.length;i++){

                    if(quoteArry[i]==file_value){
                      var index = quoteArry.indexOf(quoteArry[i]);
                    if (index > -1) {
                      quoteArry.splice(index, 1);
                    }
                        var newQuoteArry=[];
                        newQuoteArry=quoteArry;
                        $.cookie('customQuotes', newQuoteArry, { expires : 1, path : '/' });

                    }

                }*/
            /*end delete cookie*/
           // $(this).closest('.form-inline').remove();
        }
    );
    // Add button click
    $(document).on(
        'click',
        '[data-role="dynamic-fields"] > .form-inlines [data-role="add"]',
        function(e) {
            e.preventDefault();
            var container = $(this).closest('[data-role="dynamic-fields"]');
            new_field_group = container.children().filter('.form-inlines:first-child').clone();
          new_field_group.find('label').html('Upload Document'); new_field_group.find('input').each(function(){
                $(this).val('');
            });
            container.append(new_field_group);
        }
    );
});


// file upload




$(document).ready(function(){

  $('.input').focus(function(){
    $(this).parent().find(".label-txt").addClass('label-active');
  });

  $(".input").focusout(function(){
    if ($(this).val() == '') {
      $(this).parent().find(".label-txt").removeClass('label-active');
    };
  });

});


    
$("#forgotModal").click(function(){
    $("#loginForm").hide();
    $("#forgotForm").show();
})

$("#registerModal").click(function(){
    $("#loginForm").hide();
    $("#registarForm").show();
})

$("#loginModalBtn").click(function(){
    $("#registarForm").hide();
    $("#loginForm").show();
})


$("#loginModalBtn-1").click(function(){
    $("#forgotForm").hide();
    $("#loginForm").show();
})


$("#loginFormbtn").click(function(){
    $("#forgotForm, #registarForm").hide();
    $("#loginForm").show();
})

  </script>
   
</body>

</html>

