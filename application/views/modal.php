
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content loginForm">

            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="loginModal" id="loginForm">
            
                <h4><img src="<?php echo base_url();?>assets/images/logo-mono.png" alt="images"> Login</h4>

            <form id="signinForm" action="javascript:void(0);" method="post">
            <p class="center"><em id="login_response"></em></p>
            <label>
                <p class="label-txt"><i class="fa fa-user" aria-hidden="true"></i> Username</p>
                <input type="text" class="input" name="user_email" id="user_email" required autofocus>
                <div class="line-box">
                <div class="line"></div>
                </div>
            </label>
            <label>
                <p class="label-txt"><i class="fa fa-key" aria-hidden="true"></i> Password</p>
                <input type="password" class="input" name="user_password" id="user_password" required>
                <div class="line-box">
                <div class="line"></div>
                 </div>
            </label>
                <div class="loginBtn">
                    <button class="btn btn-theme brown-btn btnSubmit" onclick="login();">Login</button>
                    <!-- <a href="#" >Login</a> -->
                </div>
                <div class="frgtAccount">
                    <a href="javascript:void(0)"  id="forgotModal">Forget Password?</a>
                </div>
                <div class="signUptext">
                    Don't have an account? <span class="theme-color"> <a href="javascript:void(0)" id="registerModal" >Sign up now!</a></span>
                </div>
            </form>
          
            </div>
            <div class="registarModal" id="registarForm">
            <h4><img src="<?php echo base_url();?>assets/images/logo-mono.png" alt="images"> Join Now!</h4>
            <form id="registerForm"action="javascript:void(0);" method="post">
            <label>
                <p class="label-txt">Full name *</p>
                <input type="text" class="input" id="registerName" name="registerName">
                <div class="line-box">
                <div class="line"></div>
                </div>
            </label>
            <label>
                <p class="label-txt"> Email *</p>
                <input type="email" class="input" name="registerEmail" id="registerEmail">
                <div class="line-box">
                <div class="line"></div>
                 </div>
            </label>
             <label>
                <p class="label-txt"> Password *</p>
                <input type="password" class="input" name="registerPassword" id="registerPassword">
                <div class="line-box">
                <div class="line"></div>
                 </div>
            </label>
               <label>
                <p class="label-txt">Confirm Password *</p>
                <input type="password" class="input" id="cpassword" name="cpassword">
                <div class="line-box">
                <div class="line"></div>
                 </div>
            </label>
                 <label>
                <p class="label-txt">Mobile *</p>
                <input type="text" class="input" id="registerContact" name="registerContact">
                <div class="line-box">
                <div class="line"></div>
                 </div>
            </label>
                <div class="loginBtn">
                    <button class="btn btn-theme brown-btn" id="register-btn">Register</button>
                    
                </div>

                <div class="signUptext">
                    Already have and account <span class="theme-color"> <a href="#" id="loginModalBtn">Login</a></span>
                </div>
            </form> 
            </div>
            <div class="forgotPassword" id="forgotForm">

            <h4><img src="<?php echo base_url();?>assets/images/logo-mono.png" alt="images"> Forgot Account</h4>

            <form>
            <label>
                <p class="label-txt"> Email *</p>
                <input type="text" class="input" id="email">
                <div class="line-box">
                <div class="line"></div>
                 </div>
            </label>
                <div class="loginBtn">
                    <a href="#" class="btn btn-theme brown-btn">Send reset link</a>
                </div>
                <div class="signUptext">
                    <span class="theme-color"> <a href="#" id="loginModalBtn-1" >Go back to login</a></span>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>
</div>