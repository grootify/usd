<?php 
function normalize_str($str) {    
    $invalid = array('â€™'=>"'", 'â€˜' => "'", '’' => '\'', '‘'=>'\'', '“' => "\"", '”' => "\"", '—' => '-');        
    $str = str_replace(array_keys($invalid), array_values($invalid), $str);        
    return html_entity_decode($str);    
}

function slugify($text) {
    $text = preg_replace('~[^\pL\d]+~u', '-', $text);
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
    $text = preg_replace('~[^-\w]+~', '', $text);
    $text = trim($text, '-');
    $text = preg_replace('~-+~', '-', $text);
    $text = strtolower($text);
    if (empty($text)) { return 'n-a'; }
    return $text;
}

function no_html($html) {
    $html = strip_tags($html);
    $html = preg_replace('/[^A-Za-z0-9\. --,]/', ' ', $html);        
    $html = str_replace('&nbsp', '', $html);        
    return $html;
}

function filter_str($str) {
    $invalid = array('â€™'=>"'", 'â€˜' => "'", '’' => '\'', '‘'=>'\'', '“' => "\"", '”' => "\"", '—' => '-', '<p>' => '','</p>' => '','<br>' => '', '</br>' => '',"&nbsp" => '', "<h1>" => '', "</h1>" => '', '<strong>' => '' , '</strong>' => '', ';' => ' ', '"' => '\'',  '>' => '', '<' => '');
    $str = str_replace(array_keys($invalid), array_values($invalid), $str);
    $str = preg_replace('/\s+/', ' ', trim($str));
    return strip_tags($str);
} 

function get_words($sentence, $count = 30) {
    preg_match("/(?:[^\s,\.;\?\!]+(?:[\s,\.;\?\!]+|$)){0,$count}/", $sentence, $matches);
    return $matches[0];
}

function timify($date) {
    $date = strtotime($date);
    $now = time();
    $diff = $now - $date;

    if ($diff < 60){
        return sprintf($diff > 1 ? '%s seconds ago' : 'a second ago', $diff);
    }

    $diff = floor($diff/60);

    if ($diff < 60){
        return sprintf($diff > 1 ? '%s minutes ago' : 'one minute ago', $diff);
    }

    $diff = floor($diff/60);

    if ($diff < 24){
        return sprintf($diff > 1 ? '%s hours ago' : 'an hour ago', $diff);
    }

    $diff = floor($diff/24);

    if ($diff < 7){
        return sprintf($diff > 1 ? '%s days ago' : 'yesterday', $diff);
    }

    if ($diff < 30)
    {
        $diff = floor($diff / 7);

        return sprintf($diff > 1 ? '%s weeks ago' : 'one week ago', $diff);
    }

    $diff = floor($diff/30);

    if ($diff < 12){
        return sprintf($diff > 1 ? '%s months ago' : 'last month', $diff);
    }

    $diff = date('Y', $now) - date('Y', $date);

    return sprintf($diff > 1 ? '%s years ago' : 'last year', $diff);
}

function datify($date) {    
    return date('F d, Y', strtotime($date));
}

function sanitizedParam($param) {
    $pattern[0] = "%,%";
    $pattern[1] = "%#%";
    $pattern[2] = "%\(%";
    $pattern[3] = "%\)%";
    $pattern[4] = "%\{%";
    $pattern[5] = "%\}%";
    $pattern[6] = "%<%";
    $pattern[7] = "%>%";
    $pattern[8] = "%`%";
    $pattern[9] = "%!%";
    $pattern[10] = "%\\$%";
    $pattern[11] = "%\%%";
    $pattern[12] = "%\^%";
    $pattern[13] = "%=%";
    $pattern[14] = "%\+%";
    $pattern[15] = "%\|%";
    $pattern[16] = "%\\\%";
    $pattern[17] = "%:%";
    $pattern[18] = "%'%";
    $pattern[19] = "%\"%";
    $pattern[20] = "%;%";
    $pattern[21] = "%~%";
    $pattern[22] = "%\[%";
    $pattern[23] = "%\]%";
    $pattern[24] = "%\*%";
    $pattern[25] = "%&%";
    $sanitizedParam = preg_replace($pattern, "", $param);
    return $sanitizedParam;
}
    
function sanitizedURL($param) {
    $pattern[0] = "%,%";
    $pattern[1] = "%\(%";
    $pattern[2] = "%\)%";
    $pattern[3] = "%\{%";
    $pattern[4] = "%\}%";
    $pattern[5] = "%<%";
    $pattern[6] = "%>%";
    $pattern[7] = "%`%";
    $pattern[8] = "%!%";
    $pattern[9] = "%\\$%";
    $pattern[10] = "%\%%";
    $pattern[11] = "%\^%";
    $pattern[12] = "%\+%";
    $pattern[13] = "%\|%";
    $pattern[14] = "%\\\%";
    $pattern[15] = "%'%";
    $pattern[16] = "%\"%";
    $pattern[17] = "%;%";
    $pattern[18] = "%~%";
    $pattern[19] = "%\[%";
    $pattern[20] = "%\]%";
    $pattern[21] = "%\*%";
    $sanitizedParam = preg_replace($pattern, "", $param);
    return $sanitizedParam;
}

function getPager($totalPages, $currentPage, $pageSize=5) {
    if ($totalPages <= $pageSize) {
        $startPage = 1;
        $endPage = $totalPages;
    } else {
        if ($currentPage <= 3) {
            $startPage = 1;
            $endPage = $pageSize;
        } else if ($currentPage + 2 >= $totalPages) {
            $startPage = $totalPages - 4;
            $endPage = $totalPages;
        } else {
            $startPage = $currentPage - 2;
            $endPage = $currentPage + 2;
        }
    }
    
    $pages = range($startPage, $endPage); 
    return array(        
        'startPage' => $startPage,
        'currentPage' => $currentPage,            
        'endPage' => $endPage,            
        'totalPages' => $totalPages,
        'pages' => $pages
    );
}

function setPager($total_pages=1, $page_no=1, $slug='') {    
    $pager = getPager($total_pages, $page_no);

    if($pager['totalPages'] > 1 && sizeof($pager['pages'])>0) {
        $viewData = '<div class="row float-right">
                        <ul class="col-md-12 pagination pagination-circled pull-right mt-3" id="pagin">';
        if($pager['currentPage'] != 1){
            if($pager['totalPages']>5){
                $viewData .= '<li class="page-item ' . ($pager['currentPage'] == 1 ? 'disabled' : '').'">
                                <a class="page-link" href="' . current_url() . get_page_params() . '"><i class="fa fa-angle-double-left"></i></a>
                            </li>';
            }      
                $viewData .= '<li class="page-item ' . ($pager['currentPage'] == 1 ? 'disabled' : '').'">
                                <a class="page-link" href="' . current_url() . get_page_params($pager['currentPage'] - 1) .'"><i class="fa fa-angle-left"></i></a>
                            </li>';
        }
                            
        foreach ($pager['pages'] as $page) { 
            $viewData .= '<li class="page-item ' . ($pager['currentPage'] == $page ? 'active' : '').'">';
                if($pager['currentPage'] == $page) {
                    $viewData .= '<a class="page-link" class="active">' . $page . '</a>';
                } else {
                    $viewData .= '<a class="page-link" href="'. ($page==1 ? (current_url() . get_page_params($page)) : (current_url() . get_page_params($page))).'">'. $page .'</a>';
                }
            $viewData .= '</li>';
        }
                            
        if($pager['currentPage'] != $pager['totalPages']) {
            $viewData .= '<li class="page-item ' . ($pager['currentPage'] == $pager['totalPages'] ? 'disabled' : '') .'">
                            <a class="page-link" href="'. current_url() . get_page_params($pager['currentPage'] + 1)  .'"><i class="fa fa-angle-right"></i></a>
                        </li>';
            if($pager['totalPages']>5) {
                $viewData .= '<li class="page-item ' . ($pager['currentPage'] == $pager['totalPages'] ? 'disabled' : '').'">
                                <a class="page-link" href="'. current_url(). get_page_params($pager['totalPages']) .'"><i class="fa fa-angle-double-right"></i></a>
                            </li>';
            }
        }
        $viewData .= '</ul></div>';

        echo $viewData;
    }
}

function get_page_params($page_no=1) {
    $get_params = '';
    $get_params = get_all_get('page');        
    
    if($page_no!=1){
        if($get_params == ''){
            $get_params .= '?page='.$page_no;
        } else {
            $get_params .= '&page='.$page_no;
        }
    }
    return $get_params;
    // $page_params = $get_params . $page_slug . $page_no;
    // return $page_params;

}

function get_all_get($exlude='') {
    $firstRun = true; 

    $output = ''; 
    
    foreach($_GET as $key=>$val) { 
        if($exlude!=$key) {
            if(!$firstRun) { 
                $output .= "&"; 
            } else { 
                $firstRun = false; 
            } 
            
            $output .= $key."=".$val;            
        }
    } 
    
    if($output=='') {
        return '';
    } else {
        return '?' . $output;
    }
}   

?>