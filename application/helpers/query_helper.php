<?php 

function get_total_pages($query_condition, $limit=12){
    $CI =& get_instance();
    $CI->load->model('standard_model');
    
    $data_query['table'] = $query_condition['table'];
    $data_query['field'] = "count(distinct ".$query_condition['table'].".id) as total_ids";
    if(isset($query_condition['condition'])) {
        $data_query['condition'] = $query_condition['condition'];
    }
    if(isset($query_condition['join'])) {
        $data_query['join'] = $query_condition['join'];
    }        

    if(isset($query_condition['where_in'])) {
        $data_query['where_in'] = $query_condition['where_in'];
    }        

    if(isset($query_condition['or_where_in'])) {
        $data_query['or_where_in'] = $query_condition['or_where_in'];
    }        

    if(isset($query_condition['like'])) {
        $data_query['like'] = $query_condition['like'];
    }        
    
    $CI->standard_model->set_query_data($data_query);
    $result = $CI->standard_model->select();
    
    if(isset($result->total_ids)) {
        $total_pages = ceil($result->total_ids / $limit);
        return $total_pages;
    } else {
        return 0;
    }
}

?>