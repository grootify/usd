
jQuery(document).ready(function(){
		$(".dropdown").hover(
				function() { $('.dropdown-menu', this).stop().fadeIn("fast");
				},
				function() { $('.dropdown-menu', this).stop().fadeOut("fast");
		});
});
$(document).ready(function() {
		$(".form-check").on("keyup", function() {
				$('#ajaxResponseDiv').html('');
		});

});

function isEmail(contact_email) {
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		return regex.test(contact_email);
}


$(window).on('load', function() {
		if(window.innerWidth < 800) {
				$('.theme-searcbar').find('.inMobileform').html('');
				 console.log(window.innerWidth); 
		}
})

function setDefaultAddress(id,user_id) {
	var id=id;
	var user_id=user_id;
	$.ajax({
						type: "POST",
						data: {
								'id': id,
								'user_id': user_id,
						},
						url: BASE_URL + "api/setDefaultAdress",
						success: function(data){
								console.log(data);
								 if(data.status) {      
									// $('#address_status').html('Set as default address');
									 window.location.reload();
									 
								} else {
									 $('#address_status').html('Error to set as default address');
								}
						},
						error: function(err){
							console.log("error");
							 console.log("error in creating address");
						return;
								$("html, body").animate({ scrollTop: 0 }, "fast");
																					 
								// $('#addAddress_response').html('<i class="fa fa-exclamation-triangle"></i> Something went wrong!');
								// $('#addAddress_response').addClass('is_error');
						}
				});     
}
function deleteAddress(id) {
		var id=id;
	
	$.ajax({
						type: "POST",
						data: {
								'id': id,
						},
						url: BASE_URL + "api/deleteAdress",
						success: function(data){
								console.log(data);
								 if(data.status) {      
									 //$('#address_status').html('Address is deleted');
									 window.location.reload();
									 
								} else {
									 $('#address_status').html('Error to set as delete address');
								}
						},
						error: function(err){
							console.log("error");
							 console.log("error in deleting address");
						return;
								$("html, body").animate({ scrollTop: 0 }, "fast");
																					 
								// $('#addAddress_response').html('<i class="fa fa-exclamation-triangle"></i> Something went wrong!');
								// $('#addAddress_response').addClass('is_error');
						}
				});  
}
function editAddress(id){
		var id=id;
		$.ajax({
						type: "POST",
						data: {
								'id': id,
						},
						url: BASE_URL + "edit_addresses",
						success: function(data){
								console.log(data);
								 if(data.status) {      
									 //$('#address_status').html('Address is deleted');
									 window.reload();
									 
								} else {
								 //  $('#address_status').html('Error in editing address');
								}
						},
						error: function(err){
							console.log("error");
							 console.log("error in edit address");
						return;
								$("html, body").animate({ scrollTop: 0 }, "fast");
																					 
								// $('#addAddress_response').html('<i class="fa fa-exclamation-triangle"></i> Something went wrong!');
								// $('#addAddress_response').addClass('is_error');
						}
				});  
}

function contactForm() {   
		var contact_name = $('#contact_name').val();
		var contact_email = $('#contact_email').val();        
		var contact_mobile = $('#contact_mobile').val();
		var contact_message  = $('#contact_message').val();
		if (contact_name == "") {
				$('#ajaxResponseDivs').html('');
				$('#ajaxResponseDivs').html('Please Enter Name.');         
		} else if (contact_email == "") {
				$('#ajaxResponseDivs').html('');
				$('#ajaxResponseDivs').html('Please Enter Email.');
		} else if (!isEmail(contact_email)) {
				$('#ajaxResponseDivs').html('');
				$('#ajaxResponseDivs').html('Please Enter Valid Email.');           
		} 
		else if (contact_mobile == "") {
				$('#ajaxResponseDivs').html('');
				$('#ajaxResponseDivs').html('Please Enter Mobile Number.');         
		} else if (contact_mobile.length < 10) {
				$('#ajaxResponseDivs').html('');
				$('#ajaxResponseDivs').html('Please Enter Valid Mobile Number.');           
		} else if (contact_message == '') {
						$('#ajaxResponseDivs').html('');
						$('#ajaxResponseDivs').html('Please Enter message.');      
		} else {                   
				$('#contact-btn').attr('disabled',true);    
				$('#contact-btn').text('Submitting!...');
				var form = $('#contactForm')[0]; 
				var formData = new FormData(form);
				
				$.ajax({
						type: "post",
						url: BASE_URL+'api/send_message',
						data: formData,
						processData: false,
						contentType: false,       
						success: function (response) {
								if(response.status) {
										$('#ajaxResponseDivs').html('');
										$('#ajaxResponseDivs').css('color','#4bb543');
										$('#ajaxResponseDivs').html('Thank you for sharing details with us. We will contact you shortly.');
										// $('#submit-btn').attr('disabled', false);
										$('#contact-btn').attr('disabled',false);
										 $('#contact-btn').text('Submit');
										setTimeout(function() {
												$('#ajaxResponseDivs').fadeOut('fast');
										},3500); 
								}

						},
						error : function () {

						}
				});

				$('#contactForm').each(function() {
						this.reset();
				});
		}
} 

function setCookie(cname, cvalue, exdays) {
		var d = new Date();
		d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
		var expires = "expires=" + d.toUTCString();
		document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
		console.log(document.cookie);
		console.log(expires);
		console.log("checck");

}

 function setStatus(cart_id){

				var formData={
						id:cart_id
				};
				
				$.ajax({
						url: BASE_URL+'api/updateStatus',
						type: 'POST',
						data: formData,
						beforeSend: function(){
						},
						success: function(response) {           
								if(response.status){
									 console.log(response);
									 $('#countQuote').load(document.URL +  ' #quoteTotal');    
									 $('.cart-table').load(document.URL +  ' .quote-data'); 
									 var totalQuote=$("#quoteTotal").text();
									 if(totalQuote==1){
										 window.location.href=BASE_URL;
									 }
								}
						}, 
						error: function(error) {
						}
				});   
		}
function addToQuote(product_id,user_id){

		if (product_id) {
			 var formData={
						product_id:product_id,
						user_id:user_id
				};

				$.ajax({
						url: BASE_URL+'api/addQuote',
						type: 'POST',
						data: formData,
						beforeSend: function(){
						},
						success: function(response) {     
					if(response.message=="Product already added to Quote!"){
								$('#countQuote').load(document.URL +  ' #quoteTotal');
								$('#response').text(response.message);
								$("#response").css("display", "block");
								$('#response').addClass('alert alert-danger');
								setTimeout(function() {
												$('#response').fadeOut('fast');
											}, 2500); 
						}else{
								console.log(response);  
								$('#countQuote').load(document.URL +  ' #quoteTotal');
								$('#response').css({"background-color":"#00174e","color":"#fff"});
								$('#response').text('Product successfully added to Quote!');
								$("#response").css("display", "block");
								$('#response').removeClass('alert alert-danger');
								$('#response').addClass('alert alert-info wishlist_success');

								setTimeout(function() {
								$('#response').fadeOut('fast');
								}, 2500); 
						}    
							 
						}, 
						error: function(error) {
						}
				});    
		}

}
 
 function SetProductIntoCookie(productId,value = 0,wishlistId = 0) {

		 var cpID = $.cookie('cpID', cpID);
		 var quote;
		 var cpIDArr = [];
		
		if (cpID) {
				cpIDArr = cpID.split(',');
		}
	 
		var quantity = '';
		if(value != 0) {
				quantity = value;
		}

	if(quantity > 0) {
		var isFound = true;
		var changeQuantity = false;
		
		for(var i=0; i<cpIDArr.length; i++){
				
			cpArr = cpIDArr[i].split(':')
		 
			if(cpArr[0] == productId) {
				isFound = false;
				
			}
			if(cpArr[1] != quantity) {
				if(productId == cpArr[0]) {
					cpIDArr[i] = cpArr[0] +":"+quantity;
					console.log(cpIDArr[i]);
					changeQuantity = true;  
							
				}
			} 
		} 
	 
	if(isFound) {
			productId += (':'+ quantity);
			if(cpID && cpID!='') {
				cpID += (','+ productId);
			} else {
				cpID = productId;
			}
		 $.cookie('cpID', cpID, { expires : 10, path : '/' });
		 var totalQuote=$.cookie('totalQuote',totalQuote);
		 if(totalQuote){
				quote=parseInt(totalQuote)+1;
		 }else{
				quote=1;
		 }
		
			 $.cookie('totalQuote', quote, { expires : 10, path : '/' });
			 $('.cartTotal').load(document.URL +  ' .quoteTotal'); 

			// $('#quoteTotal').text(quote);
			 $("#quoteTotal").load("totalQuote");
			 $('#response').css({"background-color":"#00174e","color":"#fff"});
			 $('#response').text('Product successfully added to Quote!');
			 $("#response").css("display", "block");
			 $('#response').removeClass('alert alert-danger');
			 $('#response').addClass('alert alert-success wishlist_success');

			setTimeout(function() {
				$('#response').fadeOut('fast');
			}, 2500);
			// if (wishlistId != 0) {
			//   deleteWishlist(productId,value,wishlistId)
			// }
			
		} else {
		 
			if(changeQuantity) {
				cpID = '';
				
				for (var i = 0; i < cpIDArr.length; i++) {
					if(cpID == '') {            
						cpID += cpIDArr[i];
					} else {
						cpID += (',' + cpIDArr[i]);
					}
				}
				$.cookie('cpID', cpID, { expires : 10, path : '/' });
				$('#response').css({"background-color":"#00174e","color":"#fff"});
				$('#response').text('Product successfully added to Quote!');
				$("#response").css("display", "block");
				$('#response').removeClass('alert alert-danger');
				$('#response').addClass('alert alert-success wishlist_success');
			
				setTimeout(function() {
					$('#response').fadeOut('fast');
				}, 2500);
				if (true) {
				 
							window.setTimeout(function() {
								$(".alert").fadeTo(500, 0).slideUp(500, function(){
									$(this).remove(); 
								});
							}, 4000);
				}
			} else {   
					$("#response").css({"background-color":"#f2dede","color":"#a94442"});
					$('#response').text('Product already added to Quote!');
					$("#response").css("display", "block");
					$('#response').addClass('alert alert-danger');
					
				window.setTimeout(function() {
								$(".alert").fadeTo(500, 0).slideUp(500, function(){
									$(this).remove(); 
								});
					}, 4000);
				 
				//console.log("Product already added to cart!");
				// if (wishlistId != 0) {
				//   deleteWishlist(productId,value,wishlistId)
				// }
			}
		 // window.location.reload();
		}    
	}

 
}
function deleteWishlist(productId,quantity){
	var proInfo=productId +":"+ quantity;
	 
	var productArry=[];
	var cpID=$.cookie('cpID');
		if(cpID) {
				productArry = cpID.split(',');
		}
	// console.log(productArry);
	// console.log("after splice dds");
		for(var i=0;i<productArry.length;i++){
			
			 if(productArry[i]==proInfo){
				 var index = productArry.indexOf(productArry[i]);
					if (index > -1) {
						productArry.splice(index, 1);
					}
					var newProductArry=[];
					newProductArry=productArry;
		 
					$.cookie('cpID', newProductArry, { expires : 1, path : '/' });
					var totalQuote=$.cookie('totalQuote',totalQuote);
					
					var quoteno=parseInt(totalQuote)-1;
					$.cookie('totalQuote', quoteno, { expires : 30, path : '/' });
					$('.quoteTotal').text(quoteno);
				 
					$('.cartTotal').load(document.URL +  ' .quoteTotal'); 
					 console.log(totalQuote);
				 console.log("check totalQuote");
					$('.quote-data').on('click','.deletebtn',function(){
								$(this).closest('tr').remove();
					});
					if(totalQuote==1){
						window.location.href=BASE_URL;
					}
			 }
			 
		}

	
}
/* login code start*/
function validateLoginForm() {
		 $( "#signinForm" ).validate( {
				rules: {
						user_email: { required: true, email: true },
						user_password: { required: true, minlength: 5 }            
				},
				messages: { 
						user_email: "Please enter a valid email address",            
						user_password: {
								required: "Please provide a password",
								minlength: "Your password must be at least 5 characters long"
						}
				},
				errorElement: "em",
				errorPlacement: function ( error, element ) {          
						error.addClass( "help-block" );
						error.insertAfter( element );
				},
				highlight: function ( element, errorClass, validClass ) {
						$( element ).parents( ".col-sm-12" ).addClass( "has-error" ).removeClass( "has-success" );          
				},
				unhighlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-12" ).addClass( "has-success" ).removeClass( "has-error" );          
				}
		});
}
function login() {
		$('#login_response').html('');
		$('#login_response').removeClass();
		validateLoginForm();
		var username = $('#user_email').val();            
		var password = $('#user_password').val();
		var remember_me = $("input[name = 'signed']:checked").val();
	 // var recptcha_response = $('#recaptcha-login').find('.g-recaptcha-response').val();
		var r_url = $('#r_url').val();
		// if(recptcha_response==''){                
		//     $('#recaptcha-login-error').html('Please confirm captcha to proceed');
		//     return;
		// }
				if(username != '' && password != '') {
				$('#login_response').html('Please Wait..');
				$('.btnSubmit').attr("disabled", true);
				$.ajax({
						type: "POST",
						data: {
								'email':username, 
								'password':password,
								// 'g-recaptcha-response':recptcha_response,
								'remember-me' : remember_me
						},
						url: BASE_URL + "api/auth/login",
						success: function(data){
								if(data.status) { 
										console.log("true");                  
										if(data.cst) {
												 var newProductArry=[];
												 setCookie("cpID", "", 0);
												 setCookie("media", "", 0);
												 setCookie("customQuotes", "", 0);
												 setCookie("totalQuote", "", 0);
												$('#successful_login').addClass('active');
												setCookie("__logged", 'true', 30);
												setCookie("__cst", data.cst, 30);
												if (r_url!= '') {
														location = r_url;
												} else {
														location = BASE_URL + 'user/dashboard';
												}
												console.log(location);
												setTimeout(function(){ window.location.href = location; }, 1000);
										}                   
								} else {
										$('.btnSubmit').attr("disabled", false);
									 // grecaptcha.reset(window.loginRecaptcha);
										if(data.user_status && data.user_status == 'pending') {
												var message = '<span><i class="fa fa-exclamation-triangle"></i> You have not verified your email.</span><br/>';
												message += '<span id="activation-link" style="color:#777;">If you have not received the verification email, please check your spam folder.<br/> <button onclick="resend_activation()" class="btn-primary btn-xs" style="border:none;">Click Here</button> to resend.</span>'
												$('#login_response').html(message);                        
										} else {                        
												$('#login_response').html('<i class="fa fa-exclamation-triangle"></i> ' +data.message);                        
										}
										$('#login_response').addClass('is_error');
										$("html, body").animate({ scrollTop: 0 }, "fast");
								}
						},
						error: function(err){
								 //grecaptcha.reset(window.loginRecaptcha);
								$('.btnSubmit').attr("disabled", false);                            
								$("html, body").animate({ scrollTop: 0 }, "fast");
								$('#login_response').addClass('is_error');
								$('#login_response').html('<i class="fa fa-exclamation-triangle"></i> Something went wrong!');
						}
				});
		}
}

function register() {
		$('#register_response').html('');
		$('#register_response').removeClass();
		validateRegisterForm();

		var full_name = $('#registerName').val();
		var email = $('#registerEmail').val();
		var password = $('#registerPassword').val();
		var mobile = $('#registerContact').val();
		// var recptcha_response = $('#recaptcha-register').find('.g-recaptcha-response').val();
	 
		// if(recptcha_response==''){
		//     $('#recaptcha-register-error').html('Please confirm captcha to proceed');
		// }
	 
		if(full_name != '' && email != '' && password != '' && mobile != '') {        
				$('#register_response').html('Please Wait..');
				$('.btnSubmit').attr("disabled", true);
				$.ajax({
						type: "POST",
						data: {
								'full_name': full_name,
								'email': email,
								'password': password,
								'mobile': mobile
								/*'g-recaptcha-response': recptcha_response*/
						},
						url: BASE_URL + "api/auth/register",
						success: function(data){
								console.log(data);
								 if(data.status) {      
									 $('#registration_successful').show();
										setCookie("__registered", email, 30);
										setTimeout(function(){ window.location.href = BASE_URL + 'api/auth/register/pending'; }, 1000);
								} else {
										$('.btnSubmit').attr("disabled", false);
										//grecaptcha.reset(window.registerRecaptcha);                                
										$("html, body").animate({ scrollTop: 0 }, "fast");
										$('#register_response').html('<i class="fa fa-exclamation-triangle"></i> ' +data.message);
										$('#register_response').addClass('is_error');
								}
						},
						error: function(err){
							 console.log("error");
							 console.log("error in register");
							 return;
								//grecaptcha.reset(window.registerRecaptcha);
								$("html, body").animate({ scrollTop: 0 }, "fast");
								$('.btnSubmit').attr("disabled", false);                            
								$('#register_response').html('<i class="fa fa-exclamation-triangle"></i> Something went wrong!');
								$('#register_response').addClass('is_error');
						}
				});        
		}
}

function validateRegisterForm() {
		 $("#signupForm").validate( {
				rules: {
						registerName: { required: true },
						registerEmail: { required: true, email: true },
						registerPassword: { required: true, minlength: 5 },
						cpassword: {   required: true, minlength: 5,equalTo: "#registerPassword"},
						registerContact:{ required:true, minlength:10, maxlength:10, number: true},
				},
				messages: { 
						registerName: {
								required: "Please enter your full name"
						},
						registerEmail: "Please enter a valid email address",   
						registerPassword: {
								required: "Please provide a password",
								minlength: "Your password must be at least 5 characters long"
						},
						cpassword: {
								required: "Please enter same password"
						},
						registerContact:{
								 required: "Please enter valid number"
						},
									 
					 
				},
				errorElement: "em",
				errorPlacement: function ( error, element ) {          
						error.addClass( "help-block" );
						error.insertAfter( element );
				},
				highlight: function ( element, errorClass, validClass ) {
						$( element ).parents( ".col-sm-12" ).addClass( "has-error" ).removeClass( "has-success" );          
				},
				unhighlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-12" ).addClass( "has-success" ).removeClass( "has-error" );          
				}
		});
}

function validateForgotForm() {
		 $( "#forgot_form" ).validate( {
				rules: {
						forgot_email: { required: true, email: true },
				},
				messages: { 
						forgot_email: "Please enter a valid email address",
				},
				errorElement: "em",
				errorPlacement: function ( error, element ) {
						error.addClass( "help-block" );
						error.insertAfter( element );
				},
				highlight: function ( element, errorClass, validClass ) {
						$( element ).parents( ".col-sm-12" ).addClass( "has-error" ).removeClass( "has-success" );          
				},
				unhighlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-12" ).addClass( "has-success" ).removeClass( "has-error" );          
				}
		});
}
function forgotPassword(){
		$("#forgot_response").html('');
		$("#forgot_response").removeClass();
		// if(dialog_selected == 'login') {
		validateForgotForm();
		var userEmail = $('#forgot_email').val();
		// var recptcha_response = $('#recaptcha-forgot').find('.g-recaptcha-response').val();
		// if(recptcha_response==''){                
		//     $('#recaptcha-forgot-error').html('Please confirm captcha to proceed');
		//     return;
		// }
		if(userEmail != '') {
				$("#forgot_response").html('Please Wait..');
				$('.btnSubmit').attr("disabled", true);
				$.ajax({
						type: "POST",
						data: {
								'email':userEmail
								// 'g-recaptcha-response':recptcha_response              
						},
						url: BASE_URL + "api/auth/forget_password",
						success: function(data){
								//grecaptcha.reset(window.forgotRecaptcha);
								$('.btnSubmit').attr("disabled", false);

								if(data.status) {
										$('#forgot_response').addClass('is_success');
										$('#forgot_response').html('Your password reset link has been sent! If you cannot find the message in your inbox, check your junk or spam folders.');
										$('#forgot_email').val('');
								} else {
										$('#forgot_response').addClass('is_error');
										$('#forgot_response').html('<i class="fa fa-exclamation-triangle"></i> ' +data.message);                        
										$("html, body").animate({ scrollTop: 0 }, "fast");
								}
						},
						error: function(err){
							 // grecaptcha.reset(window.forgotRecaptcha);
								$('.btnSubmit').attr("disabled", false);                            
								$("html, body").animate({ scrollTop: 0 }, "fast");
								$('#forgot_response').addClass('is_error');
								$('#forgot_response').html('<i class="fa fa-exclamation-triangle"></i> Something went wrong!');
						}
				});
		}
}
function validateResetForm() {
		$( "#resetForm" ).validate( {
				rules: {
						reset_password: { required: true, minlength: 5 },
						reset_confirm_password: { required: true, minlength: 5, equalTo: "#reset_password" }
				},
				messages: { 
						reset_password: {
								required: "Please provide a password",
								minlength: "Your password must be at least 5 characters long"
						},
						reset_confirm_password: {
								required: "Please provide a confirmation password",
								minlength: "Your password must be at least 5 characters long",
								equalTo: "The new password and confirmation password do not match"
						},
				},
				errorElement: "em",
				errorPlacement: function ( error, element ) {
						error.addClass( "help-block" );
						error.insertAfter( element );
				},
				highlight: function ( element, errorClass, validClass ) {
						$( element ).parents( ".col-sm-12" ).addClass( "has-error" ).removeClass( "has-success" );          
				},
				unhighlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-12" ).addClass( "has-success" ).removeClass( "has-error" );          
				}
		});
}

function reCaptchaCallback() {
		$('.recaptcha-error').html('');
}
function resetPassword() {
		$('#reset_response').html('Please Wait..');
		$('#reset_response').removeClass('');
		validateResetForm();
		var new_password = $('#reset_password').val();
		var reset_confirm_password = $('#reset_confirm_password').val();
		var reset_email = $('#reset_email').val();
		
if(new_password != ''  && reset_email != '' && reset_confirm_password!='') {

				$('.btnSubmit').attr("disabled", true);
				$.ajax({
						type: "POST",
						data: {
								'new_password': new_password,
								'email': reset_email
						},                
						url: BASE_URL + "api/auth/reset_confirm",
						success: function(data){
							 $('.btnSubmit').attr("disabled", false);
								if(data.status) {
										$('#reset_response').html(data.message);
										$('#reset_response').addClass('is_success');
										$("#resetForm").remove();
								} else {
										// grecaptcha.reset();                                
										$("html, body").animate({ scrollTop: 0 }, "fast");
										$('#reset_response').html('<i class="fa fa-exclamation-triangle"></i> ' +data.message);
										$('#reset_response').addClass('is_error');
								}
						},
						error: function(err){
							//  grecaptcha.reset(window.resetRecaptcha);
								$("html, body").animate({ scrollTop: 0 }, "fast");
								$('.btnSubmit').attr("disabled", false);
								$('#reset_response').html('<i class="fa fa-exclamation-triangle"></i> Something went wrong!');
								$('#reset_response').addClass('is_error');
						}
				});       
		}
}


function validatePersonalInfoForm() {
		$( "#Personal_info_form" ).validate( {
				rules: {
						user_name: { required: true},
						user_mobile: { required:true, minlength:10, maxlength:10, number: true}
				},
				messages: { 
						user_name: { required: "Please provide full name" },
						user_mobile: { required: "Please provide valid mobile" },
				},
				errorElement: "em",
				errorPlacement: function ( error, element ) {
						error.addClass( "help-block" );
						error.insertAfter( element );
				},
				highlight: function ( element, errorClass, validClass ) {
						$( element ).parents( ".col-sm-12" ).addClass( "has-error" ).removeClass( "has-success" );          
				},
				unhighlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-12" ).addClass( "has-success" ).removeClass( "has-error" );          
				}
		});
}

function changeUserInfo() {
		$('#Personal_info_response').html('Please Wait..');
		$('#Personal_info_response').removeClass('');
		validatePersonalInfoForm();
		var full_name = $('#user_name').val();
		var mobile = $('#user_mobile').val();
		var email = $('#setting_user_email').val();
	 
if(full_name != ''  && mobile != '' && email!='') {

				$('.btnSubmit').attr("disabled", true);
				$.ajax({
						type: "POST",
						data: {
								'full_name': full_name,
								'mobile':mobile,
								'email': email
						},                
						url: BASE_URL + "api/auth/reset_userInfo",
						success: function(data){
							 $('.btnSubmit').attr("disabled", false);
								if(data.status) {
										$('#Personal_info_response').html(data.message);
										$('#Personal_info_response').addClass('is_success');
									 
								} else {
										$("html, body").animate({ scrollTop: 0 }, "fast");
										$('#Personal_info_response').html('<i class="fa fa-exclamation-triangle"></i> ' +data.message);
										$('#Personal_info_response').addClass('is_error');
								}
						},
						error: function(err){
								$("html, body").animate({ scrollTop: 0 }, "fast");
								$('.btnSubmit').attr("disabled", false);
								$('#Personal_info_response').html('<i class="fa fa-exclamation-triangle"></i> Something went wrong!');
								$('#Personal_info_response').addClass('is_error');
						}
				});       
		}
}
function validateAddressForm() {
		
		$( "#user_addressForm" ).validate( {
				rules: {
						company: { required: true},
						contact_f_name: { required: true},
						contact_l_name: { required: true},
						email: { required: true},
						telephone: { required:true, minlength:10, maxlength:10, number: true},
						fax: { required: true},
						attn: { required: true},
						address_1: { required: true},
						address_2: { required: true},
						city: { required: true},
						state: { required: true},
						postal_code: { required: true},
						country: { required: true}
				},
				messages: { 
						company: { required: "Please provide company name" },
						contact_f_name: { required: "Please provide first name" },
						contact_l_name: { required: "Please provide last name" },
						email: { required: "Please provide valid email" },
						telephone: { required: "Please provide valid mobile" },
						fax: { required: "Please provide fax" },
						attn: { required: "Please provide attn" },
						address_1: { required: "Please provide address line 1" },
						address_2: { required: "Please provide address line 2" },
						city: { required: "Please provide city" },
						state: { required: "Please provide state" },
						postal_code: { required: "Please provide postal_code" },
						country: { required: "Please provide country" },
				},
				errorElement: "em",
				errorPlacement: function ( error, element ) {
						error.addClass( "help-block" );
						error.insertAfter( element );
				},
				highlight: function ( element, errorClass, validClass ) {
						$( element ).parents( ".col-sm-12" ).addClass( "has-error" ).removeClass( "has-success" );          
				},
				unhighlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-12" ).addClass( "has-success" ).removeClass( "has-error" );          
				}
		});
}
function saveUserAddress() {
		$('#addAddress_response').html('Please Wait..');
		$('#addAddress_response').removeClass('');
		validateAddressForm();
		var url=$("#url").val();
		var id = $('#address_id').val();
		var company = $('#company').val();
		var user_id = $('#user_id').val();
		var contact_f_name = $('#contact_f_name').val();
		var contact_l_name = $('#contact_l_name').val();
		var email = $('#email').val();
		var telephone = $('#telephone').val();
		var fax = $('#fax').val();
		var attn = $('#attn').val();
		var address_1 = $('#address_1').val();
		var address_2 = $('#address_2').val();
		var city = $('#city').val();
		var state = $('#state').val();
		var postal_code = $('#postal_code').val();
		var country = $('#country').val();
	 
if(company != ''  && contact_f_name != '' && contact_l_name!='') {

				$('.btnSubmit').attr("disabled", true);
				$.ajax({
						type: "POST",
						data: {
								'id' :id,
								'user_id': user_id,
								'company': company,
								'contact_f_name': contact_f_name,
								'contact_l_name': contact_l_name,
								'email': email,
								'telephone': telephone,
								'fax': fax,
								'attn': attn,
								'address_1': address_1,
								'address_2': address_2,
								'city': city,
								'state': state,
								'postal_code': postal_code,
								'country': country
						},                
						url: BASE_URL + "api/addAddress",
						success: function(data){
								console.log(url);
							 $('.btnSubmit').attr("disabled", false);
								if(data.status) {
										if(url){
												window.location.href=BASE_URL + 'checkout';
										}else{
												$('#address_added_successful').html(data.message);
												 window.location.href=BASE_URL + 'addresses';
										}
										
									 
								} else {
										$("html, body").animate({ scrollTop: 0 }, "fast");
										$('#address_added_successful').html('<i class="fa fa-exclamation-triangle"></i> ' +data.message);
										$('#address_added_successful').addClass('is_error');
								}
						},
						error: function(err){
								$("html, body").animate({ scrollTop: 0 }, "fast");
								$('.btnSubmit').attr("disabled", false);
								$('#address_added_successful').html('<i class="fa fa-exclamation-triangle"></i> Something went wrong!');
								$('#address_added_successful').addClass('is_error');
						}
				});       
		}
}

function logout(){
		
				$.ajax({
						url: BASE_URL+'api/auth/logout/',
						type: 'GET',            
						success: function(response) {                
								if(response.status) {
									 window.location.href=BASE_URL;
								}
						}, 
						error: function(error) {
						}
				});
		
}
 var mediaArray=[];
function createAttachmentFormData(image) {

		if(image[0]) {
				var formImage = new FormData();
				formImage.append('userImage', image[0]);
				uploadAttachmentFormData(formImage);
		}
}



function uploadAttachmentFormData(formData){
 
	 $.ajax({
		url: BASE_URL + 'api/upload/attachment',
		type: "POST",
		data: formData,
		contentType:false,
		cache: false,
		processData: false,
		success: function(data){        
		 	if(data.location) {
		 		var uploaded_file = '<tr style="border-bottom:1px solid #eeeeee;"  id="attachemnt_media_'+data.attachment_id+'" data-attachment='+data.attachment_id+'>'+
										'<td style="border-bottom: none;"><a href="'+data.location+'" target="_blank"><img src="'+BASE_URL+'assets/images/attachment.png" style="height:40px!important;"></a></td>'+
										'<td class="quoteBtn" style="border-bottom: none;">'+
											'<button type="button" class="btn icon deletebtn" onclick="deleteAttachment('+data.attachment_id+');">'+
												'<i class="fa fa-trash-o" aria-hidden="true"></i>'+
											'</button>'+
										'</td>'+
									'</tr>';
				$('.attachment-media').append(uploaded_file);
				// $('.uploaded_file').append(box);

				var user_id=$("#user_id").val();
				if(user_id==''){
			 		var media= $.cookie('media', media);
					
			 		if(media) {
						mediaArray = media.split(',');
					}

					var is_avail = false;
					for (var i = 0; i < mediaArray.length; i++) {
						if(data.attachment_id == mediaArray[i]) {
							is_avail = true;
						}						
					}
					if(media) {
						if(!is_avail) {
							mediaArray.push(data.attachment_id);
							$.cookie('media', mediaArray, { expires : 10, path : '/' });							
						}
					} else {
						mediaArray.push(data.attachment_id);
						$.cookie('media', mediaArray, { expires : 10, path : '/' });
					}
					// $('.customQuote').hide();
					// $('.attachDiv').load(document.URL +  ' .media-table'); 
				}
			}
		}
	});
}

function setCustomQuote(custom_quote_id){
	var custom_quote_id=custom_quote_id;
	$.ajax({
		type: "POST",
		data: {
			'id': custom_quote_id,
		},
		url: BASE_URL + "api/delete_customQuote",
		success: function(data){
			console.log(data);
			if(data.status) {      
				$('.customquote-data').on('click','.deletebtn',function(){
					 $(this).closest('tr').remove();
				});
			 /* $('.custom-table').load(document.URL +  ' .customquote-data'); */
			}
		},
		error: function(err){
			console.log("error");
			console.log("error in deleting custom quote");
		return;
			$("html, body").animate({ scrollTop: 0 }, "fast");
		}
	}); 
}

function deleteAttachment(attachment_id){
	var id=attachment_id;
	var mediaArry=[];
	var medias=$.cookie('media');
	
	if(medias) {
		mediaArry = medias.split(',');
	}
	
	for(var i=0;i<mediaArry.length;i++){			
		if(mediaArry[i]==id){
			var index = mediaArry.indexOf(mediaArry[i]);
			if (index > -1) {
				mediaArry.splice(index, 1);
			}
			var newMediaArry=[];
			newMediaArry=mediaArry;
			$.cookie('media', newMediaArry, { expires : 1, path : '/' });				 
		}
		 
	}
	$.ajax({
		type: "POST",
		data: {
			'id': id,
		},
		url: BASE_URL + "api/delete_attachment",
		success: function(data){
			if(data.status) {      
				$("#attachemnt_media_"+attachment_id).remove();
			 
				// $('.media-data').on('click','.deletebtn',function(){  
				// 	$(this).closest('tr').remove();
				// }); 
				// $('.customQuote').show();
			}
		},
		error: function(err){
			console.log("error");
			console.log("error in deleting address");
		return;
			$("html, body").animate({ scrollTop: 0 }, "fast");
		}
	});  
}
$('#upload_upload_attachment').change(function() {
		 var user_id=$('#user_id').val();
		 var upload = document.getElementById('upload_upload_attachment');
		 var image = upload.files;
		 createAttachmentFormData(image);
		
});

$('.uploaded_file'). on('click','.removeAttachemnt',function(){
		var file_value=$(this).parent().find('span').text();
		var mediaArry=[];
		var media=$.cookie('media');
		if(media) {
				mediaArry = media.split(',');
		}
		for(var i=0;i<mediaArry.length;i++){
			
			 if(mediaArry[i]==file_value){
				 var index = mediaArry.indexOf(mediaArry[i]);
					if (index > -1) {
						mediaArry.splice(index, 1);
					}
					var newMediaArry=[];
					newMediaArry=mediaArry;
		 
						 $.cookie('media', newMediaArry, { expires : 1, path : '/' });

			}
			 
		}
	 $(this).parent().remove();
		
});


function showCustomQuoteForm(){
	$('.customQuoteForm').css('display','flex');
	$('.couldntfind').hide();

}
$(document).on('keyup', '.form-control', function(){
	$(this).removeClass('errBorder');
	$(this).parent().find('p').hide();
})
function addCustomItem(){
	var itemName = $('#customItemName').val();
	var itemQuantity = $('#customItemQuantity').val();
	var is_valid = true;
	if(itemName =='') {
		is_valid = false;	
		$('#customItemName').addClass('errBorder');
		$('#customItemName').parent().find('p').show();
	} else {
		$('#customItemName').removeClass('errBorder');
		$('#customItemName').parent().find('p').hide();
	}

	if(itemQuantity =='' || itemQuantity == 0) {
		is_valid = false;
		$('#customItemQuantity').addClass('errBorder');
		$('#customItemQuantity').parent().find('p').show();
	} else {
		$('#customItemQuantity').removeClass('errBorder');
		$('#customItemQuantity').parent().find('p').hide();
	}

	if(is_valid) {
		var user_id=$("#user_id").val();
		
		if(user_id) {
			var formData={
				name : itemName,
				quantity : itemQuantity,
			};
				// attachment_id: attachmentIDs
			$.ajax({
				url: BASE_URL+'api/add_custom_item',
				type: 'POST',
				data: formData,
				success: function(response) {           
					console.log(response);
					if(response.status){
						
					}
				},
				error: function(error) {}
			});
		} else {
			var customQuotes=$.cookie('customQuotes',customQuotes);
			
			var customQuote = {
				Itemname : itemName,
				quantity : itemQuantity,
			};
			
			var quote=customQuote.Itemname +":"+ customQuote.quantity;
			if(customQuotes){
				 customQuoteDetails=customQuotes.split(',');
				 customQuoteDetails.push(quote);
			}else{
				 customQuoteDetails.push(quote);
			}
			$.cookie('customQuotes', customQuoteDetails, { expires : 10, path : '/' });			
		}
		
		// $('.customDiv').load(document.URL +  ' .custom-table');
		// $('.attachDiv').load(document.URL +  ' .media-table'); 
		var itemHTML = '<tr style="border-bottom:1px solid #eeeeee;">'+
							'<td class="customItemValue">'+itemName+'</td>'+
							'<td class="customItemValue">'+itemQuantity+'</td>'+
							'<td class="quoteBtn" style="border-bottom: none;">'+
								'<button type="button" class="btn icon deletebtn">'+
									'<i class="fa fa-trash-o" aria-hidden="true"></i>'+
								'</button>'+
							'</td>'+
						'</tr>';
		$('.customquote-data').append(itemHTML);

		$('#customItemName').val('');
		$('#customItemQuantity').val('');
	}

}
	
var attachmentIDs = [];
function addCustomQuote(){
	var customQuoteArry=[];
	$('#content>.dynamic>.form-inline').each(function(index){ 
		var Itemname=$(this).find('.Itemname').val();
		var quantity=$(this).find('.quantity').val();
		if(Itemname=='' && quantity==''){
			$("#error-block").show();
			$("#error-block2").show();
			return;
		}else if(Itemname==''){
			 $("#error-block").show();
			 return;
		}else{
			 var customQuote = {
						Itemname : $(this).find('.Itemname').val(),
						quantity : $(this).find('.quantity').val(),
				};
				customQuoteArry.push(customQuote);
		}
	});
	if(Itemname==''){
		 $("#error-block").show();
		 return;
	}else{
		$("#attachment_media").find(".box-style").each(function(){ attachmentIDs.push($(this).data('attachment')) });
		var user_id=$("#user_id").val();
			var formData={
						user_id:user_id,
						quoteArry:customQuoteArry,
						attachment_id: attachmentIDs
			};
			 $.ajax({
						url: BASE_URL+'api/addCustomQuote',
						type: 'POST',
						data: formData,
						success: function(response) {           
								if(response.status){
										$('.customDiv').load(document.URL +  ' .custom-table');
										$('.attachDiv').load(document.URL +  ' .media-table'); 
										$('#response').css({"background-color":"#00174e","color":"#fff"});
										$('#response').text('Custom Quote added to Quote!');
										$("#response").css("display", "block");
										$('#response').removeClass('alert alert-danger');
										$('#response').addClass('alert alert-success wishlist_success');

			setTimeout(function() {
				$('#response').fadeOut('fast');
			}, 2500);       
								}
						}, 
						error: function(error) {
						}
				});   
	}

}

var customQuoteDetails = [];
function addCustomQuoteIntoCookie(){
	$('#content>.dynamic>.form-inline').each(function(index){    
		var Itemname=$(this).find('.Itemname').val();
		var quantity=$(this).find('.quantity').val();
		if(Itemname=='' && quantity==''){
			$("#error-block").show();
			$("#error-block2").show();
			return;
		}else if(Itemname==''){
			 $("#error-block").show();
			 return;
		}  
			var customQuotes=$.cookie('customQuotes',customQuotes);
			console.log(customQuotes);
			var customQuote = {
						Itemname : $(this).find('.Itemname').val(),
						quantity : $(this).find('.quantity').val(),
				};
				var quote=customQuote.Itemname +":"+ customQuote.quantity;
				if(customQuotes){
					 customQuoteDetails=customQuotes.split(',');
					 customQuoteDetails.push(quote);
				}else{
					 customQuoteDetails.push(quote);
				}
			 $.cookie('customQuotes', customQuoteDetails, { expires : 10, path : '/' });
			 $('.customDiv').load(document.URL +  ' .custom-table');
			 $('.attachDiv').load(document.URL +  ' .media-table'); 
			 
		});

}
function sendQuotationInMail(email,user_id,quote_id){
	
	$.ajax({
						type: "POST",
						data: {
								'user_id': user_id,
								'quote_id':quote_id,
								'email':email
						},
						url: BASE_URL + "api/auth/sendQuotationInMail",
						success: function(data){
							 
								 if(data) {  
									console.log(data);
								} 
						},
						error: function(err){
							console.log("error");
							console.log("error in submitting quotation");
							return;
							 
				},
	}); 
}
function submitUserQuotations(email,user_id,quote_id,address_id){

		 $.ajax({
						type: "POST",
						data: {
								'user_id': user_id,
								'quote_id':quote_id,
								'address_id':address_id
						},
						url: BASE_URL + "api/updateQuotationProcess",
						success: function(data){
							 
								 if(data.status==true) {  
									$('#countQuote').load(document.URL +  ' #quoteTotal');    
									$('.cart-table').load(document.URL +  ' .quote-data');     
									$('.checkout-title').html('');
									$('.checkout').html('');
									var textMsg='<div class="col-md-12"><center><h5 style="text-align:center;">Thank you your quotation request has been submitted.We will get back to you soon.</h5></center></div>';
									$('.checkout').append(textMsg);
									sendQuotationInMail(email,user_id,quote_id);
								} 
						},
						error: function(err){
							console.log("error");
							console.log("error in submitting quotation");
							return;
							 
				},
	}); 
}
function submitQuotation(email,user_id,quote_id,address_id){
			var email=email;
			var user_id=user_id;
			var quote_id=quote_id;
			var address_id=address_id;
		 
			if(user_id){
				$.ajax({
					type: "GET",
					data: {
					'user_id': user_id
					},
					url: BASE_URL + "api/getUserAddress",
					success: function(data){
						if(data.status==false) {  
								$('#response').text("Please add address!");
								$("#response").css("display", "block");
								$('#response').addClass('alert alert-danger');
								setTimeout(function() {
								$('#response').fadeOut('fast');
								}, 2500);
								return;
						} else{
							// console.log(user_id);
							// return;
								submitUserQuotations(email,user_id,quote_id,address_id);
						}
					},
					error: function(err){
						console.log("error");
						console.log("error in submitting quotation");
						return;
					},
				}); 
		 }   
}
$( document ).ready(function() {
	 var product_id=$('#getViewById').val();
	 if(product_id){
			 $.ajax({
						type: "POST",
						data: {
								'product_id': product_id
						},
						url: BASE_URL + "api/updateProductViews",
						success: function(data){
							 
								 if(data) {      
								 console.log(data);
								} 
						},
						error: function(err){
							console.log("error");
							console.log("error in submitting quotation");
							return;
							 
				},
	});  
	 }

});


function deleteCustomQuote(itemname,qty){
	var proInfo=itemname +":"+ qty;
	var productArry=[];
	var customQuotes=$.cookie('customQuotes');
		if(customQuotes) {
				productArry = customQuotes.split(',');
		}
		for(var i=0;i<productArry.length;i++){
			
			 if(productArry[i]==proInfo){
				 var index = productArry.indexOf(productArry[i]);
					if (index > -1) {
						productArry.splice(index, 1);
					}
					var newProductArry=[];
					newProductArry=productArry;
					$.cookie('customQuotes', newProductArry, { expires : 1, path : '/' });
					$('.customquote-data').on('click','.deletebtn',function(){
								$(this).closest('tr').remove();
					});
				 
			 }
			 
		}
	}

	// animation


  (function() {
  var Util,
    __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  Util = (function() {
    function Util() {}

    Util.prototype.extend = function(custom, defaults) {
      var key, value;
      for (key in custom) {
        value = custom[key];
        if (value != null) {
          defaults[key] = value;
        }
      }
      return defaults;
    };

    Util.prototype.isMobile = function(agent) {
      return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(agent);
    };

    return Util;

  })();

  this.WOW = (function() {
    WOW.prototype.defaults = {
      boxClass: 'wow',
      animateClass: 'animated',
      offset: 0,
      mobile: true
    };

    function WOW(options) {
      if (options == null) {
        options = {};
      }
      this.scrollCallback = __bind(this.scrollCallback, this);
      this.scrollHandler = __bind(this.scrollHandler, this);
      this.start = __bind(this.start, this);
      this.scrolled = true;
      this.config = this.util().extend(options, this.defaults);
    }

    WOW.prototype.init = function() {
      var _ref;
      this.element = window.document.documentElement;
      if ((_ref = document.readyState) === "interactive" || _ref === "complete") {
        return this.start();
      } else {
        return document.addEventListener('DOMContentLoaded', this.start);
      }
    };

    WOW.prototype.start = function() {
      var box, _i, _len, _ref;
      this.boxes = this.element.getElementsByClassName(this.config.boxClass);
      if (this.boxes.length) {
        if (this.disabled()) {
          return this.resetStyle();
        } else {
          _ref = this.boxes;
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            box = _ref[_i];
            this.applyStyle(box, true);
          }
          window.addEventListener('scroll', this.scrollHandler, false);
          window.addEventListener('resize', this.scrollHandler, false);
          return this.interval = setInterval(this.scrollCallback, 50);
        }
      }
    };

    WOW.prototype.stop = function() {
      window.removeEventListener('scroll', this.scrollHandler, false);
      window.removeEventListener('resize', this.scrollHandler, false);
      if (this.interval != null) {
        return clearInterval(this.interval);
      }
    };

    WOW.prototype.show = function(box) {
      this.applyStyle(box);
      return box.className = "" + box.className + " " + this.config.animateClass;
    };

    WOW.prototype.applyStyle = function(box, hidden) {
      var delay, duration, iteration;
      duration = box.getAttribute('data-wow-duration');
      delay = box.getAttribute('data-wow-delay');
      iteration = box.getAttribute('data-wow-iteration');
      return box.setAttribute('style', this.customStyle(hidden, duration, delay, iteration));
    };

    WOW.prototype.resetStyle = function() {
      var box, _i, _len, _ref, _results;
      _ref = this.boxes;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        box = _ref[_i];
        _results.push(box.setAttribute('style', 'visibility: visible;'));
      }
      return _results;
    };

    WOW.prototype.customStyle = function(hidden, duration, delay, iteration) {
      var style;
      style = hidden ? "visibility: hidden; -webkit-animation-name: none; -moz-animation-name: none; animation-name: none;" : "visibility: visible;";
      if (duration) {
        style += "-webkit-animation-duration: " + duration + "; -moz-animation-duration: " + duration + "; animation-duration: " + duration + ";";
      }
      if (delay) {
        style += "-webkit-animation-delay: " + delay + "; -moz-animation-delay: " + delay + "; animation-delay: " + delay + ";";
      }
      if (iteration) {
        style += "-webkit-animation-iteration-count: " + iteration + "; -moz-animation-iteration-count: " + iteration + "; animation-iteration-count: " + iteration + ";";
      }
      return style;
    };

    WOW.prototype.scrollHandler = function() {
      return this.scrolled = true;
    };

    WOW.prototype.scrollCallback = function() {
      var box;
      if (this.scrolled) {
        this.scrolled = false;
        this.boxes = (function() {
          var _i, _len, _ref, _results;
          _ref = this.boxes;
          _results = [];
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            box = _ref[_i];
            if (!(box)) {
              continue;
            }
            if (this.isVisible(box)) {
              this.show(box);
              continue;
            }
            _results.push(box);
          }
          return _results;
        }).call(this);
        if (!this.boxes.length) {
          return this.stop();
        }
      }
    };

    WOW.prototype.offsetTop = function(element) {
      var top;
      top = element.offsetTop;
      while (element = element.offsetParent) {
        top += element.offsetTop;
      }
      return top;
    };

    WOW.prototype.isVisible = function(box) {
      var bottom, offset, top, viewBottom, viewTop;
      offset = box.getAttribute('data-wow-offset') || this.config.offset;
      viewTop = window.pageYOffset;
      viewBottom = viewTop + this.element.clientHeight - offset;
      top = this.offsetTop(box);
      bottom = top + box.clientHeight;
      return top <= viewBottom && bottom >= viewTop;
    };

    WOW.prototype.util = function() {
      return this._util || (this._util = new Util());
    };

    WOW.prototype.disabled = function() {
      return !this.config.mobile && this.util().isMobile(navigator.userAgent);
    };

    return WOW;

  })();

}).call(this);


wow = new WOW(
  {
    animateClass: 'animated',
    offset: 100
  }
);
wow.init();
