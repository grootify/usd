$(document).ready(function() {
    $('#scholarship_table').DataTable();    
    $('.mdl-data-table').DataTable({
        "aaSorting": [],
        "aoColumnDefs": [{
           "bSortable": false,
           "aTargets": ["sorting_disabled"]
        }],        
    });


    $("#drop-area").on('dragenter', function (e){
        e.preventDefault();
        $(this).css('background', '#BBD5B8');
    });

     $("#drop-area").on('dragover', function (e){
        e.preventDefault();
    });

     $("#drop-area").on('drop', function (e){
        $(this).css('background', '#D8F9D3');
        e.preventDefault();
        var image = e.originalEvent.dataTransfer.files;
        createFormData(image);
    });


    $('#cover_image').change(function() {
        var upload = document.getElementById('cover_image');
        var image = upload.files;
        createCoverFormData(image);
    });
    $('#brand_image').change(function(){
        var upload = document.getElementById('brand_image');
        var image = upload.files;
        createFormData(image, 'brand');
    });

    $('#catalog_image').change(function(){
        var upload = document.getElementById('catalog_image');
        var image = upload.files;
        createFormData(image, 'catalog');
    });


     $("#deleteBrand").on('click', function(){
        $('#brand-uploaded').hide();
        $('#deleteBrand').hide();
        $('#brand-unuploaded').show();
        $('#brands_logo').val('');        
    });

    $('#product_image').change(function(){
        var upload = document.getElementById('product_image');
        var image = upload.files;
        createProductCoverFormData(image);
    });
     $("#deleteProduct").on('click', function(){
        $('#product-uploaded').hide();
        $('#product-uploaded').find('img').remove();        
        $('#deleteProduct').hide();
        $('#drop-area').show();
        $('#product_image').val('');        
    });


    $("#deleteCover").on('click', function(){
        $('#cover-uploaded').hide();
        $('#deleteCover').hide();
        $('#cover-unuploaded').show();
        $('#coverImage').val('');        
    });

    $('#gallery_image').change(function() {
        var upload = document.getElementById('gallery_image');
        var image = upload.files;
        createProductFormData(image);
    });
  function createProductFormData(image){
    if(image[0]) {
        var formImage = new FormData();
        formImage.append('userImage', image[0]);
        var id = $('#id').val();
        if(id) {
            formImage.append('id', id);        
        }
        uploadProductFormData(formImage);
    }
}
    
    if( $('#blogEdit').length ) {
        onBLogLoad();
    }    
    if( $('#brandEdit').length){
        onBrandLoad();
    }
    if( $('#catalogEdit').length){
        onCatalogLoad();
    }
    if($('#coverEdit').length){
        onCoverLoad();
    }
    if($('#offerEdit').length){
        onOfferLoad();
    }
    if($('#productEdit').length){
        onProductLoad();
    }
    if($('#userEdit').length){
        onUserLoad();
    }

});

function createProductCoverFormData(image){
    var formImage = new FormData();
    formImage.append('userImage', image[0]);
    uploadProductCoverFormData(formImage);
}
function uploadProductCoverFormData(formData){
    $.ajax({
        url: BASE_URL + 'admin/upload/product_media',
        type: "POST",
        data: formData,
        contentType:false,
        cache: false,
        processData: false,
        success: function(data){        
            if(data.location) {
                $('#product_logo').val(data.location);
                var img = "<img src='"+ data.location +"'>";
                $('#product-uploaded').find('img').remove();
                $('#product-uploaded').append(img);
                $('#drop-area').hide();
                $('#product-uploaded').show();
                $('#deleteProduct').show();
            }
        }
    });
}
function createFormData(image, type) {
    var formImage = new FormData();
    formImage.append('userImage', image[0]);
    uploadFormData(formImage, type);
}
function createCoverFormData(image){
    var formImage = new FormData();
    formImage.append('userImage', image[0]);
    uploadCoverFormData(formImage);
}
function uploadCoverFormData(formData){
     $.ajax({
        url: BASE_URL + 'admin/upload/media',
        type: "POST",
        data: formData,
        contentType:false,
        cache: false,
        processData: false,
        success: function(data){        
            if(data.location) {
                $('#coverImage').val(data.location);
                var img = "<img src='"+ data.location +"'style="+"width:100%;height:auto;"+">";
                $('#cover-uploaded').html(img);
                $('#cover-unuploaded').hide();
                $('#cover-uploaded').show();
                $('#deleteCover').show();
            }
        }
    });
}

function uploadFormData(formData, type) {
    $.ajax({
        url: BASE_URL + 'admin/upload/media',
        type: "POST",
        data: formData,
        contentType:false,
        cache: false,
        processData: false,
        success: function(data){        
            if(data.location) {
                if(type == 'brand') {
                    $('#brands_logo').val(data.location);
                    var img = "<img src='"+ data.location +"'style="+"width:200px;height:auto;"+">";
                    $('#brand-uploaded').html(img);
                    $('#brand-unuploaded').hide();
                    $('#brand-uploaded').show();
                    $('#deleteBrand').show();
                } else if(type=='catalog') {
                    $('#catalog_logo').val(data.location);
                    var img = "<img src='"+ data.location +"'style="+"width:200px;height:auto;"+">";
                    $('#catalog-uploaded').html(img);
                    $('#catalog-unuploaded').hide();
                    $('#catalog-uploaded').show();
                    $('#deleteCatalog').show();
                }
            }
        }
    });
}

function uploadProductFormData(formData) {
    $.ajax({
        url: BASE_URL + 'admin/upload/product',
        type: "POST",
        data: formData,
        contentType:false,
        cache: false,
        processData: false,
        success: function(data){        
            if(data.location) {

                // $('#coverImage').val(data.location);
                var img = "<div class='col col-lg-3'><div id='gallery_media_"+data.media_id+"' data-media='"+data.media_id+"' class='gallery_media'><img src='"+ data.location +"'>";
                    img += '<a onclick="deleteGalleryMedia('+data.media_id+')" class="frontsubmit-card-action frontsubmit-card-action-delete" title="Remove"><i class="fa fa-times"></i></a></div></div>';
                $('#gallery-uploaded').append(img);       
            
            }
        }
    });
}
function deleteGalleryMedia(mediaID) {

   if (confirm('Cofirm Delete this Media?')) {
       var formData = {
           media_id: mediaID
       };

       $('#gallery_media_'+mediaID).parent().remove();
       // $.ajax({
       //     url: BASE_URL+'admin/api/delete_media',
       //     type: 'POST',
       //     data: formData,
       //     /*beforeSend: function(){
       //     },*/
       //     success: function(response) {
       //         if(response.status){
       //             $('#gallery_media_'+mediaID).remove();
       //         }
       //     },
       //     error: function(error) {
       //     }
       // });
   }
}

function deleteBrand(brand_id){
    
    var r = confirm("confirm to delete!");
    if (r != true) {
        return;
    }    
        
    var formData = {
        'id' : brand_id,    
    };   

    $.ajax({
        url: BASE_URL+'admin/api/delete_brand',
        type: 'POST',
        data: formData,
        success: function(response) {           

            if(response.status == true) {                
                $('#brand_'+brand_id).remove();                
            }
        }, 
        error: function(error) {
        }
    });
}

function deleteCatalog(catalog_id){
    
    var r = confirm("confirm to delete!");
    if (r != true) {
        return;
    }    
        
    var formData = {
        'id' : catalog_id,    
    };   

    $.ajax({
        url: BASE_URL+'admin/api/delete_catalog',
        type: 'POST',
        data: formData,
        success: function(response) {           

            if(response.status == true) {                
                $('#catalog_'+catalog_id).remove();                
            }
        }, 
        error: function(error) {
        }
    });
}



function setCoverStatus(cover_id, action){
     if(action == 'delete') {
        var r = confirm("confirm to delete!");
        if (r != true) {
            return;
        }
    } else {
        status = $('#coverStatus_'+cover_id).text();       
        if(status == '1') {         
            action = 'publish';
        } else {            
            action = 'unpublish';
        }
    }
        
    var formData = {
        'id' : cover_id,
        'action' : action
    };   

    $.ajax({
        url: BASE_URL+'admin/api/update_cover',
        type: 'POST',
        data: formData,
        success: function(response) {           

            if(response.status == true) {
                if(action == 'delete') {
                    $('#cover_'+cover_id).remove();
                } 
            }
        }, 
        error: function(error) {
        }
    });
}
function setUserStatus(user_id,action) {
    if(action == 'delete') {
        var r = confirm("confirm to delete!");
        if (r != true) {
            return;
        }
    } else {
        status = $('#userStatus_'+user_id).text();       
        if(status == '1') {         
            action = 'publish';
        } else {            
            action = 'unpublish';
        }
    }
        
    var formData = {
        'id' : user_id,
        'action' : action
    };   

    $.ajax({
        url: BASE_URL+'admin/api/update_user',
        type: 'POST',
        data: formData,
        success: function(response) {           

            if(response.status == true) {
                if(action == 'delete') {
                    $('#user_'+user_id).remove();
                } 
            }
        }, 
        error: function(error) {
        }
    });
}
function setOfferStatus(offer_id, action) {
    if(action == 'delete') {
        var r = confirm("confirm to delete!");
        if (r != true) {
            return;
        }
    } else {
        status = $('#offerStatus_'+offer_id).text();       
        if(status == '1') {         
            action = 'publish';
        } else {            
            action = 'unpublish';
        }
    }
        
    var formData = {
        'id' : offer_id,
        'action' : action
    };   

    $.ajax({
        url: BASE_URL+'admin/api/update_offer',
        type: 'POST',
        data: formData,
        success: function(response) {           

            if(response.status == true) {
                if(action == 'delete') {
                    $('#offer_'+offer_id).remove();
                } 
            }
        }, 
        error: function(error) {
        }
    });
}

function setProductStatus(product_id, that) {
    var action = $(that).data('action');
    if(action == 'delete') {
        var r = confirm("confirm to delete!");
        if (r != true) {
            return;
        }
    }        
        
    var formData = {
        'id' : product_id,
        'action' : action
    };   
    
    $.ajax({
        url: BASE_URL+'admin/api/update_product',
        type: 'POST',
        data: formData,
        success: function(response) {           

            if(response.status == true) {
                if(action == 'delete') {
                    $('#product_'+product_id).remove();
                } else if(action == 'unpublish') {
                    $(that).data('action', 'publish');
                    $(that).addClass('btn-success');
                    $(that).removeClass('btn-warning');
                    $(that).html('<i class="fa fa-play-circle" aria-hidden="true"></i>');
                } else {         
                    $(that).data('action', 'unpublish');                    
                    $(that).removeClass('btn-success');
                    $(that).addClass('btn-warning');
                    $(that).html('<i class="fa fa-pause-circle" aria-hidden="true"></i>');
                }
            }
        }, 
        error: function(error) {
        }
    });
}

function setBlogStatus(blogId, action) {
     
    if(action == 'delete') {
        var r = confirm("confirm to delete!");
        if (r != true) {
            return;
        }
    } else {
        status = $('#blogStatus_'+blogId).text();       
        if(status == 'Published') {         
            action = 'unpublish';
        } else {            
            action = 'publish';
        }
    }
        
    var formData = {
        'id' : blogId,
        'action' : action
    };   

    $.ajax({
        url: BASE_URL+'admin/api/update_article',
        type: 'POST',
        data: formData,
        /*beforeSend: function(){
        },*/
        success: function(response) {           

            if(response.status == true) {
                if(action == 'delete') {
                    $('#article_'+blogId).remove();
                } else if(action == 'publish') {
                    $('#actionButton_'+blogId).html('<i class="material-icons">remove_circle_outline</i> Unpublish');
                    $('#actionButton_'+blogId).removeClass('btn-success');
                    $('#actionButton_'+blogId).addClass('btn-warning');
                    $('#blogStatus_'+blogId).text('Published');
                } else {
                    $('#actionButton_'+blogId).html('<i class="material-icons">check_circle</i> Publish');
                    $('#actionButton_'+blogId).removeClass('btn-warning');
                    $('#actionButton_'+blogId).addClass('btn-success');
                    $('#blogStatus_'+blogId).text('Pending');
                }
            }
        }, 
        error: function(error) {
        }
    });
}

function saveArticle(){    
    var title = $('#blog-title').val();
    var description = tinymce.get('blog-description').getContent();
    var coverImage = $('#coverImage').val();
    coverImage = coverImage.replace(BASE_URL,'');
    var tags = $('#blog-tags').val();
    var slug = $('#blog-slug').val();

    var blogID = $('#blog-id').val();

    var formData = {
        blog_id: blogID,
        title: title,
        description: description,
        cover_image: coverImage,
        tags: tags,
        slug: slug
    };

    $.ajax({
        url: BASE_URL+'admin/api/save_article',
        type: 'POST',
        data: formData,
        /*beforeSend: function(){
        },*/
        success: function(response) {           
            if(response.status){
                if(response.blog_id){                    
                    location.href = BASE_URL + 'admin/blog/edit/' + response.blog_id;                    
                } else {
                    alert('Blog Saved!');
                }
            }
        }, 
        error: function(error) {
        }
    });    
}
function onCoverLoad(){
    coverID = $('#cover-id').val();
    if(coverID!='' && coverID!=0) {
        $.ajax({
            url: BASE_URL+'admin/api/get_cover/'+coverID,
            type: 'GET',            
            success: function(response) {                
                if(response.status) {
                    cover = response.data;
                   
                    $('#title').val(cover.title);
                    if(cover.url!=""){
                        $('#coverImage').val(cover.url);
                        var img = "<img src='"+ BASE_URL + cover.url+"'style="+"width:100%;height:auto;"+">";
                        $('#cover-uploaded').html(img);
                        $('#cover-unuploaded').hide();
                        $('#cover-uploaded').show();
                        $('#deleteCover').show();
                    }
                   
                }
            }, 
            error: function(error) {
            }
        });
    }
}
function onUserLoad() {
    userID = $('#user-id').val();
    if(userID!='' && userID!=0) {
        $.ajax({
            url: BASE_URL+'admin/api/get_user/'+userID,
            type: 'GET',            
            success: function(response) {                
                if(response.status) {
                    user = response.data;
                    $('#full_name').val(user.full_name);
                    $('#email').val(user.email);
                    $('#mobile').val(user.mobile);
                   
                }
            }, 
            error: function(error) {
            }
        });
    }
}
function onOfferLoad(){
    offerID = $('#offer-id').val();
    if(offerID!='' && offerID!=0) {
        $.ajax({
            url: BASE_URL+'admin/api/get_offer/'+offerID,
            type: 'GET',            
            success: function(response) {                
                if(response.status) {
                    offer = response.data;
                   
                    $('#title').val(offer.title);
                    if(offer.url!=""){
                        $('#coverImage').val(offer.url);
                        var img = "<img src='"+ BASE_URL + offer.url+"'style="+"width:100%;height:auto;"+">";
                        $('#cover-uploaded').html(img);
                        $('#cover-unuploaded').hide();
                        $('#cover-uploaded').show();
                        $('#deleteCover').show();
                    }
                   
                }
            }, 
            error: function(error) {
            }
        });
    }
}
function onBrandLoad(){
    brandID = $('#brand-id').val();
    if(brandID!='' && brandID!=0) {
        $.ajax({
            url: BASE_URL+'admin/api/get_brand/'+brandID,
            type: 'GET',            
            success: function(response) {                
                if(response.status) {
                    brand = response.data;
                   
                    $('#brand_name').val(brand.name);
                    $('#brand_link').val(brand.link);
                    if(brand.image!=""){
                        $('#brands_logo').val(brand.image);
                        var img = "<img src='"+ BASE_URL + brand.image+"'style="+"width:200px;height:auto;"+">";
                        $('#brand-uploaded').html(img);
                        $('#brand-unuploaded').hide();
                        $('#brand-uploaded').show();
                        $('#deleteBrand').show();
                    }
                   
                }
            }, 
            error: function(error) {
            }
        });
    }
}

function onCatalogLoad() {
    catalogID = $('#catalog-id').val();
    if(catalogID!='' && catalogID!=0) {
        $.ajax({
            url: BASE_URL+'admin/api/get_catalog/'+catalogID,
            type: 'GET',            
            success: function(response) {                
                if(response.status) {
                    catalog = response.data;
                   
                    $('#catalog_name').val(catalog.name);
                    $('#catalog_link').val(catalog.link);
                    if(catalog.image!=""){
                        $('#catalog_logo').val(catalog.image);
                        var img = "<img src='"+ BASE_URL + catalog.image+"'style="+"width:200px;height:auto;"+">";
                        $('#catalog-uploaded').html(img);
                        $('#catalog-unuploaded').hide();
                        $('#catalog-uploaded').show();
                        $('#deleteCatalog').show();
                    }
                   
                }
            }, 
            error: function(error) {
            }
        });
    }
}

function saveCover(){
    var title = $('#title').val();
    var coverImage = $('#coverImage').val();
    coverImage = coverImage.replace(BASE_URL,'');
    var coverID = $('#cover-id').val();
    if(title==''){
        $('#ajaxResponseDiv').html('');
        $('#ajaxResponseDiv').html('Please Enter Title.');   
    }else if(coverImage==''){
         $('#ajaxResponseDiv').html('');
        $('#ajaxResponseDiv').html('Please Upload Cover Image.');
    }else{
        var formData = {
            cover_id: coverID,
            title: title,
            url: coverImage
           
        };
       $.ajax({
            url: BASE_URL+'admin/api/save_cover',
            type: 'POST',
            data: formData,
            success: function(response) {      
            console.log(response);     
                if(response.status){
                    if(response.cover_id){                    
                        location.href = BASE_URL + 'admin/covers/edit/' + response.cover_id;                    
                    } else {
                        alert('Cover Saved!');
                    }
                }
            }, 
            error: function(error) {
            }
        });    
    }
    
}
function saveOffer() {
    var title = $('#title').val();
    var coverImage = $('#coverImage').val();
    coverImage = coverImage.replace(BASE_URL,'');
    var offerID = $('#offer-id').val();
     if(title==''){
        $('#ajaxResponseDiv').html('');
        $('#ajaxResponseDiv').html('Please Enter Title.');   
    }else if(coverImage==''){
         $('#ajaxResponseDiv').html('');
        $('#ajaxResponseDiv').html('Please Upload Cover Image.');
    }else{
        var formData = {
        offerID: offerID,
        title: title,
        url: coverImage
       
    };
       $.ajax({
            url: BASE_URL+'admin/api/save_offer',
            type: 'POST',
            data: formData,
            success: function(response) {      
            console.log(response);     
                if(response.status){
                    if(response.offer_id){                    
                        location.href = BASE_URL + 'admin/offers/edit/' + response.offer_id;                    
                    } else {
                        alert('Offer Saved!');
                    }
                }
            }, 
            error: function(error) {
            }
        });
    }
    
}

function saveBrand(){    
    var name = $('#brand_name').val();
    var link = $('#brand_link').val();
    var brands_logo = $('#brands_logo').val();
    brands_logo = brands_logo.replace(BASE_URL,'');
    var brandID = $('#brand-id').val();
     if(name==''){
        $('#ajaxResponseDiv').html('');
        $('#ajaxResponseDiv').html('Please Enter Brand Name.');   
    } else if(link=='') {
        $('#ajaxResponseDiv').html('');
        $('#ajaxResponseDiv').html('Please Enter Brand Website Link.');   
    } else if(brands_logo=='') {
        $('#ajaxResponseDiv').html('');
        $('#ajaxResponseDiv').html('Please Upload Brand Image.');
    }else{
        var formData = {
        brand_id: brandID,
        name: name,
        link: link,
        image: brands_logo
    };
       $.ajax({
            url: BASE_URL+'admin/api/save_brand',
            type: 'POST',
            data: formData,
            success: function(response) {      
            console.log(response);     
                if(response.status){
                    if(response.brand_id){                    
                        location.href = BASE_URL + 'admin/brands/edit/' + response.brand_id;                    
                    } else {
                        alert('Brand Saved!');
                    }
                }
            }, 
            error: function(error) {
            }
        }); 
    }
       
}

function saveCatalog(){    
    var name = $('#catalog_name').val();
    var catalog_logo = $('#catalog_logo').val();
    catalog_logo = catalog_logo.replace(BASE_URL,'');
    var catalogID = $('#catalog-id').val();
     if(name==''){
        $('#ajaxResponseDiv').html('');
        $('#ajaxResponseDiv').html('Please Enter Catalog Name.');
    } else if(catalog_logo=='') {
        $('#ajaxResponseDiv').html('');
        $('#ajaxResponseDiv').html('Please Upload Catalog Image.');
    }else{
        var formData = {
        catalog_id: catalogID,
        name: name,        
        image: catalog_logo
    };
       $.ajax({
            url: BASE_URL+'admin/api/save_catalog',
            type: 'POST',
            data: formData,
            success: function(response) {      
            console.log(response);     
                if(response.status){
                    if(response.catalog_id){                    
                        location.href = BASE_URL + 'admin/catalogs/edit/' + response.catalog_id;
                    } else {
                        alert('Catalog Saved!');
                    }
                }
            }, 
            error: function(error) {
            }
        }); 
    }
       
}

function onProductLoad(){
     productID = $('#product-id').val();
    if(productID!='' && productID!=0) {
        $.ajax({
            url: BASE_URL+'admin/api/get_product/'+productID,
            type: 'GET',            
            success: function(response) {                
                if(response.status) {
                    product = response.data;
                   $('#title').val(product.product_info.title);
                   $('#catalog_id').val(product.product_info.catalog_id);
                   $('#part_no').val(product.product_info.part_no);
                   $('#sku_number').val(product.product_info.sku_number);
                   $('#ships_within').val(product.product_info.ships_within);
                   $('#sell_unit').val(product.product_info.sell_unit);
                   $('#list_price').val(product.product_info.list_price);
                   $('#product_description').val(product.product_info.description);
                   if(product.product_info.url!=""){
                        $('#product_logo').val(product.product_info.url);
                        var img = "<img src='"+ BASE_URL + product.product_info.url+"'>";
                        $('#product-uploaded').find('img').remove();
                        $('#product-uploaded').append(img);
                        $('#drop-area').hide();
                        $('#product-uploaded').show();
                        $('#deleteProduct').show();
                    }
                     if(product.product_images.image!=""){
                        for (var i = 0; i < product.product_images.length; i++) {
                            var img = "<div class='col col-lg-3'><div id='gallery_media_"+product.product_images[i].id+"' data-media='"+product.product_images[i].id+"' class='gallery_media'><img src='"+ BASE_URL +product.product_images[i].container_name+"/"+product.product_images[i].image +"'>";
                            img += '<a onclick="deleteGalleryMedia('+product.product_images[i].id+')" class="frontsubmit-card-action frontsubmit-card-action-delete" title="Remove"><i class="fa fa-times"></i></a></div></div>';
                            $('#gallery-uploaded').append(img);
                        }
                             // $('#gallery-unuploaded').hide();
                             $('#gallery-uploaded').show();
                    }
                    if(product.product_section!=''){
                        $('.dynamic').html('');
                        for (var i = 0; i < product.product_section.length; i++) {
                         var row='<div class="form-inline mt-4"><div class="col-lg-4><p class="mg-b-10">Product Section Name</p><input class="form-control section_name" name="section_name[]" id="section_name" placeholder="Enter Product Section Name " type="text" style="width:100%;" value="'+product.product_section[i].name+
                        '"></div><div class="col-lg-4"><p class="mg-b-10">Product Section Description</p><textarea rows="2" name="section_description[]" id="section_description" class="form-control section_description" placeholder="Enter Product Description" style="width:100%;">'+ product.product_section[i].description+
                        '</textarea></div><div class="col-lg-1" style="margin-top: 23px;"><button class="btn btn-danger" data-role="remove"><i class="fa fa-minus"></i></button></div></div>';
                        $('.dynamic').append(row);
                        }
                            
                    }
                   
                }
            }, 
            error: function(error) {
            }
        });
    }
}
function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}
function saveUser() {
    var user_id=$('#user-id').val();
    var full_name=$('#full_name').val();
    var email=$('#email').val();
    var mobile=$('#mobile').val();
    if (full_name == "") {
        $('#ajaxResponseDiv').html('');
        $('#ajaxResponseDiv').html('Please Enter Name.');         
    } else if (email == "") {
        $('#ajaxResponseDiv').html('');
        $('#ajaxResponseDiv').html('Please Enter Email.');
    } else if (!isEmail(email)) {
        $('#ajaxResponseDiv').html('');
        $('#ajaxResponseDiv').html('Please Enter Valid Email.');           
    } 
    else if (mobile == "") {
        $('#ajaxResponseDiv').html('');
        $('#ajaxResponseDiv').html('Please Enter Mobile Number.');         
    } else if (mobile.length < 10) {
        $('#ajaxResponseDiv').html('');
        $('#ajaxResponseDiv').html('Please Enter Valid Mobile Number.');           
    } else {    
         var formData = {
            user_id:user_id,
            full_name:full_name,
            email: email,
            mobile: mobile
         };
         
         $.ajax({
            url: BASE_URL+'admin/api/save_user',
            type: 'POST',
            data: formData,
            success: function(response) {      
                console.log(response);     
                if(response.status){
                    if(response.user_id){                    
                      location.href = BASE_URL + 'admin/users/edit/' + response.user_id;                    
                    } else {
                      alert('User Saved!');
                    }
                }
            }, 
            error: function(error) {
            }
         });
    }
   
}
index = 1;
function saveProduct(){
    // var productInfoArry=[];
    var product_id=$('#product-id').val();
    var title=$('#title').val();
    var catalog_id=$('#catalog_id').val();
    var part_no=$('#part_no').val();
    var sku_number=$('#sku_number').val();
    var sell_unit=$('#sell_unit').val();
    var list_price=$('#list_price').val();
    var product_description=$('#product_description').val();
    var ships_within=$('#ships_within').val();
    var url = $('#product_logo').val();
    url = url.replace(BASE_URL,'');
    // $('#product_section .card').each(function(index){  
    //    var productInfo = {
    //         section_name : $(this).find('.section_name').val(),
    //         section_description : $(this).find('.section_description').val(),
    //     };
    //     productInfoArry.push(productInfo);   
    // });
      var mediaIDs = [];
    $("#gallery-uploaded").find(".gallery_media").each(function(){ mediaIDs.push($(this).data('media')) });
    isValid = true;
    
    if(title==''){
        isValid = false;
        $('#errResponse').append('<div id="indexToast-'+index+'" class="toast bg-danger"><div class="toast-body text-white">Please Enter Product Title.</div></div>');
        var indexToast = $('#indexToast-'+index);
        indexToast.hide().slideDown(500, 'swing').css('opacity', 0).animate({opacity: 1}, {queue: false, duration: 500});
        doSetTimeoutErrorResponse(indexToast)
        index++;    
    }
    if(catalog_id==''){
        isValid = false;
        $('#errResponse').append('<div id="indexToast-'+index+'" class="toast bg-danger"><div class="toast-body text-white">Please Select Catalog.</div></div>');
        var indexToast = $('#indexToast-'+index);
        indexToast.hide().slideDown(500, 'swing').css('opacity', 0).animate({opacity: 1}, {queue: false, duration: 500});
        doSetTimeoutErrorResponse(indexToast);
        index++;
    }
    if(part_no==''){
        isValid = false;
        $('#errResponse').append('<div id="indexToast-'+index+'" class="toast bg-danger"><div class="toast-body text-white">Please Enter Product Part Number.</div></div>');
        var indexToast = $('#indexToast-'+index);
        indexToast.hide().slideDown(500, 'swing').css('opacity', 0).animate({opacity: 1}, {queue: false, duration: 500});
        doSetTimeoutErrorResponse(indexToast)        
        index++;
    }
    if(sku_number==''){
        isValid = false;
        $('#errResponse').append('<div id="indexToast-'+index+'" class="toast bg-danger"><div class="toast-body text-white">Please Enter Product SKU Number.</div></div>');
        var indexToast = $('#indexToast-'+index);
        indexToast.hide().slideDown(500, 'swing').css('opacity', 0).animate({opacity: 1}, {queue: false, duration: 500});
        doSetTimeoutErrorResponse(indexToast)        
        index++;
    }

    if(isValid) {        
        var formData = {
            product_id:product_id,
            title:title,
            catalog_id: catalog_id,
            part_no: part_no,
            sku_number: sku_number,
            sell_unit: sell_unit, 
            ships_within:ships_within,
            list_price:list_price,
            product_description:product_description,
            url:url,
            mediaIDs:mediaIDs,
            // productInfoArry:productInfoArry
        };

        $.ajax({
            url: BASE_URL+'admin/api/save_product',
            type: 'POST',
            data: formData,
            success: function(response) {      
                if(response.status){
                    if(response.product_id){                    
                      location.href = BASE_URL + 'admin/products/edit/' + response.product_id;                    
                    } else {
                      alert('Product Saved!');
                    }
                }
            }, 
            error: function(error) {
            }
        })
    }
}
function doSetTimeoutErrorResponse(indexToast) {
    setTimeout(function() {
        indexToast.fadeOut(500);
    }, 5000);
}
function onBLogLoad(){
    blogID = $('#blog-id').val();
    if(blogID!='' && blogID!=0) {
        $.ajax({
            url: BASE_URL+'admin/api/get_article/'+blogID,
            type: 'GET',            
            success: function(response) {                
                if(response.status) {
                    blog = response.data;
                    console.log(blog);
                    $('#blog-title').val(blog.title);
                    // tinymce.get('blog-description').setContent(blog.description);
                    $('#blog-description').html(blog.description);
                    
                    if(blog.cover_image!=""){
                        $('#coverImage').val(blog.cover_image);
                        var img = "<img src='"+ BASE_URL + blog.cover_image+"'>";
                        $('#cover-uploaded').html(img);
                        $('#cover-unuploaded').hide();
                        $('#cover-uploaded').show();
                        $('#deleteCover').show();
                    }
                    $('#blog-tags').val(blog.tags);
                    $('#blog-slug').val(blog.slug);
                }
            }, 
            error: function(error) {
            }
        });
    }
}

/*tinymce.init({ 
        selector: 'textarea',
        plugins: 'link lists wordcount image code',
        menubar:false,
        extended_valid_elements: 'span',
        branding: false,
        height: 222,
        
        toolbar: 'undo redo | bold italic | bullist numlist | alignleft aligncenter alignright | link | removeformat restoredraft | code',
});*/
function updateQuoteAmount(quote_id){
  var formData = {
        quote_id: quote_id 
    };

    $.ajax({
        url: BASE_URL+'admin/api/update_quoteAmount',
        type: 'POST',
        data: formData,
        success: function(response) {        
        console.log(response);   
            if(response.status){
                 $('.product_section').load(document.URL +  ' .quote_product');
                 $('.custom_section').load(document.URL +  ' .custom_product');
            }
        }, 
        error: function(error) {
        }
    });    
}
function updateAmount(quote_id,id,product_id) {
   var total= $('#amount_'+product_id).val();
   var formData = {
        id: id,
        product_id: product_id,
        total: total
    };

    $.ajax({
        url: BASE_URL+'admin/api/update_quoteProduct',
        type: 'POST',
        data: formData,
        success: function(response) {        
        console.log(response);   
            if(response.status){
                $('.quote_product').load(document.URL +  ' .quote_data');
                updateQuoteAmount(quote_id,total);
                $('#response').css({"background-color":"#00174e","color":"#fff"});
                $('#response').text("Quote has been updated successfully!");
                $("#response").css("display", "block");
                $('#response').removeClass('alert alert-danger');
                $('#response').addClass('alert alert-info wishlist_success');
                setTimeout(function() {
                $('#response').fadeOut('fast');
                }, 2500); 
            }
        }, 
        error: function(error) {
        }
    });    

}
function updateCustomProductAmount(quote_id,custom_id) {
   var total= $('#custom_'+custom_id).val();
   var formData = {
        custom_id: custom_id,
        quote_id: quote_id,
        total: total
    };

    $.ajax({
        url: BASE_URL+'admin/api/update_customProduct',
        type: 'POST',
        data: formData,
        success: function(response) {        
        console.log(response);   
            if(response.status){
                $('.custom_section').load(document.URL +  ' .custom_data');
                updateQuoteAmount(quote_id,total);
                $('#response').css({"background-color":"#00174e","color":"#fff"});
                $('#response').text("Custom Quote has been updated successfully!");
                $("#response").css("display", "block");
                $('#response').removeClass('alert alert-danger');
                $('#response').addClass('alert alert-info wishlist_success');
                setTimeout(function() {
                $('#response').fadeOut('fast');
                }, 2500); 
            }
        }, 
        error: function(error) {
        }
    });   
}
function updateAttachmentQuotation(quote_id) {
    var attachInfoArry=[];
    $('#attachemnt_section>.dynamic>.form-inline').each(function(index){  
       var attachInfo = {
            item_name : $(this).find('.item_name').val(),
            quantity : $(this).find('.quantity').val(),
            attach_amount : $(this).find('.attach_amount').val(),
        };
        attachInfoArry.push(attachInfo);   
    });
    var formData = {
        quote_id:quote_id,
        attachInfoArry:attachInfoArry
    };
    $.ajax({
            url: BASE_URL+'admin/api/updateAttachement',
            type: 'POST',
            data: formData,
            success: function(response) {      
                console.log(response);     
                if(response.status){
                    updateQuoteAmount(quote_id);
                    $('#response').css({"background-color":"#00174e","color":"#fff"});
                    $('#response').text("Attachment has been updated successfully!");
                    $("#response").css("display", "block");
                    $('#response').removeClass('alert alert-danger');
                    $('#response').addClass('alert alert-info wishlist_success');
                    setTimeout(function() {
                    $('#response').fadeOut('fast');
                    }, 2500); 
                }
            }, 
            error: function(error) {
            }
         });
}

function sendQuotation(quote_id) {
    var formData = {
        quote_id:quote_id
    };
    $.ajax({
            url: BASE_URL+'admin/api/sendQuotation',
            type: 'POST',
            data: formData,
            success: function(response) {      
               if(response.status=="true"){
                $('#response').css({"background-color":"#00174e","color":"#fff"});
                $('#response').text("Quotation has been successfully acknowledged and sent to customer!");
                $("#response").css("display", "block");
                $('#response').removeClass('alert alert-danger');
                $('#response').addClass('alert alert-info wishlist_success');
                 setTimeout(function() {
                    $('#response').fadeOut('fast');
                    }, 2500); 
                }
            }, 
            error: function(error) {
            }
         });
}